<?php

namespace Tests\Feature;

use App\Search;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class viewOnlyLiveSearchesTest extends TestCase
{

	use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
    	$this->assertTrue(true);
    }

    public function testViewOnlySearchesWithoutTenancies()
    {
    	$search = factory(Search::class)->create();

    	$this->assertEquals(1, $search->id);
    }

}
