<?php

namespace App;

use XeroLaravel;
use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
	protected $fillable = ['status', 'tenancy_id', 'amount', 'date', 'scheduled_invoice_id', 'type'];

	protected $dates = ['date'];

	public function scheduledInvoice()
	{
		return $this->belongsTo(ScheduledInvoice::class);
	}

	public function invoice()
	{
		return $this->belongsTo(Invoice::class);
	}
	
	public function displayAmount()
	{
		return convertPenceToPounds($this->amount);
	}

	public function displayVat()
	{
		return convertPenceToPounds($this->vat);
	}

	public function displayTotal()
	{
		return convertPenceToPounds($this->amount + $this->vat);
	}


	public function isPending()
	{
		if ($this->status == 'Pending') {
			return true;
		}
		return false;
	}

	public function isInvoiced()
	{
		if ($this->status == 'Invoiced') {
			return true;
		}
		return false;
	}

	public function isPaid()
	{
		if ($this->status == 'Paid') {
			return true;
		}
		return false;
	}

	public function getStatus()
	{
		if ($this->invoice) {
			return $this->invoice->status;
		}

		return $this->status;
	}

	public function getXeroInvoiceStatus()
	{
		$invoice = XeroLaravel::getInvoiceById($this->xero_ref);

		return $invoice['Status'];
	}

	public function getInvoiceStatus()
	{
		if ($this->invoice) {
			return $this->invoice->id. ' | '. $this->invoice->status. ' | '.$this->invoice->xero_ref;
		}
		return '';
	}

}
