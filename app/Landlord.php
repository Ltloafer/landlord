<?php

namespace App;

use App\Traits\CreatesXeroContacts;
use Illuminate\Database\Eloquent\Model;

use App\LandlordDoc;

class Landlord extends Model
{
    use CreatesXeroContacts;

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function notes()
    {
    	return $this->hasMany(Note::class);
    }

    public function landlordDocs()
    {
        return $this->hasMany(LandlordDoc::class);
    }

    public function assistants()
    {
        return $this->hasMany(Assistant::class);
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function hasDocument($id)
    {
        foreach ($this->landlordDocs as $landlordDoc) {
            if ($landlordDoc->landlord_doc_types_id == $id) {
                return true;
            }
        }
        return false;
    }

    public function hasDocumentationComplete($docTypes)
    {
        $docListIds = LandlordDoc::where('landlord_id', $this->id)->pluck('landlord_doc_types_id');

        foreach($docTypes as $docType) {
            if (! $docListIds->contains($docType->id)) {
                return false;
            }
        }

        return true;
    }

}
