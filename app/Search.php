<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Search extends Model
{
	use SoftDeletes;

	protected $table='searches';

	protected $dates = ['start_date', 'end_date', 'offer_sent'];

	public function notes()
	{
		return $this->hasMany(Note::class);
	}

	public function searchAreas()
	{
		return $this->belongsToMany(SearchArea::class, 'searches_search_area', 'searches_id', 'search_area_id');
	}

	public function job()
	{
		return $this->belongsTo(Job::class);
	}

	public function tenant()
	{
		return $this->belongsTo(Tenant::class);
	}

	public function property()
	{
		return $this->belongsTo(Property::class);
	}

	public function productionCompany()
	{
		return $this->belongsTo(ProductionCompany::class, 'production_company_id');
	}

	public function tenancies()
	{
		return $this->hasMany(Tenancy::class);
	}

	public function hotelBookings()
	{
		return $this->hasMany(HotelBooking::class);
	}

	public function forProductionCompany()
	{
		if ($this->production_company_id) {
			return true;
		}

		return false;
	}

	public function forPreviousTenant()
	{
		if ($this->tenant) {
			return 'Yes';
		}

		return 'No';
	}

	public function isRegisteredToArea($area)
	{
		$registeredIds = $this->searchAreas->pluck('id');

		if ($registeredIds->contains($area->id)) {
			return true;
		}
		return false;
	}

	public function hasBeenConverted()
	{
		if ($this->hotelBookings()->count() > 0 && $this->tenancies()->count()  > 0 ) {
			return true;
		}
		return false;
	}

	public function displayCommissionRate()
	{
		return $this->comm_rate ? $this->comm_rate /100 : '';
	}

	public function displayDeposit()
	{
		return $this->holding_deposit ? $this->holding_deposit / 100 : '';
	}

	public function displayOfferAmount()
	{
		return $this->offer_amount ? $this->offer_amount / 100 : '';
	}
}
