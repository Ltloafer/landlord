<?php

namespace App;

use XeroLaravel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fee extends Model
{
	use SoftDeletes;

	protected $fillable = ['fee_type_id', 'amount', 'vat', 'date', 'status', 'scheduled_invoice_id', 'tenancy_id', 'up-to-break', 'xeroStatus'];

	protected $dates = ['date'];
	

	public function feeType()
	{
		return $this->belongsTo(FeeType::class);
	}

	public function tenancy()
	{
		return $this->belongsTo(Tenancy::class);
	}

	public function scheduledInvoice()
	{
		return $this->belongsTo(ScheduledInvoice::class, 'scheduled_invoice_id');
	}

	// public function landlord()
	// {
	// 	return $this->belongsTo(Landlord::class);
	// }

	public function invoice()
	{
		return $this->belongsTo(Invoice::class);
	}

	public function isPending()
	{
		if ($this->status == 'Pending') {
			return true;
		}
		return false;
	}

	public function isInvoiced()
	{
		if ($this->status == 'Invoiced') {
			return true;
		}
		return false;
	}

	public function isPaid()
	{
		if ($this->status == 'Paid') {
			return true;
		}
		return false;
	}

	public function isCredit()
	{
		if ($this->feeType->entry == 'credit') {
			return true;
		}
		return false;
	}

	public function displayAmount()
	{
		return convertPenceToPounds($this->amount);
	}

	public function displayVat()
	{
		return convertPenceToPounds($this->vat);
	}

	public function displayTotal()
	{
		$total = $this->amount + $this->vat;

		return convertPenceToPounds($total);
	}

	// public function getXeroInvoiceStatus()
	// {
	// 	$invoice = XeroLaravel::getInvoiceById($this->xero_ref);

	// 	return $invoice['Status'];
	// }

	public function getFeeDescription()
	{
		if ($this->fee_type_id == 1 || $this->fee_type_id == 2 || $this->fee_type_id == 4) {
			return 'Administration';
		} elseif ($this->fee_type_id == 3) {
			return 'Commission';
		}
	}

	public function updateWithInvoiceDetails($invoice)
	{
		$this->xero_ref = $invoice['InvoiceNumber'];
		$this->xero_inv_id = $invoice['InvoiceID'];
		$this->status = 'Invoiced';
		$this->save();
	}

	
	public function getInvoiceStatus()
	{
		if ($this->invoice) {
			return $this->invoice->id. ' | '. $this->invoice->status. ' | '.$this->invoice->xero_ref;
		}
		return '';
	}

	public function getLandlordOrAgentContact()
	{
		if ($this->tenancy->property->agent) {

			return XeroLaravel::getContact($this->tenancy->property->agent->xero_contactID);
			
		} else {

			return XeroLaravel::getContact($this->tenancy->property->landlord->xero_contactID);
		}
	}

	public function getLandlordOrAgentContactId()
	{
		if ($this->tenancy->property->agent) {

			return $this->tenancy->property->agent->contact->id;
		}

		return $this->tenancy->property->landlord->contact->id;
	}

	public function getPeriodDateRange()
	{
		if ($this->scheduledInvoice) {

			return $this->scheduledInvoice->start_date->format('d/m/y'). ' to '. $this->scheduledInvoice->end_date->format('d/m/y');
		}
		
		return $this->tenancy->start_date->format('d/m/y'). ' to '. $this->tenancy->finish_date->format('d/m/y');
	}
}

