<?php

namespace App\Traits;

trait ChecksForInstallmentsTrait {

	protected function checkInstallments($tenancy, $request)
	{
		if ($request->input('installment_1_date')) {
			$tenancy->installments()->create([
				'payment_date' => $request->input('installment_1_date'),
				'amount' => ($request->input('installment_1_payment') * 100)
			]);
		}

		if ($request->input('installment_2_date')) {
			$tenancy->installments()->create([
				'payment_date' => $request->input('installment_2_date'),
				'amount' => ($request->input('installment_2_payment') * 100)
			]);
		}

		if ($request->input('installment_3_date')) {
			$tenancy->installments()->create([
				'payment_date' => $request->input('installment_3_date'),
				'amount' => ($request->input('installment_3_payment') * 100)
			]);
		}

		if ($request->input('installment_4_date')) {
			$tenancy->installments()->create([
				'payment_date' => $request->input('installment_4_date'),
				'amount' => ($request->input('installment_4_payment') * 100)
			]);
		}

		if ($request->input('installment_5_date')) {
			$tenancy->installments()->create([
				'payment_date' => $request->input('installment_5_date'),
				'amount' => ($request->input('installment_5_payment') * 100)
			]);
		}

		if ($request->input('installment_6_date')) {
			$tenancy->installments()->create([
				'payment_date' => $request->input('installment_6_date'),
				'amount' => ($request->input('installment_6_payment') * 100)
			]);
		}
	}
	
}