<?php

namespace App\Traits;

use XeroLaravel;

trait CreatesXeroContacts
{
	protected function createNewXeroContact($resource)
	{
		try {

			$contact = XeroLaravel::createContact($resource);

			$resource->xero_contactID = $contact->ContactID;

			$resource->save();

		} catch (\Exception $e) {
			return back()->with('alert_message', 'We could not save that contact. Maybe they are already a contact? Thanks');
		}
	}
}