<?php

namespace App\Traits;

use App\Contact;

trait CreatesContacts
{
	protected function createNewContact($resource, $request = null)
	{
		try {

			$contact = new Contact;

			$contact->name = $resource->name;
			$contact->email = $resource->email;
			$contact->phone = $resource->phone;
			$contact->address = $resource->address;
			
			if ($request) {

				$contact->line_1 = $request->input('line_1');
				$contact->line_2 = $request->input('line_2');
				$contact->line_3 = $request->input('line_3');
				$contact->postcode = $request->input('postcode');
			}
			
			$contact->xero_contactID = $resource->xero_contactID;
			$contact->save();

			$resource->contact_id = $contact->id;

			$resource->save();

		} catch (\Exception $e) {
			return back()->with('alert_message', 'We could not save that contact. Maybe they are already a contact? Thanks');
		}
	}
}