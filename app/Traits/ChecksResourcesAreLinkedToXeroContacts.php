<?php

namespace App\Traits;

use XeroLaravel;

trait ChecksResourcesAreLinkedToXeroContacts {

	protected function ChecksResourceIsLinkedToXeroContact($resource)
	{
		  if ($resource->xero_contactID) {

            $this->checkXeroContactByGUID($resource);

        } else {

            return back()->with('alert_message', 'You need to check you have linked your resources to Xero contacts. Thanks');
        }
    }


	protected function checkXeroContactByGUID($resource)
	{
		try {

			$xeroContact = XeroLaravel::getContact($resource->xero_contactID);


		} catch (\Exception $e) {

			return back()->with('alert_message', 'We could not find all the contacts on Xero. Please check them before re-trying. Thanks');
		}
	} 
}