<?php

namespace App\Traits;

use Carbon\Carbon;

trait AddFeesToTenancyTrait {

	protected function addFeesForDirectTenancy($tenancy)
	{
		if ($tenancy->charge_landlord_admin_fee) {
			$tenancy->fees()->create([
				'fee_type_id' => 1,
				'date' => Carbon::today(),
				'status' => 'Pending',
				'amount' => 22000,
				'vat' => 4400
			]);
		}

		if ($tenancy->charge_tenant_admin_fee) {
			$tenancy->fees()->create([
				'fee_type_id' => 2,
				'date' => Carbon::today(),
				'status' => 'Pending',
				'amount' => 22000,
				'vat' => 4400
			]);

			$tenancy->fees()->create([
				'fee_type_id' => 4,
				'date' => Carbon::today(),
				'status' => 'Pending',
				'amount' => 22000,
				'vat' => 4400
			]);
		}
		
		$tenancy->deposits()->create([
			'date' => Carbon::today(),
			'status' => 'Pending',
			'type' => 'Security',
			'amount' => $tenancy->security
		]);
	}

	protected function addCommissionFees($tenancy)
	{
		if ($tenancy->isStandardType()) {

			if ($tenancy->comm_charging == 'to_break') {
				
				$tenancy->fees()->create([
					'fee_type_id' => 3,
					'date' => Carbon::today(),
					'status' => 'Pending',
					'amount' => $tenancy->monthlyCommissionAmount() * $tenancy->break_clause,
					'up-to-break' => true,
					'vat' => ((($tenancy->monthlyCommissionAmount() * $tenancy->break_clause) / 100) * 20),
				]);
			}
		}


		elseif ($tenancy->isInstallmentsType()) {

			if ($tenancy->comm_charging == 'all_upfront' ) {

				$tenancy->fees()->create([
					'fee_type_id' => 3,
					'date' => Carbon::today(),
					'status' => 'Pending',
					'amount' => $tenancy->getTotalCommission(), // invoice all commission in the first installment.
					'up-to-break' => true,
					'vat' => (($tenancy->getTotalCommission()/100)*20)
				]);
			} 

			if ($tenancy->comm_charging == 'by_period' ) {
				foreach ($tenancy->installments as $installment) {

					$tenancy->fees()->create([
						'fee_type_id' => 3,
						'date' => $installment->payment_date,
						'status' => 'Pending',
						'amount' => (($installment->amount / 100) * ($tenancy->comm_rate / 100)),
						'up-to-break' => false,
						'vat' => ((($installment->amount/100) * ($tenancy->comm_rate / 100)) / 100 ) * 20
					]);
				}
			}
		}
	}
}

