<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
	protected $fillable = ['supplier_id', 'amount', 'tenancy_id', 'date', 'expense_type_id',  'status', 'scheduled_invoice_id', 'inv_ref', 'file'];

	protected $dates = ['date'];

    public function expenseType()
    {
    	return $this->belongsTo(ExpenseType::class);
    }

    public function supplier()
    {
    	return $this->belongsTo(Supplier::class);
    }

    public function tenancy()
    {
        return $this->belongsTo(Tenancy::class);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function displayAmount()
    {
    	return convertPenceToPounds($this->amount);
    }

    public function isCredit()
	{
		return false;
	}



}
