<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandlordDocType extends Model
{
    protected $table="landlord_doc_types";

}
