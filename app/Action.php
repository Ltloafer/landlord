<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Action extends Model
{
	use SoftDeletes;

	public function tenancy()
	{
		return $this->belongsTo(Tenancy::class);
	}

	public function scheduledInvoice()
	{
		return $this->belongsTo(ScheduledInvoice::class);
	}

	public function deposit()
	{
		return $this->belongsTo(Deposit::class);
	}

	public function adminRechargeFee()
	{
		return $this->belongsTo(Fee::class, 'tenant_admin_recharge_fee_id');
	}

	public function landlordAdminFee()
	{
		return $this->belongsTo(Fee::class, 'landlord_admin_fee_id');
	}

	public function commissionFee()
	{
		return $this->belongsTo(Fee::class, 'commission_fee_id');
	}

	public function landlordPayment()
	{
		return $this->belongsTo(RentPayable::class, 'landlord_payment_id');
	}

	public function expense()
	{
		return $this->belongsTo(Expense::class, 'expense_id');
	}

	public function getActionDetails()
	{
		if ($this->type == 'rental_payment') {
			$rental_invoice = $this->scheduledInvoice->xero_ref;

			if ($this->tenant_admin_fee_id && $this->deposit_id) {
				$rental_invoice = $rental_invoice.' including admin fee £264 and deposit £'.convertPenceToPounds($this->deposit->amount);
			}

			return $rental_invoice;
		}

		if ($this->type == 'admin_recharge') {
			return $this->adminRechargeFee->xero_ref;
		}

		if ($this->type == 'landlord_admin_fee') {
			return $this->landlordAdminFee->xero_ref;
		}

		if ($this->type == 'commission_fee') {
			return $this->commissionFee->xero_ref;
		}

		if ($this->type == 'landlord_payment') {
			return $this->landlordPayment->xero_ref;
		}

		if ($this->type == 'expense_payment') {
			return $this->expense->xero_ref;
		}
	}

	public function isARentalPayment()
	{
		if ($this->type == 'rental_payment') {
			return true;
		}

		return false;
	}

	public function isForAPeriod()
	{
		if ($this->type == 'rental_payment' || $this->type == 'landlord_payment') {
			return true;
		}

		return false;
	}
}

