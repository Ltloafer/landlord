<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RentPayable extends Model
{
	use SoftDeletes;
	
	protected $fillable = ['status', 'tenancy_id', 'amount', 'date', 'scheduled_invoice_id', 'type'];

	protected $dates = ['date'];

	use Notifiable;

	public function scheduledInvoice()
	{
		return $this->belongsTo(ScheduledInvoice::class);
	}

	public function tenancy()
	{
		return $this->belongsTo(Tenancy::class);
	}

	public function displayAmount()
	{
		return convertPenceToPounds($this->amount);
	}

	public function displayVat()
	{
		return convertPenceToPounds($this->vat);
	}

	public function displayTotal()
	{
		return convertPenceToPounds($this->amount + $this->vat);
	}

	public function invoice()
	{
		return $this->belongsTo(Invoice::class);
	}

	public function isPending()
	{
		if ($this->status == 'Pending') {
			return true;
		}
		return false;
	}

	public function isInvoiced()
	{
		if ($this->status == 'Invoiced') {
			return true;
		}
		return false;
	}

	public function isPaid()
	{
		if ($this->status == 'Paid') {
			return true;
		}
		return false;
	}

	public function paymentDue()
	{
		$amount = $this->scheduledInvoice->amount;

		$fees = $this->scheduledInvoice->fees;

		foreach ($fees as $fee) {
			if ($fee->fee_type_id == 1) {
				$amount = $amount - ($fee->amount + $fee->vat);
			}

			if ($fee->fee_type_id == 3) {
				$amount = $amount - ($fee->amount + $fee->vat);
			}
		}

		$disbursementsAmount = $this->scheduledInvoice->getTotalDisbursements();

		$amount = $amount - $disbursementsAmount;

		return ($amount);
	}

	public function displayPaymentDue() 
	{
		return convertPenceToPounds($this->paymentDue());
	}

	public function displayDueAmount()
	{
		return convertPenceToPounds($this->paymentDue());
	}

	public function scheduledAdminFee()
    {
        $fee = $this->scheduledInvoice->fees->where('fee_type_id', 1)->where('status', 'Scheduled')->first();

        return $fee;
    }

    public function landlordAdminFee()
    {
        $fee = $this->scheduledInvoice->fees->where('fee_type_id', 1)->first();

        return $fee;
    }

    public function scheduledCommissionFee()
    {
        $fee = $this->scheduledInvoice->fees->where('fee_type_id', 3)->where('status', 'Scheduled')->first();

        return $fee;
    }

     public function commissionFee()
    {
        $fee = $this->scheduledInvoice->fees->where('fee_type_id', 3)->first();

        return $fee;
    }

    public function updateWithInvoiceDetails($invoice)
    {
    	$this->xero_ref = $invoice['InvoiceNumber'];
		$this->xero_inv_id = $invoice['InvoiceID'];
		$this->status = 'Invoiced';
		$this->save();
    }

    public function getInvoiceStatus()
	{
		if ($this->invoice) {
			return $this->invoice->id. ' | '. $this->invoice->status. ' | '.$this->invoice->xero_ref;
		}
		return '';
	}

}
