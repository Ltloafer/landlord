<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
	protected $fillable = ['note'];

	public function property()
	{
		return $this->belongsTo(Property::class);
	}

	public function tenant()
	{
		return $this->belongsTo(Tenant::class);
	}

	public function landlord()
	{
		return $this->belongsTo(Landlord::class);
	}

	public function searches()
	{
		return $this->belongsTo(Search::class);
	}

	public function contact()
	{
		return $this->belongsTo(Contact::class);
	}

	public function agent()
	{
		return $this->belongsTo(Agent::class);
	}
	
}
