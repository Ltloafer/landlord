<?php

namespace App\Notifications;

use App\Tenancy;
use App\RentPayable;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class LandlordPaymentDue extends Notification
{
    use Queueable;

    protected $rentPayable;
    private $rentPeriod;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(RentPayable $rentPayable)
    {
        $this->rentPayable = $rentPayable;
        $this->rentPeriod = $this->getScheduledPeriod();
    }

   

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Invoice '. $this->rentPayable->xero_ref. ' is due for payment.')
                    ->line('This is a landlord payment in respect of '. $this->rentPayable->tenancy->property->address. '.')
                    ->line('Rental period '. $this->rentPeriod. '.')
                    ->action('Pay invoice', url('/tenancies/'. $this->rentPayable->tenancy_id))
                    ->line('Thank you!');
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'rentPayableId' => $this->rentPayable->id,
            'scheduledInvoiceId' => $this->rentPayable->scheduled_invoice_id,
            'message' => 'Landlord Invoice '. $this->rentPayable->xero_ref. ' for rent  for the period '. $this->rentPeriod . ' is due for payment.'
        ];
    }

     private function getScheduledPeriod()
    {
        return $this->rentPayable->tenancy->start_date->format('jS \\of F Y').' to '. $this->rentPayable->tenancy->finish_date->format('jS \\of F Y');
    }
}
