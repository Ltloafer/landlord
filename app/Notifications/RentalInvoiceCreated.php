<?php

namespace App\Notifications;

use App\Tenancy;
use App\ScheduledInvoice;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RentalInvoiceCreated extends Notification
{
    use Queueable;

    protected $scheduledInvoice;

    private $rentPeriod;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ScheduledInvoice $scheduledInvoice)
    {
        $this->scheduledInvoice = $scheduledInvoice;
        $this->rentPeriod = $this->getScheduledPeriod();
    }

   

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Invoice '. $this->scheduledInvoice->xero_ref. ' has been generated.')
                    ->line('This is a rental invoice for '. $this->scheduledInvoice->tenancy->property->address. '.')
                    ->line('Rental period '. $this->rentPeriod. '.')
                    ->action('Mark as paid', url('/tenancies/'. $this->scheduledInvoice->tenancy_id))
                    ->line('Thank you!');
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'scheduledInvoiceId' => $this->scheduledInvoice->id,
            'message' => 'Rental invoice '. $this->scheduledInvoice->xero_ref. ' for rent for the period '. $this->rentPeriod . ' has been generated.'
        ];
    }

     private function getScheduledPeriod()
    {
        return $this->scheduledInvoice->tenancy->start_date->format('jS \\of F Y').' to '. $this->scheduledInvoice->tenancy->finish_date->format('jS \\of F Y');
    }
}
