<?php

namespace App\Notifications;

use App\Tenancy;
use App\RentPayable;
use App\Notifications\LandlordPaymentDue;

class Notifier
{
	public function newNotification($notifier, $notificationType, $data)
	{
        $notifier->notify(new $notificationType($data));
	}

	public function notifyLandlordPaymentDue(Tenancy $tenancy, RentPayable $rentPayable)
	{
		$n = $tenancy->notifications->where('type', 'App\Notifications\LandlordPaymentDue');

		foreach ($n as $b) {

			if ($b->data['rentPayableId'] == $rentPayable->id) {
				return;
			}
		}

		$tenancy->notify(new LandlordPaymentDue($rentPayable));
	}
	
}



