<?php

namespace App\Notifications;

use App\Fee;
use App\Tenancy;
use App\RentPayable;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class LandlordAdminFeeInvoiceGenerated extends Notification
{
    use Queueable;

    protected $fee;
    
    private $rentPeriod;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Fee $fee)
    {
        $this->fee = $fee;
        $this->rentPeriod = $this->getScheduledPeriod();
    }

   

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Invoice '. $this->fee->xero_ref. ' has been generated.')
                    ->line('This is a landlord admin fee invoice in respect of '. $this->fee->tenancy->property->address. '.');
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'feeId' => $this->fee->id,
            'message' => 'Landlord Admin Fee Invoice '. $this->fee->xero_ref. ' for the period '. $this->rentPeriod . ' has been generated.'
        ];
    }

     private function getScheduledPeriod()
    {
        return $this->fee->tenancy->start_date->format('jS \\of F Y').' to '. $this->fee->tenancy->finish_date->format('jS \\of F Y');
    }
}
