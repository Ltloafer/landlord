<?php

namespace App\Console\Commands;

use App\Production;
use Illuminate\Support\Str;
use Illuminate\Console\Command;

class SplitProductionNameCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'production:split-name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Split the names in productions table into first and second names";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $productions = Production::whereNull('first_name')->get();

        foreach($productions as $production) {
            $this->info($production->name);

            $firstName = Str::before($production->name, ' ');
            $lastName = Str::after($production->name, ' ');

            $production->first_name = $firstName;
            $production->last_name = $lastName;
            $production->save();

            $this->info($firstName);
        }
    }
}
