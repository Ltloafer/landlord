<?php

namespace App\Console\Commands;

use App\Invoice;
use App\Deposit;
use Illuminate\Console\Command;

class createDepositInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createDepositInvoices:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create invoices from existing models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $periods = Deposit::all();

        // foreach ($periods as $fee) {
        //     $fee->invoice_id = null;
        //     $fee->save();
        // }

        foreach ($periods as $period) {

            if (! $period->invoice_id && $period->xero_inv_id ) {

                $invoice = Invoice::where('xero_inv_id', $period->xero_inv_id)->first();

                if ($invoice) {
                    $period->invoice_id = $invoice->id;
                    $period->save();
                    $this->info('Attaching invoice.');


                } else {


                    $this->info('Creating new invoice.');

                    $invoice = new Invoice();
                    $invoice->date = $period->date;
                    $invoice->reference = $period->tenancy->property->address;
                    $invoice->type = 'ACCREC';
                    $invoice->status = 'invoiced';
                    $invoice->tenancy_id = $period->tenancy_id;
                    $invoice->xero_ref = $period->xero_ref;
                    $invoice->xero_inv_id = $period->xero_inv_id;
                    $invoice->contact_id = $period->tenancy->property->landlord->contact_id;

                    $invoice->save(); 

                    $period->invoice_id = $invoice->id;
                    $period->save();
                }
            }
        }
    }
}
