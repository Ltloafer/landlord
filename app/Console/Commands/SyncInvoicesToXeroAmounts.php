<?php

namespace App\Console\Commands;

use XeroLaravel;
use App\Invoice;
use App\Tenancy;
use Illuminate\Console\Command;
use App\Services\Schedules\RentalScheduleRunner;

class syncInvoicesToXeroAmounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncInvoicesToXeroAmounts:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the invoice amounts from Xero and store in our local invoices table';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $invoices = Invoice::all();

        try {

            foreach ($invoices as $invoice) {

                $this->info('Running Invoice '. $invoice->id);

                $this->info('Invoice '. $invoice->xero_inv_id);

                $xero_invoice = XeroLaravel::getInvoiceById($invoice->xero_inv_id);

                $invoice->amount = $xero_invoice['SubTotal'] * 100;
                $invoice->vat = $xero_invoice['TotalTax'] * 100;
                $invoice->total_amount = $xero_invoice['Total'] * 100;
                $invoice->due_date = $xero_invoice['DueDate'];

                $amountDue = $xero_invoice['AmountDue'] * 100;

                if ($amountDue < 1) {

                    $invoice->status = 'paid';
                }

                $invoice->save();
            }
            
        } catch (\Exception $e)
        {
            $this->error('Invoice '. $invoice->id. ' has hit an error.');
        }
    }
}
