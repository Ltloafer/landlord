<?php

namespace App\Console\Commands;

use App\Tenant;
use App\Agent;
use App\ProductionCompany;
use App\Hotel;
use App\Landlord;
use App\Contact;
use App\Supplier;
use Illuminate\Console\Command;

class createContacts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createContacts:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create contacts command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tenants = Tenant::all();

        foreach ($tenants as $tenant) {
            $this->runModel($tenant);
        }

        foreach (Landlord::all() as $landlord) {
            $this->runModel($landlord);
        }

        foreach (Agent::all() as $agent) {
            $this->runModel($agent);
        }

        foreach (Hotel::all() as $hotel) {
            $this->runModel($hotel);
        }

        foreach (ProductionCompany::all() as $company) {
            $this->runModel($company);
        }

        foreach (Supplier::all() as $supplier) {
            $this->runModel($supplier);
        }
    }

    public function runModel($model)
    {
        if (! $model->contact_id && $model->xero_contactID ) {

            $this->info('Creating new contact for '.$model->name);

            $contact = new Contact;
            $contact->name = $model->name;
            $contact->email = $model->email;
            $contact->phone = $model->phone;
            $contact->xero_contactID = $model->xero_contactID;
            $contact->save();

            $model->contact_id = $contact->id;
            $model->save();
        }
    }

}


