<?php

namespace App\Console\Commands;

use App\Tenancy;
use Illuminate\Console\Command;
use App\Services\Schedules\PayablesScheduleRunner;

class RunRentPayables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rentPayables:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the rent payables schedules';

    protected $runner;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PayablesScheduleRunner $runner)
    {
        parent::__construct();

        $this->runner = $runner;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tenancies = Tenancy::where('type', 'direct')->get();

        foreach ($tenancies as $tenancy) {

            $this->info('Running Tenancy '.$tenancy->id. '. '. $tenancy->property->address);

            // $periods = $tenancy->getScheduledInvoicesForTenancy();

            $payables = $tenancy->getScheduledRentPayablesForTenancy();

            foreach ($payables as $payable) {

                try {

                    $this->runner->runScheduledPayablesFromConsole($payable);

                     // $schedulerService->runPayablesSchedule($tenancy);

                    $this->info('Tenancy payable period '. $payable->id. ' has run successfully.');
                }

                catch (\Exception $e)
                {
                    $this->error('Tenancy payable period '. $payable->id. ' has hit an error.');
                }
            }
        }
    }
}
