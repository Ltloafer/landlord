<?php

namespace App\Console\Commands;

use App\Tenancy;
use Illuminate\Console\Command;
use App\Services\Schedules\RentalScheduleRunner;

class RunRentalSchedules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rentalSchedules:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the rental schedules';

    protected $runner;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RentalScheduleRunner $runner)
    {
        parent::__construct();

        $this->runner = $runner;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tenancies = Tenancy::where('type', 'direct')->get();

        foreach ($tenancies as $tenancy) {

            $this->info('Running Tenancy '.$tenancy->id. '. '. $tenancy->property->address);

            $periods = $tenancy->getScheduledInvoicesForTenancy();

            foreach ($periods as $period) {

                try {

                    $this->runner->runSchedulerFromConsole($period);

                     // $schedulerService->runPayablesSchedule($tenancy);

                    $this->info('Tenancy period '. $period->id. ' has run successfully.');
                }

                catch (\Exception $e)
                {
                    $this->error('Tenancy period '. $period->id. ' has hit an error.');
                }
            }
        }
    }
}
