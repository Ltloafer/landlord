<?php

namespace App\Console\Commands;

use App\Tenancy;
use Illuminate\Console\Command;
use App\Services\Schedules\SplitCommsScheduleRunner;

class RunSplitCommSchedules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'splitCommSchedules:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the split commission schedules';

    protected $splitCommsRunner;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SplitCommsScheduleRunner $splitCommsRunner)
    {
        parent::__construct();

        $this->splitCommsRunner = $splitCommsRunner;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tenancies = Tenancy::where('type', 'split-comm')->get();

        foreach ($tenancies as $tenancy) {

            $this->info('Running Tenancy '.$tenancy->id. '. '. $tenancy->property->address);

            $periods = $tenancy->getScheduledInvoicesForTenancy();

            foreach ($periods as $period) {

                try {

                    $this->splitCommsRunner->runSchedulerFromConsole($period);

                    $this->info('Tenancy period '. $period->id. ' has run successfully.');
                }

                catch (\Exception $e)
                {
                    $this->error('Tenancy period '. $period->id. ' has hit an error.');
                }
            }
        }
    }
}
