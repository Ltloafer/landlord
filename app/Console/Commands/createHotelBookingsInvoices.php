<?php

namespace App\Console\Commands;

use App\Invoice;
use App\HotelBooking;
use Illuminate\Console\Command;

class createHotelBookingsInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createHotelInvoices:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create invoices from existing models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $periods = Hotelbooking::all();

        // foreach ($periods as $fee) {
        //     $fee->invoice_id = null;
        //     $fee->save();
        // }

        foreach ($periods as $period) {

            if (! $period->invoice_id && $period->xero_inv_id ) {

                $this->info('Creating new hotel invoice.');

                $invoice = new Invoice();
                $invoice->date = $period->finish_date;
                $invoice->reference = $period->confirmation_reference;
                $invoice->type = 'ACCREC';
                $invoice->status = 'invoiced';
                $invoice->hotel_id = $period->id;
                $invoice->xero_ref = $period->xero_ref;
                $invoice->xero_inv_id = $period->xero_inv_id;
                $invoice->contact_id = $period->hotel->contact_id;

                $invoice->save(); 

                $period->invoice_id = $invoice->id;
                $period->save();
            }
        }
    }
}
