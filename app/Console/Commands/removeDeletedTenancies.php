<?php

namespace App\Console\Commands;

use App\Tenancy;
use Illuminate\Console\Command;

class removeDeletedTenancies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'removeDeletedTenancies:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove deleted command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tenancies = Tenancy::onlyTrashed()->get();

        foreach($tenancies as $tenancy) {

            foreach ($tenancy->fees as $fee) {
                $this->deleteModal($fee, 'Fee', $tenancy);
            }

            foreach ($tenancy->deposits as $deposit) {
                $this->deleteModal($deposit, 'Deposit', $tenancy);
            }

            foreach ($tenancy->scheduledInvoices as $period) {
                $this->deleteModal($period, 'Scheduled Invoice', $tenancy);
            }

            foreach ($tenancy->rentPayables as $rent) {
                $this->deleteModal($rent, 'Rent payables', $tenancy);
            }

            foreach ($tenancy->expenses as $expense) {
                $this->deleteModal($expense, 'Expense', $tenancy);
            }
        }
    }

    private function deleteModal($modal, $type, $tenancy)
    {
        $modal->delete();
        $this->info('Deleting  '.$type. ' '. $modal->id . ' on '. $tenancy->id);
    }
}


