<?php

namespace App\Console\Commands;

use App\Fee;
use App\Invoice;
use Illuminate\Console\Command;

class createFeeInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createFeeInvoices:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create invoices from existing models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fees = Fee::all();

        // foreach ($fees as $fee) {
        //     $fee->invoice_id = null;
        //     $fee->save();
        // }

        foreach ($fees as $fee) {

            if (! $fee->invoice_id && $fee->xero_inv_id ) {

                $this->info('Creating new invoice.');

                $invoice = new Invoice();
                $invoice->date = $fee->date;
                $invoice->reference = $fee->tenancy->property->address;
                $invoice->type = 'ACCREC';
                $invoice->amount = $fee->amount;
                $invoice->vat = $fee->vat;
                $invoice->total_amount = $fee->amount + $fee->vat;
                $invoice->status = 'invoiced';
                $invoice->tenancy_id = $fee->tenancy_id;
                $invoice->xero_ref = $fee->xero_ref;
                $invoice->xero_inv_id = $fee->xero_inv_id;

                try {

                    if ($fee->fee_type_id == 1 || 3) {
                        if ($fee->tenancy->property->agent_id) {
                            $invoice->contact_id = $fee->tenancy->property->agent->contact_id;
                        } elseif ($fee->tenancy->property->landlord_id) {
                            $invoice->contact_id = $fee->tenancy->property->landlord->contact_id;
                        }
                    }

                    if ($fee->fee_type_id == 2) {
                        if ($fee->tenancy->production_company_id) {
                            $invoice->contact_id = $fee->tenancy->productionCompany->contact_id;
                        } else {
                            $invoice->contact_id=  $fee->tenancy->tenant->contact_id;
                        }
                    }

                } catch (\Exception $e)
                {
                    dd($e, $fee->id);
                }

                $invoice->save(); 

                $fee->invoice_id = $invoice->id;
                $fee->save();
            }
        }
    }
}
