<?php

namespace App\Console\Commands;

use App\ScheduledInvoice;
use App\Invoice;
use Illuminate\Console\Command;

class createScheduledInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createScheduledInvoices:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create invoices from existing models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $periods = ScheduledInvoice::all();

        // foreach ($periods as $fee) {
        //     $fee->invoice_id = null;
        //     $fee->save();
        // }

        foreach ($periods as $period) {

            if (! $period->invoice_id && $period->xero_inv_id ) {

                $invoice = Invoice::where('xero_inv_id', $period->xero_inv_id)->first();

                if ($invoice) {
                    $period->invoice_id = $invoice->id;
                    $period->save();

                } else {


                    $this->info('Creating new invoice.');

                    $invoice = new Invoice();
                    $invoice->date = $period->scheduled_date;
                    $invoice->reference = $period->tenancy->property->address;
                    $invoice->type = 'ACCREC';
                    $invoice->amount = $period->amount;
                    $invoice->vat = 0;
                    $invoice->total_amount = $period->amount;
                    $invoice->status = 'invoiced';
                    $invoice->tenancy_id = $period->tenancy_id;
                    $invoice->xero_ref = $period->xero_ref;
                    $invoice->xero_inv_id = $period->xero_inv_id;

                    try {

                        if ($period->tenancy->production_company_id) {
                            $invoice->contact_id = $period->tenancy->productionCompany->contact_id;
                        } else {
                            $invoice->contact_id=  $period->tenancy->tenant->contact_id;
                        }

                    } catch (\Exception $e)
                    {
                        dd($e, $period->id);
                    }

                    $invoice->save(); 

                    $period->invoice_id = $invoice->id;
                    $period->save();
                }
            }
        }
    }
}
