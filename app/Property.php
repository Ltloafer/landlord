<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    use SoftDeletes;
    
    protected $table = 'properties';

    protected $fillable = ['address', 'postcode', 'agent_id'];

    public function landlord()
    {
        return $this->belongsTo(Landlord::class);
    }

    public function agent()
    {
        return $this->belongsTo(Agent::Class);
    }

    public function tenancy()
    {
        return $this->hasMany(Tenancy::class);
    }

    public function propertyDocs()
    {
        return $this->hasMany(PropertyDoc::class);
    }

    public function notes()
    {
    	return $this->hasMany(Note::class);
    }

    public function search()
    {
        return $this->hasMany(Search::class);
    }

    public function hasDocument($id)
    {
        foreach ($this->propertyDocs as $propertyDoc) {
            if ($propertyDoc->property_doc_types_id == $id) {
                return true;
            }
        }
        return false;
    }

    public function hasDocumentationComplete($docTypes)
    {
        $docListIds = PropertyDoc::where('property_id', $this->id)->pluck('property_doc_types_id');

        foreach($docTypes as $docType) {
            if (! $docListIds->contains($docType->id)) {
                return false;
            }
        }
        return true;
    }

    public function getFile()
    {
       
    }



}



