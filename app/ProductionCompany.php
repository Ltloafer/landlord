<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionCompany extends Model
{
	protected $table='production_companies';

	public function search()
	{
		return $this->belongsTo(Search::class);
	}

	public function contactClass()
	{
		return $this->belongsTo(Contact::class, 'contact_id');
	}

	public function displayAddress()
	{
		$address = $this->contactClass->line_1. ', '. $this->contactClass->line_2. ', '. $this->contactClass->line_3;

		return $address;
	}

}
