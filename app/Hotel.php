<?php

namespace App;

use XeroLaravel;
use Illuminate\Database\Eloquent\SoftDeletes;


use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
	use SoftDeletes;
	
	public function bookings()
	{
		return $this->hasMany(HotelBooking::class);
	}

	public function contact()
	{
		return $this->belongsTo(Contact::class);
	}
}
