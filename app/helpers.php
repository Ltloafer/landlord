<?php

use Carbon\Carbon;

if (! function_exists('convertPenceToPounds')) {
	function convertPenceToPounds($value) 
	{
		$convertedValue = $value / 100;

		return number_format($convertedValue, 2);
	}
}

if (! function_exists('formatAmount')) {
	function formatAmount($value) 
	{
		return number_format($value, 2);
	}
}


if (! function_exists('createCarbonDate')) {
	function createCarbonDate($date)
	{
		
		if (Carbon::hasFormat($date, 'd/m/yy')) {

			return Carbon::createFromFormat('d/m/yy', $date);
		} else {
			return  Carbon::createFromFormat('yy-m-d', $date);
		}
	}
}