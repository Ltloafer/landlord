<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
	use SoftDeletes;

	protected $dates = ['date', 'reconciled', 'last_sync', 'due_date', 'paid_on'];

	public function contact()
	{
		return $this->belongsTo(Contact::class);
	}

	public function deposits()
	{
		return $this->hasMany(Deposit::class);
	}

	public function fees()
	{
		return $this->hasMany(Fee::class);
	}

	public function expenses()
	{
		return $this->hasMany(Expense::class);
	}

	public function scheduledInvoices()
	{
		return $this->hasMany(ScheduledInvoice::class);
	}

	public function rentPayables()
	{
		return $this->hasMany(RentPayable::class);
	}

	public function hotelBookings()
	{
		return $this->hasMany(HotelBooking::class);
	}

	public function displayAmount()
	{
		return convertPenceToPounds($this->amount);
	}

	public function displayVat()
	{
		return convertPenceToPounds($this->vat);
	}

	public function displayTotalAmount()
	{
		return convertPenceToPounds($this->total_amount);
	}

	public function isAuthorised()
	{
		if ($this->xero_status == 'Authorised') {
			return true;
		}
		return false;
	}

}

