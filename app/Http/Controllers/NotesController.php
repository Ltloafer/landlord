<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Note::with(['property', 'tenant', 'landlord', 'contact'])->orderBy('created_at', 'desc')->get();

        $filteredNotes = $this->addAttributesToAllowFiltering($notes);

        return view('Notes.notes')->with('notes', $filteredNotes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modelType = $request->input('model-type');
        $modelId = $request->input('modelId');

        $note = new Note();

        if ($modelType == 'landlord') {
            $note->landlord_id = $modelId;
        } 

        if ($modelType == 'property') {
            $note->property_id = $modelId;
        } 

        if ($modelType == 'agent') {
            $note->agent_id = $modelId;
        }

           if ($modelType == 'search') {
            $note->search_id = $modelId;
        }

        $note->note = $request->input('note');

        $note->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     private function addAttributesToAllowFiltering($notes)
    {
        $filteredNotes = $notes->map(function ($note) {
            if ($note->landlord) {
                $note->landlordName = $note->landlord->name;
            } else {
                $note->landlordName = '';
            }

            if ($note->property) {
                $note->propertyAddress = $note->property->address;
            } else {
                $note->propertyAddress = '';
            }

             if ($note->contact) {
                $note->contactName = $note->contact->name;
            } else {
                $note->contactName = '';
            }

            return $note;
        });

        return $filteredNotes;
    }
}
