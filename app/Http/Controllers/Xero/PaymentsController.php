<?php

namespace App\Http\Controllers\Xero;

use App\Action;
use XeroLaravel;
use Carbon\Carbon;
use App\RentPayable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'amount' => 'required|numeric',
        ]);

        $invoice = XeroLaravel::getInvoiceById($request->input('invoiceID'));

        if ($request->input('account') == 'client') {

            $account = XeroLaravel::getAccountById(config('xero.accounts.client')); // Client account

        } else {

            $account = XeroLaravel::getAccountById(config('xero.accounts.trading')); // Trading account
        }

        $payment = XeroLaravel::createPayment();
        $payment->setInvoice($invoice);
        $payment->setAccount($account);
        $payment->setAmount($request->input('amount'));
        $payment->setReference($invoice->InvoiceNumber);

        try {

            XeroLaravel::savePayment($payment);

        } catch (\Exception $e) {

            \Log::error($e);

            return back()->with('alert_message', 'There was an error trying to make this payment. Please try again. Thanks');
        }

        $actionId = $request->input('actionId');

        if ($actionId) {
            $action = Action::findOrFail($actionId);
            $action->completed_at = Carbon::now();
            $action->save();
        }

        return back()->with('success_message', 'This payment has been allocated to the invoice. Thank you.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
