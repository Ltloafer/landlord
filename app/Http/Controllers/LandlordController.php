<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Note;
use XeroLaravel;
use App\Landlord;
use App\Assistant;
use App\LandlordDocType;
use Illuminate\Http\Request;
use App\Traits\CreatesXeroContacts;
use App\Traits\CreatesContacts;

class LandlordController extends Controller
{
    use CreatesXeroContacts, CreatesContacts;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $landlords = Landlord::orderBy('name')->get();

        $agents = Agent::all();

        return view('Landlords.landlords')->with('landlords', $landlords)->with('agents', $agents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $landlord = new Landlord();
        $landlord->name = $request->input('name');
        $landlord->phone = $request->input('phone');
        $landlord->address = $request->input('address');
        $landlord->postcode = $request->input('postcode');
        $landlord->email = $request->input('email');
        $landlord->save();

        $this->createNewXeroContact($landlord);

        if ($request->input('assistant_name')) {
            $assistant = $landlord->assistants()->create([
                'name' => $request->input('assistant_name'),
                'phone' => $request->input('assistant_phone'),
                'email' => $request->input('assistant_email')
            ]);
        };

        if ($request->input('agentId')) {
            $landlord->agent_id = $request->input('agentId');
            $landlord->save();
        }

        if ($request->input('agent_name')) {
            $agent = new Agent();
            $agent->name =  $request->input('agent_name');
            $agent->phone =  $request->input('agent_phone');
            $agent->email =  $request->input('agent_email');
            $agent->save();

            $landlord->agent_id = $agent->id;
            $landlord->save();
        };

        if ($request->input('property_address')) {
            $property = $landlord->properties()->create([
                'address' => $request->input('property_address'),
                'postcode' => $request->input('property_postcode')
            ]);

            XeroLaravel::createXeroAccountCodeForProperty($property);

            $this->createNewContact($landlord);

            return redirect('/properties/'. $property->id);

        } else {

            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Landlord $landlord)
    {
        $notes = Note::where('landlord_id', $landlord->id)->orderBy('created_at', 'desc')->get();

        $requiredDocuments = LandlordDocType::all();

        $agents = Agent::all();

        return view('Landlords.landlord')->with('landlord', $landlord)
        ->with('notes', $notes)
        ->with('agents', $agents)
        ->with('requiredDocuments', $requiredDocuments);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Landlord $landlord)
    {
        return view('Landlords.edit-landlord')->with('landlord', $landlord);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Landlord $landlord)
    {
        // $landlord = Landlord::findOrFail($id);

        if ($request->input('agentId')) {
            $landlord->agent_id = $request->input('agentId');
        }

        if ($request->input('createContactOnXero')) {

            $this->createNewXeroContact($landlord);

        } elseif ($request->input('xero_contactId')) {

            $landlord->xero_contactID = $request->input('xero_contactId');

            $landlord->save();
        }

        $landlord->name = $request->input('name');
        $landlord->phone = $request->input('phone');
        $landlord->address = $request->input('address');
        $landlord->postcode = $request->input('postcode');
        $landlord->email = $request->input('email');
        $landlord->save();


        return $this->index();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
