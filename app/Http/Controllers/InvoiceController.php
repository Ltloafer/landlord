<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Invoice;
use XeroLaravel;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::where('type', 'ACCREC')
        ->orderBy('id', 'desc')
        ->get();

        return view('Invoices.invoices')->with('invoices', $invoices);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payables()
    {
        $invoices = Invoice::where('type', 'ACCPAY')
        ->orderBy('id', 'desc')
        ->get();

        return view('Invoices.payables')->with('invoices', $invoices);
    }


    public function syncWithXero()
    {
        $invs = XeroLaravel::getInvoices();

        $xero_invoices = collect($invs)->recursive();

        foreach ($xero_invoices as $xeroInvoice) {

            $invoice = Invoice::where('xero_inv_id', $xeroInvoice['InvoiceID'])->first();

            if ($invoice) {
                $invoice->status = Str::title($xeroInvoice['Status']);
                $invoice->due_date = $xeroInvoice['DueDate'];
                $invoice->paid_on = $xeroInvoice['FullyPaidOnDate'] ?? null;
                $invoice->save();
            }
        }

        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {

        return view('Invoices.invoice')->with('invoice', $invoice);
        dd($invoice->load('fees', 'expenses', 'scheduledInvoices', 'rentPayables', 'hotelbookings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
