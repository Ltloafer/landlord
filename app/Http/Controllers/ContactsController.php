<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use XeroPHP\Application\PrivateApplication;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::with(['notes'])->get();

        return view('Contacts.contacts')->with('contacts', $contacts);


        //These are the minimum settings - for more options, refer to examples/config.php
        $config = [
            'oauth' => [
                'callback'         => 'http://localhost/',
                'consumer_key'     => '4I3ZJ3F1FJVDH6Y79L6KOG4UUIL1IP',
                'consumer_secret'  => 'UEZ0LJPCUBRNRHUFVMN5GDQWFPXXTL',
                'rsa_private_key'  => 'file:///Users/neil/privatekey.pem',
            ],
        ];

        $xero = new PrivateApplication($config);

        // $contacts = $xero->load(Contact::class)->execute();

        $org = $xero->load('Accounting\\Organisation')->execute();

        dd($org);
        $invs = $xero->load('Accounting\\Invoice')->execute();

        $invoices = collect($invs);

        // $invoices->each(function ($invoice) {
        //     dump($invoice["Total"]);
        // });

        // dd();

        // foreach($invoices as $key => $value) {
            // dump($key, $value["Total"]);
        // }



        // dd();

        $invoice = $xero->loadByGUID('Accounting\\Invoice', 'RPT445-1');

        dd($invoice);

        $url = new \XeroPHP\Remote\URL($xero, sprintf('%s/%s/OnlineInvoice', 'Invoices', '4f9d9bcc-3c75-4884-93a4-19d3b5a4b5f8'));

        // dd($url);

        $request = new \XeroPHP\Remote\Request($xero, $url);
        $request->send();

        $online_invoices = $request->getResponse()->getElements();

        $onlineInvUrl = $online_invoices[0]['OnlineInvoiceUrl'];

        // foreach($online_invoices as $invoice){
        //     dd($invoice['OnlineInvoiceUrl']);
        // }

        $inv = collect($invoice);

        $invoice = collect($inv)->recursive();

        // $invoice->each(function ($item, $key) {
        //     dump($key, $item);
        // });


        // dd($invoice);
        
        // $invPayments = collect($inv["Payments"]);

        // dd($invPayments);

        // $arrlength = count($invPayments);

        // dd($arrlength);

        // foreach($invPayments as $payment) {
        //     var_dump($payment["Amount"]);
        //     var_dump($payment["Date"]);
        // }

        // dd(array_get($invoice, 'Contact.Name'));
        // dd($inv["Payments"][0]["Amount"]);
        // dd(array_get($invoice, 'Payments..Amount'));

        // dd($inv);
        // dd($inv["Payments"]);
        // dd($inv["Contact"]["Name"]);
        // dd($inv["Total"]);

        // dd($invs);
        // dd($org);


        $contacts = Contact::with(['notes'])->get();

        // return view('Contacts.contacts')->with('contacts', $contacts);
        return view('Contacts.invoice')->with('invoice', $invoice)->with('invUrl', $onlineInvUrl);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = new contact();
        $contact->name = $request->input('name');
        $contact->phone = $request->input('phone');
        $contact->address = $request->input('address');
        $contact->email = $request->input('email');
        $contact->save();

        if ($request->input('note')) {
            $note = $contact->notes()->create([
                'note' => $request->input('note')
            ]);
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
