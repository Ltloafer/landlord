<?php

namespace App\Http\Controllers;

use App\Note;
use App\Agent;
use App\Property;
use App\Landlord;
use XeroLaravel;
use App\Tenant;
use App\Search;
use App\PropertyDocType;
use Illuminate\Http\Request;

use Illuminate\Support\Str;

class PropertyController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
     	$properties = Property::with('landlord', 'agent')->orderBy('address')->get();

     	$landlords = Landlord::all();

        $agents = Agent::all();

        return view('Properties.properties')->with('properties', $properties)->with('landlords', $landlords)
        ->with('agents', $agents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $property = new Property();

        if ($request->input('agentOrLandlordId')) {

            if (Str::is('agent*', $request->input('agentOrLandlordId'))) {
                $property->agent_id =  Str::after($request->input('agentOrLandlordId'), 'agent_');
            }

            if (Str::is('landlord*', $request->input('agentOrLandlordId'))) {
                $property->landlord_id = Str::after($request->input('agentOrLandlordId'), 'landlord_');
            }
        } else {

            if ($request->input('landlordId')) {
                $property->landlord_id = $request->input('landlordId');
            }

            if ($request->input('agentId')) {
                $property->agent_id = $request->input('agentId');
            }
        }

        $property->address = $request->input('address');
        $property->postcode = $request->input('postcode');

        $property->save();

        if ($request->input('searchId')) {
            $search = Search::find($request->input('searchId'));
            $search->property_id = $property->id;
            $search->save();
        }

        XeroLaravel::createXeroAccountCodeForProperty($property);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $property = Property::findOrFail($id);

        $tenants = Tenant::all();

        $notes = Note::where('property_id', $id)->orderBy('created_at', 'desc')->get();

        $requiredDocuments = PropertyDocType::all();

        return view('Properties.property')->with('property', $property)
        ->with('tenants', $tenants)
        ->with('notes', $notes)
        ->with('requiredDocuments', $requiredDocuments);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $property = Property::findOrFail($id);

        if ($request->input('createCodeOnXero')) {

            // $this->createNewXeroContact($landlord);

        } elseif ($request->input('xero_code')) {

            $property->xero_code = $request->input('xero_code');

            $property->save();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    


}
