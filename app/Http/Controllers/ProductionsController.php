<?php

namespace App\Http\Controllers;

use App\Production;
use Illuminate\Http\Request;

class ProductionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productions = Production::orderBy('first_name')->get();
        
        foreach($productions as $production) {
            $production->fullName = $production->joinNames();
        }

        return view('Productions.productions')->with('productions', $productions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'unique:productions'
        ]);

        $production = new Production();
        $production->first_name = $request->input('first_name');
        $production->last_name = $request->input('last_name');
        $production->phone = $request->input('phone');
        $production->email = $request->input('email');
        
        $production->line_1 = $request->input('line1');
        $production->line_2 = $request->input('line2');
        $production->line_3 = $request->input('line3');
        $production->line_4 = $request->input('line4');
        $production->postcode = $request->input('postcode');

        $production->name = $production->first_name . ' ' . $production->last_name;

        $production->save();

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Production $production)
    {
        return view('Productions.edit-production')->with('production', $production);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Production $production)
    {
        $production->first_name = $request->input('first_name');
        $production->last_name = $request->input('last_name');
        $production->phone = $request->input('phone');
        $production->email = $request->input('email');
        
        $production->line_1 = $request->input('line1');
        $production->line_2 = $request->input('line2');
        $production->line_3 = $request->input('line3');
        $production->line_4 = $request->input('line4');
        $production->postcode = $request->input('postcode');

        $production->name = $production->first_name . ' ' . $production->last_name;

        $production->save();

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Production $production)
    {
        $production->delete();

        return $this->index();
    }
}
