<?php

namespace App\Http\Controllers;

use App\Agent;
use App\SearchArea;
use Illuminate\Http\Request;

class AgentSearchAreasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $agents = Agent::all();

        // return view('Agents.agents')->with('agents', $agents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Agent $agent, Request $request)
    {
        [$keys, $values] = array_divide($request->except(['_token']));  // split the  array down to just key values

        $agent->searchAreas()->sync($keys); 

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        // $agent->searchAreas()->attach(2);

        $notes = $agent->notes->sortByDesc('created_at');

        $searchAreas = searchArea::all();

        return view('Agents.agent')
        ->with('agent', $agent)
        ->with('searchAreas', $searchAreas)
        ->with('notes', $notes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
