<?php

namespace App\Http\Controllers;

use App\Search;
use App\ProductionCompany;
use Illuminate\Http\Request;
use App\Traits\CreatesXeroContacts;
use App\Traits\CreatesContacts;


class ProductionCompanyController extends Controller
{
    use CreatesXeroContacts, CreatesContacts;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = ProductionCompany::all();

        return view('Production-companies.companies')->with('companies', $companies);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new ProductionCompany();
        $company->name = $request->input('name');
        $company->phone = $request->input('phone');
        $company->address = $request->input('address');
        $company->postcode = $request->input('postcode');
        $company->contact = $request->input('contact');
        $company->email = $request->input('email');
        $company->save();

        if ($request->input('searchId')) {
            $search = Search::find($request->input('searchId'));
            $search->production_company_id = $company->id;
            $search->save();
        }

        $this->createNewXeroContact($company);

        $this->createNewContact($company, $request);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
