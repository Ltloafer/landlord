<?php

namespace App\Http\Controllers;

use App\Property;
use App\PropertyDoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Services\StoreUploadedDocument;

class PropertyDocumentsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Property $property, Request $request, StoreUploadedDocument $storeUploadedDocument)
    {
        $document = new PropertyDoc();
        $document->property_id = $property->id;
        $document->property_doc_types_id = $request->input('documentTypeId');

        $document = $storeUploadedDocument->storeDocument($property, $document, $request);

        return redirect('/properties/'. $property->id);
    }


    /**
     * Download a stored document
     * @param  Request $request  
     * @param  $documentId 
     * @return  \Illuminate\Http\Response
     */
    public function downloadDocument(Request $request, $documentId)
    {
        $property = Property::findOrFail($request->input('model_id'));

        $document = $property->propertyDocs->where('property_doc_types_id', $documentId)->first();

        return response()->download(storage_path('app/public/documents/property/' .$property->id .'/'. $document->file));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
