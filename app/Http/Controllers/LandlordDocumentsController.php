<?php

namespace App\Http\Controllers;

use App\Landlord;
use App\LandlordDoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Services\StoreUploadedDocument;

class LandlordDocumentsController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Landlord $landlord, Request $request, storeUploadedDocument $storeUploadedDocument)
    {
        $document = new landlordDoc();
        $document->landlord_id = $landlord->id;
        $document->landlord_doc_types_id = $request->input('documentTypeId');

        $document = $storeUploadedDocument->storeDocument($landlord, $document, $request);

        return redirect('/landlords/'. $landlord->id);
    }

    /**
     * Download a stored document
     * @param  Request $request  
     * @param  $documentId 
     * @return  \Illuminate\Http\Response
     */
    public function downloadDocument(Request $request, $documentId)
    {
        $landlord = Landlord::findOrFail($request->input('model_id'));

        $document = $landlord->landlordDocs->where('landlord_doc_types_id', $documentId)->last();

        return response()->download(storage_path('app/public/documents/landlord/' .$landlord->id .'/'. $document->file));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
