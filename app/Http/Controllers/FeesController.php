<?php

namespace App\Http\Controllers;

use App\Fee;
use App\Tenancy;
use Illuminate\Http\Request;
use App\Services\Invoicing\FeeInvoicer;

class FeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tenancy = Tenancy::findOrFail($request->input('tenancyId'));

        $disbursement = $tenancy->fees()->create([
            'fee_type_id' => $request->input('fee_type'),
            'date' => createCarbonDate($request->input('date')),
            'amount' => ($request->input('amount')*100),
            'vat' => (($request->input('amount')*100) / 100) * 20
        ]);

        return redirect()->back();
    }

    public function invoiceFee(Request $request, FeeInvoicer $feeInvoicer)
    {
        $fee = Fee::find($request->input('feeId'));

        $invoiceDate = $request->input('invoice_date');

        $invoice = $feeInvoicer->invoiceFee($fee, $invoiceDate);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fee $fee)
    {
        // dd($request);
        // $fee->date = createCarbonDate($request->input('date'));
        $fee->amount = $request->input('amount') * 100;
        $fee->vat = $request->input('vatAmount') * 100;
        $fee->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($feeId)
    {
        $fee = Fee::findOrFail($feeId);
        
        if ($fee->invoice_id) {
            return back()->with('alert_message', 'This fee has been invoiced. Please delete the invoice first.');
        }
        
        $fee->delete();
        
        return back()->with('success_message', 'The fee was removed.');
    }

    public function markAsPaid($feeId)
    {
        $fee = Fee::findOrFail($feeId);

        $fee->status = 'Paid';
        $fee->save();

        return back();
    }
}
