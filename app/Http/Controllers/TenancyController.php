<?php

namespace App\Http\Controllers;

use XeroLaravel;
use App\Search;
use App\FeeType;
use App\Tenancy;
use App\Tenant;
use App\Property;
use Carbon\Carbon;
use App\DisbursementType;
use Illuminate\Http\Request;
use App\Notifications\Notifier;
use App\Services\SchedulerService;
use App\Services\Tenancies\StoreTenancy;
use App\Services\PeriodAttributesBuilder as Builder;
use App\Services\Tenancies\ChecksForXeroContacts;
// use App\Traits\ChecksResourcesAreLinkedToXerocontacts;


class TenancyController extends Controller
{
    // use ChecksResourcesAreLinkedToXerocontacts;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->input('t') == 'running') {
            $tenancies = Tenancy::where('status', 'running')
            ->orderBy('created_at', 'desc')
            ->get();
            
        } elseif ($request->input('t') == 'completed') {
            $tenancies = Tenancy::where('status', 'completed')
            ->orderBy('created_at', 'desc')
            ->get();
        }
        
        else {
            $tenancies = Tenancy::orderBy('created_at', 'desc')->get();
        }

        $availableTenants = Tenant::whereNotNull('xero_ContactID')->get();

        $agentProperties = Property::whereNotNull('agent_id')->get();
        $agentProperties->load('agent');

        $directProperties = Property::whereNotNull('landlord_id')->get();

        return view('Tenancies.tenancies')
        ->with('tenancies', $tenancies)
        ->with('availableTenants', $availableTenants)
        ->with('agentProperties', $agentProperties)
        ->with('directProperties', $directProperties);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StoreTenancy $storeTenancy, ChecksForXeroContacts $checksForXeroContacts, SchedulerService $schedulerService)
    {

        $search = Search::find($request->input('searchId'));

        $checksForXeroContacts->checkResourcesAreLinkedToXeroContacts($search);

        $tenancy = $storeTenancy->storeTenancy($request);

        $schedulerService->createNewSchedule($tenancy);

        return redirect('/tenancies/'. $tenancy->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Builder $builder, Notifier $notifier)
    {
        try {

            $tenancy = Tenancy::findOrFail($id);

            $disbursementTypes = DisbursementType::all();

            $feeTypes = FeeType::all();

            $accountEntries = $tenancy->getAllAccountEntries();

        // $contacts = XeroLaravel::getContacts();

            $clientAccountEntries = $tenancy->getClientAccountEntries();

            $scheduledPeriods = $builder->buildPeriodAttributes($tenancy);

            $fees = $builder->buildFeeAttributes($tenancy);

            $schedule = $this->formSchedule($tenancy);

            $expenses = $builder->buildExpenseAttributes($tenancy);

        } catch (\Exception $e) {

            \Log::error($e);
            // dd($e);

            return back()->with('alert_message', 'We seem to have an issue at the moment. Please try again in a moment.');
        }


        return view('Tenancies.tenancy')
        ->with('tenancy', $tenancy)
        ->with('disbursementTypes', $disbursementTypes)
        ->with('fees', $fees)
        ->with('feeTypes', $feeTypes)
        ->with('accountEntries', $accountEntries)
        ->with('clientAccountEntries', $clientAccountEntries)
        ->with('expenses', $expenses)
        ->with('schedule', $schedule)
        ->with('scheduledPeriods', $scheduledPeriods);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    public function markAsCompleted(Request $request, $id)
    {
        $tenancy = Tenancy::findOrFail($id);
        
        $tenancy->status = 'completed';
        $tenancy->save();
        
        return back();
    }
    
    public function setStatus(Request $request, $id)
    {
        $tenancy = Tenancy::findOrFail($id);
        
        $tenancy->status = $request->input('status');
        $tenancy->save();
        
        return back();
    }
    
    public function setReference(Request $request, $id)
    {
        $tenancy = Tenancy::findOrFail($id);
        
        $tenancy->reference = $request->input('reference');
        
        // dd($request);
        
        $tenancy->save();
        
        return redirect('/tenancies/'. $tenancy->id);
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function formSchedule($tenancy)
    {
        $schedule = collect();

        foreach($tenancy->fees as $fee) {

            if ($fee->scheduledInvoice) {
                $fee->itemDate = $fee->scheduledInvoice->scheduled_date;
            } elseif ($fee->rentPayable) {
                $fee->itemDate = $fee->rentPayable->date;
            } else {
               $fee->itemDate = $fee->date;
           }
           $fee->itemType = $fee->feeType->type;

           $schedule->push($fee);
       }

       foreach($tenancy->deposits as $fee) {
           if ($fee->scheduledInvoice) {
            $fee->itemDate = $fee->scheduledInvoice->scheduled_date;
        }  else {
           $fee->itemDate = $fee->date;
       }

       $fee->itemType = $fee->type. ' deposit';

       $schedule->push($fee);
   }



   foreach($tenancy->scheduledInvoices as $fee) {
    $fee->itemDate = $fee->scheduled_date;
    $fee->itemType = 'Rental payment';

    $schedule->push($fee);
}

foreach($tenancy->rentPayables as $fee) {
    $fee->itemDate = $fee->date;
    $fee->itemType = 'Landlord payment';

    $schedule->push($fee);
}

$sortedSchedule = $schedule->sortBy(function ($item) {

    if ($item instanceof \App\ScheduledInvoice) {
        return $item->scheduled_date;
    }

    return $item->itemDate;
});

return $sortedSchedule;

        // dd($sortedSchedule);
}
}



