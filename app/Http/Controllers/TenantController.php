<?php

namespace App\Http\Controllers;

use App\Tenant;
use XeroLaravel;
use App\Contact;
use App\Search;
use Illuminate\Http\Request;
use App\Traits\CreatesXeroContacts;
use App\Traits\CreatesContacts;

class TenantController extends Controller
{
    use CreatesXeroContacts, CreatesContacts;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tenants = Tenant::all();

        return view('Tenants.tenants')->with('tenants', $tenants);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tenant = new Tenant();
        $tenant->name = $request->input('name');
        $tenant->phone = $request->input('phone');
        $tenant->phone = $request->input('phone') ? $request->input('phone') : ' ';
        $tenant->address = $request->input('address');
        // $tenant->postcode = $request->input('postcode');
        $tenant->email = $request->input('email');
        $tenant->save();

        if ($request->input('searchId')) {
            $search = Search::find($request->input('searchId'));
            $search->tenant_id = $tenant->id;
            $search->save();
        }

        $this->createNewXeroContact($tenant);

        $this->createNewContact($tenant, $request);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tenant = Tenant::findOrFail($id);

        if ($request->input('createContactOnXero')) {

            $this->createNewXeroContact($tenant);

        } elseif ($request->input('xero_contactId')) {

            $tenant->xero_contactID = $request->input('xero_contactId');

            $tenant->save();
        }

        return back();
    }

    // public function createXeroContact($resource)
    // {
    //     $contact = XeroLaravel::createContact($resource);

    //     return $contact->ContactID;


        // $contact = resolve('XeroContact');
        // $contact->setAccountNumber('DMA01');
        // $contact->setContactStatus('ACTIVE');
        // $contact->setName('Carbonardo Industries UK LTD');
        // $contact->setFirstName('Amo');
        // $contact->setLastName('Chohan');
        // $contact->setEmailAddress('neilbaker26@hotmail.com');
        // $contact->setDefaultCurrency('GBP');
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
