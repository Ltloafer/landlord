<?php

namespace App\Http\Controllers;

use App\Search;
use App\Tenancy;
use App\Notification;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $searches = Search::doesntHave('tenancies')->orDoesntHave('hotelBookings')->get()->load('notes');

        $tenancies = Tenancy::all();

        return view('home')->with('searches', $searches)->with('tenancies', $tenancies);
    }
}
