<?php

namespace App\Http\Controllers;

use App\Search;
use Carbon\Carbon;
use App\Mail\SendOffer;
use Illuminate\Http\Request;

class SearchOfferController extends Controller
{
    public function makeOffer(Search $search)
    {
    	return view('Searches.make-offer')->with('search', $search);
    }
    
     public function sendOffer(Search $search)
    {
    	\Mail::to(\Auth::user())->send(new SendOffer($search));

    	$search->offer_sent = Carbon::now();
    	$search->save();

    	return redirect('/searches/'. $search->id);
    }

    public function createDirectTenancy(Search $search)
    {
    	return view('Searches.new-direct-tenancy')->with('search', $search);
    }

    public function createTenancy(Search $search)
    {
    	return view('Searches.new-split-comm-tenancy')->with('search', $search);
    }

     public function offerAccepted(Search $search)
    {
    	// \Mail::to(\Auth::user())->send(new SendOffer($search));

    	$search->offer_accepted = Carbon::now();
    	$search->save();

    	return redirect('/searches/'. $search->id);
    }
}
