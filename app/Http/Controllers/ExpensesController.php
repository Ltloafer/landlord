<?php

namespace App\Http\Controllers;

use App\Expense;
use App\Tenancy;
use App\Disbursement;
use Illuminate\Http\Request;
use App\Services\Actions\CreatesAction;
use App\Services\Schedules\CreatesXeroSupplierBill;

class ExpensesController extends Controller
{
    protected $createsBill;
    protected $createsAction;

    public function __construct(CreatesXeroSupplierBill $createsBill, CreatesAction $createsAction)
    {
        $this->createsBill = $createsBill;
        $this->createsAction = $createsAction;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tenancy = Tenancy::findOrFail($request->input('tenancyId'));

        $invs = $tenancy->scheduledInvoices;

        $rentPayables = $tenancy->rentPayables;

        if (! $tenancy->hasScheduledPayables()) {
                return back()->with('alert_message', 'We cannot add a disbursment as the final landlord payment has been made.');
            }

        if ($request->hasFile('file')) {

            $filename =$request->file('file')->getClientOriginalName();

            $sluggedFilename = str_replace(" ","-", $filename);

            $request->file('file')->storeAs('documents/tenancies/'. $tenancy->id. '/supplier_invoices', $sluggedFilename);
        
        } else {

            $sluggedFilename = '';
        }

        if ($tenancy->hasScheduledPayables()) {

            $nextRentPayable = $rentPayables->firstWhere('status', 'Scheduled');

            $expense = $tenancy->expenses()->create([
                'expense_type_id' => $request->input('expense_type_id'),
                'supplier_id' => $request->input('supplier_id'),
                'date' => createCarbonDate($request->input('date')),
                'amount' => ($request->input('amount')*100),
                'status' => 'Scheduled',
                'scheduled_invoice_id' => $nextRentPayable->scheduled_invoice_id,
                'inv_ref' => $request->input('inv_ref'),
                'file' => $sluggedFilename
            ]);

        } else {

            $expense = $tenancy->expenses()->create([
                'expense_type_id' => $request->input('expense_type_id'),
                'supplier_id' => $request->input('supplier_id'),
                'date' => createCarbonDate($request->input('date')),
                'amount' => ($request->input('amount')*100),
                'status' => 'Pending',
                'inv_ref' => $request->input('inv_ref'),
                'file' => $sluggedFilename
            ]);
        }

        $this->addDisbursementForExpense($expense);

        $this->generateXeroBillForExpense($expense);

        $this->createsAction->createExpenseAction($expense);

        return back();
    }


    public function generateXeroBillForExpense(Expense $expense)
    {
        try {

            $invoice = $this->createsBill->createXeroBill($expense);

            $expense->xero_ref = $invoice['InvoiceNumber'];
            $expense->xero_inv_id = $invoice['InvoiceID'];
            $expense->status = 'Invoiced';
            $expense->save();

        } catch (\Exception $e) {

            \Log::info($e);

            return back()->with('alert_message', 'We met an error trying to create this invoice on xero.');
        }
    }

      /**
     * Download a stored document
     * @param  Request $request  
     * @param  $documentId 
     * @return  \Illuminate\Http\Response
     */
    public function downloadInvoice(Request $request, $expenseId)
    {
        // $expense = Expense::findOrFail($expenseId);

        // return response()->download(storage_path('app/public/documents/tenancies/' .$expense->tenancy->id .'/supplier_invoices'. $expense->file));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $expense = Expense::findOrFail($id);

        return response()->download(storage_path('app/public/documents/tenancies/' .$expense->tenancy->id .'/supplier_invoices/'. $expense->file));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addDisbursementForExpense($expense)
    {
        $disbursement = new Disbursement();
        $disbursement->expense_id = $expense->id;
        $disbursement->date = $expense->date;
        $disbursement->amount = $expense->amount;
        $disbursement->status = $expense->status;
        $disbursement->disbursement_type_id = 1;
        $disbursement->tenancy_id = $expense->tenancy_id;
        $disbursement->type = $expense->expense_type_id;
        $disbursement->save();
    }
}
