<?php

namespace App\Http\Controllers;

use App\Agent;
use App\SearchArea;
use Illuminate\Http\Request;
use App\Traits\CreatesContacts;
use App\Traits\CreatesXeroContacts;

class AgentsController extends Controller
{
    use CreatesXeroContacts, CreatesContacts;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agents = Agent::all();

        return view('Agents.agents')->with('agents', $agents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agent = new agent();
        $agent->name = $request->input('name');
        $agent->phone = $request->input('phone');
        $agent->address = $request->input('address');
        $agent->postcode = $request->input('postcode');
        $agent->email = $request->input('email');
        $agent->save();

        $this->createNewXeroContact($agent);

        $this->createNewContact($agent);

        if ($request->input('note')) {
            $note = $agent->notes()->create([
                'note' => $request->input('note')
            ]);
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        $notes = $agent->notes->sortByDesc('created_at');

        $searchAreas = SearchArea::all();

        return view('Agents.agent')
        ->with('agent', $agent)
        ->with('searchAreas', $searchAreas)
        ->with('notes', $notes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agent = Agent::findOrFail($id);

         if ($request->input('createContactOnXero')) {

            $this->createNewXeroContact($agent);

        } elseif ($request->input('xero_contactId')) {

            $agent->xero_contactID = $request->input('xero_contactId');

            $agent->save();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
