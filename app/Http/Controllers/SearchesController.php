<?php

namespace App\Http\Controllers;

use App\Job;
use App\Note;
use App\Search;
use App\Tenant;
use App\Property;
use App\SearchArea;
use Illuminate\Http\Request;

class SearchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Searches.searches');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $search = new Search();

        if ($request->input('tenant_id')) {

            $tenant = Tenant::findOrFail($request->input('tenant_id'));
            $search->tenant_id = $tenant->id;
            $search->name = $tenant->name;
            $search->phone = $tenant->phone;
            $search->address = $tenant->address;
            $search->email = $tenant->email;

        } else {

            $search->name = $request->input('name');
            $search->phone = $request->input('phone');
            $search->address = $request->input('address');
            $search->email = $request->input('email');
        }


        $search->production = $request->input('production');

        $search->let_type = '';


        if ($request->input('production_company_id')) {
            $search->production_company_id = $request->input('production_company_id');
        }
        

        if ($request->input('startdate')) {
            $search->start_date = str_replace("/", "-", $request->input('startdate'));
        }

        if ($request->input('enddate')) {
            $search->end_date = str_replace("/", "-", $request->input('enddate'));
        }

        $search->save();

        if ($request->input('note')) {
            $note = new Note();
            $note->search_id = $search->id;
            $note->note = $request->input('note');
            $note->save();
        }

        $job = new Job();
        $job->save();
        
        $search->job_id = $job->id;
        $search->save();

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Search $search)
    {
        $searchAreas = SearchArea::all();

        $directProperties = Property::whereNotNull('landlord_id')
        ->whereNotNull('account_code')
        ->get();

        $agentProperties = Property::whereNotNull('agent_id')
        ->whereNotNull('account_code')
        ->get();

        $agentProperties->load('agent');

        return view('Searches.search')->with('search', $search)
        ->with('searchAreas', $searchAreas)
        ->with('directProperties', $directProperties)
        ->with('agentProperties', $agentProperties);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $search = Search::find($id);

        if ($request->input('startdate')) {
            $search->start_date = str_replace("/", "-", $request->input('startdate'));
        }

        if ($request->input('enddate')) {
            $search->end_date = str_replace("/", "-", $request->input('enddate'));
        }

        if ($request->input('break_clause')) {
            $search->break_clause = $request->input('break_clause');
        }

        if ($request->input('comm_rate')) {
            $search->comm_rate = $request->input('comm_rate') * 100;
        }

        if ($request->input('deposit')) {
            $search->holding_deposit = $request->input('deposit') * 100;
        }

        if ($request->input('offer_amount')) {
            $search->offer_amount = $request->input('offer_amount') * 100;
        }

        $search->save();

        return back();
    }

    public function setPropertyOnSearch(Request $request)
    {
        $search = Search::find($request->searchId);

        $search->property_id = $request->input('property_id');
        $search->save();

        return back();
    }

    public function setTenant(Request $request)
    {
        $search = Search::find($request->searchId);

        $search->tenant_id = $request->input('tenant_id');
        $search->save();

        return back();
    }

    public function setProductionCompany(Request $request)
    {
        $search = Search::find($request->searchId);

        if ($request->input('production_company_id') == 'none') {
            $search->production_company_id = null;
            $search->save();

            return back();
        }

        $search->production_company_id = $request->input('production_company_id');
        $search->save();

        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $search = Search::find($id);

        $search->delete();

        return $this->index();
    }

    public function convertSearchToDirectLet(Search $search)
    {
       $directProperties = Property::whereNotNull('landlord_id')
       ->whereNotNull('account_code')
       ->get();

       $searchAreas = SearchArea::all();

       return view('Searches.convert-to-direct')->with('search', $search)->with('directProperties', $directProperties)
       ->with('searchAreas', $searchAreas);
   }
}
