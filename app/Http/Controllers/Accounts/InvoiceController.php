<?php

namespace App\Http\Controllers\Accounts;

use App\Invoice;
use XeroLaravel;
use App\Tenancy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use XeroPHP\Application\PrivateApplication;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;


class InvoiceController extends Controller
{
	/**
    * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
		// $invs = $this->xero->load('Accounting\\Invoice')->execute();

		$invs = XeroLaravel::authorisedInvoicesReceivable();

		$invoices = collect($invs)->recursive();

		$invoices = $invoices->sortByDesc('Date');

		return view('Accounts.invoices')->with('invoices', $invoices);
	}

     public function showInvoicesPayable()
     {
          $invs = XeroLaravel::invoicesPayable();

          // dd($invs);

          foreach ($invs as $inv) {
               // var_dump($inv["invoiceNumber"]);
               # code...
          }

          // dd();

          $invoices = collect($invs)->recursive();

          $invoices = $invoices->sortByDesc('Date');

          return view('Accounts.invoices-payable')->with('invoices', $invoices);

     }

     public function viewDraftInvoices()
     {
       $invs = XeroLaravel::draftInvoicesReceivable();

       $invoices = collect($invs)->recursive();

       $invoices = $invoices->sortByDesc('Date');

       return view('Accounts.draft-invoices')->with('invoices', $invoices);

  }

		/**
     	* Display the specified resource.
	    *
	    * @param  int  $id
	    * @return \Illuminate\Http\Response
	    */
     	public function show($id)
     	{
               $invoice = Invoice::find($id);

               $xeroInvoice = XeroLaravel::getInvoiceById($invoice->xero_inv_id);

               dd($xeroInvoice);

               return view('Accounts.xero_invoice')->with('invoice', $xeroInvoice);
          }

          public function viewXeroInvoice($invoiceID)
          {
             $invUrl = XeroLaravel::getXeroInvoiceUrl($invoiceID);

             return redirect()->away($invUrl);
        }

     	/**
     	* Show the form for creating a new resource.
     	*
     	* @return \Illuminate\Http\Response
     	*/
     	public function create()
     	{
     		$invoice = new XeroInvoice();

			// Set up the invoice contact
			// Assign the contact to the invoice
     		$contact = resolve('XeroContact');
     		$contact->setAccountNumber('DMA01');
     		$contact->setContactStatus('ACTIVE');
     		$contact->setName('Carbonardo Industries UK LTD');
     		$contact->setFirstName('Amo');
     		$contact->setLastName('Chohan');
     		$contact->setEmailAddress('amo.chohan@gmail.com');
     		$contact->setDefaultCurrency('GBP');

			// Assign the contact to the invoice
     		$invoice->setContact($contact);

			// Set the type of invoice being created (ACCREC / ACCPAY)
     		$invoice->setType('ACCREC');

     		$dateInstance = new \DateTime();
     		$invoice->setDate($dateInstance);
     		$invoice->setDueDate($dateInstance);
     		// $invoice->setInvoiceNumber('DMA-00002');
     		$invoice->setReference('24 Ainger Road, London. NW3 3AS');

			// Provide an URL which can be linked to from Xero to view the full order
     		$invoice->setUrl('http://yourdomain/fullpathtotheorder');
     		$invoice->setCurrencyCode('GBP');

     		$invoice->setStatus('Draft');

			// Create some order lines
     		$line1 = resolve('XeroInvoiceLine');
     		$line1->setDescription('Rental payment due in respect of the above mentioned property 01/04/18- 30/04/18');

     		$line1->setQuantity(1);
     		$line1->setUnitAmount(6716.67);
     		$line1->setTaxAmount(0);
     		$line1->setLineAmount(6716.67);
			$line1->setDiscountRate(0); // Percentage

			// Add the line to the order
			$invoice->addLineItem($line1);
			
			// $invoice->save();

			XeroLaravel::saveInvoice($invoice);

			dd($invoice);	
		}

     	/**
     	* Store a newly created resource in storage.
     	*
     	* @param  \Illuminate\Http\Request  $request
     	* @return \Illuminate\Http\Response
     	*/
     	public function store(Request $request)
     	{
     		$tenancy = Tenancy::findOrFail($request->input('tenancyId'));

     		// dd($tenancy);

     		$invoice = new XeroInvoice();

			// Set up the invoice contact
			// Assign the contact to the invoice
     		$contact = resolve('XeroContact');
     		$contact->setAccountNumber('DMA01');
     		$contact->setContactStatus('ACTIVE');
     		$contact->setName('Carbonardo Industries UK LTD');
     		$contact->setFirstName('Amo');
     		$contact->setLastName('Chohan');
     		$contact->setEmailAddress('neilbaker26@hotmail.com');
     		$contact->setDefaultCurrency('GBP');

			// Assign the contact to the invoice
     		$invoice->setContact($contact);

			// Set the type of invoice being created (ACCREC / ACCPAY)
     		$invoice->setType('ACCREC');

     		$dateInstance = new \DateTime();
     		$invoice->setDate($dateInstance);
     		$invoice->setDueDate($dateInstance);
     		// $invoice->setInvoiceNumber('DMA-00002');
     		$invoice->setReference($tenancy->property->address);

			// Provide an URL which can be linked to from Xero to view the full order
     		$invoice->setUrl('http://yourdomain/fullpathtotheorder');
     		$invoice->setCurrencyCode('GBP');

     		$invoice->setStatus('Draft');

			// Create some order lines
     		$line1 = resolve('XeroInvoiceLine');
     		$line1->setDescription('Rental payment due in respect of the above mentioned property 01/04/18- 30/04/18');

     		$line1->setQuantity(1);
     		$line1->setUnitAmount($tenancy->monthly_rate/100);
     		$line1->setTaxAmount(0);
     		$line1->setLineAmount($tenancy->monthly_rate/100);
			$line1->setDiscountRate(0); // Percentage

			// Add the line to the order
			$invoice->addLineItem($line1);
			
			// $invoice->save();

			XeroLaravel::saveInvoice($invoice);

			dd($invoice);	
      }

      public function convertToTradingTheme($invoiceId)
      {     
          $invoice = Invoice::find($invoiceId);

          $xeroInvoice = XeroLaravel::updateInvoiceBrandingThemeToTrading($invoice->xero_inv_id);

          return back();
     }

     public function convertAllHotelInvoicesToTradingTheme()
     {
          $invoices = Invoice::whereNotNull('hotel_id')
          ->get();

          foreach($invoices as $invoice) {

               $invoice = Invoice::find($invoice->id);

               $xeroInvoice = XeroLaravel::updateInvoiceBrandingThemeToTrading($invoice->xero_inv_id);

               sleep(2);
          }
          

          return back();
     }

      public function convertAllHotelInvoicesToTradingThemeByHotel($id)
     {
          $invoices = Invoice::where('hotel_id', $id)
          ->get();

          foreach($invoices as $invoice) {

               $invoice = Invoice::find($invoice->id);

               $xeroInvoice = XeroLaravel::updateInvoiceBrandingThemeToTrading($invoice->xero_inv_id);

               sleep(2);
          }

          return back();
     }


}

