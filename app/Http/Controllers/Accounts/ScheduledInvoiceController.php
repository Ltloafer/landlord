<?php

namespace App\Http\Controllers\Accounts;

use XeroLaravel;
use App\Tenancy;
use App\RentPayable;
use App\ScheduledInvoice;
use Illuminate\Http\Request;
use App\Services\SchedulerService;
use App\Http\Controllers\Controller;
use App\Services\ScheduledInvoices;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;


class ScheduledInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scheduledInvoices = ScheduledInvoice::all();

        return view('Accounts.scheduled-invoices')->with('scheduledInvoices', $scheduledInvoices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generateSchedule(Request $request, SchedulerService $schedulerService)
    {
        $tenancy = Tenancy::findOrFail($request->input('tenancyId'));

        $schedulerService->createNewSchedule($tenancy);

        return redirect()->back();
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request, SchedulerService $schedulerService)
     {
        $tenancy = Tenancy::findOrFail($request->input('tenancyId'));

        $schedulerService->addScheduledPeriod($tenancy, $request);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $scheduledInvoice = ScheduledInvoice::find($id);

        $scheduledInvoice->scheduled_date = \Carbon\Carbon::createFromFormat('d/m/Y', $request->input('date'));
        $scheduledInvoice->start_date = \Carbon\Carbon::createFromFormat('d/m/Y', $request->input('startdate'));
        $scheduledInvoice->end_date = \Carbon\Carbon::createFromFormat('d/m/Y', $request->input('enddate'));
        $scheduledInvoice->amount = ($request->input('amount') * 100);
        $scheduledInvoice->add_info = $request->add_info;
        $scheduledInvoice->save();

        return back();
    }

    public function markAsPaid($id)
    {
        $invoice = ScheduledInvoice::findOrFail($id);
        $invoice->status = 'Paid';
        $invoice->save();

        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $scheduledInvoice = ScheduledInvoice::find($id);

        $rentPayable = $scheduledInvoice->rentPayable;

        try {

            $scheduledInvoice->delete();

            if ($rentPayable) {
                $rentPayable->delete();
            }

        } catch (\Exception $e) {

            return back()->with('flash_message', 'We had a problem deleting that.');
        }

        return back();
    }

    public function invoiceRun(Tenancy $tenancy, SchedulerService $schedulerService)
    {
        if ($tenancy->isViaAgent()) {

            $schedulerService->runSplitCommsSchedule($tenancy);

        } else {

            $schedulerService->runRentalSchedule($tenancy);

            $schedulerService->runPayablesSchedule($tenancy);
        }

        return back();
    }

}
