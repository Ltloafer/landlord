<?php

namespace App\Http\Controllers;

use App\Payment;
use XeroLaravel;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $xero_payments = XeroLaravel::getPayments();

        } catch (\Exception $e) {

            return back()->with('alert_message', 'We cannot connect with Xero at the moment.');
        }

        $xero_payments = collect($xero_payments)->recursive();

        $payments = collect();

        foreach ($xero_payments as $xero_payment) {
            $payment = new Payment;
            $payment->date = $xero_payment['Date'];
            $payment->customer = $xero_payment['Invoice']['Contact']['Name'];
            $payment->amount = number_format($xero_payment['Amount'],2);

            try {

                $payment->bank = $xero_payment['Account']['Code'];
            
            } catch (\Exception $e) {
                \Log::info($e);
            }

            $payment->reconciled = $xero_payment['IsReconciled'];
            $payment->status = $xero_payment['Status'] ;
            $payment->invNo = $xero_payment['Invoice']['InvoiceNumber'];
            $payments->push($payment);
        }

        $xeroPayments = $payments->sortByDesc('Date');

        return view('Invoices.payments')->with('payments', $xeroPayments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
