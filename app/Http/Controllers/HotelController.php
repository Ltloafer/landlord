<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Contact;
use XeroLaravel;
use Illuminate\Http\Request;
use App\Traits\CreatesContacts;
use App\Traits\CreatesXeroContacts;


class HotelController extends Controller
{
    use CreatesXeroContacts, CreatesContacts;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotels = Hotel::all()->sortBy('name');

        return view('Hotels.hotels')->with('hotels', $hotels);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hotel = New Hotel();
        $hotel->name = $request->input('name');
        $hotel->email = $request->input('email');
        $hotel->address = $request->input('address');
        $hotel->postcode = $request->input('postcode');
        $hotel->contact_name = $request->input('contact_name');
        $hotel->save();

        $this->createNewXeroContact($hotel);

        $this->createNewContact($hotel);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {

        if ($request->input('createContactOnXero')) {

            $this->createNewXeroContact($hotel);

        } elseif ($request->input('xero_contactId')) {

            $hotel->xero_contactID = $request->input('xero_contactId');

            $hotel->save();

        } else {

            $hotel->name = $request->input('name');
            $hotel->email = $request->input('email');
            $hotel->address = $request->input('address');
            $hotel->postcode = $request->input('postcode');
            $hotel->contact_name = $request->input('contact_name');
            $hotel->save();

            if ($hotel->xero_contactID) {

                XeroLaravel::updateContact($hotel);
            }
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
