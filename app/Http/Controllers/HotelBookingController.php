<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Invoice;
use App\Search;
use Carbon\Carbon;
use App\HotelBooking;
use Illuminate\Http\Request;
use App\Traits\CreatesXeroContacts;
use App\Services\Invoicing\HotelInvoicer;

class HotelBookingController extends Controller
{
    use CreatesXeroContacts;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotelBookings = HotelBooking::orderBy('created_at', 'desc')->paginate(9);

        $hotels = Hotel::all();

        return view('Hotels.hotel-bookings')
        ->with('hotelBookings', $hotelBookings)
        ->with('hotels', $hotels);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        // $checksForXeroContacts->checkResourcesAreLinkedToXeroContacts($request);

        // $hotelBooking = $storehotelBooking->storehotelBooking($request);

        if ($searchId  = $request->input('searchId')) {
            $search = Search::findOrFail($searchId);
        }
        
        if ($request->input('hotel_id')) {

            $hotel = Hotel::find($request->input('hotel_id')); 

        } elseif  ($request->input('hotel_name')) {

            $hotel = New Hotel();
            $hotel->name = $request->input('hotel_name');
            $hotel->email = $request->input('hotel_email');

            if (! $request->input('hotel_address')) {
                return back()->with('alert_message', 'You must enter a hotel address');
            }

            $hotel->address = $request->input('hotel_address');
            $hotel->postcode = $request->input('hotel_postcode');
            $hotel->save();
        } 

        $hotelBooking = new HotelBooking();

        if ($request->input('searchId')) {

           $search = Search::findOrFail($request->input('searchId'));

        //  if ($search->tenant_id) {
        //     $hotelBooking->tenant_id = $search->tenant_id;
        // } else {
        //     $hotelBooking->tenant_id = $this->createNewTenant($request);
        // }

           $hotelBooking->search_id = $search->id;

           if ($search->productionCompany) {
            $hotelBooking->production_company_id = $search->production_company_id;
        }
    }

    $hotelBooking->production = $request->input('production');

    $hotelBooking->hotel_id = $hotel->id;

    $hotelBooking->confirmation_reference = $request->input('confirmation_reference');

    $hotelBooking->start_date = str_replace("/", "-", $request->input('startdate'));

    $hotelBooking->finish_date = str_replace("/", "-", $request->input('enddate'));

    $hotelBooking->comm_rate = ($request->input('comm_rate') * 100);

    $hotelBooking->room_type = $request->input('room_type');

    $hotelBooking->nightly_rate = ($request->input('nightly_rate') * 100);

    $hotelBooking->save();

    if (! $hotel->xero_contactID) {
        $this->createNewXeroContact($hotel);
    }

    if ($searchId  = $request->input('searchId')) {
        $search = Search::findOrFail($searchId);
        $search->status = 'completed';
        $search->save();
    }

    return redirect('/hotel-bookings');
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(HotelBooking $hotelBooking)
    {
        return view('Hotels.booking')->with('booking', $hotelBooking);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(HotelBooking $hotelBooking)
    {
        return view('Hotels.edit-booking')->with('booking', $hotelBooking)->with('search', $hotelBooking->search);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HotelBooking $hotelBooking)
    {
        if ($request->input('searchId')) {

            $search = Search::findOrFail($request('searchId'));

            //  if ($search->tenant_id) {
            //     $hotelBooking->tenant_id = $search->tenant_id;
            // } else {
            //     $hotelBooking->tenant_id = $this->createNewTenant($request);
            // }

            // $hotelBooking->search_id = $search->id;

            // if ($search->productionCompany) {
                    //     $hotelBooking->production_company_id = $search->production_company_id;
                    // }
        }


        $hotelBooking->hotel_id = $request->input('hotel_id');

        $hotelBooking->confirmation_reference = $request->input('confirmation_reference');

        // $hotelBooking->start_date =  Carbon::createFromFormat('d/m/Y', $request->input('startdate'));

         $hotelBooking->start_date = str_replace("/", "-", $request->input('startdate'));

         $hotelBooking->finish_date = str_replace("/", "-", $request->input('enddate'));


        // $hotelBooking->finish_date = Carbon::createFromFormat('d/m/Y', $request->input('enddate'));

        $hotelBooking->comm_rate = ($request->input('comm_rate') * 100);

        $hotelBooking->room_type = $request->input('room_type');

        $hotelBooking->nightly_rate = ($request->input('nightly_rate') * 100);

        $hotelBooking->save();

        return back();
    }

    public function checkout(Request $request, HotelInvoicer $hotelInvoicer)
    {
        $booking = HotelBooking::find($request->input('bookingId'));

        if (! $booking->confirmation_reference) {
            return back()->with('alert_message', 'This booking requires a confirmation reference to process an invoice');
        }

        // if (! $booking->hotel->email) {
        //     return back()->with('alert_message', 'You need to add a contact email address for the hotel to process an invoice.');
        // }

        if (! $booking->hotel->xero_contactID) {
            return back()->with('alert_message', 'You need to add this hotel as a contact on Xero.');
        }

        // $invoice = $booking->invoice();

        try {

            $xero_invoice = $hotelInvoicer->createXeroInvoice($booking);

        } catch (\Exception $e) {
            \Log::info($e);

            dd($e);

            return back()->with('alert_message', 'We met an error trying to invoice this booking.');
        }

        $invoice = new Invoice();

        $invoice->date = Carbon::now();
        $invoice->reference = $booking->confirmation_reference;
        $invoice->type = 'ACCREC';
        $invoice->hotel_id = $booking->hotel_id;
        $invoice->contact_id = $booking->hotel->contact_id;
        $invoice->status = 'invoiced';
        $invoice->xero_ref =  $xero_invoice['InvoiceNumber'];
        $invoice->xero_inv_id = $xero_invoice['InvoiceID'];
        $invoice->amount = $xero_invoice['SubTotal'] * 100;
        $invoice->vat = $xero_invoice['TotalTax'] * 100;
        $invoice->total_amount = $xero_invoice['Total'] * 100;

        $invoice->due_date = $xero_invoice['DueDate'];
        $invoice->last_sync = Carbon::now();
        $invoice->save(); 

        $booking->invoice_id = $invoice->id;
        $booking->completed = true;
        $booking->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = HotelBooking::find($id);

        $booking->delete();

        return 'okay';
    }
}
