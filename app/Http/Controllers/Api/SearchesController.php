<?php

namespace App\Http\Controllers\Api;

use App\Note;
use App\Search;
use App\Tenant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchesController extends Controller
{
    public function convertToTenant($id, Request $request)
    {
        $search = Search::findOrFail($id);

        $tenant = new Tenant();
        $tenant->name = $search->name;
        $tenant->address = $search->address;
        $tenant->phone = $search->phone;
        $tenant->email = $search->email;
        $tenant->save();

        $search->delete();

        return $tenant;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $combinedSearches = collect();

        $searches = Search::doesntHave('tenancies')->doesntHave('hotelBookings')
        ->whereNull('starred')
        ->get()
        ->load('notes', 'searchAreas');

        foreach ($searches as $search) {
            if ($search->notes->isNotEmpty()) {
                $search->lastNote = $search->notes->last()->note;
            }
            $combinedSearches->push($search);
        }

        $starredSearches = Search::doesntHave('tenancies')->doesntHave('hotelBookings')
        ->whereNotNull('starred')
        ->get()
        ->load('notes', 'searchAreas');

        foreach ($starredSearches as $search) {
            if ($search->notes->isNotEmpty()) {
                $search->lastNote = $search->notes->last()->note;
            }
            $combinedSearches->push($search);
        }

        return $combinedSearches;
    }

    /**
     * Display a listing all searches including soft deleted     
     * *
     * @return \Illuminate\Http\Response
     */
    public function searchesIncArchived()
    {
        $searches = Search::with(['notes'])->withTrashed()->get();

        foreach ($searches as $search) {
            if ($search->notes->isNotEmpty()) {
                $search->lastNote = $search->notes->last()->note;
            }
        }

        return $searches;
    }

    public function markAsStarred($id)
    {
        $search = Search::find($id);

        $search->starred = 1;
        $search->save();

        return 'okay';
    }

     public function removeStarred($id)
    {
        $search = Search::find($id);

        $search->starred = null;
        $search->save();

        return 'okay';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
