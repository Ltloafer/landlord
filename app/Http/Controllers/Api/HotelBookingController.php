<?php

namespace App\Http\Controllers\Api;


use App\Hotel;
use App\Search;
use Carbon\Carbon;
use App\HotelBooking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\CreatesXeroContacts;
use App\Services\Invoicing\HotelInvoicer;

class HotelBookingController extends Controller
{
    use CreatesXeroContacts;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $bookings = HotelBooking::orderBy('created_at', 'desc');
        $bookings = HotelBooking::with('hotel', 'search')->get();

        foreach ($bookings as $booking) {
            $booking->hotelName = $booking->hotel->name;
            $booking->searchName = $booking->search->name;
            $booking->startDate = $booking->start_date->format('d/m/y');
            $booking->finishDate = $booking->finish_date->format('d/m/y');
            $booking->nights = $booking->start_date->diffInDays($booking->finish_date);
            $booking->nightlyRate = $booking->getNightlyRate();
            $booking->commissionRate = $booking->getCommissionRate();
            $booking->totalCommission = $booking->getCommissionTotal();

            if ($booking->completed) {
                $booking->status = 'Invoiced '. optional($booking->invoiceModel)->xero_ref;
            } else {
                $booking->status = 'Pending';
            }
        }

        return $bookings;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        // $checksForXeroContacts->checkResourcesAreLinkedToXeroContacts($request);

        // $hotelBooking = $storehotelBooking->storehotelBooking($request);

        if ($searchId  = $request->input('searchId')) {
            $search = Search::findOrFail($searchId);
        }
        
        if ($request->input('hotel_id')) {

            $hotel = Hotel::find($request->input('hotel_id')); 

        } elseif  ($request->input('hotel_name')) {

            $hotel = New Hotel();
            $hotel->name = $request->input('hotel_name');
            $hotel->email = $request->input('hotel_email');

            if (! $request->input('hotel_address')) {
                return back()->with('alert_message', 'You must enter a hotel address');
            }

            $hotel->address = $request->input('hotel_address');
            $hotel->postcode = $request->input('hotel_postcode');
            $hotel->save();
        } 

        $hotelBooking = new HotelBooking();

        if ($request->input('searchId')) {

           $search = Search::findOrFail($request->input('searchId'));

        //  if ($search->tenant_id) {
        //     $hotelBooking->tenant_id = $search->tenant_id;
        // } else {
        //     $hotelBooking->tenant_id = $this->createNewTenant($request);
        // }

           $hotelBooking->search_id = $search->id;

           if ($search->productionCompany) {
            $hotelBooking->production_company_id = $search->production_company_id;
        }
    }

    $hotelBooking->production = $request->input('production');

    $hotelBooking->hotel_id = $hotel->id;

    $hotelBooking->confirmation_reference = $request->input('confirmation_reference');

    $hotelBooking->start_date = str_replace("/", "-", $request->input('startdate'));

    $hotelBooking->finish_date = str_replace("/", "-", $request->input('enddate'));

    $hotelBooking->comm_rate = ($request->input('comm_rate') * 100);

    $hotelBooking->room_type = $request->input('room_type');

    $hotelBooking->nightly_rate = ($request->input('nightly_rate') * 100);

    $hotelBooking->save();

    if (! $hotel->xero_contactID) {
        $this->createNewXeroContact($hotel);
    }

    if ($searchId  = $request->input('searchId')) {
        $search = Search::findOrFail($searchId);
        $search->status = 'invoiced';
        $search->save();
    }

    return redirect('/hotel-bookings');
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = HotelBooking::find($id)->load('hotel', 'search');

        $booking->hotelName = $booking->hotel->name;
        $booking->searchName = $booking->search->name;
        $booking->startDate = $booking->start_date->format('m/d/y');
        $booking->finishDate = $booking->finish_date->format('m/d/y');
        $booking->nights = $booking->start_date->diffInDays($booking->finish_date);
        $booking->commissionRate = $booking->getCommissionRate();
        $booking->totalCommission = $booking->getCommissionTotal();
        $booking->nightlyRate = $booking->getNightlyRate();

        $booking->hotelName = $booking->hotel->name;
        $booking->searchName = $booking->search->name;

        if ($booking->completed) {
            $booking->status = 'Invoiced';
        } else {
            $booking->status = 'Pending';
        }

        return $booking;

        return view('Hotels.booking')->with('booking', $hotelBooking);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(HotelBooking $hotelBooking)
    {
        return view('Hotels.edit-booking')->with('booking', $hotelBooking)->with('search', $hotelBooking->search);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HotelBooking $hotelBooking)
    {
        if ($request->input('searchId')) {

            $search = Search::findOrFail($request('searchId'));

            //  if ($search->tenant_id) {
            //     $hotelBooking->tenant_id = $search->tenant_id;
            // } else {
            //     $hotelBooking->tenant_id = $this->createNewTenant($request);
            // }

            // $hotelBooking->search_id = $search->id;

            // if ($search->productionCompany) {
                    //     $hotelBooking->production_company_id = $search->production_company_id;
                    // }
        }


        $hotelBooking->hotel_id = $request->input('hotel_id');

        $hotelBooking->confirmation_reference = $request->input('confirmation_reference');

        $hotelBooking->start_date =  Carbon::createFromFormat('d/m/Y', $request->input('startdate'));

        $hotelBooking->finish_date = Carbon::createFromFormat('d/m/Y', $request->input('enddate'));

        $hotelBooking->comm_rate = ($request->input('comm_rate') * 100);

        $hotelBooking->room_type = $request->input('room_type');

        $hotelBooking->nightly_rate = ($request->input('nightly_rate') * 100);

        $hotelBooking->save();

        return back();
    }

    public function checkout(Request $request, HotelInvoicer $hotelInvoicer)
    {
        $booking = HotelBooking::find($request->input('bookingId'));

        if (! $booking->confirmation_reference) {
            // return back()->with('alert_message', 'This booking requires a confirmation reference to process an invoice');
        }

        if (! $booking->hotel->email) {
            // return back()->with('alert_message', 'You need to add a contact email address for the hotel to process an invoice.');
        }

        if (! $booking->hotel->xero_contactID) {
            // return back()->with('alert_message', 'You need to add this hotel as a contact on Xero.');
        }

        $invoice = $booking->invoice();

        dd($invoice);

        try {

            $invoice = $hotelInvoicer->createXeroInvoice($booking);

            $booking->status = 'Invoiced';
            $booking->xero_ref = $invoice['InvoiceNumber'];
            $booking->xero_inv_id = $invoice['InvoiceID'];
            $booking->completed = true;
            $booking->save();

        } catch (\Exception $e) {
            \Log::info($e);

            return back()->with('alert_message', 'We met an error trying to invoice this booking.');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function markHotelAsPaid($id)
    {
        $booking = HotelBooking::find($id);

        $booking->hotel_paid = Carbon::now();
        $booking->save();

        return;
    }
}
