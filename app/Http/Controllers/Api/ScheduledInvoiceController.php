<?php

namespace App\Http\Controllers\Api;

use App\ScheduledInvoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScheduledInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $scheduledInvoice = ScheduledInvoice::findOrFail($id);

        if ($scheduledInvoice->tenancy->isDirect()) {

            return [
                'scheduledInvoice' => $scheduledInvoice,
                'rentPayable' => $scheduledInvoice->rentPayable,
                'amountDue' => $scheduledInvoice->rentPayable->paymentDue(),
                'period' => $scheduledInvoice->start_date->format('jS \\of F y') .' to '. $scheduledInvoice->end_date->format('jS \\of F y')
            ];
        }

        return [
            'scheduledInvoice' => $scheduledInvoice,
            'period' => $scheduledInvoice->start_date->format('jS \\of F y') .' to '. $scheduledInvoice->end_date->format('jS \\of F y')
        ];
    }

    public function generateStatement($id)
    {
        $scheduledInvoice = ScheduledInvoice::findOrFail($id);

        $rentPayable =  $scheduledInvoice->rentPayable;

        $amountDue = convertPenceToPounds($scheduledInvoice->rentPayable->paymentDue());

        $fees = $scheduledInvoice->fees->where('fee_type_id', 1);

        foreach ($fees as $fee) {
            $fee->formattedAmount = $fee->displayTotal();  
        }

        $commissions = $scheduledInvoice->fees->where('fee_type_id', 3);

        foreach ($commissions as $commission) {
            $commission->formattedAmount = convertPenceToPounds($commission->amount + $commission->vat);
        }

        $expenses = $scheduledInvoice->expenses;

        foreach ($expenses as $expense) {
            $expense->type = $expense->expenseType->type;
            $expense->formattedAmount = $expense->displayAmount();
        }

        $period = $scheduledInvoice->start_date->format('jS \\of F y') .' to '. $scheduledInvoice->end_date->format('jS \\of F y');

        $data = [
            'scheduledInvoice' => $scheduledInvoice,
            'rentPayable' => $rentPayable,
            'amountDue' => $amountDue,
            'commissions' => $commissions,
            'expenses' => $expenses,
            'period' => $period,
            'fees' => $fees,
            'rentAmount' =>convertPenceToPounds($scheduledInvoice->amount)
        ];

        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
