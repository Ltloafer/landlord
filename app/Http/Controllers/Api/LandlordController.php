<?php

namespace App\Http\Controllers\Api;

use App\Note;
use App\Landlord;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LandlordController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $landlords = Landlord::all();

        return $landlords;
    }

       public function getRecentNotes($id)
    {
        $notes = Note::where('landlord_id', $id)
        ->orderBy('created_at')
        ->take(3)
        ->get();

        return $notes;
    }

    public function getOlderNotes($id)
    {
        $notes = Note::where('landlord_id', $id)
        ->where('id', '>', '3')
        ->orderBy('created_at')
        ->get();

        return $notes;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $landlord = new Landlord();
        $landlord->name = $request->input('name');
        $landlord->phone = $request->input('phone');
        $landlord->email = $request->input('email');
        $landlord->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
