<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Installment extends Model
{
	protected $fillable = ['payment_date', 'amount'];

	protected $dates = ['payment_date'];

    public function tenancy()
    {
    	return $this->belongsTo(Tenancy::class);
    }

    public function getAmount()
    {
    	return convertPenceToPounds($this->amount);
    }
}
