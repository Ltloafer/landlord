<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disbursement extends Model
{
	protected $fillable = ['type', 'amount', 'disbursement_type_id', 'date'];

	protected $dates = ['date'];

	public function disbursementType()
	{
		return $this->belongsTo(DisbursementType::class);
	}

	public function isCredit()
	{
		if ($this->disbursementType->entry == 'credit') {
			return true;
		}
		return false;
	}

	public function displayAmount()
	{
		return convertPenceToPounds($this->amount);
	}
    
}
