<?php

namespace App;

use App\SearchArea;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
	public function landlords()
	{
		return $this->hasMany(Landlord::class);
	}

	public function notes()
	{
		return $this->hasMany(Note::Class);
	}

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    /**
     * The search areas attached to an agent.
     */
    public function searchAreas()
    {
    	return $this->belongsToMany(SearchArea::class, 'agent_search_area');
    }

    public function isRegisteredToArea($area)
    {
    	$registeredIds = $this->searchAreas->pluck('id');

    	if ($registeredIds->contains($area->id)) {
    		return true;
    	}
    	return false;
    }

}
