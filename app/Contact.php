<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public function notes()
    {
    	return $this->hasMany(Note::class);
    }

    public function displayAddress()
    {
    	$address = $this->line_1. ', '. $this->line_2. ', '. $this->line_3;

    	return $address;
    }

    
}
