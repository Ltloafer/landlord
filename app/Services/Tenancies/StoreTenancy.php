<?php 

namespace App\Services\Tenancies;

use App\Search;
use App\Tenant;
use App\Tenancy;
use App\Property;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\CreatesXeroContacts;
use App\Traits\AddFeesToTenancyTrait;
use App\Traits\ChecksForInstallmentsTrait;

class StoreTenancy
{
	use CreatesXeroContacts, AddFeesToTenancyTrait, ChecksForInstallmentsTrait;


	public function storeTenancy(Request $request)
	{
		$tenancy = new Tenancy();

		$tenancy->type = $request->input('tenancy_type');

		if ($request->input('let_type')) {
			$tenancy->let_type = $request->input('let_type');
		}

		if ($search = Search::find($request->input('searchId'))) {

			$tenancy = $this->addSearchDetails($tenancy, $search, $request);
		}

		$tenancy->property_id = $search->property_id;

		// $tenancy->start_date = str_replace("/", "-", $request->input('startdate'));
		$tenancy->start_date = $search->start_date;
		// $tenancy->finish_date = str_replace("/", "-", $request->input('enddate'));
		$tenancy->finish_date = $search->end_date;

		$tenancy->total_rent = $request->input('total_rent') * 100;
		// $tenancy->comm_rate = $request->input('comm_rate') * 100;
		$tenancy->comm_rate = $search->comm_rate;
		
		$tenancy->break_clause = $search->break_clause;

		$tenancy->holding_deposit = ($request->input('holding_deposit') *100);

		$tenancy->security = ($request->input('security_deposit') *100);

		$tenancy->notice_period = $request->input('notice_period');
		$tenancy->monthly_rate = ($request->input('monthly_rate') * 100);

		$tenancy->production_company_id = $search->production_company_id;

		if ($tenancy->production_company_id) {
			$tenancy->invoice_production_company = true;
		}

		$tenancy->charge_landlord_admin_fee = $request->input('landlord_admin_fee') ? true : false;
		$tenancy->charge_tenant_admin_fee = $request->input('tenant_admin_fee') ? true : false;

		$tenancy->comm_charging = $request->input('commission_charging');
		
		$tenancy->save();


		if ($tenancy->isDirect()) {

			$this->addFeesForDirectTenancy($tenancy);
		}

		$this->checkInstallments($tenancy, $request);

		$this->addCommissionFees($tenancy);

		// dd($tenancy);

		return $tenancy;
	}

	protected function addSearchDetails($tenancy, $search, $request)
	{
		// $tenancy->tenant_id = $search->tenant_id ?? $this->createNewTenant($request);
		$tenancy->tenant_id = $search->tenant_id;

		$tenancy->search_id = $search->id;

		if ($search->productionCompany) {

			$tenancy->production_company_id = $search->production_company_id;
		}

		return $tenancy;
	}

	private function createNewTenant($request)
	{
		$tenant = new Tenant();
		$tenant->name = $request->input('tenant_name');
		$tenant->phone = $request->input('tenant_phone');
		$tenant->address = $request->input('tenant_address');
		$tenant->email = $request->input('tenant_email');
		$tenant->save();

		$this->createNewXeroContact($tenant);

		return $tenant->id;
	}
}
