<?php 

namespace App\Services\Tenancies;

use App\Tenant;
use App\Search;
use App\Property;
use Illuminate\Http\Request;
use App\Traits\ChecksResourcesAreLinkedToXeroContacts;

class ChecksForXeroContacts {

    use ChecksResourcesAreLinkedToXeroContacts;


	public function checkResourcesAreLinkedToXeroContacts(Search $search)
	{
		// $tenant = Tenant::findOrFail($request->input('tenant'));

  //       $this->checksResourceIsLinkedToXeroContact($tenant);

        
        $property = Property::findOrFail($search->property_id);

        if ($property->landlord) {

            if ($property->landlord->xero_contactID) {

                $this->checksResourceIsLinkedToXeroContact($property->landlord);

            } else {

                return back()->with('alert_message', 'The landlord did not link to a contact on Xero. Please link them before re-trying. Thanks');

            }

        } elseif ($property->agent) {

            if ($property->agent->xero_contactID) {

                $this->checksResourceIsLinkedToXeroContact($property->agent);

            } else {

                return back()->with('alert_message', 'The agent did not link to a contact on Xero. Please link them before re-trying. Thanks');

            }

        } else {

            return back()->with('alert_message', 'The property is not linked to an agent or landlord. Please check this before re-trying. Thanks');
        }
	}
} 