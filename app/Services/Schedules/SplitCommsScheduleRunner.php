<?php

namespace App\Services\Schedules;

use App\Fee;
use App\User;
use App\Invoice;
use Notification;
use XeroLaravel;
use Carbon\Carbon;
use App\ScheduledInvoice;
use App\Services\Actions\CreatesAction;
use App\Notifications\CommissionInvoiceCreated;
use App\Services\Schedules\CreatesXeroAgentFeeInvoice as CreatesFeeInvoice;

class SplitCommsScheduleRunner 
{
	protected $createsXeroInvoice;
	protected $createsAction;

	public function __construct(CreatesFeeInvoice $createsFeeInvoice, CreatesAction $createsAction)
	{
		$this->createsFeeInvoice = $createsFeeInvoice;
		$this->createsAction = $createsAction;
	}

	public function runScheduler($period)
	{
		if ($period->scheduled_date->isToday()) {

			try {

				$this->runPeriod($period);

			} catch (\Exception $e) {

				\Log::info($e);

				return back()->with('alert_message', 'We met an error trying to run the scheduled period '. $period->id);
			}
		} 
	}

	public function runSchedulerFromConsole($period)
	{
		if ($period->scheduled_date->isToday()) {

			try {

				$this->runPeriod($period);

			} catch (\Exception $e) {

				\Log::info($e);
			}
		} 
	}

	public function runPeriod($period)
	{
		if ($period->scheduledCommissionFee()) {

			$this->createCommissionInvoice($period, $period->scheduledCommissionFee());

			$this->createsAction->createCommissionFeeAction($period->commissionFee());

			Notification::send(User::where('role', 'admin')->get(), new CommissionInvoiceCreated($period));
		}

		$period->status = 'RUN';
		$period->save();
	}

	/**
     * Create a commission invoice for the landlord
     * @param XeroInvoice $invoice
     * @param ScheduledTenantFee $s_inv
     * @return $invoice
     */
	protected function createCommissionInvoice(ScheduledInvoice $period, Fee $fee)
	{
		$xero_invoice = $this->createsFeeInvoice->createXeroFeeInvoice($period, $fee);

		$fee->updateWithInvoiceDetails($xero_invoice);

		$invoice = new Invoice;
		$invoice->date = Carbon::now();
		$invoice->reference = $xero_invoice['Reference'];
		$invoice->type = 'ACCREC';
		$invoice->tenancy_id = $period->tenancy_id;
		$invoice->contact_id = $period->tenancy->property->agent->contact_id;
		$invoice->status = 'invoiced';
		$invoice->xero_ref =  $xero_invoice['InvoiceNumber'];
		$invoice->xero_inv_id = $xero_invoice['InvoiceID'];
		$invoice->amount = $xero_invoice['SubTotal'] * 100;
		$invoice->vat = $xero_invoice['TotalTax'] * 100;
		$invoice->total_amount = $xero_invoice['Total'] * 100;
		$invoice->due_date = $xero_invoice['DueDate'];
		$invoice->last_sync = Carbon::now();
		$invoice->save(); 

		$fee->invoice_id = $invoice->id;
		$fee->save();
	}
}