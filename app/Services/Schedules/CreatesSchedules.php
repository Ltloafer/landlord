<?php

namespace App\Services\Schedules;

use App\Tenancy;
use App\RentPayable;
use App\ScheduledInvoice;

class CreatesSchedules
{
    private $tenancy;

    public function createSchedule(Tenancy $tenancy)
    {
        $this->tenancy = $tenancy;

        $tenancyFinishDate = $tenancy->finish_date;
        $inv_date = $tenancy->start_date;
        $startOfMonthDate = $tenancy->start_date;

        do {

            $period = new ScheduledInvoice();
            $period->tenancy_id = $tenancy->id;
            $period->amount = $tenancy->monthly_rate;
            $inv_date = $startOfMonthDate->copy()->subDays(7);
            $period->scheduled_date = $inv_date;
            $period->start_date = $startOfMonthDate;
            $endOfMonthDate = $startOfMonthDate->copy()->addMonth()->subDays(1);
            $period->end_date = $endOfMonthDate;

            $period->save();

            if ($this->tenancy->isDirect()) {

                $this->createRentPayable($period);
                $this->createCommissionFee($period);
                $this->checkForPendingTenantAdminFee($period);
                $this->checkForPendingTenantAdminRechargeFee($period);
                $this->checkForPendingSecurityDeposit($period);

            } elseif ($this->tenancy->isViaAgent()) {

                $this->createCommissionFee($period);
            }

            $startOfMonthDate->addMonth();

        } while ($this->notExceededEndOfTenacy($endOfMonthDate));

        $this->setFinishedAttributesForTheLastPeriod($period);
        // $commFee = $period->fees->where('fee_type_id', 3)->last();
    }

    protected function notExceededEndOfTenacy($endOfMonthDate)
    {
        if ($endOfMonthDate->greaterThanOrEqualTo($this->tenancy->finish_date)) {
            return false;
        }
        // need to check not past the tenancy finsih date to avoid spiralling! 
        return true;
    }

    private function checkForPendingTenantAdminFee($period)
    {
        $adminFee = $this->tenancy->getAdminFeeForTenant();

        if ($adminFee && $adminFee->isPending()) {
            $adminFee->scheduled_invoice_id = $period->id;
            $adminFee->status = 'scheduled';
            $adminFee->save();
        }
    }

    private function checkForPendingTenantAdminRechargeFee($period)
    {
        $adminFee = $this->tenancy->getAdminRechargeFeeForTenant();

        if ($adminFee && $adminFee->isPending()) {
            $adminFee->scheduled_invoice_id = $period->id;
            $adminFee->status = 'scheduled';
            $adminFee->save();
        }
    }


    private function checkForPendingSecurityDeposit($period)
    {
        $deposit = $this->tenancy->securityDeposit();

        if ($deposit && $deposit->isPending()) {
            $deposit->scheduled_invoice_id = $period->id;
            $deposit->status = 'scheduled';
            $deposit->save();
        }
    }

    private function createRentPayable($period)
    {
        $rent = new RentPayable();
        $rent->scheduled_invoice_id = $period->id;
        $rent->status = 'Scheduled';
        $rent->date = $period->start_date;
        $rent->amount = $period->amount;
        $rent->tenancy_id = $period->tenancy_id;
        $rent->save();

        $this->checkForPendingLandlordAdminFee($period);
    }

    private function checkForPendingLandlordAdminFee($period)
    {
        $adminFee = $period->tenancy->getLandlordAdminFee();

        if ($adminFee && $adminFee->isPending()) {
            $adminFee->scheduled_invoice_id = $period->id;
            $adminFee->status = 'Scheduled';
            $adminFee->save();
        }
    }

    private function createCommissionFee($period)
    {
        $pendingCommission = $period->tenancy->fees
        ->where('status', 'Pending')
        ->where('fee_type_id', 3)
        ->first();

        if ($pendingCommission) {
            $pendingCommission->scheduled_invoice_id = $period->id;
            $pendingCommission->status = 'Scheduled';
            $pendingCommission->save();
        }

        if ($period->tenancy->comm_charging == 'by_period') {
            $period->fees()->create([
                'fee_type_id' => 3,
                'date' => $period->scheduled_date,
                'tenancy_id' => $period->tenancy_id,
                'status' => 'Scheduled',
                'amount' => $period->tenancy->monthlyCommissionAmount(),
                'vat' => (($period->tenancy->monthlyCommissionAmount()/100)*20)
            ]);

        } elseif ($period->tenancy->comm_charging == 'to_break') {
            if ($period->start_date > $period->tenancy->breakOutDate()) {
                $period->fees()->create([
                    'fee_type_id' => 3,
                    'date' => $period->scheduled_date,
                    'tenancy_id' => $period->tenancy_id,
                    'status' => 'Scheduled',
                    'amount' => $period->tenancy->monthlyCommissionAmount(),
                    'vat' => (($period->tenancy->monthlyCommissionAmount()/100)*20)
                ]);
            } 
        }
    }


    public function addScheduledPeriod(Tenancy $tenancy, $request)
    {
        $this->tenancy = $tenancy;

        $period = new ScheduledInvoice();
        $period->tenancy_id = $tenancy->id;
        $period->amount = ($request->amount * 100);
        $period->start_date = $request->start_date;
        $period->end_date = $request->end_date;
        $period->scheduled_date = $request->date;
        $period->add_info = $request->add_info;
        $period->save();

        $this->createRentPayable($period);
        $this->createCommissionFee($period);
        $this->checkForPendingTenantAdminFee($period);
        $this->checkForPendingSecurityDeposit($period);
    }

    private function setFinishedAttributesForTheLastPeriod($period)
    {
        $period->end_date = $this->tenancy->finish_date;
        $period->save();
    }
}
