<?php 

namespace App\Services\Schedules;

use XeroLaravel;
use XeroLaravelDirect;

use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;

class CreatesXeroInvoice
{
	public function createXeroInvoice($s_inv)
	{
		$contact = $this->getContact($s_inv->tenancy);

		$invoice = new XeroInvoice();
		$invoice->setContact($contact);
		$invoice->setType('ACCREC');
		$dateInstance = new \DateTime();
		$invoice->setDate($dateInstance);
		$invoice->setDueDate($dateInstance);
		$invoice->setReference($s_inv->tenancy->property->address);

		$invoice->setUrl('http://yourdomain/fullpathtotheorder');
		$invoice->setCurrencyCode('GBP');
		$invoice->setStatus(config('xero.invoices.rental_invoice_type'));
		$invoice->setBrandingThemeID(config('xero.branding_themes.client'));

		$invoice = $this->addRentalInvoiceLine($invoice, $s_inv);

		XeroLaravelDirect::saveInvoice($invoice); // Needs to go to client account 
		
		return $invoice;
	}

	/**
     * Add the rental element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledInvioice $s_inv
     * @return $invoice
     */
	protected function addRentalInvoiceLine($invoice, $s_inv)
	{
		$monthDateRange = $s_inv->start_date->format('d/m/y'). ' to '. $s_inv->end_date->format('d/m/y');

		$line = resolve('XeroInvoiceLine');
		$line->setDescription('Rental payment due in respect of the above mentioned property '. $monthDateRange. '. '.$s_inv->add_info);

		$line->setQuantity(1);
		$line->setUnitAmount($s_inv->amount/100);
		$line->setTaxAmount(0);
		$line->setTaxType('NONE');
		
		$line->setLineAmount($s_inv->amount/100);

		\Log::info($s_inv->tenancy->property->xero_code);

		$line->setAccountCode($s_inv->tenancy->property->account_code);

		$invoice->addLineItem($line);

		return $invoice;
	}

	protected function getContact($tenancy)
	{
		if ($tenancy->productionCompany) {

			return XeroLaravel::getContact($tenancy->productionCompany->xero_contactID);
		}

		return XeroLaravel::getContact($tenancy->tenant->xero_contactID);
	}

	protected function getLandlordOrAgentContact($tenancy)
	{
		if ($tenancy->property->agent) {

			return XeroLaravel::getContact($tenancy->property->agent->xero_contactID);
		}

		return XeroLaravel::getContact($tenancy->property->landlord->xero_contactID);
	}


	
}