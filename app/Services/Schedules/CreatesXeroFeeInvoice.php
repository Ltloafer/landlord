<?php 

namespace App\Services\Schedules;

use XeroLaravel;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;

class CreatesXeroFeeInvoice
{
	public function createXeroFeeInvoice($rentPayable, $fee)
	{
		$invoice = new XeroInvoice();
		$invoice->setContact($this->getLandlordOrAgentContact($rentPayable->tenancy));
		$invoice->setType('ACCREC');

		$dateInstance = new \DateTime();
		$invoice->setDate($dateInstance);
		$invoice->setDueDate($dateInstance);
		$invoice->setReference($rentPayable->tenancy->property->address);

		$invoice->setUrl('http://yourdomain/fullpathtotheorder');
		$invoice->setCurrencyCode('GBP');
		$invoice->setStatus(config('xero.invoices.fee_invoice_type'));
        $invoice->setBrandingThemeID(config('xero.branding_themes.trading'));

		$invoice = $this->addInvoiceLineForFee($invoice, $rentPayable, $fee);

		XeroLaravel::saveInvoice($invoice);

		return $invoice;
	}


     /**
     * Add the rental element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledInvioice $s_inv
     * @return $invoice
     */
    protected function addInvoiceLineForFee($invoice, $rentPayable, $fee)
    {
    	$monthDateRange = $rentPayable->scheduledInvoice->start_date->format('d/m/y'). ' to '. 
        $rentPayable->scheduledInvoice->end_date->format('d/m/y');

    	$line = resolve('XeroInvoiceLine');
    	$line->setDescription($fee->getFeeDescription(). ' fee due in respect of the above mentioned property '. $monthDateRange);
    	$line->setQuantity(1);
    	$line->setUnitAmount($fee->amount/100);
    	$line->setAccountCode(config('xero.account_codes.fee_invoice_code'));
		$line->setTaxAmount((($fee->amount/100)*20)/100);
    	$invoice->addLineItem($line);

    	return $invoice;
    }

    protected function getContact($tenancy)
    {
        return XeroLaravel::getContact($tenancy->tenant->xero_contactID);
    }

	protected function getLandlordOrAgentContact($tenancy)
    {
        if ($tenancy->agent) {

            $contact = XeroLaravel::getContact($tenancy->property->agent->xero_contactID);
        } else {

            $contact = XeroLaravel::getContact($tenancy->property->landlord->xero_contactID);
        }

        return $contact;
    }
	
}