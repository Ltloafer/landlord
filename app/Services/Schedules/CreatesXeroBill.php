<?php 

namespace App\Services\Schedules;

use XeroLaravel;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;

class CreatesXeroBill 
{
	public function createXeroBill($rentPayable, $fee = null)
	{
		$invoice = new XeroInvoice();

		$invoice->setContact($this->getLandlordOrAgentContact($rentPayable->tenancy));

		$invoice->setType('ACCPAY');

		$dateInstance = new \DateTime();
		$invoice->setDate($dateInstance);
		$invoice->setDueDate($dateInstance);
		$invoice->setReference($rentPayable->tenancy->property->address);
		$invoice->setUrl('http://yourdomain/fullpathtotheorder');
		$invoice->setCurrencyCode('GBP');
        $invoice->setStatus('Draft');
        
		$invoice->setInvoiceNumber($rentPayable->tenancy_id. '-'. $rentPayable->id);
		$invoice = $this->addRentalInvoiceLineForLandlordRent($invoice, $rentPayable);
		
		XeroLaravel::saveInvoice($invoice);

		return $invoice;
	}


    /**
     * Add the rental element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledInvioice $s_inv
     * @return $invoice
     */
    protected function addRentalInvoiceLineForLandlordRent($invoice, $rentPayable)
    {
    	$monthDateRange = $rentPayable->scheduledInvoice->start_date->format('d/m/y'). ' to '. 
        $rentPayable->scheduledInvoice->end_date->format('d/m/y');

    	$line = resolve('XeroInvoiceLine');
    	$line->setDescription('Rental payment due in respect of the above mentioned property '. $monthDateRange);

    	$line->setQuantity(1);
    	$line->setUnitAmount($rentPayable->paymentDue()/100);
    	$line->setAccountCode($rentPayable->scheduledInvoice->tenancy->property->account_code);
        $line->setTaxAmount(0);
        $line->setTaxType('NONE');
    	$line->setLineAmount($rentPayable->paymentDue()/100);

    	$invoice->addLineItem($line);

    	return $invoice;
    }


    protected function getLandlordOrAgentContact($tenancy)
    {
        if ($tenancy->agent) {

            $contact = XeroLaravel::getContact($tenancy->property->agent->xero_contactID);
        } else {

            $contact = XeroLaravel::getContact($tenancy->property->landlord->xero_contactID);
        }

        return $contact;
    }


}