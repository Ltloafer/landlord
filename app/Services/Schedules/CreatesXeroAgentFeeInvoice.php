<?php 

namespace App\Services\Schedules;

use XeroLaravel;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;

class CreatesXeroAgentFeeInvoice
{
    public function createXeroFeeInvoice($period, $fee)
    {
        $invoice = new XeroInvoice();
        $invoice->setContact($this->getAgentContact($period->tenancy));
        $invoice->setType('ACCREC');

        $dateInstance = new \DateTime();
        $invoice->setDate($dateInstance);
        $invoice->setDueDate($dateInstance);
        $invoice->setReference($period->tenancy->property->address);

        $invoice->setUrl('http://yourdomain/fullpathtotheorder');
        $invoice->setCurrencyCode('GBP');
        $invoice->setStatus('Draft');
        $invoice->setBrandingThemeID(config('xero.branding_themes.trading'));

        $invoice = $this->addInvoiceLineForFee($invoice, $period, $fee);

        XeroLaravel::saveInvoice($invoice);

        return $invoice;
    }


    /**
     * Add the rental element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledInvioice $s_inv
     * @return $invoice
     */
    protected function addInvoiceLineForFee($invoice, $period, $fee)
    {
        $monthDateRange = $period->start_date->format('d/m/y'). ' to '. $period->end_date->format('d/m/y');

        $line = resolve('XeroInvoiceLine');
        $line->setDescription('Commission fee due in respect of the above mentioned property '. $monthDateRange);

        $line->setQuantity(1);
        $line->setUnitAmount($fee->amount/100);
        $line->setAccountCode($period->tenancy->property->xero_code);
        $line->setTaxAmount((($fee->amount/100)*20)/100);
        $line->setLineAmount($fee->amount/100);
        $invoice->addLineItem($line);

        return $invoice;
    }

    protected function getAgentContact($tenancy)
    {
        $contact = XeroLaravel::getContact($tenancy->property->agent->xero_contactID);

        return $contact;
    }
}