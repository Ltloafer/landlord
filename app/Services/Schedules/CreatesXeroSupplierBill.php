<?php 

namespace App\Services\Schedules;

use XeroLaravel;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;

class CreatesXeroSupplierBill
{
	public function createXeroBill($expense)
	{
		$invoice = new XeroInvoice();

		$invoice->setContact($this->getSupplierContact($expense));
		$invoice->setType('ACCPAY');

		$dateInstance = new \DateTime();
		$invoice->setDate($dateInstance);
		$invoice->setDueDate($dateInstance);

		$invoice->setReference($expense->tenancy->property->address);
		$invoice->setUrl('http://yourdomain/fullpathtotheorder');
		$invoice->setCurrencyCode('GBP');
        $invoice->setStatus('Draft');
		$invoice->setInvoiceNumber($expense->inv_ref);

		$invoice = $this->addInvoiceLineForExpense($invoice, $expense);
		
		XeroLaravel::saveInvoice($invoice);

		return $invoice;
	}


    /**
     * Add the rental element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledInvioice $s_inv
     * @return $invoice
     */
    protected function addInvoiceLineForExpense($invoice, $expense)
    {
    	$line = resolve('XeroInvoiceLine');
    	$line->setDescription('Expense for the above mentioned property.');

    	$line->setQuantity(1);
    	$line->setUnitAmount($expense->amount/100);
    	$line->setAccountCode($expense->tenancy->property->xero_code);
    	$line->setLineAmount($expense->amount/100);

    	$invoice->addLineItem($line);

    	return $invoice;
    }

    protected function getSupplierContact($expense)
    {
        $contact = XeroLaravel::getContact($expense->supplier->xero_contactID);

        return $contact;
    }
}