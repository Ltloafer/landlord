<?php 

namespace App\Services\Schedules;

use XeroLaravel;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;

class CreatesXeroRechargeFeeInvoice
{
    public function createXeroFeeInvoice($fee) 
    {
        $invoice = new XeroInvoice();
        $invoice->setContact($this->getContact($fee->tenancy));
        $invoice->setType('ACCREC');

        $dateInstance = new \DateTime();
        $invoice->setDate($dateInstance);
        $invoice->setDueDate($dateInstance);
        $invoice->setReference($fee->tenancy->property->address);

        $invoice->setUrl('http://yourdomain/fullpathtotheorder');
        $invoice->setCurrencyCode('GBP');
        $invoice->setStatus('Authorised');
        $invoice->setBrandingThemeID(config('xero.branding_themes.trading'));

        $invoice = $this->addInvoiceLineForFee($invoice, $fee);

        XeroLaravel::saveInvoice($invoice);

        $fee->updateWithInvoiceDetails($invoice);

        return $invoice;
    }

    /**
     * Add the rental element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledInvioice $s_inv
     * @return $invoice
     */
    protected function addInvoiceLineForFee($invoice, $fee)
    {
        $feeType = $fee->getFeeDescription(); 

        $line = resolve('XeroInvoiceLine');
        $line->setDescription($feeType. ' fee charge.');
        $line->setQuantity(1);
        $line->setUnitAmount($fee->amount/100);
        $line->setAccountCode('A05');
        $line->setTaxAmount((($fee->amount/100)*20)/100);

        $invoice->addLineItem($line);

        return $invoice;
    }

    protected function getContact($tenancy)
    {
        $contact = XeroLaravel::getContact($tenancy->tenant->xero_contactID);

        return $contact;
    }
}