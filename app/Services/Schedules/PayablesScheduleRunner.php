<?php

namespace App\Services\Schedules;

use App\Fee;
use App\User;
use Notification;
use App\Invoice;
use App\RentPayable;
use Carbon\Carbon;
use App\Services\Actions\CreatesAction;
use App\Notifications\LandlordPaymentDue;
use App\Services\Schedules\CreatesXeroBill;
use App\Services\Schedules\CreatesXeroInvoice;
use App\Services\Schedules\CreatesXeroFeeInvoice;
use App\Notifications\LandlordAdminFeeInvoiceGenerated;
use App\Notifications\LandlordCommissionInvoiceGenerated;

class PayablesScheduleRunner 
{
	protected $createsXeroBill;
	protected $createsXeroFeeInvoice;
	protected $createsAction;

	public function __construct(CreatesXeroBill $createsXeroBill, CreatesXeroFeeInvoice $createsXeroFeeInvoice, CreatesAction $createsAction)
	{
		$this->createsXeroBill = $createsXeroBill;
		$this->createsXeroFeeInvoice = $createsXeroFeeInvoice;
		$this->createsAction = $createsAction;
	}

	public function runScheduledPayables($rentPayable)
	{
		if ($rentPayable->date->isToday()) {

			try {

				$this->runPeriod($rentPayable);

			} catch (\Exception $e) {
				\Log::info($e);
				return back()->with('alert_message', 'We met an error trying to run the scheduled period '. $rentPayable->id);
			}
		} 
	}

	public function runScheduledPayablesFromConsole($rentPayable)
	{
		if ($rentPayable->date->isToday()) {

			try {

				$this->runPeriod($rentPayable);

			} catch (\Exception $e) {
				\Log::info($e);
			}
		}
	}

	private function runPeriod($rentPayable)
	{
		$xero_invoice = $this->createsXeroBill->createXeroBill($rentPayable);

		$rentPayable->updateWithInvoiceDetails($xero_invoice);

		$this->invoiceScheduledAdminFee($rentPayable);

		$this->invoiceScheduledCommissionFee($rentPayable);

		$invoice = new Invoice;
		$invoice->date = Carbon::now();
		$invoice->reference = $rentPayable->tenancy->property->address;
		$invoice->type = 'ACCPAY';
		$invoice->tenancy_id = $rentPayable->tenancy_id;
		$invoice->contact_id = $rentPayable->tenancy->property->landlord->contact_id;
		$invoice->status = 'invoiced';
		$invoice->xero_ref =  $xero_invoice['InvoiceNumber'];
		$invoice->xero_inv_id = $xero_invoice['InvoiceID'];
		$invoice->amount = $xero_invoice['SubTotal'] * 100;
		$invoice->vat = $xero_invoice['TotalTax'] * 100;
		$invoice->total_amount = $xero_invoice['Total'] * 100;
		$invoice->due_date = $xero_invoice['DueDate'];
		$invoice->last_sync = Carbon::now();
		$invoice->xero_status = $xero_invoice['Status'];
		$invoice->save(); 

		$rentPayable->invoice_id = $invoice->id;
		$rentPayable->amount = $invoice->amount;
		$rentPayable->save();

		$this->createsAction->createLandlordPaymentAction($rentPayable);

		Notification::send($this->getAdminUsers(), new LandlordPaymentDue($rentPayable));
	}

	protected function invoiceScheduledAdminFee($rentPayable)
	{
		if ($rentPayable->scheduledAdminFee()) {
			$this->createAdminFeeInvoice($rentPayable, $rentPayable->scheduledAdminFee());

			$this->createsAction->createLandlordAdminFeeAction($rentPayable->landlordAdminFee());
		}
	}

	protected function invoiceScheduledCommissionFee($rentPayable)
	{
		if ($rentPayable->scheduledCommissionFee()) {
			$this->createCommissionInvoice($rentPayable, $rentPayable->scheduledCommissionFee());

			$this->createsAction->createCommissionFeeAction($rentPayable->commissionFee());
		}
	}

	/**
     * Create a commission invoice for the landlord
     * @param XeroInvoice $invoice
     * @param ScheduledTenantFee $s_inv
     * @return $invoice
     */
	protected function createAdminFeeInvoice(RentPayable $rentPayable, Fee $fee)
	{
		$xero_invoice = $this->createsXeroFeeInvoice->createXeroFeeInvoice($rentPayable, $fee);

		$fee->updateWithInvoiceDetails($xero_invoice);

		$invoice = new Invoice;
		$invoice->date = Carbon::now();
		$invoice->reference = $rentPayable->tenancy->property->address;
		$invoice->type = 'ACCREC';
		$invoice->tenancy_id = $rentPayable->tenancy_id;
		$invoice->contact_id = $rentPayable->tenancy->property->landlord->contact_id;
		$invoice->status = 'invoiced';
		$invoice->xero_ref =  $xero_invoice['InvoiceNumber'];
		$invoice->xero_inv_id = $xero_invoice['InvoiceID'];
		$invoice->amount = $xero_invoice['SubTotal'] * 100;
		$invoice->vat = $xero_invoice['TotalTax'] * 100;
		$invoice->total_amount = $xero_invoice['Total'] * 100;
		$invoice->due_date = $xero_invoice['DueDate'];
		$invoice->last_sync = Carbon::now();
		$invoice->save();

		$fee->invoice_id = $invoice->id;
		$fee->save();

		
		Notification::send($this->getAdminUsers(), new LandlordAdminFeeInvoiceGenerated($fee));
	}

	/**
     * Create a commission invoice for the landlord
     * @param XeroInvoice $invoice
     * @param ScheduledTenantFee $s_inv
     * @return $invoice
     */
	protected function createCommissionInvoice(RentPayable $rentPayable, Fee $fee)
	{
		$xero_invoice = $this->createsXeroFeeInvoice->createXeroFeeInvoice($rentPayable, $fee);

		$fee->updateWithInvoiceDetails($xero_invoice); 

		$invoice = new Invoice;
		$invoice->date = Carbon::now();
		$invoice->reference = $rentPayable->tenancy->property->address;
		$invoice->type = 'ACCREC';
		$invoice->tenancy_id = $rentPayable->tenancy_id;
		$invoice->contact_id = $rentPayable->tenancy->property->landlord->contact_id;
		$invoice->status = 'invoiced';
		$invoice->xero_ref =  $xero_invoice['InvoiceNumber'];
		$invoice->xero_inv_id = $xero_invoice['InvoiceID'];
		$invoice->amount = $xero_invoice['SubTotal'] * 100;
		$invoice->vat = $xero_invoice['TotalTax'] * 100;
		$invoice->total_amount = $xero_invoice['Total'] * 100;
		$invoice->due_date = $xero_invoice['DueDate'];
		$invoice->last_sync = Carbon::now();
		$invoice->save(); 

		$fee->invoice_id = $invoice->id;
		$fee->save();


		Notification::send($this->getAdminUsers(), new LandlordCommissionInvoiceGenerated($fee));
	}

	private function getAdminUsers()
	{
		return User::where('role', 'admin')->get();
	}
	
}