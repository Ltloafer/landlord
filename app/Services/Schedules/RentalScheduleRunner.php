<?php

namespace App\Services\Schedules;

use App\User;
use App\Invoice;
use Notification;
use XeroLaravel;
use Carbon\Carbon;
use App\Services\Actions\CreatesAction;
use App\Notifications\RentalInvoiceCreated;
use App\Services\Schedules\CreatesXeroInvoice;
use App\Services\Schedules\CreatesXeroRechargeFeeInvoice;

class RentalScheduleRunner 
{
	protected $createsXeroInvoice;
	protected $createsRecharge;
	protected $createsAction;
	protected $invoice;
	protected $local_invoice;

	public function __construct(CreatesXeroInvoice $createsXeroInvoice, CreatesXeroRechargeFeeInvoice $createsRecharge, CreatesAction $createsAction)
	{
		$this->createsXeroInvoice = $createsXeroInvoice;
		$this->createsRecharge = $createsRecharge;
		$this->createsAction = $createsAction;
	}

	public function runScheduler($period)
	{
		if ($period->scheduled_date->isToday()) {

			try {

				$this->runPeriod($period);

			} catch (\Exception $e) {
				\Log::info($e);

				return back()->with('alert_message', 'We met an error trying to run the scheduled period '. $period->id);
			}
		} 
	}

	public function runSchedulerFromConsole($period)
	{
		if ($period->scheduled_date->isToday()) {

			try {

				$this->runPeriod($period);

			} catch (\Exception $e) {
				\Log::info($e);
			}
		} 
	}


	public function runPeriod($period)
	{
		$this->invoice = $this->createsXeroInvoice->createXeroInvoice($period);

		$this->local_invoice = new Invoice;

		$this->local_invoice->date = Carbon::now();
		$this->local_invoice->save();

		$this->addScheduledTenantFeeToInvoice($period);

		$this->addAdminRechargeToInvoice($period);

		$this->addSecurityDepositToInvoice($period);

		$period->updateWithInvoiceData($this->invoice);

		
		$this->local_invoice->reference = $this->invoice['Reference'];
		$this->local_invoice->type = 'ACCREC';
		$this->local_invoice->tenancy_id = $period->tenancy_id;
		// 
		if ($period->tenancy->invoiceProductionCompany()) {
			$this->local_invoice->contact_id = $period->tenancy->productionCompany->contact_id;
		} else {
			$this->local_invoice->contact_id = $period->tenancy->tenant->contact_id;
		}

		$this->local_invoice->status = 'invoiced';
		$this->local_invoice->xero_ref =  $this->invoice['InvoiceNumber'];
		$this->local_invoice->xero_inv_id = $this->invoice['InvoiceID'];
		$this->local_invoice->amount = $this->invoice['SubTotal'] * 100;
		$this->local_invoice->vat = $this->invoice['TotalTax'] * 100;
		$this->local_invoice->total_amount = $this->invoice['Total'] * 100;
		$this->local_invoice->due_date = $this->invoice['DueDate'];
		$this->local_invoice->last_sync = Carbon::now();
		$this->local_invoice->xero_status = $this->invoice['Status'];
		$this->local_invoice->save(); 

		$period->invoice_id = $this->local_invoice->id;
		$period->save();

		$this->createRentalPaymentAction($period);

		$this->createAdminRechargeAction($period);

		$this->sendNotificationsToAdminUsers($period);
	}


	private function addScheduledTenantFeeToInvoice($period)
	{
		if ($scheduledTenantFee = $period->scheduledTenantFee()) {
			$this->addAdminFee($scheduledTenantFee);
		}
	}

	private function addAdminRechargeToInvoice($period)
	{
		if ($scheduledTenantRechargeFee = $period->scheduledTenantRechargeFee()) {
			$this->invoiceAdminRecharge($scheduledTenantRechargeFee);
		}
	}

	private function addSecurityDepositToInvoice($period)
	{
		if ($scheduledSecurityDeposit = $period->scheduledDeposit()) {
			$this->addSecurityDeposit($scheduledSecurityDeposit, $period);
		}
	}

	private function createRentalPaymentAction($period)
	{
		$this->createsAction->createRentalPaymentAction($period);
	}

	private function createAdminRechargeAction($period)
	{
		if ($scheduledTenantRechargeFee = $period->scheduledTenantRechargeFee()) {
			$this->createsAction->createAdminRechargeAction($scheduledTenantRechargeFee);
		}
	}

	private function sendNotificationsToAdminUsers($period)
	{
		$users = User::where('role', 'admin')->get();

		Notification::send($users, new RentalInvoiceCreated($period));
	}

	/**
     * Add the tenant admin fee element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledTenantFee $s_inv
     * @return $invoice
     */
	protected function addAdminFee($scheduledTenantFee)
	{
		$line = resolve('XeroInvoiceLine');
		$line->setDescription('Admin fee');

		$line->setQuantity(1);
		$line->setUnitAmount($scheduledTenantFee->amount/100);
		$line->setTaxAmount((($scheduledTenantFee->amount/100)*20)/100);
		$line->setAccountCode(config('xero.account_codes.fee_invoice_code'));

		$this->invoice->addLineItem($line);

		XeroLaravel::saveInvoice($this->invoice);

		$this->updateModel($scheduledTenantFee);
	}

	/**
     * Add the security deposit element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledDeposit $scheduledDeposit
     * @return $invoice
     */
	protected function addSecurityDeposit($scheduledDeposit, $period)
	{
		$line = resolve('XeroInvoiceLine');
		$line->setDescription('Security deposit');

		$line->setQuantity(1);
		$line->setUnitAmount($scheduledDeposit->amount/100);
		$line->setTaxAmount(0);
		$line->setTaxType('NONE');
		$line->setLineAmount($scheduledDeposit->amount/100);
		$line->setAccountCode($period->tenancy->property->account_code);

		$this->invoice->addLineItem($line);

		XeroLaravel::saveInvoice($this->invoice);

		$this->updateModel($scheduledDeposit);
	}

	protected function updateModel($model)
	{
		$model->status = 'Invoiced';
		$model->xero_ref = $this->invoice['InvoiceNumber'];
		$model->xero_inv_id = $this->invoice['InvoiceID'];
		$model->invoice_id = $this->local_invoice->id;
		$model->save();
	}

	private function invoiceAdminRecharge($fee)
	{
		$xero_invoice = $this->createsRecharge->createXeroFeeInvoice($fee);

		$invoice = new Invoice;
		$invoice->date = Carbon::now();
		$invoice->reference = $xero_invoice['Reference'];
		$invoice->type = 'ACCREC';
		$invoice->tenancy_id = $fee->tenancy_id;
		$invoice->contact_id = $fee->tenancy->property->landlord->contact_id;
		$invoice->status = 'invoiced';
		$invoice->xero_ref =  $xero_invoice['InvoiceNumber'];
		$invoice->xero_inv_id = $xero_invoice['InvoiceID'];
		$invoice->amount = $xero_invoice['SubTotal'] * 100;
		$invoice->vat = $xero_invoice['TotalTax'] * 100;
		$invoice->total_amount = $xero_invoice['Total'] * 100;
		$invoice->due_date = $xero_invoice['DueDate'];
		$invoice->last_sync = Carbon::now();
		$invoice->save(); 

		$fee->invoice_id = $invoice->id;
		$fee->save();
	}

	
}