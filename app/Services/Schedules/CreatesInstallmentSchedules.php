<?php

namespace App\Services\Schedules;

use App\Tenancy;
use App\RentPayable;
use App\ScheduledInvoice;
use Carbon\Carbon;

class CreatesInstallmentSchedules
{
    private $tenancy;

    public function createSchedule(Tenancy $tenancy)
    {
        $this->tenancy = $tenancy;

        $tenancyFinishDate = $tenancy->finish_date;
        $inv_date = $tenancy->start_date;
        $startOfMonthDate = $tenancy->start_date;

        $endOfPeriodDate = $tenancyFinishDate;

        $installments = $tenancy->installments;

        $totalInstallmentsCount = $installments->count();

        $installmentCounter = 0;

        foreach ($installments as $installment)
        {
            $installmentCounter++;

            $period = new ScheduledInvoice();
            $period->tenancy_id = $tenancy->id;
            $period->amount = $installment->amount;
            
            if ($installmentCounter == 1) {
                $inv_date = Carbon::today();
            } else {
                // $inv_date = $installment->payment_date->copy()->subDays(7);
                $inv_date = $installment->payment_date;
            }

            $period->scheduled_date = $inv_date;
            $period->start_date = $installment->payment_date;

            if ($installments->get($installmentCounter)) {

                $period->end_date = $installments->get($installmentCounter)->payment_date->copy()->subDays(1);
                
            } else {

                $period->end_date = $tenancyFinishDate;
            }

            $period->save();

            if ($this->tenancy->isDirect()) {

                $this->createRentPayable($period);
                $this->createCommissionFee($period);
                $this->checkForPendingTenantAdminFee($period);
                $this->checkForPendingTenantAdminRechargeFee($period);
                $this->checkForPendingSecurityDeposit($period);

            } elseif ($this->tenancy->isViaAgent()) {

               $this->createCommissionFee($period);
           }
       } 
   }

// protected function notExceededEndOfTenacy($endOfMonthDate)
//  {
//     if ($endOfMonthDate->greaterThanOrEqualTo($this->tenancy->finish_date)) {
//         return false;
//     }
//     return true;
// }

   private function checkForPendingTenantAdminFee($period)
   {
    $adminFee = $this->tenancy->getAdminFeeForTenant();

    if ($adminFee && $adminFee->isPending()) {
        $adminFee->scheduled_invoice_id = $period->id;
        $adminFee->status = 'scheduled';
        $adminFee->save();
    }
}

private function checkForPendingSecurityDeposit($period)
{
    $deposit = $this->tenancy->securityDeposit();

    if ($deposit->isPending()) {
        $deposit->scheduled_invoice_id = $period->id;
        $deposit->status = 'scheduled';
        $deposit->save();
    }
}

private function createRentPayable($period)
{
    $rent = new RentPayable();
    $rent->scheduled_invoice_id = $period->id;
    $rent->status = 'Scheduled';
    $rent->date = $period->start_date;
    $rent->amount = $period->amount;
    $rent->tenancy_id = $period->tenancy_id;
    $rent->save();

    $this->checkForPendingLandlordAdminFee($period);
}

private function checkForPendingLandlordAdminFee($period)
{
    $adminFee = $period->tenancy->getLandlordAdminFee();

    if ($adminFee && $adminFee->isPending()) {
        $adminFee->scheduled_invoice_id = $period->id;
        $adminFee->status = 'Scheduled';
        $adminFee->save();
    }
}

private function checkForPendingTenantAdminRechargeFee($period)
{
    $adminFee = $this->tenancy->getAdminRechargeFeeForTenant();

    if ($adminFee && $adminFee->isPending()) {
        $adminFee->scheduled_invoice_id = $period->id;
        $adminFee->status = 'scheduled';
        $adminFee->save();
    }
}


private function createCommissionFee($period)
{
    $pendingCommission = $period->tenancy->fees
    ->where('status', 'Pending')
    ->where('fee_type_id', 3)
    ->first();

    if ($pendingCommission) {
        $pendingCommission->scheduled_invoice_id = $period->id;
        $pendingCommission->status = 'Scheduled';
        $pendingCommission->save();
    }

    if ($period->tenancy->comm_charging == 'to_break')
    {
        if ($period->start_date > $period->tenancy->breakOutDate()) {
            $period->fees()->create([
                'fee_type_id' => 3,
                'date' => $period->scheduled_date,
                'tenancy_id' => $period->tenancy_id,
                'status' => 'Scheduled',
                // 'amount' => $period->tenancy->monthlyCommissionAmount()
                'amount' => $period->commissionAmount()
            ]);
        } 
    }
}

public function addScheduledPeriod(Tenancy $tenancy, $request)
{
    $this->tenancy = $tenancy;

    $period = new ScheduledInvoice();
    $period->tenancy_id = $tenancy->id;
    $period->amount = ($request->amount * 100);
    $period->start_date = $request->start_date;
    $period->end_date = $request->end_date;
    $period->scheduled_date = $request->date;
    $period->add_info = $request->add_info;
    $period->save();

    $this->createRentPayable($period);
    $this->createCommissionFee($period);
    $this->checkForPendingTenantAdminFee($period);
    $this->checkForPendingSecurityDeposit($period);
}
}
