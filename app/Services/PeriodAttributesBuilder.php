<?php
namespace App\Services;

use XeroLaravel;
use App\Tenancy;

class PeriodAttributesBuilder 
{
    public function buildPeriodAttributes(Tenancy $tenancy)
    {
        $schedules = $tenancy->scheduledInvoices;

        if ($tenancy->isDirect()) {

            foreach ($schedules as $schedule) {

                $schedule->period = $schedule->start_date->format('jS \\of F y').' - '.$schedule->end_date->format('jS \\of F y');
                $schedule->newdate = $schedule->rentPayable->date->format('jS \\of F y');
                $schedule->rentPayableAmount = $schedule->displayAmount();
                $schedule->commission = $schedule->displayCommission();
                $schedule->landlordAdminFees = $schedule->landlordAdminFees();
                $schedule->disbursements = $schedule->displayTotalDisbursements();
                $schedule->paymentDue = $schedule->rentPayable->displayPaymentDue();

                if ($schedule->rentPayable->xero_inv_id) {

                    $schedule = $this->addXeroInvoiceData($schedule);

                } else {
                    $schedule->rentPayableStatus = 'PENDING';
                }

                $schedule->scheduledDate = $schedule->scheduled_date->format('jS \\of F y');
                $schedule->rentalAmount = $schedule->getAmount();
                $schedule->fees = $schedule->tenantFees();

                if ($schedule->xero_inv_id) {

                    $schedule = $this->addXeroRentalInvoiceData($schedule);

                } else {

                    $schedule->rentalInvoiceStatus = 'PENDING';
                }
            }

        } else {

            foreach ($schedules as $schedule) {

                $schedule->period = $schedule->start_date->format('jS \\of F y').' - '.$schedule->end_date->format('jS \\of F y');
                $schedule->commission = $schedule->displayCommission();
                $schedule->scheduledDate = $schedule->scheduled_date->format('jS \\of F y');
                $schedule->rentalAmount = $schedule->displayAmount();
                $schedule->fees = $schedule->tenantFees();
            }
        }

        return $schedules;
    }


    public function addXeroInvoiceData($schedule)
    {
        $schedule->rentPayableStatus = $schedule->rentPayable->invoice->status;
        $schedule->landlordXeroInvRef = $schedule->rentPayable->xero_ref;

        return $schedule;
    }

    public function addXeroRentalInvoiceData($schedule)
    {
        $schedule->rentalInvoiceStatus = optional($schedule->invoice)->status;
        $schedule->xeroInvRef = $schedule->xero_ref;

        return $schedule;
    }

    public function buildFeeAttributes($tenancy)
    {
        $fees = $tenancy->fees->where('fee_type_id', '!=', 4);

        foreach ($fees as $fee) {

            if ($fee->invoice) {
                $fee->xeroStatus = title_case($fee->invoice->status);

            }  elseif ($fee->status == 'Pending') {
                $fee->xeroStatus = 'Pending';
            }
             elseif ($fee->status == 'Scheduled') {
                $fee->xeroStatus = 'Scheduled ' . $fee->scheduledInvoice->scheduled_date->format('d/m/y');
            }
        }

        return $fees;
    }

    public function buildExpenseAttributes($tenancy)
    {
        $expenses = $tenancy->expenses;

        foreach ($tenancy->expenses as $expense) {

            if ($expense->invoice) {
                $expense->xeroStatus = $expense->invoice->status;
            }
        }

        return $expenses;
    }
}






