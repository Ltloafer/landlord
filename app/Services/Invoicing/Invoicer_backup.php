<?php 

namespace App\Services\Invoicing;

use XeroLaravel;
use App\Notifications\LandlordPaymentDue;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;

Class invoicer
{
	public function createInvoice($s_inv)
	{
		$contact = $this->getContact($s_inv->tenancy);

		if (! $contact) {
			dd('No contact on xero');
		}

		$invoice = new XeroInvoice();

		$invoice->setContact($contact);
		$invoice->setType('ACCREC');

		$dateInstance = new \DateTime();
		$invoice->setDate($dateInstance);
		$invoice->setDueDate($dateInstance);
            // $invoice->setInvoiceNumber('DMA-00002');
		$invoice->setReference($s_inv->tenancy->property->address);

            // Provide an URL which can be linked to from Xero to view the full order
		$invoice->setUrl('http://yourdomain/fullpathtotheorder');
		$invoice->setCurrencyCode('GBP');
		$invoice->setStatus('Draft');

		$invoice = $this->addRentalInvoiceLine($invoice, $s_inv);

		XeroLaravel::saveInvoice($invoice);

		return $invoice;
	}

	protected function getContact($tenancy)
	{
        $contact = XeroLaravel::getContact($tenancy->tenant->xero_ContactID);

        return $contact;
    }


     /**
     * Add the rental element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledInvioice $s_inv
     * @return $invoice
     */
    protected function addRentalInvoiceLine($invoice, $s_inv)
    {
    	$monthDateRange = $s_inv->start_date->format('d/m/y'). ' to '. $s_inv->end_date->format('d/m/y');

    	$line = resolve('XeroInvoiceLine');
    	$line->setDescription('Rental payment due in respect of the above mentioned property '. $monthDateRange. '. '.$s_inv->add_info);

    	$line->setQuantity(1);
    	$line->setUnitAmount($s_inv->tenancy->monthly_rate/100);
    	$line->setTaxAmount(0);
    	$line->setLineAmount($s_inv->tenancy->monthly_rate/100);

        $invoice->addLineItem($line);

        return $invoice;
    }

    
     /**
     * Add the security deposit element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledDeposit $scheduledDeposit
     * @return $invoice
     */
     protected function addSecurityDeposit($invoice, $scheduledDeposit)
     {
         $line = resolve('XeroInvoiceLine');
         $line->setDescription('Security deposit');

         $line->setQuantity(1);
         $line->setUnitAmount($scheduledDeposit->amount/100);
         $line->setTaxAmount(0);
         $line->setLineAmount($scheduledDeposit->amount/100);

         $invoice->addLineItem($line);

         XeroLaravel::saveInvoice($invoice);

         $this->updateModel($scheduledDeposit, $invoice);
     }
}

