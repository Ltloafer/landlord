<?php 

namespace App\Services\Invoicing;

use XeroLaravel;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;

Class Invoicer
{
    /**
     * @var DateTime
     */
    protected $invoiceDate;

    /**
     * @var string 
     */
    protected $reference;

    /**
     * @var App/Contact
     */
    protected $contact;

    /**
     * @var string
     */
    protected $type;


    protected function xeroInvoice()
    {
        $invoice = new XeroInvoice();
        $invoice->setContact($this->contact);
        $invoice->setType($this->type);

        $invoice->setDate(\DateTime::createFromFormat('Y-m-d', $this->invoiceDate));
        $invoice->setDueDate(new \DateTime());
        $invoice->setReference($this->reference);

        // $invoice->setUrl('http://yourdomain/fullpathtotheorder');
        $invoice->setCurrencyCode('GBP');
        $invoice->setStatus(config('xero.invoices.fee_invoice_type'));
        $invoice->setBrandingThemeID(config('xero.branding_themes.trading'));

        return $invoice;
    }


     /**
     * Add the rental element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledInvioice $s_inv
     * @return $invoice
     */
     protected function addInvoiceLine($invoice, $description, $fee, $unitAmount, $taxAmount)
     {
        $line = resolve('XeroInvoiceLine');
        $line->setDescription($description);
        $line->setQuantity(1);
        $line->setUnitAmount($unitAmount);
        $line->setAccountCode(config('xero.account_codes.fee_invoice_code'));
        $line->setTaxAmount($taxAmount);
        $invoice->addLineItem($line);

        return $invoice;
    }
}
