<?php 

namespace App\Services\Invoicing;

use Invoice;
use XeroLaravel;
use App\HotelBooking;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;

class HotelInvoicer
{
	public function createXeroInvoice(HotelBooking $booking)
	{
		$invoice = new XeroInvoice();
		$invoice->setContact($this->getContact($booking));
		$invoice->setType('ACCREC');

		$dateInstance = new \DateTime();
		$invoice->setDate($dateInstance);
		$invoice->setDueDate($dateInstance);
		$invoice->setReference($booking->confirmation_reference);

		$invoice->setUrl('http://yourdomain/fullpathtotheorder');
		$invoice->setCurrencyCode('GBP');
		// $invoice->setStatus(config('xero.invoices.fee_invoice_type'));
        $invoice->setStatus('AUTHORISED');
        $invoice->setBrandingThemeID(config('xero.branding_themes.trading'));

		$invoice = $this->addInvoiceLineForFee($invoice, $booking);

		XeroLaravel::saveInvoice($invoice);

		return $invoice;
	}


     /**
     * Add the rental element to the Invoice.
     * @param XeroInvoice $invoice
     * @param ScheduledInvioice $s_inv
     * @return $invoice
     */
    protected function addInvoiceLineForFee($invoice, $booking)
    {
    	$monthDateRange = $booking->start_date->format('d/m/y'). ' to '. 
        $booking->finish_date->format('d/m/y');

    	$line = resolve('XeroInvoiceLine');
    	$line->setDescription('Commission for confirmation reference '. $booking->confirmation_reference);
    	$line->setQuantity(1);
        // dd($booking->getCommissionTotal());
    	$line->setUnitAmount($booking->getCommissionTotal());
    	$line->setAccountCode('A08');

        // dd((($booking->getCommissionTotalForXero())*20)/100);
        // dd($booking->getCommissionTotalTax());

		$line->setTaxAmount($booking->getCommissionTotalTax());
    	$invoice->addLineItem($line);

    	return $invoice;
    }

    protected function getContact($booking)
    {
        return XeroLaravel::getContact($booking->hotel->xero_contactID);
    }
	
}