<?php 

namespace App\Services\Invoicing;

use App\Fee;
use App\Invoice;
use XeroLaravel;
use Carbon\Carbon;
use LTLoafer\XeroLaravel\XeroLaravelInvoice as XeroInvoice;

class FeeInvoicer extends Invoicer
{
    protected $fee;

    public function invoiceFee(Fee $fee, $invoiceDate)
    {
        $this->setProperties($fee, $invoiceDate);

        if ($fee->fee_type_id != 3) {

            return back()->with('alert_message', 'We can only invoice commission fees in this way. Thanks');
        }

        try {

            $xeroInvoice = $this->xeroInvoice();

            $monthDateRange = $fee->getPeriodDateRange();

            $description = $fee->getFeeDescription(). ' fee due in respect of the above mentioned property '. $monthDateRange . '. ';

            if ($fee->fee_type_id == 3) {
                $description = $description . 'Commission ' . $fee->tenancy->displayCommissionRate() . '%';
            }

            $unitAmount = $fee->amount / 100;

            $taxAmount = (($fee->amount/100)*20) / 100;

            $xeroInvoice = $this->addInvoiceLine($xeroInvoice, $description, $fee, $unitAmount, $taxAmount);

            XeroLaravel::saveInvoice($xeroInvoice);

        } catch (\Exception $e) {

            \Log::error($e);

            return back()->with('alert_message', 'We cannot invoice that fee. Please try again.');
        }

        $invoice = $this->invoice($xeroInvoice);

        return $invoice;
    }

    private function setProperties($fee, $invoiceDate)
    {
        $this->fee = $fee;
        $this->invoiceDate = $invoiceDate;
        $this->contact = $fee->getLandlordOrAgentContact();
        $this->type = 'ACCREC';
        $this->reference = $this->fee->tenancy->property->address;
    }

    private function invoice($xeroInvoice)
    {
        $invoice = new Invoice;
        $invoice->date = $this->invoiceDate;
        $invoice->reference = $xeroInvoice['Reference'];
        $invoice->type = 'ACCREC';
        $invoice->tenancy_id = $this->fee->tenancy_id;
        $invoice->contact_id = $this->fee->getLandlordOrAgentContactId();
        $invoice->status = 'invoiced';
        $invoice->xero_ref =  $xeroInvoice['InvoiceNumber'];
        $invoice->xero_inv_id = $xeroInvoice['InvoiceID'];
        $invoice->amount = $xeroInvoice['SubTotal'] * 100;
        $invoice->vat = $xeroInvoice['TotalTax'] * 100;
        $invoice->total_amount = $xeroInvoice['Total'] * 100;
        $invoice->due_date = $xeroInvoice['DueDate'];
        $invoice->last_sync = Carbon::now();
        $invoice->save(); 

        $this->fee->invoice_id = $invoice->id;
        $this->fee->status = 'Invoiced';
        $this->fee->save();
    }

}