<?php 

namespace App\Services;

use App\Services\Schedules\CreatesSchedules;
use App\Services\Schedules\CreatesInstallmentSchedules;
use App\Services\Schedules\PayablesScheduleRunner;
use App\Services\Schedules\RentalScheduleRunner;
use App\Services\Schedules\SplitCommsScheduleRunner;

class SchedulerService
{
    protected $payablesRunner;
    protected $rentalsRunner;
    protected $createsSchedules;
    protected $createsInstallmentSchedules;
    protected $splitCommsRunner;

    public function __construct(PayablesScheduleRunner $payablesRunner, RentalScheduleRunner $rentalsRunner, CreatesSchedules $createsSchedules, SplitCommsScheduleRunner $splitCommsRunner, CreatesInstallmentSchedules $createsInstallmentSchedules)
    {
        $this->payablesRunner = $payablesRunner;
        $this->createsSchedules = $createsSchedules;
        $this->rentalsRunner = $rentalsRunner;
        $this->splitCommsRunner = $splitCommsRunner;
        $this->createsInstallmentSchedules = $createsInstallmentSchedules;
    }

    public function runRentalSchedule($tenancy)
    {
        $scheduledInvoices = $tenancy->getScheduledInvoicesForTenancy();

        foreach($scheduledInvoices as $period) {

            $this->rentalsRunner->runScheduler($period);
        }
    }

    public function runSplitCommsSchedule($tenancy)
    {
        $scheduledInvoices = $tenancy->getScheduledInvoicesForTenancy();

        foreach ($scheduledInvoices as $period) {

            $this->splitCommsRunner->runScheduler($period);
        }
    }

    public function runPayablesSchedule($tenancy)
    {
        $rentPayables = $tenancy->getScheduledRentPayablesForTenancy();

        foreach ($rentPayables as $payable) {

            $this->payablesRunner->runScheduledPayables($payable);
        }

    }


    public function createNewSchedule($tenancy)
    {
        if ($tenancy->isInstallmentsType()) {

            $this->createsInstallmentSchedules->createSchedule($tenancy);
        } else {

            $this->createsSchedules->createSchedule($tenancy);
        }
    }

    public function addScheduledPeriod($tenancy, $request)
    {
        $this->createsSchedules->addScheduledPeriod($tenancy, $request);
    }

}