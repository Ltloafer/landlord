<?php

namespace App\Services\Actions;

use App\Fee;
use App\Action;
use App\Expense;
use App\RentPayable;
use App\ScheduledInvoice;

Class CreatesAction
{
	public function createRentalPaymentAction(ScheduledInvoice $scheduledInvoice)
	{
		$action = new Action();
		$action->type = "rental_payment";
		$action->description = "Tenant rental payment";
		$action->tenancy_id = $scheduledInvoice->tenancy_id;
		$action->scheduled_invoice_id = $scheduledInvoice->id;

		if ($tenantAdminFee = $scheduledInvoice->scheduledTenantFee()) {
			$action->tenant_admin_fee_id = $tenantAdminFee->id;
		}

		if ($depositFee = $scheduledInvoice->scheduledDeposit()) {
			$action->deposit_id = $depositFee->id;
		} 
		$action->save();
	}

	public function createAdminRechargeAction(Fee $fee)
	{
		$action = new Action();
		$action->type = "admin_recharge";
		$action->description = "Tenant admin fee payment";
		$action->tenancy_id = $fee->tenancy_id;
		$action->scheduled_invoice_id = $fee->scheduledInvoice->id;
		$action->tenant_admin_recharge_fee_id = $fee->id;
		$action->save();
	}

	public function createLandlordAdminFeeAction(Fee $fee)
	{
		$action = new Action();
		$action->type = "landlord_admin_fee";
		$action->description = "Landlord admin fee payment";
		$action->tenancy_id = $fee->tenancy_id;
		$action->scheduled_invoice_id = $fee->scheduledInvoice->id;
		$action->landlord_admin_fee_id = $fee->id;
		$action->save();
	}

	public function createCommissionFeeAction(Fee $fee)
	{
		$action = new Action();
		$action->type = "commission_fee";
		$action->description = "Commission fee payment";
		$action->tenancy_id = $fee->tenancy_id;
		$action->scheduled_invoice_id = $fee->scheduledInvoice->id;
		$action->commission_fee_id = $fee->id;
		$action->save();
	}

	public function createLandlordPaymentAction(RentPayable $rentPayable )
	{
		$action = new Action();
		$action->type = "landlord_payment";
		$action->description = "Landlord payment";
		$action->tenancy_id = $rentPayable->tenancy_id;
		$action->scheduled_invoice_id = $rentPayable->scheduledInvoice->id;
		$action->landlord_payment_id = $rentPayable->id;
		$action->save();
	}

	public function createExpenseAction(Expense $expense)
	{
		$action = new Action();
		$action->type = "expense_payment";
		$action->description = "Supplier expense payment";
		$action->tenancy_id = $expense->tenancy_id;
		$action->expense_id = $expense->id;

		if ($expense->scheduled_invoice_id) {
			$action->scheduled_invoice_id = $expense->scheduled_invoice_id;
		}

		$action->save();
	}
	
}