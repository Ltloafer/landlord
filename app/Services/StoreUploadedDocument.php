<?php 

namespace App\Services;

use App\Landlord;
use App\Property;

class storeUploadedDocument 
{
	public function storeDocument($model, $document, $request)
	{
        try 
        {
            $filename =$request->file('uploadedDocument')->getClientOriginalName();

            $sluggedFilename = str_replace(" ","-", $filename);

            if ($model instanceof Landlord) {
            	$request->file('uploadedDocument')->storeAs('documents/landlord/'. $model->id, $sluggedFilename);
            } 

            elseif ($model instanceof Property) {
            	$request->file('uploadedDocument')->storeAs('documents/property/'. $model->id, $sluggedFilename);
            }
 
            $document->file = $sluggedFilename; 

            // $directory = $model->id;

            // if (\Storage::disk('s3')->exists($directory.'/'.$filename))
            // {
            //     $record->delete();    

            //     return redirect()->back()->with('flash_message', 'There is already a record with that filename - Please delete that record first');
            // }

            $document->save();

            return $document;
        }

        catch (\Exception $e)
        {
            \Log::error('Uploading document failed!');
            \Log::error($e->getMessage());

            $document->delete();

            return redirect()->back()->with('flash_message', 'We had a problem uploading that record - Please try again');
        }
    }
	
}