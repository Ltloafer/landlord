<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment
{
	public $date;

	public $invNo;

	public $customer;

	public $reference;

	public $amount;

	public $bank;

	public $reconciled;

	public $status;

	public function isReconciled()
	{
		if ($this->reconciled == 'true') {
			return true;
		}

		return false;
	}
	
}

