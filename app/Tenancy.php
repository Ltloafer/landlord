<?php

namespace App;

use App\Deposit;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenancy extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates =['start_date', 'finish_date'];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'let_type' => 'standard',
    ];

    public function property()
    {
    	return $this->belongsTo('App\Property');
    }

    public function tenant()
    {
    	return $this->belongsTo('App\Tenant');
    }

    public function rentalplan()
    {
    	return $this->hasOne('App\Rentalplan');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function disbursements()
    {
        return $this->hasMany(Disbursement::class);
    }

    public function fees()
    {
        return $this->hasMany(Fee::class);
    }

    public function adminFees()
    {
        return $this->hasMany(Fee::class)->where('fee_type_id', '!=', 3);
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class);
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    public function scheduledInvoices()
    {
        return $this->hasMany(ScheduledInvoice::class);
    }

    public function rentPayables()
    {
        return $this->hasMany(RentPayable::class);
    }

    public function invoicedScheduledInvoices()
    {
        return $this->scheduledInvoices->where('status', '!=', 'pending');
    }

    public function actions()
    {
        return $this->hasMany(Action::class);
    }

    public function pendingActions()
    {
        return $this->hasMany(Action::class)->whereNull('completed_at');
    }

    public function search()
    {
        return $this->belongsTo(Search::class);
    }

    public function productionCompany()
    {
        return $this->belongsTo(ProductionCompany::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function dateOrderedInvoices()
    {
        return $this->hasMany(Invoice::class)->orderBy('date');
    }

    public function getExpenseTypes()
    {
        return ExpenseType::all();
    }

    public function getAllSuppliers()
    {
        return Supplier::all();
    }

    public function installments()
    {
        return $this->hasMany(Installment::class);
    }

    public function getAllSuppliersWithXeroContacts()
    {
        return Supplier::whereNotNull('xero_contactID')->get();
    }


    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMail()
    {
        return 'neilbaker26@hotmail.com';
    }

    public function isDirect()
    {
        if ($this->type == 'direct') {
            return true;
        }
        return false;
    }

    public function isViaAgent()
    {
        if ($this->type == 'split-comm') {
            return true;
        }
    }

    public function getAdminFeeForTenant()
    {
        $fee = $this->fees->firstWhere('fee_type_id', 2);

        return $fee;
    }

    public function getAdminRechargeFeeForTenant()
    {
        $fee = $this->fees->firstWhere('fee_type_id', 4);

        return $fee;
    }

    public function getLandlordAdminFee()
    {
        $fee = $this->fees->firstWhere('fee_type_id', 1);

        return $fee;
    }

    public function getTenantAdminFee()
    {
        $fee = $this->fees->firstWhere('fee_type_id', 2);

        return $fee->amount ?? 0;
    }

    public function landlordCommissions()
    {
        $commissions = $this->hasMany(Fee::class)->where('fee_type_id', 3);

        return $commissions;
    }

    public function getBreakOutDate()
    {
        return $this->start_date->addMonths($this->break_clause)->subDays(1)->format('jS \\of F Y'); 
    }

    public function breakOutDate()
    {
        return $this->start_date->addMonths($this->break_clause)->subDays(1);
    }

    public function getEarliestFinishDate()
    {
        return $this->start_date->addMonths($this->break_clause)->addDays($this->notice_period)->format('jS \\of F Y');
    }

    public function isScottySpecial()
    {
        if ($this->type_id == 3) {
            return true;
        }
        return false;
    }

    public function getHoldingDeposit()
    {
        return convertPenceToPounds($this->holding_deposit);
    }

    public function getSecurityDeposit()
    {
        return convertPenceToPounds($this->security);
    }

    public function securityDeposit()
    {
        $deposit = $this->deposits->firstWhere('type', 'Security');

        return $deposit;
    }

    public function hasInstallments()
    {
        if ($this->installments->count() > 0) {
            return true;
        }
        return false;
    }

    public function displayTenancyType()
    {
        if ($this->let_type == 'standard') {
            if ($this->type == 'direct') {
                return 'Direct let (AST)';
            }

            if ($this->type == 'split-comm') {
                return 'Split Commission';
            }

        } elseif (
            $this->let_type == 'installments') {
            return 'Short let (installments)';
        }
    }

    public function getTotalDisbursementCredits()
    {
        $creditTotal = 0;

        foreach ($this->disbursements as $disbursement) {
            if ($disbursement->disbursementType->entry == 'credit') {
                $creditTotal = $creditTotal + $disbursement->amount;
            }
        }
        return $creditTotal;
    }

    public function displayTotalDisbursementCredits()
    {
        return convertPenceToPounds($this->getTotalDisbursementCredits());
    }

    public function getTotalDisbursementDebits()
    {
        $debitTotal = 0;

        foreach ($this->disbursements as $disbursement) {
            if ($disbursement->disbursementType->entry == 'debit') {
                $debitTotal = $debitTotal + $disbursement->amount;
            }
        }
        return $debitTotal;
    }

    public function displayTotalDisbursementDebits()
    {
        return convertPenceToPounds($this->getTotalDisbursementDebits());
    }

    public function getDisbursementBalance()
    {
        $balance = $this->getTotalDisbursementCredits() - $this->getTotalDisbursementDebits();

        return $balance;
    }

    public function displayDisbursementBalance()
    {
        return convertPenceToPounds($this->getDisbursementBalance());
    }

    public function getTotalFees()
    {
        $feeTotal = 0;

        foreach ($this->fees->where('fee_type_id', '!=', 4) as $fee) {
            $feeTotal = $feeTotal + $fee->amount;
        }
        return $feeTotal;
    }

    public function displayTotalFees()
    {
        return convertPenceToPounds($this->getTotalFees());
    }

    public function displayTotalFeesVat()
    {
        $total  = (($this->getTotalFees()/100)*20);

        return convertPenceToPounds($total);
    }

    public function displayTotalFeesTotal()
    {
        $total = ((($this->getTotalFees()/100)*20) + $this->getTotalFees());

        return convertPenceToPounds($total);
    }

    /**
     * Get all account entries for the account ledger
     * 
     * @return [collection] $accountEntries
     */
    public function getAllAccountEntries()
    {
        $accountEntries = collect();

        if ($this->disbursements) {
            foreach ($this->disbursements as $disbursement) {
                $disbursement->entry = $disbursement->disbursementType->entry;
                $disbursement->type = $disbursement->disbursementType->type;
                $accountEntries->push($disbursement);
            }
        }

        if ($this->fees) {
            foreach ($this->fees as $fee) {
                $fee->entry = $fee->feeType->entry;
                $fee->type = $fee->feeType->type;
                $accountEntries->push($fee);
            }
        }

        if ($this->expenses) {
            foreach ($this->expenses as $expense) {
                $expense->entry = $expense->expenseType->entry;
                $expense->type = $expense->expenseType->type;
                $accountEntries->push($expense);
            }
        }


        return $accountEntries->sortBy('created_at');
    }

    public function getLedgerCreditTotals() 
    {
        $total = 0;

        $accountEntries = $this->getAllAccountEntries()->where('entry', '=', 'credit');

        foreach ($accountEntries as $entry) {
            $total = $total + $entry->amount;
        }
        return $total;
    }

    public function displayLedgerCreditTotals()
    {
        return convertPenceToPounds($this->getLedgerCreditTotals());
    }

    public function getLedgerDebitTotals() 
    {
        $total = 0;

        $accountEntries = $this->getAllAccountEntries()->where('entry', '=', 'debit');

        foreach ($accountEntries as $entry) {
            $total = $total + $entry->amount;
        }
        return $total;
    }

    public function displayLedgerDebitTotals()
    {
        return convertPenceToPounds($this->getLedgerDebitTotals());
    }

    public function getLedgerBalance()
    {
        return $this->getLedgerCreditTotals() - $this->getLedgerDebitTotals();
    }

    public function displayLedgerBalance()
    {
        return convertPenceToPounds($this->getLedgerBalance());
    }

    public function displayExpensesTotal()
    {
        $total = 0;

        foreach ($this->expenses as $expense) {
            $total = $total + $expense->amount;
        }
        return convertPenceToPounds($total);
    }

    public function getClientAccountEntries()
    {
     $accountEntries = collect();

     foreach ($this->disbursements as $disbursement) {
        $disbursement->entry = $disbursement->disbursementType->entry;
        $disbursement->type = $disbursement->disbursementType->type;
        $accountEntries->push($disbursement);
    }

        // foreach ($this->fees as $fee) {
        //     $fee->entry = $fee->feeType->entry;
        //     $fee->type = $fee->feeType->type;
        //     $accountEntries->push($fee);
        // }

        // foreach ($this->expenses as $expense) {
        //     $expense->entry = $expense->expenseType->entry;
        //     $expense->type = $expense->expenseType->type;
        //     $accountEntries->push($expense);
        // }

    foreach($this->scheduledInvoices as $s_inv) {
        if ($s_inv->status == 'Paid') {
            $s_inv->date = today();
            $s_inv->amount = $s_inv->amount;
            $s_inv->type = 'Rent';
            $s_inv->entry = 'credit';
            $accountEntries->push($s_inv);
        }
    }

    return $accountEntries->sortBy('created_at');
}

public function monthlyCommissionAmount()
{
    $amount = ($this->monthly_rate / 100) * ($this->comm_rate / 100);

    return $amount;
}

public function getScheduledInvoicesForTenancy()
{
    return $this->scheduledInvoices->where('status', 'pending');
}

public function getScheduledRentPayablesForTenancy()
{
    return $this->rentPayables->where('status', 'Scheduled');
}

public function isInstallmentsType()
{
    if ($this->let_type == 'installments') {
        return true;
    }
    return false;
}

public function isStandardType() 
{
    if ($this->let_type == 'standard') {
        return true;
    }
    return false;
}

public function getTotalCommission()
{
    return ($this->total_rent / 100 ) * ($this->comm_rate / 100);
}

public function displayCommissionRate()
{
    return $this->comm_rate / 100;
}

public function displayCommissionType()
{
    if ($this->comm_charging == 'all_upfront') {
        return 'All upfront';
    }

    if ($this->comm_charging == 'by_period') {
        return 'By period';
    }

    if ($this->comm_charging == 'to_break') {
        return 'To break';
    }
}


public function hasScheduledPayables()
{
    // $rentPayables = $this->rentPayables;

    if ($nextRentPayable = $this->rentPayables->firstWhere('status', 'Scheduled')) {
        return true;
    }

    return false;
}

public function invoicesProductionCompany()
{
    if ($this->invoice_production_company == true) {
        return 'Yes';
    }

    return 'No';
}

public function invoiceProductionCompany()
{
    if ($this->invoice_production_company == true) {
        return true;
    }

    return false;
}


public function invoiceTenantAdminFee()
{
    if ($this->charge_tenant_admin_fee) {
        return 'Yes';
    }

    return 'No';
}

public function invoiceLandlordAdminFee()
{
    if ($this->charge_landlord_admin_fee) {
        return 'Yes';
    }

    return 'No';
}

public function holdingDeposits()
{
    return $this->hasMany(Deposit::class)->where('type', 'Holding');
}

public function completed()
{
    return $this->status == 'completed' ? true : false;
}


}
