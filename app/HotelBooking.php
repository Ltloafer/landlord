<?php

namespace App;

use App\Invoice;
use Carbon\Carbon;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelBooking extends Model
{
	use Notifiable, SoftDeletes;

	protected $dates =['start_date', 'finish_date'];

    public function hotel()
    {
    	return $this->belongsTo(Hotel::class);
    }

    public function tenant()
    {
    	return $this->belongsTo('App\Tenant');
    }

    public function fees()
    {
    	return $this->hasMany(Fee::class);
    }

    public function actions()
    {
    	return $this->hasMany(Action::class);
    }

    public function pendingActions()
    {
    	return $this->hasMany(Action::class)->whereNull('completed_at');
    }

    public function search()
    {
    	return $this->belongsTo(Search::class);
    }

    public function productionCompany()
    {
    	return $this->belongsTo(ProductionCompany::class);
    }

    public function invoiceModel()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id');
    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMail()
    {
    	return 'neilbaker26@hotmail.com';
    }

    public function getNightlyRate()
    {
    	return $this->nightly_rate / 100;
    }

    public function getCommissionRate()
    {
    	return $this->comm_rate / 100;
    }

    public function getCommissionTotal()
    {
    	$totalRoomCharge = ($this->start_date->diffInDays($this->finish_date) * $this->nightly_rate) / 100;

    	return number_format(($totalRoomCharge / 100) * ($this->comm_rate / 100),2, '.', '');
    }

     public function getCommissionTotalTax()
    {
        $totalRoomCharge = ($this->start_date->diffInDays($this->finish_date) * $this->nightly_rate) / 100;

        return number_format((((($totalRoomCharge / 100) * ($this->comm_rate / 100)) * 20) / 100) , 2, '.', '');
    }


    public function getVat()
    {
        $totalRoomCharge = ($this->start_date->diffInDays($this->finish_date) * $this->nightly_rate) / 100;

        $totalWithComm = ($totalRoomCharge / 100) * ($this->comm_rate / 100);

        $vat = ($totalWithComm/100) * 20;

        return number_format($vat, 2);
    }

    public function getTotal()
    {
        $totalRoomCharge = ($this->start_date->diffInDays($this->finish_date) * $this->nightly_rate) / 100;

        $totalWithComm = ($totalRoomCharge / 100) * ($this->comm_rate / 100);

        $vat = ($totalWithComm/100) * 20;

        return number_format(($totalWithComm + $vat),2);
    }

    public function isCompleted()
    {
    	if ($this->completed) {
    		return 'Yes';
    	}

    	return 'No';
    }


    public function getTotalFees()
    {
    	$feeTotal = 0;

    	foreach ($this->fees->where('fee_type_id', '!=', 4) as $fee) {
    		$feeTotal = $feeTotal + $fee->amount;
    	}
    	return $feeTotal;
    }

    public function displayTotalFees()
    {
    	return convertPenceToPounds($this->getTotalFees());
    }

    public function displayTotalFeesVat()
    {
    	$total  = (($this->getTotalFees()/100)*20);

    	return convertPenceToPounds($total);
    }

    public function getTotalCommission()
    {
    	return ($this->total_rent/100) * $this->comm_rate;
    }

    public function invoice()
    {
        $invoice = new Invoice();

        $invoice->date = Carbon::now();
        $invoice->reference = $this->confirmation_reference;
        $invoice->type = 'ACCREC';
        $invoice->hotel_id = $this->id;
        $invoice->status = 'pending';
        $invoice->save(); 

        return $invoice;
    }

      public function getPeriodDates()
    {
       return $this->start_date->format('jS \\of F y').' - '.$this->end_date->format('jS \\of F y');
   }



}
