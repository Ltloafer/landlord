<?php

namespace App;

use XeroLaravel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduledInvoice extends Model
{
    use SoftDeletes;
    
    protected $table = "scheduled_invoices";

    protected $dates = ['scheduled_date', 'start_date', 'end_date'];

    public function tenancy()
    {
    	return $this->belongsTo(Tenancy::class);
    }

    public function fees()
    {
        return $this->hasMany(Fee::class, 'scheduled_invoice_id');
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class, 'scheduled_invoice_id');
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class, 'scheduled_invoice_id');
    }

    public function rentPayable()
    {
        return $this->hasOne(RentPayable::class, 'scheduled_invoice_id');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function displayAmount()
    {
    	return convertPenceToPounds($this->amount);

        // return $this->amount / 100;
    }

    public function getAmount()
    {
        return $this->amount / 100;

    }

    public function displayVat()
    {
        return convertPenceToPounds($this->vat);
    }

    public function displayTotal()
    {
        return convertPenceToPounds($this->amount + $this->vat);
    }

    public function isInvoiced()
    {
        if ($this->status == 'Invoiced') {
            return true;
        }
        return false;
    }

    public function isPaid()
    {
        if ($this->status == 'Paid') {
            return true;
        }
        return false;
    }

    public function getXeroInvoiceStatus()
    {
        $invoice = XeroLaravel::getInvoiceById($this->xero_ref);

        return $invoice['Status'];
    }

    public function xeroInvoiceCanBeViewed()
    {
        $status = $this->getXeroInvoiceStatus();

        if ($status == 'DRAFT') {
            return false;
        }

        return true;
    }


    public function isFullyPaid()
    {
        $invoice = XeroLaravel::getInvoiceById($this->xero_inv_id);

        if ($invoice["FullyPaidOnDate"]) {
            return 'Paid';
        } 

        return '';
    }



    public function scheduledTenantFee()
    {
        $fees = $this->fees->firstWhere('fee_type_id', 2);

        return $fees;
    }

    public function scheduledTenantRechargeFee()
    {
        $fee = $this->fees->firstWhere('fee_type_id', 4);

        return $fee;
    }

    public function scheduledDeposit()
    {
        $deposit = $this->deposits->firstWhere('type', 'Security');

        return $deposit;
    }

    public function tenantFees()
    {
        $fee = $this->fees->firstWhere('fee_type_id', 2);

        if ($fee) {
            return convertPenceToPounds($fee->amount);
        } else {
            return '';
        }
    }

    public function landlordAdminFees()
    {
        $fee = $this->fees->firstWhere('fee_type_id', 1);

        if ($fee) {
            return convertPenceToPounds($fee->amount);
        } else {
            return '';
        }
    }

    public function scheduledCommissionFee()
    {
        $fee = $this->fees->where('fee_type_id', 3)->where('status', 'Scheduled')->first();

        return $fee;
    }

    public function commissionAmount()
    {
        $amount = ($this->amount / 100) * ($this->comm_rate / 100);

        return $amount;
    }


    public function commissionFee()
    {
        $fee = $this->fees->where('fee_type_id', 3)->first();

        return $fee;
    }

    public function commission()
    {
        $comms = $this->fees->firstWhere('fee_type_id', 3);

        if ($comms) {
            return $comms->amount;
        }
        return 0;
    }

    public function displayCommission()
    {
        return convertPenceToPounds($this->commission());
    }

    public function getTotalDisbursements()
    {
        $expenses = $this->expenses;

        $total = 0;

        foreach ($expenses as $expense) {
            $total += $expense->amount;
        }
        return $total;
    }

    public function displayTotalDisbursements()
    {
        return convertPenceToPounds($this->getTotalDisbursements());
    }

    public function updateWithInvoiceData($invoice)
    {
        $this->xero_ref = $invoice['InvoiceNumber'];
        $this->xero_inv_id = $invoice['InvoiceID'];
        $this->status = 'Invoiced';
        $this->save();
    }

    public function getPeriodDates()
    {
        return $this->start_date->format('jS \\of F y').' - '.$this->end_date->format('jS \\of F y');
    }

    public function getInvoiceStatus()
    {
        if ($this->invoice) {
            return $this->invoice->id. ' | '. $this->invoice->status. ' | '.$this->invoice->xero_ref;
        }
        return '';
    }



}

