<?php

namespace App;

use XeroLaravel;
use Illuminate\Database\Eloquent\Model;

class RentalCharge extends Model
{
	protected $table = 'rental_charges';
	
    protected $fillable = ['status', 'tenancy_id', 'amount', 'date', 'scheduled_invoice_id', 'type'];

	protected $dates = ['date'];
	
	public function displayAmount()
	{
		return convertPenceToPounds($this->amount);
	}

	public function isPending()
	{
		if ($this->status == 'Pending') {
			return true;
		}
		return false;
	}

	public function isInvoiced()
	{
		if ($this->status == 'Invoiced') {
			return true;
		}
		return false;
	}

	public function isPaid()
	{
		if ($this->status == 'Paid') {
			return true;
		}
		return false;
	}

	public function getXeroInvoiceStatus()
	{
		$invoice = XeroLaravel::getInvoiceById($this->xero_ref);

		return $invoice['Status'];
	}
}
