<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenant extends Model
{
	use SoftDeletes;
	
	public function tenancy()
	{
		return $this->hasMany(Tenancy::class);
	}

	public function contact()
	{
		return $this->belongsTo(Contact::class);
	}

	public function displayAddress()
	{
		$address = $this->contact->line_1. ', '. $this->contact->line_2. ', '. $this->contact->line_3;

		return $address;
	}

}

