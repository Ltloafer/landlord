<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
	public function search()
	{
		return $this->hasOne(Search::class);
	}
    
}
