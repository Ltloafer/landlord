<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
	public function joinNames()
	{
		return $this->first_name . ' ' . $this->last_name;
	}
    
}
