<?php

namespace App\Mail;

use App\Search;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOffer extends Mailable
{
    use Queueable, SerializesModels;

    public $search;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Search $search)
    {
        $this->search = $search;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Email.send-offer')
         ->subject('Tenancy offer');
    }
}
