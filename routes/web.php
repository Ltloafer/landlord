<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

	Route::group(['prefix' => 'api'], function () {
		Route::resource('/landlords', 'Api\LandlordController');

		Route::post('/search/convertToTenant/{id}', 'Api\SearchesController@convertToTenant');
		Route::post('/search/markAsStarred/{id}', 'Api\SearchesController@markAsStarred');
		Route::post('/search/removeStarred/{id}', 'Api\SearchesController@removeStarred');

		Route::get('/searchesIncArchived', 'Api\SearchesController@searchesIncArchived');

		Route::resource('/searches', 'Api\SearchesController');
		
		Route::get('hotel-bookings/markHotelAsPaid/{id}', 'Api\HotelBookingController@markHotelAsPaid');
		Route::resource('/hotel-bookings', 'Api\HotelBookingController');

		Route::get('/recentNotes/property/{id}', 'Api\NotesController@getRecentNotes');
		Route::get('/olderNotes/property/{id}', 'Api\NotesController@getOlderNotes');

		Route::get('/recentNotes/landlord/{id}', 'Api\LandlordController@getRecentNotes');
		Route::get('/olderNotes/landlord/{id}', 'Api\LandlordController@getOlderNotes');

		Route::get('/getSearchAreas', 'Api\SearchAreaController@index');

		Route::resource('/invoices', 'Api\InvoiceController');

		Route::resource('/fees', 'Api\FeesController');
		Route::resource('/expenses', 'Api\ExpensesController');

		Route::resource('/xeroContacts', 'Api\XeroContactsController');
		Route::resource('/xeroCodes', 'Api\XeroCodesController');

		Route::get('/getScheduledInvoice/{id}', 'Api\ScheduledInvoiceController@show');
		Route::get('/generateStatement/{id}', 'Api\ScheduledInvoiceController@generateStatement');
	});

	Route::group(['prefix' => 'admin'], function () {
		Route::get('/properties', 'Admin\PropertiesController@manageNominalCodes');
		Route::resource('/account-codes', 'Admin\AccountCodesController');
	});


	Route::get('/home', 'HomeController@index')->name('home');



	/*--------- Property routes ---------------------------------------------------------------*/
	Route::post('/properties/downloadDocument/{id}', 'PropertyDocumentsController@downloadDocument');
	Route::resource('/properties', 'PropertyController');
	Route::resource('/property.documents', 'PropertyDocumentsController');



	/*--------- Tenants routes ---------------------------------------------------------------*/
	Route::resource('/tenants', 'TenantController');



	/*--------- Landlord routes ---------------------------------------------------------------*/
	Route::post('/landlords/downloadDocument/{id}', 'LandlordDocumentsController@downloadDocument');

	Route::resource('/landlords', 'LandlordController');
	Route::resource('/landlord.documents', 'LandlordDocumentsController');
	Route::resource('/landlord.assistants', 'LandlordAssistantsController');


	Route::resource('/production-companies', 'ProductionCompanyController');



	/*--------- Tenancy routes ---------------------------------------------------------------*/
	Route::get('/search-to-direct-let/{search}', 'SearchesController@convertSearchToDirectLet');
	Route::post('/tenancies/{id}/setStatus', 'TenancyController@setStatus');
	Route::post('/tenancies/{id}/setReference', 'TenancyController@setReference');
	Route::resource('/tenancies', 'TenancyController');


	/*--------- Invoices routes ---------------------------------------------------------------*/
	Route::get('/invoices/sync-with-xero', 'InvoiceController@syncWithXero');
	Route::get('/invoices/payables', 'InvoiceController@payables');
	Route::resource('/invoices', 'InvoiceController');


	/*--------- Payment routes ---------------------------------------------------------------*/
	Route::resource('/payments', 'PaymentController');

	/*--------- Hotel booking routes ---------------------------------------------------------------*/
	Route::post('/checkout-hotel-booking', 'HotelBookingController@checkout');
	Route::resource('/hotel-bookings', 'HotelBookingController');

	Route::resource('/hotels', 'HotelController');



	/*--------- Searches routes ---------------------------------------------------------------*/
	Route::post('setPropertyOnSearch', 'SearchesController@setPropertyOnSearch');
	Route::post('setTenantOnSearch', 'SearchesController@setTenant');
	Route::post('setProductionCompanyOnSearch', 'SearchesController@setProductionCompany');
	Route::get('/searches/{search}/make-offer', 'SearchOfferController@makeOffer');
	Route::get('/searches/{search}/send-offer', 'SearchOfferController@sendOffer');
	Route::get('/searches/{search}/offer-accepted', 'SearchOfferController@offerAccepted');
	Route::get('/searches/{search}/new-tenancy', 'SearchOfferController@createTenancy');
	Route::get('/searches/{search}/new-tenancy-direct', 'SearchOfferController@createDirectTenancy');
	Route::resource('/searches', 'SearchesController');
	Route::resource('/searches.search-areas', 'Search_SearchAreasController');


	Route::group(['prefix' => 'xero'], function () {
		Route::resource('/contacts', 'Xero\ContactsController');
		Route::resource('/tracking', 'Xero\TrackingController');
		// Route::resource('/tenants', 'Xero\TenantsController');
		Route::resource('/payments', 'Xero\PaymentsController');
		Route::resource('/branding-themes', 'Xero\BrandingThemesController');
		
		Route::resource('/directs/contacts', 'Xero\Directs\ContactsController');
	});



	/*--------- Accounts routes ---------------------------------------------------------------*/
	Route::get('/invoices/viewXeroInvoice/{id}', 'Accounts\InvoiceController@viewXeroInvoice');
// Route::get('/invoices/draft', 'Accounts\InvoiceController@viewDraftInvoices');

// Route::get('/invoices/scheduled/run', 'Accounts\ScheduledInvoiceController@invoiceRun');

	Route::get('/invoices/invoicesPayable', 'Accounts\InvoiceController@showInvoicesPayable');

	Route::get('/convertAllHotelInvoicesToTradingTheme', 'Accounts\InvoiceController@convertAllHotelInvoicesToTradingTheme');
	Route::get('/convertAllHotelInvoicesToTradingThemeByHotel/{id}', 'Accounts\InvoiceController@convertAllHotelInvoicesToTradingThemeByHotel');

	Route::get('/xero-invoices/convertToTradingTheme/{id}', 'Accounts\InvoiceController@convertToTradingTheme');

	Route::resource('/xero-invoices', 'Accounts\InvoiceController');



	Route::get('/viewDraftInvoices', 'Accounts\InvoiceController@viewDraftInvoices');

	Route::get('/scheduledInvoices/run/{tenancy}', 'Accounts\ScheduledInvoiceController@invoiceRun');

	Route::post('/generateScheduledInvoices', 'Accounts\ScheduledInvoiceController@generateSchedule');


	Route::get('/scheduledInvoices/markAsPaid/{id}', 'Accounts\ScheduledInvoiceController@markAsPaid');
	Route::resource('/scheduledInvoices', 'Accounts\ScheduledInvoiceController');

	Route::post('/payableInvoices', 'Accounts\InvoicesPayableController@store');
	Route::get('/payableInvoices', 'Accounts\InvoicesPayableController@index');


	/*--------- Rent payables routes ---------------------------------------------------------------*/
	Route::resource('/rentPayables', 'RentPayableController');


	/*--------- Contacts routes ---------------------------------------------------------------*/
	Route::resource('/accounts', 'Accounts\AccountsController');

	/*--------- Contacts routes ---------------------------------------------------------------*/
	Route::resource('/contacts', 'ContactsController');


	/*--------- Notes routes ---------------------------------------------------------------*/
	Route::resource('/notes', 'NotesController');



	/*--------- Disimbursments routes ---------------------------------------------------------------*/
	Route::resource('/disbursements', 'DisbursementsController');



	/*--------- Fees routes ---------------------------------------------------------------*/
	Route::get('/fees/markAsPaid/{id}', 'FeesController@markAsPaid');
	Route::post('/invoiceFee', 'FeesController@invoiceFee');
	Route::resource('/fees', 'FeesController');


	/*--------- Expense routes ---------------------------------------------------------------*/
	Route::resource('/expenses', 'ExpensesController');



	/*--------- Agents routes ---------------------------------------------------------------*/
	Route::resource('/agents', 'AgentsController');
	Route::resource('/agent.search-areas', 'AgentSearchAreasController');


	/*--------- Suppliers  routes ---------------------------------------------------------------*/
	Route::resource('/suppliers', 'SuppliersController');


	/*--------- Search-area  routes ---------------------------------------------------------------*/
	Route::resource('/search-areas', 'SearchAreaController');

	/*--------- Production  routes ---------------------------------------------------------------*/
	Route::resource('/productions', 'ProductionsController');

});

// URL::forceScheme('https');
