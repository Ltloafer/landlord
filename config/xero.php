<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Xero configuration
    |--------------------------------------------------------------------------
    |
    | Define the configuration settings for connecting with Xero.
    | These are the minimum settings - for more options, refer to examples/config.php.
    |
    */

    'oauth' => [
        'callback'  => 'http://localhost/',
        'consumer_key'  => env('XERO_CONSUMER_KEY'),
        'consumer_secret'   => env('XERO_CONSUMER_SECRET'),
        'rsa_private_key'   => env('RSA_PRIVATE_KEY'),
        // 'rsa_private_key'   => 'file:///Users/neil/code/landlord/privatekey.pem',
    ],

    'oauth2' => [
        'tenant_id_direct' => env('TENANT_ID_DIRECT'),
        'tenant_id' => env('TENANT_ID'),
        'redirect_uri' => env('XERO_REDIRECT_URI'),
        'client_id' => env('XERO_CLIENT_ID'),
        'client_secret' => env('XERO_CLIENT_SECRET'),
    ],
    
    'accounts' => [
        'client' => env('XERO_CLIENT_ACCOUNT'),
        'trading' => env('XERO_TRADING_ACCOUNT')
    ],

    'account_codes' => [
        'fee_invoice_code' => env('XERO_FEE_INVOICE_ACCOUNT_CODE')
    ],

    'invoices' => [
        'fee_invoice_type' => env('XERO_FEE_INVOICE_TYPE'),
        'rental_invoice_type' => env('XERO_RENTAL_INVOICE_TYPE')
    ],

    'branding_themes' => [
        'client' => env('XERO_CLIENT_THEME'),
        'trading' => env('XERO_TRADING_THEME')
    ],
];

