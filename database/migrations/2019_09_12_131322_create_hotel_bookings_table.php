<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id')->nullable();
            $table->integer('tenant_id')->nullable();
            $table->integer('billable_id')->nullable();
            $table->string('confirmation_reference')->nullable();
            $table->integer('search_id')->nullable();
            $table->DATE('start_date')->nullable();
            $table->DATE('finish_date')->nullable();
            $table->integer('nightly_rate')->nullable();
            $table->string('currency')->default('stirling');
            $table->integer('comm_rate')->nullable();
            $table->string('room_type')->nullable();
            $table->boolean('completed')->default(false);
            $table->string('status')->default('pending');
            $table->string('xero_ref')->nullable();
            $table->string('xero_inv_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_bookings');
    }
}
