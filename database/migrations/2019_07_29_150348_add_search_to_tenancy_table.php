<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSearchToTenancyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenancies', function($table) {
            $table->integer('search_id')->nullable(); 
            $table->integer('production_company_id')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenancies', function($table) {
            $table->dropColumn('search_id')->nullable(); 
            $table->dropColumn('production_company_id'); 
        });
    }
}
