<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('description');
            $table->integer('tenancy_id');
            $table->integer('scheduled_invoice_id')->nullable();
            $table->integer('tenant_admin_fee_id')->nullable();
            $table->integer('landlord_admin_fee_id')->nullable();
            $table->integer('deposit_id')->nullable();
            $table->integer('commission_fee_id')->nullable();
            $table->integer('tenant_admin_recharge_fee_id')->nullable();
            $table->integer('supplier_disbursement_id')->nullable();
            $table->integer('disbursement_charge_id')->nullable();
            $table->integer('landlord_payment_id')->nullable();
            $table->integer('expense_id')->nullable();
            $table->integer('disbursement_id')->nullable();
            $table->date('completed_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
