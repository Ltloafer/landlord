<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('type');
            $table->integer('amount')->nullable();
            $table->integer('vat')->nullable();
            $table->integer('tenancy_id')->nullable();
            $table->string('status')->nullable();
            $table->integer('scheduled_invoice_id')->nullable();
            $table->string('xero_inv_id')->nullable();
            $table->string('xero_ref')->nullable();
            $table->date('reconciled')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
