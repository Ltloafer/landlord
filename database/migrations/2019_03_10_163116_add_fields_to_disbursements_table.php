<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDisbursementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('disbursements', function($table) { 
            $table->integer('expense_id')->nullable();
            $table->boolean('paid')->nullable();
            $table->string('status')->nullable();
            $table->string('xero_inv_id')->nullable();
            $table->string('xero_ref')->nullable();
            $table->date('reconciled')->nullable();
            $table->integer('scheduled_invoice_id')->nullable();
            $table->integer('rent_payable_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
