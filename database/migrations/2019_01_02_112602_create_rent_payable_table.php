<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentPayableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent_payables', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('vat')->nullable();
            $table->integer('tenancy_id')->nullable();
            $table->string('status')->nullable();
            $table->integer('scheduled_invoice_id')->nullable();
            $table->string('xero_inv_id')->nullable();
            $table->string('xero_ref')->nullable();
            $table->date('reconciled')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rent_payables');
    }
}
