<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expense_type_id')->nullable();
            $table->integer('tenancy_id')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->integer('amount')->nullable();
            $table->boolean('paid')->nullable();
            $table->DATE('date');
            $table->string('status')->nullable();
            $table->string('xero_inv_id')->nullable();
            $table->string('xero_ref')->nullable();
            $table->date('reconciled')->nullable();
            $table->integer('scheduled_invoice_id')->nullable();
            $table->string('file')->nullable();
            $table->string('inv_ref')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
