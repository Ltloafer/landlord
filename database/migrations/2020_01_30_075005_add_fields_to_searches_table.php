<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('searches', function (Blueprint $table) {
            $table->integer('weekly_rate')->nullable();
            $table->string('currency')->default('stirling');
            $table->integer('notice_period')->nullable();
            $table->integer('holding_deposit')->nullable();
            $table->integer('monthly_rate')->nullable();
            $table->integer('break_clause')->nullable();
            $table->integer('comm_rate')->nullable();
            $table->string('type')->default('direct');
            $table->string('let_type');
            $table->integer('security')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
