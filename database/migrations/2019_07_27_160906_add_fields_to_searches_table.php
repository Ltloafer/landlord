<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('searches', function($table) {
            $table->integer('job_id')->nullable(); 
            $table->integer('tenant_id')->nullable(); 
            $table->string('production')->nullable();
            $table->string('production_contact')->nullable();
            $table->string('production_phone')->nullable();
            $table->string('production_email')->nullable();
            $table->DATE('start_date')->nullable();
            $table->DATE('end_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('searches', function($table) { 
            $table->dropColumn('job_id'); 
            $table->dropColumn('tenant_id'); 
            $table->dropColumn('production');
            $table->dropColumn('production_contact');
            $table->dropColumn('production_phone');
            $table->dropColumn('production_email');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
        });

    }
}
