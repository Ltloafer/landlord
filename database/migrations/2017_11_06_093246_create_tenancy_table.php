<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenancyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenancies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('property_id');
            $table->integer('rentalplan_id')->nullable();
            $table->DATE('start_date')->nullable();
            $table->DATE('finish_date')->nullable();
            $table->integer('weekly_rate')->nullable();
            $table->string('currency')->default('stirling');
            $table->integer('notice_period')->nullable();
            $table->integer('holding_deposit')->nullable();
            $table->integer('monthly_rate')->nullable();
            $table->integer('break_clause')->nullable();
            $table->integer('comm_rate')->nullable();
            $table->string('type')->default('direct');
            $table->string('let_type');
            $table->integer('total_rent')->nullable();
            $table->integer('security')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenancies');
    }
}
