<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('date')->nullable();
            $table->integer('contact_id')->nullable();
            $table->string('reference')->nullable();
            $table->string('type')->nullable();
            $table->integer('tenancy_id')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('vat')->nullable();
            $table->integer('total_amount')->nullable();
            $table->string('xero_inv_id')->nullable();
            $table->string('xero_ref')->nullable();
            $table->timestamp('due_date')->nullable();
            $table->timestamp('last_sync')->nullable();
            $table->timestamp('reconciled')->nullable();
            $table->string('status')->default('invoiced');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
