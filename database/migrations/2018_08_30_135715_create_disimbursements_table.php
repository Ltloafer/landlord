<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisimbursementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disbursements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('disbursement_type_id');
            $table->integer('amount');
            $table->integer('landlord_id')->nullable();
            $table->integer('tenant_id')->nullable();
            $table->integer('tenancy_id')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->string('to')->nullable();
            $table->string('from')->nullable();
            $table->DATE('date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disbursements');
    }
}
