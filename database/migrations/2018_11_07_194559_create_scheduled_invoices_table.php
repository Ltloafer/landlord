<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduledInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenancy_id');
            $table->DATETIME('scheduled_date')->nullable();
            $table->DATETIME('start_date')->nullable();
            $table->DATETIME('end_date')->nullable();
            $table->string('status')->default('pending');
            $table->string('xero_ref')->nullable();
            $table->string('xero_inv_id')->nullable();
            $table->integer('amount')->nullable();
            $table->text('add_info')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduled_invoices');
    }
}
