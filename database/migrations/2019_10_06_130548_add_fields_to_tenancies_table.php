<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTenanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenancies', function($table) {
            $table->boolean('charge_landlord_admin_fee')->default(true);
            $table->boolean('charge_tenant_admin_fee')->default(true);
            $table->boolean('invoice_production_company')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenancies', function($table) {
            $table->dropColumn('charge_landlord_admin_fee');
            $table->boolean('charge_tenant_admin_fee');
            $table->boolean('invoice_production_company');
        });
    }
}
