<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('landlord_id')->nullable();
            $table->integer('property_id')->nullable();
            $table->integer('tenancy_id')->nullable();
            $table->integer('tenant_id')->nullable();
            $table->text('note');
            $table->integer('search_id')->nullable();
            $table->integer('contact_id')->nullable();
            $table->integer('agent_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
