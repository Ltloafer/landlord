<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->string('line_1')->nullable();
            $table->string('line_2')->nullable();
            $table->string('line_3')->nullable();
            $table->string('line_4')->nullable();
            $table->string('type')->nullable();
            $table->string('xero_contactID')->nullable();
            $table->timestamp('last_sync')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->dropColumn('line_1');
            $table->dropColumn('line_2');
            $table->dropColumn('line_3');
            $table->dropColumn('line_4');
            $table->dropColumn('type');
            $table->dropColumn('xero_contactID');
            $table->dropColumn('last_sync');
        });
    }
}
