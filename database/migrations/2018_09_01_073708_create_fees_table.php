<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fee_type_id');
            $table->integer('amount');
            $table->integer('vat')->nullable();
            $table->integer('landlord_id')->nullable();
            $table->integer('tenant_id')->nullable();
            $table->integer('tenancy_id')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->boolean('paid')->nullable();
            $table->string('paid_method')->nullable();
            $table->DATE('date')->nullable();
            $table->string('status')->nullable();
            $table->string('xero_inv_id')->nullable();
            $table->string('xero_ref')->nullable();
            $table->date('reconciled')->nullable();
            $table->integer('scheduled_invoice_id')->nullable();
            $table->boolean('up-to-break')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fees');
    }
}
