<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductionToHotelBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_bookings', function($table) {
            $table->string('production')->nullable(); 
            $table->integer('production_company_id')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('hotel_bookings', function($table) {
            $table->dropColumn('production');
            $table->dropColumn('production_company_id');
        });
    }
}
