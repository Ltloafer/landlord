<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(PropertyDocTypesSeeder::class);
    	$this->command->info('Property document types table seeded!');

    	$this->call(LandlordDocTypesSeeder::class);
    	$this->command->info('Landord document types table seeded!');

    	$this->call(DisbursementTypesSeeder::class);
    	$this->command->info('Disbursement types table seeded!');

    	$this->call(FeeTypesSeeder::class);
    	$this->command->info('Fee types table seeded!');

    	$this->call(ExpenseTypesSeeder::class);
    	$this->command->info('Expense types table seeded!');
    }
}


class PropertyDocTypesSeeder extends Seeder {
	public function run()
	{
		DB::table('property_doc_types')->delete();

		DB::table('property_doc_types')->insert([
			'id' => 1,
			'name' => 'Electrical indemnity certificate',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('property_doc_types')->insert([
			'id' => 2,
			'name' => 'Gas safety / NO gas letter',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('property_doc_types')->insert([
			'id' => 3,
			'name' => 'Carbon monoxide indemnity',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('property_doc_types')->insert([
			'id' => 4,
			'name' => 'BBSA indemnity',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('property_doc_types')->insert([
			'id' => 5,
			'name' => 'Legionella indemnity / certificate',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);
	}
}

class LandlordDocTypesSeeder extends Seeder {
	public function run()
	{
		DB::table('landlord_doc_types')->delete();

		DB::table('landlord_doc_types')->insert([
			'id' => 1,
			'name' => 'Proof of address',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('landlord_doc_types')->insert([
			'id' => 2,
			'name' => 'Id',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('landlord_doc_types')->insert([
			'id' => 3,
			'name' => 'Proof of ownership',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('landlord_doc_types')->insert([
			'id' => 4,
			'name' => 'NRL approval number',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('landlord_doc_types')->insert([
			'id' => 5,
			'name' => 'Terms and conditions signed',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);
	}
}


class DisbursementTypesSeeder extends Seeder {
	public function run()
	{
		DB::table('disbursement_types')->delete();

		DB::table('disbursement_types')->insert([
			'id' => 1,
			'type' => 'Deposit Paid to Client Account',
			'entry' => 'credit',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('disbursement_types')->insert([
			'id' => 2,
			'type' => 'Deposit Paid to DPS',
			'entry' => 'debit',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('disbursement_types')->insert([
			'id' => 3,
			'type' => 'Rent Paid to Client Account',
			'entry' => 'credit',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('disbursement_types')->insert([
			'id' => 4,
			'type' => 'Rent Paid to Landlord',
			'entry' => 'debit',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('disbursement_types')->insert([
			'id' => 5,
			'type' => 'Admin Fee Paid by Tenant',
			'entry' => 'credit',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);
	}
}

class FeeTypesSeeder extends Seeder {
	public function run()
	{
		DB::table('fee_types')->delete();

		DB::table('fee_types')->insert([
			'id' => 1,
			'type' => 'Admin Fee Landlord',
			'entry' => 'debit',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('fee_types')->insert([
			'id' => 2,
			'type' => 'Admin Fee Tenant',
			'entry' => 'debit',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('fee_types')->insert([
			'id' => 3,
			'type' => 'Golden Ticket Commission',
			'entry' => 'debit',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);
	}
}

class ExpenseTypesSeeder extends Seeder {
	public function run()
	{
		DB::table('expense_types')->delete();

		DB::table('expense_types')->insert([
			'id' => 1,
			'type' => 'Cleaning',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);

		DB::table('expense_types')->insert([
			'id' => 2,
			'type' => 'Maintenance',
			'created_at' => new DateTime(),
			'updated_at' => new DateTime()
		]);
	}
}







