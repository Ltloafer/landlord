<?php

namespace LTLoafer\XeroLaravel\Facades;

use Illuminate\Support\Facades\Facade;

class XeroLaravelDirect extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'xerolaraveldirect';
    }
}
