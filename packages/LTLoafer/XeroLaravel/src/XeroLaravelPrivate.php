<?php

namespace LTLoafer\XeroLaravel;

use XeroPHP\Remote\URL as XeroURL;
use XeroPHP\Models\Accounting\Invoice;
use XeroPHP\Models\Accounting\Contact;
use XeroPHP\Models\Accounting\Account;
use XeroPHP\Models\Accounting\Payment;
use XeroPHP\Remote\Request as XeroRequest;
use XeroPHP\Models\Accounting\BrandingTheme;
use XeroPHP\Models\Accounting\TrackingCategory;

class XeroLaravelPrivate
{

	/**
	 * An instance of Private Application of Xero-php
	 * 
	 * @var /XeroPHP/Application/PrivateApplication
	 */
	protected $xero;

	public function __construct($xero)
	{
		$this->xero = $xero;
	}

	public function authorisedInvoicesReceivable()
	{
		return $this->xero->load(Invoice::class)
		->where('Status', Invoice::INVOICE_STATUS_AUTHORISED)
		->where('Type', Invoice::INVOICE_TYPE_ACCREC)
		->page(1)
		->execute();
	}

	public function draftInvoicesReceivable()
	{
		return $this->xero->load(Invoice::class)
		->where('Status', Invoice::INVOICE_STATUS_DRAFT)
		->where('Type', Invoice::INVOICE_TYPE_ACCREC)
		->page(1)
		->execute();
	}

	public function getInvoices()
	{
		return $this->xero->load(Invoice::class)
		// ->where('Status', Invoice::INVOICE_STATUS_AUTHORISED)
		// ->where('Type', Invoice::INVOICE_TYPE_ACCREC)
		->page(1)
		->execute();
	}

	public function invoicesPayable()
	{
		return $this->xero->load(Invoice::class)
		// ->where('Status', Invoice::INVOICE_STATUS_DRAFT)
		->where('Type', Invoice::INVOICE_TYPE_ACCPAY)
		->page(1)
		->execute();
	}

	public function getInvoiceById($invoiceNumber)
	{
		return $this->xero->loadByGUID('Accounting\\Invoice', $invoiceNumber);
	}

	public function getAccountById($id)
	{
		return $this->xero->loadByGUID(Account::class, $id);
	}

	public function getAccounts()
	{
		return $this->xero->load(Account::class)->execute();
	}

	public function createXeroAccountCodeForProperty($property)
	{
		$account = new Account($this->xero);

		$account->setCode('PC' . $property->id)
		->setName($property->address)
		->setType('LIABILITY')
		->setTaxType('NONE')
		->save();

		$property->xero_code = $account->AccountID;
		$property->account_code = $account->Code;
		$property->save();
	}

	public function getXeroInvoiceUrl($invoiceId)
	{
		$url = new XeroURL($this->xero, sprintf('%s/%s/OnlineInvoice', 'Invoices', $invoiceId));

		$request = new XeroRequest($this->xero, $url);
		$request->send();

		$online_invoices = $request->getResponse()->getElements();

		return $online_invoices[0]['OnlineInvoiceUrl'];
	}

	public function createInvoice()
	{
		return new Invoice();
	}

	public function saveInvoice($invoice)
	{
		// dd($invoice);

		$this->xero->save($invoice);

		return 'Saved';
	}

	public function getAllInvoicePayments($invoice, $xeroRef)
	{
		// dd($xeroRef);
		return $this->xero->load(Payment::class)
    // ->where('Status', Invoice::INVOICE_STATUS_AUTHORISED)
		->where('Reference', $xeroRef)
    // ->where('Type', Invoice::INVOICE_TYPE_ACCREC)
		->execute();
	}

	public function getPayments()
	{
		return $this->xero->load(Payment::class)->execute();
	}



	public function createPayment()
	{
		return new Payment();
	}

	public function savePayment($payment)
	{
		$this->xero->save($payment);
		
	}

	public function getContacts()
	{
		$contacts = $this->xero->load(Contact::class)->execute();

		return $contacts;
	}

	public function getContact($guid)
	{
		$contact = $this->xero->loadByGUID(Contact::class, $guid);

		return $contact;
	}

	public function createContact($resource)
	{
		$contact =  new Contact($this->xero);

		$contact->setName($resource->name)
		->setEmailAddress($resource->email)
		->setDefaultCurrency('GBP')
		->setContactStatus('ACTIVE');

		$contact->save();

		return $contact;
	}

	public function updateContact($resource)
	{
		$xeroContact = $this->getContact($resource->xero_contactID);

		$xeroContact->setName($resource->name)
		->setEmailAddress($resource->email)
		->setContactStatus('ACTIVE');

		$xeroContact->save();
	}

	public function updateInvoiceBrandingThemeToTrading($invoiceNumber)
	{
		$invoice = $this->getInvoiceById($invoiceNumber);

		$invoice->setBrandingThemeID(config('xero.branding_themes.trading'));

		$invoice->save();
	}

	public function getTrackingCategories()
	{
		$cats = $this->xero->load(TrackingCategory::class)->execute();

		return $cats;
	}

	public function getTrackingCategory($guid)
	{
		$cat = $this->xero->loadByGUID(TrackingCategory::class, $guid);

		return $cat;
	}

	public function getBrandingThemes()
	{
		$themes = $this->xero->load(BrandingTheme::class)->execute();

		return $themes;
	}
	


}