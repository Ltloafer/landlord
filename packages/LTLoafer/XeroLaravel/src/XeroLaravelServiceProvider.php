<?php

namespace LTLoafer\XeroLaravel;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

use Carbon\Carbon;

class XeroLaravelServiceProvider extends ServiceProvider 
{
      /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
      protected $defer = true;

      
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
        
        
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $config = $this->app->config->get('xero');
        $tenantId = config('xero.oauth2.tenant_id');
        $tenantId_direct = config('xero.oauth2.tenant_id_direct');
        
         $this->app->bind('XeroPrivate', function () use ($config) {
                return new \XeroPHP\Application\PrivateApplication($config);
            });
            $this->app->bind('XeroPublic', function () use ($config) {
                return new \XeroPHP\Application\PublicApplication($config);
            });
            $this->app->bind('XeroPartner', function () use ($config) {
                return new \XeroPHP\Application\PartnerApplication($config);
            });
            $this->app->bind('XeroInvoice', function(){
               return new \XeroPHP\Models\Accounting\Invoice();
           });
            $this->app->bind('XeroPayment', function(){
               return new \XeroPHP\Models\Accounting\Payment();
           });
            $this->app->bind('XeroInvoiceLine', function(){
                return new \XeroPHP\Models\Accounting\Invoice\LineItem();
            });
            $this->app->bind('XeroContact', function(){
                return new \XeroPHP\Models\Accounting\Contact();
            });
        
        /**
        1.   Check if there is a token in the database
        2.   Check if token expired
        3.   If not expired, use the token
        4.   If expired, use the refresh token
        5.   Get a new token, store the access token and refresh token, and the expiry dates.
        6.   Use the new token.
        
        When using a refresh token the received token contains a new refresh token. Thie refresh token must be used for future token refreshes. The older (not used) refresh token can be used for another 30 mins following a new one being issued but not after that, The lastest one will last 60 days. 
        
        **/
        
        
        
        if (! $token = DB::table('xero_tokens')->orderBy('id', 'desc')->take(1)->first()) { 
            // needs to be done by latest to be cleaner - but needs created_at 
            $token = $this->checkForToken();
        } else {
            
            try {
                
                if ($this->isExpired($token)) {
                    $token = $this->useRefreshToken($token);
                    
                } else {
                    $token = $token->access_token;
                }
                
            } catch (Exception $e) {
                
                $token = $this->checkForToken();
            }
        }
        
        $this->app->singleton('xerolaravel', function () use ($token, $tenantId) {
            return new XeroLaravelPrivate(new \XeroPHP\Application($token, $tenantId));
        });
        
        $this->app->singleton('xerolaraveldirect', function () use ($token, $tenantId_direct) {
            return new XeroLaravelPrivate(new \XeroPHP\Application($token, $tenantId_direct));
        });
    }
    
    private function checkForToken()
    {
        $provider = new \Calcinai\OAuth2\Client\Provider\Xero([
            'clientId' => config('xero.oauth2.client_id'),
            'clientSecret' => config('xero.oauth2.client_secret'),
            'state' =>  '123',
            'redirectUri' => config('xero.oauth2.redirect_uri')
        ]);
        
        if (!isset($_GET['code'])) {

            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl([
                'scope' => 'openid profile email accounting.contacts accounting.settings accounting.transactions offline_access'
            ]);

            // $_SESSION['oauth2state'] = $provider->getState();
            Session::put('oauth2state',  $provider->getState());
            // $_SESSION['oauth2state'] = $provider->getState();
            header('Location: ' . $authUrl);
            exit;

        // Check given state against previously stored one to mitigate CSRF attack
        // } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
        // } elseif (empty($_GET['state']) || ($_GET['state'] !== Session::get('oauth2state'))) {
        } elseif (empty($_GET['state'])) {
            
            // unset($_SESSION['oauth2state']);
            Session::forget('oauth2state');
            exit('Invalid state');

        } else {
            
            // Try to get an access token (using the authorization code grant)
            
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
            
            dd($provider->getTenants($token));
            
            DB::table('xero_tokens')->insert([
                'access_token' => $token,
                'refresh_token' => $token->getRefreshToken(),
                'created_at' => Carbon::now()
            ]);
            
            return $token;
        }
    }
    
    private function isExpired($token)
    {
        return (new Carbon($token->created_at))->diffInMinutes(Carbon::now()) > 15;
    }
    
    private function useRefreshToken($token)
    {
        $provider = new \Calcinai\OAuth2\Client\Provider\Xero([
            'clientId' => config('xero.oauth2.client_id'),
            'clientSecret' => config('xero.oauth2.client_secret'),
            'state' =>  '123',
            'redirectUri' => config('xero.oauth2.redirect_uri')
        ]);
        
        $newToken = $provider->getAccessToken('refresh_token', [
            'grant_type' => 'refresh_token',
            'refresh_token' => $token->refresh_token
        ]);
        
        DB::table('xero_tokens')->insert([
            'access_token' => $newToken,
            'refresh_token' => $newToken->getRefreshToken(),
            'created_at' => Carbon::now()
        ]);
        
        return $newToken;
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['xerolaravel', 'xerolaraveldirect'];
    }
    
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        $this->publishes([
            __DIR__.'/../config/xerolaravel.php' => config_path('xerolaravel.php'),
        ], 'xerolaravel.config');
    }
}
