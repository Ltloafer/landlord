<?php 

namespace LtLoafer\XeroLaravel\Models;

use XeroPHP\Models\Accounting\Invoice;

class InvoiceMethods
{
	public function authorisedInvoicesReceivable()
	{
		return $this->xero->load(Invoice::class)
		->where('Status', Invoice::INVOICE_STATUS_AUTHORISED)
		->where('Type', Invoice::INVOICE_TYPE_ACCREC)
		->page(1)
		->execute();
	}
}