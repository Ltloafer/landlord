<?php

namespace LTLoafer\XeroLaravel;

use XeroPHP\Remote\URL as XeroURL;
use XeroPHP\Models\Accounting\Invoice;
use XeroPHP\Remote\Request as XeroRequest;

class XeroLaravelInvoice extends Invoice
{
	public function __construct()
	{
		parent::__construct();
	}

}