<?php

// Available placeholders: LTLoafer, XeroLaravel, ltloafer, xerolaravel
return [
    'src/MyPackage.php' => 'src/XeroLaravel.php',
    'config/mypackage.php' => 'config/xerolaravel.php',
    'src/Facades/MyPackage.php' => 'src/Facades/XeroLaravel.php',
    'src/MyPackageServiceProvider.php' => 'src/XeroLaravelServiceProvider.php',
];