<strong>Split Commission Schedule</strong>
<div class="row">
    <div class="col-md-12">
        <div class="inset-items">
            <table class="table m-b-none">
                <thead>
                    <!-- <th></th> -->
                    <th>Period</th>
                    <th>Run date</th>
                    <th>Rental amount</th>
                    <th>Commission</th>
                    <th>Status</th>
                    <th></th>
                </thead>

                <tbody>

                    @foreach ($scheduledPeriods as $period)

                    @if ($period->rentPayableStatus == 'PAID')
                    <tr class="table table-warning">
                        <!-- <td>{{ $period->id }}</td> -->
                        <td>{{ $period->period }}</td>
                        <td>{{ $period->scheduledDate }}</td>
                        <td>{{ $period->rentalAmount }}</td>
                        <td>{{ $period->commission }}</td>
                        <td>{{ $period->status }}</td>
                        <td></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/" v-on:click.prevent="viewLandlordStatement({{ $period->id }})">View statement</a>
                                </div>
                            </div>
                        </td>
                    </tr>


                    @elseif ($period->status == 'RUN')
                    <tr class="table table-success">
                        <!-- <td>{{ $period->id }}</td> -->
                        <td>{{ $period->period }}</td>
                        <td>{{ $period->scheduledDate }}</td>
                        <td>{{ $period->rentalAmount }}</td>
                        <td>{{ $period->commission }}</td>
                        <td>{{ $period->status }}</td>
                        <td></td>
                        <td></td>
                    </div>
                </td>
            </tr>


            @elseif ($period->rentPayableStatus == 'AUTHORISED')
            <tr class="table table-danger">
                <!-- <td>{{ $period->id }}</td> -->
                <td>{{ $period->period }}</td>
                <td>{{ $period->scheduledDate }}</td>
                <td>{{ $period->rentalAmount }}</td>
                <td>{{ $period->commission }}</td>
                <td>{{ $period->status }}</td>
                <td></td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="/" v-on:click.prevent="openLandlordBill({{ $period->id }})">Mark as paid</a>
                            <a class="dropdown-item" href="/" v-on:click.prevent="viewLandlordStatement({{ $period->id }})">View statement</a>
                        </div>
                    </div>
                </td>
            </tr>

            @else

            <tr>
                <!-- <td>{{ $period->id }}</td> -->
                <td>{{ $period->period }}</td>
                <td>{{ $period->scheduledDate }}</td>
                <td>{{ $period->rentalAmount }}</td>
                <td>{{ $period->commission }}</td>
                <td>{{ $period->status }}</td>
                <td></td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="/" v-on:click.prevent="editScheduledInvoice({{ $period->id }})">Edit</a>
                            <a class="dropdown-item" href="/" v-on:click.prevent="viewLandlordStatement({{ $period->id }})">View statement</a>
                        </div>
                    </div>
                </td>
            </tr>
            @endif

            @endforeach

        </tbody>
    </table>
</div>


@if (! $tenancy->scheduledInvoices->count() > 0)
<form class="form-horizontal" method="POST" action="/generateScheduledInvoices">
    <input type="hidden" name="tenancyId" value="{{ $tenancy->id }}">
    {{ csrf_field() }}
    <button type="submit" class="btn btn-sm btn-outline-primary float-right">Generate schedule</button>
</form>
@endif

</div>
</div>