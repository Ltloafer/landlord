@extends('layouts.layout')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
            <div class="card-header">Tenancies
             <!--    <div class="dropdown float-right display-inline">
                    <button class="btn btn-sm btn-outline-secondary add-btn float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Add a tenancy
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" data-toggle="modal" data-target="#add-direct-tenancy">Direct let (AST)</a>
                        <a class="dropdown-item" data-toggle="modal" data-target="#add-dir-install-tenancy">Short let (installments)</a>
                        <a class="dropdown-item" data-toggle="modal" data-target="#add-splitComm-tenancy">Split comm </a>
                    </div>
                </div> -->
            </div>

            <div class="card-body">
                <table class="table m-b-none">
                    <thead>
                        <th>Property</th>
                        <th>Reference</th>
                        <th>Status</th>
                        <th>Type</th>
                        <th>Tenant</th>
                        <th>Start date</th>
                        <th>End date</th>
                    </thead>

                    <tbody>
                        @foreach($tenancies as $tenancy)
                        <tr>
                            <td>{{ $tenancy->property->address }}</td>
                            <td>{{ $tenancy->reference }}</td>
                            <td>{{ $tenancy->status }}</td>
                            <td>{{ $tenancy->displayTenancyType() }}</td>
                            <td>{{ $tenancy->tenant->name}}</td>
                            <td>{{ $tenancy->start_date->format('jS \\of F Y') }}</td>
                            <td>{{ $tenancy->finish_date->format('jS \\of F Y') }}</td>
                            <td><a href="tenancies/{{$tenancy->id}}" class="btn btn-sm btn-outline-secondary">View</a></td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>



                @include('Modals.add-tenancy-form')
                @include('Tenancies.Modals.add-direct-tenancy')
                @include('Tenancies.Modals.add-direct-tenancy-installments')
                @include('Tenancies.Modals.add-splitComm-tenancy')

            </div>
        </div>
    </div>
</div>
</div>

@endsection
