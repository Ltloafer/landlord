     <!-- <strong>Disbursements</strong> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="inset-items">
                    <table class="table m-b-none">
                      <thead>
                        <th></th>
                        <th>Date</th>
                        <th>Type</th>
                        <th></th>
                        <th>Credit</th>
                        <th>Debit</th>
                      </thead>

                      <tbody>
                        @foreach ($tenancy->disbursements as $disbursement)
                        <tr>
                          <td></td>
                          <td>{{ $disbursement->date->format('jS \\of F Y') }}</td>

                          <td>{{ $disbursement->disbursementType->type }}</td>

                          <td></td>

                          @if ($disbursement->isCredit())
                          <td>{{ $disbursement->displayAmount() }}</td>
                          <td></td>
                          @else 
                          <td></td>
                          <td>{{ $disbursement->displayAmount() }}</td>
                          @endif

                        </tr>
                        @endforeach
                      </tbody>

                      <tfoot>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td><strong>Totals £</strong></td>
                          <td><strong>{{ $tenancy->displayTotalDisbursementCredits() }}</strong></td>
                          <td><strong>{{ $tenancy->displayTotalDisbursementDebits() }}</strong></td>
                        </tr>

                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td><strong>Balance £ </strong></td>
                          <td><strong>{{ $tenancy->displayDisbursementBalance() }}</strong></td>
                        </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>