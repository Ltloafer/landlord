@extends('layouts.layout')
@section('content')

<tenancy tenancytype="{{ $tenancy->type }}" inline-template>
    <div>

        <div class="container-fluid" v-cloak>
            <div class="row justify-content-center">
                <div class="col-md-12 mt-3">
                    <span class="float-right mr-3">
                        <div class="btn-group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                <a class="dropdown-item" data-toggle="modal" data-target="#addTenancyExpense">Add disbursement</a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#addTenancyFee">Add fees</a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#newScheduledInvoice">New scheduled period</a>
                                <a class="dropdown-item" href="/scheduledInvoices/run/{{ $tenancy->id }}">Run scheduler</a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#setReference">Set reference</a>
                                
                            </div>
                        </div>
                    </span>
                    
                 
                    <div class="">
                       <tabs>
                        <tab name="Summary">
                            @include('Tenancies._details')
                        </tab>
                        <tab name="Schedule">
                            @include('Tenancies._schedule')
                        </tab>
                        <tab name="Invoices">
                            @include('Tenancies._invoices')
                        </tab>

                        @if ($tenancy->pendingActions->count() > 0)
                        <tab name="Actions" suffix="<span class='suffix'>{{ $tenancy->pendingActions->count() }}</span>"> 
                            @else
                            <tab name="Actions"> 
                                @endif
                                <div class="actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="inset-items">
                                                <table class="table m-b-none">
                                                    <tbody>
                                                        @if ($tenancy->pendingActions->count() < 1)
                                                        <p>There are no pending actions for this tenancy.</p>
                                                        @endif

                                                        @foreach ($tenancy->pendingActions as $action)
                                                        <tr>
                                                            <td><i class="fas fa-exclamation-circle fa-2x"></i></td>

                                                            <td>
                                                                <strong>{{ $action->description }}</strong>
                                                                <div>{{ $action->getActionDetails() }}</div>
                                                            </td>

                                                            <td>
                                                                @if ($action->isForAPeriod())
                                                                <strong>For period</strong>
                                                                <div>{{ $action->scheduledInvoice->getPeriodDates() }}</div>
                                                                @endif
                                                            </td>

                                                            <td>
                                                                @if ($action->isARentalPayment())

                                                                <button class="btn btn-outline-success" v-on:click.prevent="openAction({{ $action }})"><i class="fa fa-check fa-1x mr-1"></i> Confirm receipt of payment</button>

                                                                @else

                                                                <button class="btn btn-outline-primary" v-on:click.prevent="openAction({{ $action }})"><i class="fa fa-check fa-1x mr-1"></i>Confirm payment</button>

                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tab>



                            <tab name="Rental schedule">
                                @include('Tenancies._scheduled-invoices')
                            </tab>

                            @if (! $tenancy->isViaAgent())
                            <tab name="Landlord schedule">
                                @include('Tenancies._landlord-schedule')
                            </tab>
                            <tab name="Disbursements">
                                @include('Tenancies._expenses')
                            </tab>
                            <tab name="Deposits">
                                @include('Tenancies._deposits')
                            </tab>
                            @endif
                            <tab name="Fees">
                                @include('Tenancies._fees')
                            </tab>
                        </tabs>
                    </div>
                </div>

                @include('Tenancies.Modals.add-disbursement')
                @include('Tenancies.Modals.add-fee')
                @include('Tenancies.Modals.add-expense')
                @include('Tenancies.Modals.add-scheduled-invoice')
                @include('Tenancies.Modals.set-reference')

                <div v-if="openRentalSchedule">
                    <scheduled-invoice :scheduledinvoiceid="scheduledInvoiceId" :scheduledaction="scheduledAction" inline-template>
                        <div>
                            @include('Tenancies.Modals.edit-scheduled-invoice')
                            @include('Tenancies.Modals.delete-scheduled-invoice')
                        </div>
                    </scheduled-invoice> 
                </div>

                <div v-if="editLandlordSchedulePeriod">
                    <landlord-schedule :scheduledinvoiceid="scheduledInvoiceId" inline-template>
                        <div>
                            @include('Tenancies.Modals.edit-landlord-schedule')
                        </div>
                    </landlord-schedule>
                </div>

                <div v-if="viewStatement">
                    <landlord-statement :scheduledinvoiceid="scheduledInvoiceId"></landlord-statement>
                </div>

                <div v-if="openLandlordBillModel">
                    <landlord-bill :scheduledinvoiceid="scheduledInvoiceId" :actionid="actionId"></landlord-bill>
                </div>

                <div v-if="openRentalInvoiceModal">
                    <rental-invoice :scheduledinvoiceid="scheduledInvoiceId" :actionid ="actionId"></rental-invoice>
                </div>

                <div v-if="openFeeInvoiceModal">
                    <fee-invoice :fee="fee" :actionid="actionId"></fee-invoice>
                </div>

                <invoice_fee :fee="fee" v-if="invoiceFeeModal"></invoice_fee>
                
                <delete_fee :fee="fee" :feeid="feeId" v-if="deleteFeeModal"></delete_fee>

                <div v-if="openFeeModal">
                    <fee :feeid="feeId" inline-template>
                        <div>
                            @include('Tenancies.Modals.edit-fee')
                        </div>
                    </fee>
                </div>

                <div v-if="openExpenseModal">
                    <expense :expense="expense" :actionid="actionId"></expense>
                </div>

                <div v-if="splitCommView" class="card-body">
                    @include('Tenancies._split-comm')
                </div>

            </div>
        </div>
    </div>
</tenancy>

@endsection
