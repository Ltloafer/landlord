<!-- <strong>Rental schedule</strong> -->
<div class="row">
    <div class="col-md-12">
        <div class="inset-items">
            <table class="table m-b-none">
                <thead>
                    <th>Date</th>
                    <th></th>
                    <!-- <th>Id</th> -->
                    <th>£</th>
                    <th>£</th>
                    <th>Status</th>
                </thead> 

                <tbody>

                    @if ($tenancy->isDirect())

                    @foreach($schedule as $item)

                    @if ($item instanceof \App\ScheduledInvoice)
                    <tr>
                        <td>{{ $item->scheduled_date->format('jS \\of F y') }}</td>
                        <td>Rental Invoice <br> <small>{{ $item->getPeriodDates() }}</small></td>
                        <td>{{ $item->displayAmount() }}</td>
                        <td></td>
                        <td>{{ $item->status }} <br><small>{{ $item->getInvoiceStatus() }}</small></td>
                    </tr>

                    @elseif ($item instanceof \App\Rentpayable)
                    <tr>
                        <td>{{ optional($item->date)->format('jS \\of F y') }}</td>
                        <td>Landlord payment  <br> <small>{{ $item->scheduledInvoice->getPeriodDates() }}</small></td>
                        <td></td>
                        <td>{{ $item->displayAmount() }}</td>
                        <td>{{ $item->status }} <br><small>{{ $item->getInvoiceStatus() }}</small>
                        </tr>

                        @elseif ($item instanceof \App\Fee)
                        <tr>
                            <td>{{ optional($item->itemDate)->format('jS \\of F y') }}</td>
                            <td>{{ $item->feeType->type }}

                                @if ($item->fee_type_id == 3)

                                <br>
                                @if ($item->tenancy->comm_charging == 'all_upfront') 
                                <small>All upfront</small>
                                @elseif ($item->scheduledInvoice)
                                <small>{{ $item->scheduledInvoice->getPeriodDates() }}</small>
                                @endif
                                @endif

                            </td>
                            <td>{{ $item->displayAmount() }}</td>
                            <td></td>
                            <td>{{ $item->status }} <br><small>{{ $item->getInvoiceStatus() }}</small></td>
                        </tr>

                        @elseif ($item instanceof \App\Deposit)
                        <tr>
                            <td>{{ optional($item->itemDate)->format('jS \\of F y') }}</td>
                            <td>{{ $item->itemType }}</td>
                            <td>{{ $item->displayAmount() }}</td>
                            <td></td>
                            <td>{{ $item->status }} <br><small>{{ $item->getInvoiceStatus() }}</small>
                            </tr>

                            @else

                            <tr>
                                <td>{{ optional($item->itemDate)->format('jS \\of F y') }}</td>
                                <td>{{ $item->itemType }}</td>
                                <td>{{ $item->displayAmount() }}</td>
                                <td></td>
                                <td>{{ $item->status }} <br><small>{{ $item->getInvoiceStatus() }}</small>
                                </tr>

                                @endif

                                @endforeach

                                @else

                                @foreach($schedule as $item)

                                @if ($item instanceof \App\Fee)
                                <tr>
                                    <td>{{ optional($item->itemDate)->format('jS \\of F y') }}</td>
                                    <td>{{ $item->feeType->type }}

                                        @if ($item->fee_type_id == 3)

                                        <br>
                                        @if ($item->tenancy->comm_charging == 'all_upfront') 
                                        <small>All upfront</small>
                                        @elseif ($item->scheduledInvoice)
                                        <small>{{ $item->scheduledInvoice->getPeriodDates() }}</small>
                                        @endif
                                        @endif

                                    </td>
                                    <td>{{ $item->displayAmount() }}</td>
                                    <td></td>
                                    <td>{{ $item->status }} <br><small>{{ $item->getInvoiceStatus() }}</small></td>
                                </tr>
                                

                                @endif

                                @endforeach

                                @endif













<!-- 
                    @foreach($tenancy->holdingDeposits as $deposit)
                    <tr>
                        <td>{{ $deposit->date->format('d/m/y') }}</td>
                        <td>Holding deposit</td>
                        <td></td>
                        <td>{{ $deposit->displayAmount() }}</td>
                        <td>{{ $deposit->xero_ref }}</td>
                        <td>Status</td>
                    </tr>
                    @endforeach

                    @foreach($tenancy->adminFees as $fee)
                    <tr>
                        <td>{{ $fee->date->format('d/m/y') }}</td>
                        <td>{{ $fee->feeType->type }}</td>
                        <td></td>
                        <td>{{ $fee->displayAmount() }}</td>
                        <td>{{ $fee->xero_ref }}</td>
                        <td>Status</td>
                    </tr>
                    @endforeach

                    @foreach($tenancy->scheduledInvoices as $period)
                    <tr>
                        <td>{{ $period->scheduled_date->format('d/m/y') }}</td>
                        <td>Rental payment</td>
                        <td></td>
                        <td>{{ $period->displayAmount() }}</td>
                        <td>{{ $period->xero_ref }}</td>
                        <td>Status</td>
                    </tr>

                    @foreach($period->expenses as $expense)
                    <tr>
                        <td>{{ $expense->date->format('d/m/y') }}</td>
                        <td></td>
                        <td>{{ $expense->expenseType->type }}</td>
                        <td>{{ $expense->displayAmount() }}</td>
                        <td>{{ $expense->xero_ref }}</td>
                        <td>Status</td>
                    </tr>
                    @endforeach


                    @if ($period->rentPayable)
                    <tr>
                        <td>{{ $period->rentPayable->date->format('d/m/y') }}</td>
                        <td></td>
                        <td>Landlord payment</td>
                        <td>{{ $period->rentPayable->displayAmount() }}</td>
                        <td>{{ $period->rentPayable->xero_ref }}</td>
                        <td>Status</td>
                    </tr>
                    @endif -->

                    <!-- @endforeach -->


                </tbody>
            </table>
        </div>
    </div>
</div>