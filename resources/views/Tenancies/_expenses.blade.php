<!-- <strong>Disbursements</strong> -->
<div class="row">
  <div class="col-md-12">
    <div class="inset-items">
      <table class="table m-b-none">
        <thead>
          <th>Date</th>
          <th>Type</th>
          <th>Supplier</th>
          <th>Xero Ref</th>
          <th>Status</th>
          <th>Amount</th>
        </thead>

        <tbody>
          @foreach ($expenses as $expense)

          @if ($expense->xeroStatus == 'PAID')
          <tr class="table table-warning"> 
            <td>{{ $expense->date->format('jS \\of F Y') }}</td>
            <td>{{ $expense->expenseType->type }}</td>
            <td>{{ $expense->supplier->name }}</td>
            <td>{{ $expense->xero_ref }}</td>
            <td>{{ $expense->xeroStatus }}</td>
            <td>{{ $expense->displayAmount() }}</td>
          </tr>

          @elseif ($expense->xeroStatus == 'RECONCILED')
          <tr class="table table-success">
           <td>{{ $expense->date->format('jS \\of F Y') }}</td>
           <td>{{ $expense->expenseType->type }}</td>
           <td>{{ $expense->supplier->name }}</td>
           <td>{{ $expense->xero_ref }}</td>
           <td>{{ $expense->xeroStatus }}</td>
           <td>{{ $expense->displayAmount() }}</td>
         </tr>

         @elseif ($expense->xeroStatus == 'AUTHORISED')
         <tr class="table table-danger">
           <td>{{ $expense->date->format('jS \\of F Y') }}</td>
           <td>{{ $expense->expenseType->type }}</td>
           <td>{{ $expense->supplier->name }}</td>
           <td><a href="/expenses/{{ $expense->id}}">{{ $expense->xero_ref }}</a></td>
           <td>{{ $expense->xeroStatus }}</td>
           <td>{{ $expense->displayAmount() }}</td>
         </tr>

         @else
         <tr>
           <td>{{ $expense->date->format('jS \\of F Y') }}</td>
           <td>{{ $expense->expenseType->type }}</td>
           <td>{{ $expense->supplier->name }}</td>
           <td>{{ $expense->xero_ref }}</td>
           <td>{{ $expense->xeroStatus }}</td>
           <td>{{ $expense->displayAmount() }}</td>
         </tr>
         @endif

         @endforeach
       </tbody>

       <tfoot>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td><strong>Total £</strong></td>
          <td><strong>{{ $tenancy->displayExpensesTotal() }}</strong></td>
        </tr>
      </tfoot>
    </table>

  </div>
</div>
</div>