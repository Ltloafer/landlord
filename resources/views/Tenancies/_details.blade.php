
<div class="row">
    <div class="col-sm-4">
        <address>
            <strong>Reference<span class="search-attribute">{{ $tenancy->reference }}</span></strong> <br>
        </address>
    </div>
</div>

<div class="row mt-1">

    <div class="col-sm-4">
        <address>
            <strong>Property</strong><span class="search-attribute">{{ $tenancy->property->address ?? '' }}</span> <br>
            <hr>

            <div class="text-muted">

                <strong>Reference<span class="search-attribute">{{ $tenancy->reference }} </span></strong><br><br>
                
                Postcode<span class="search-attribute">{{ $tenancy->property->postcode ?? '' }} </span><br><br>


                @if ($tenancy->property)

                @if ($tenancy->property->landlord)
                Type<span class="search-attribute">Direct</span><br>
                Landlord<span class="search-attribute">{{ $tenancy->property->landlord->name ?? '' }} </span><br>

                @elseif ($tenancy->property->agent)
                Type<span class="search-attribute">Split commission</span><br>
                Agent<span class="search-attribute">{{ $tenancy->property->agent->name ?? '' }} </span><br>
                @endif

                @else
                <br><br>
                @endif


                <!-- Phone <span class="search-attribute">{{ $tenancy->property->phone ?? $tenancy->phone }} </span><br> -->
                <!-- Email <span class="search-attribute">{{ $tenancy->tenant->email ?? $tenancy->email}} </span><br><br> -->
                <!-- Agent<br> -->
                <br>

                Invoice admin fee<span class="search-attribute">{{ $tenancy->invoiceLandlordAdminFee() }}</span><br>
            </div>

        </address>
    </div>

    <div class="col-sm-4">
        <address>
            <strong>Tenant</strong> <span class="search-attribute">{{ $tenancy->tenant->name ?? '' }}</span>
            <div class="text-muted">

                <hr>
                Address <span class="search-attribute">{{ $tenancy->tenant->address ?? '' }}</span> <br>
                Postcode <span class="search-attribute">{{ $tenancy->tenant->postcode ?? '' }} </span><br>
                Phone <span class="search-attribute">{{ $tenancy->tenant->phone ?? $tenancy->phone }} </span><br>
                Email <span class="search-attribute">{{ $tenancy->tenant->email ?? $tenancy->email}} </span><br><br>
                Invoice admin fee <span class="search-attribute">{{ $tenancy->invoiceTenantAdminFee() }}</span><br>
            </div>
        </address>
    </div>


    <div class="col-sm-4">
        <address>
            <strong>Production company</strong><span class="search-attribute">{{ $tenancy->productionCompany->name ?? 'n/a' }}</span>
            <div class="text-muted">
                <hr>
                Address <span class="search-attribute">{{ $tenancy->productionCompany->address ?? '' }}</span> <br>
                Postcode <span class="search-attribute">{{ $tenancy->productionCompany->postcode ?? '' }} </span><br>
                Phone <span class="search-attribute">{{ $tenancy->productionCompany->phone ?? '' }} </span><br>
                Email <span class="search-attribute">{{ $tenancy->productionCompany->email ?? '' }} </span><br><br>
                Invoice production company <span class="search-attribute">{{ $tenancy->invoicesProductionCompany() }}</span><br>
            </div>
        </address>
    </div>
</div>


<div class="row mt-5">

    <div class="col-sm-4">
        <address>
            <strong>Tenancy</strong>
            <span class="search-attribute">{{ $tenancy->start_date->format('jS \\of F Y') }} - {{ $tenancy->finish_date->format('jS \\of F Y') }}</span>

            <hr>

            <div class="text-muted">
                Minimum notice period<span class="search-attribute">{{ $tenancy->notice_period }} days.</span><br>
                Commission rate<span class="search-attribute">{{ $tenancy->displayCommissionRate() }}% </span><br>
                Commission type<span class="search-attribute">{{ $tenancy->displayCommissionType() }} </span><br>
                Holding deposit<span class="search-attribute">£{{ $tenancy->getHoldingDeposit() }} </span><br>
                Security deposit<span class="search-attribute">£{{ $tenancy->getSecurityDeposit() }} </span><br>
                Break clause period<span class="search-attribute">{{ $tenancy->break_clause }} months </span><br>
                Earliest break-out date<span class="search-attribute">{{ $tenancy->getBreakOutDate() }} </span><br>
                Earliest finish date (inc. notice period)<span class="search-attribute">{{ $tenancy->getEarliestFinishDate() }} </span><br>
            </div>
        </address>
    </div>

    <div class="col-sm-4">
        <address>
            <strong>Installments</strong>
            @if ($tenancy->hasInstallments())
            <span class="search-attribute">{{ $tenancy->installments->count() }}</span>
            @endif
            <hr>

            <div class="text-muted">
                @foreach ($tenancy->installments as $installment)
                {{ $installment->payment_date->format('jS \\of F Y') }}<span class="search-attribute">£{{ $installment->getAmount() }}</span><br>
                @endforeach
            </div>
        </address>
    </div>
    
    <div class="col-sm-4">
        <address>
            <strong>Status</strong>
            
            <span class="search-attribute">{{ $tenancy->status }}</span>
            <hr>
            
            
            <span class="search-attribute">
                <div class="text-muted">
                    @if (! $tenancy->completed())
                    <form class="form-horizontal" method="POST" action="/tenancies/{{ $tenancy->id }}/setStatus">
                        <input type="hidden" name="status" value="completed">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-outline-secondary btn-sm">Mark as completed</button>
                    </form>
                    @else
                    <form class="form-horizontal" method="POST" action="/tenancies/{{ $tenancy->id }}/setStatus">
                        <input type="hidden" name="status" value="running">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-outline-secondary btn-sm">Mark as un-completed</button>
                    </form>
                    @endif
                </div>
            </span>
            
            
            
        </address>
    </div>
</div>

