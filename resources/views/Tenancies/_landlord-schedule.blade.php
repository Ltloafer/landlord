<!-- <strong>Landlord period</strong> -->
<div class="row">
    <div class="col-md-12">
        <div class="inset-items">
            <table class="table m-b-none">
                <thead>
                    <!-- <th></th> -->
                    <th>Period</th>
                    <th>Date of invoicing</th>
                 <!--    <th>Rent</th>
                    <th>Commission</th>
                    <th>Admin charges</th> -->
                    <th>Disbursements</th>
                    <th>Amount due</th>
                    <th>Inv id</th>
                    <th>Xero Inv. Ref.</th>
                    <th>Status</th>
                    <th></th>
                </thead>

                <tbody>

                    @foreach ($scheduledPeriods as $period)

                    <!-- {{ $period->rentPayableStatus }} -->

                    @if ($period->rentPayableStatus == 'PAID')
                    <tr class="table table-warning">
                        <!-- <td>{{ $period->id }}</td> -->
                        <td>{{ $period->period }}</td>
                        <td>{{ $period->newdate }}</td>
                      <!--   <td>{{ $period->rentPayableAmount }}</td>
                        <td>{{ $period->commission }}</td>
                        <td>{{ $period->adminFees }}</td> -->
                        <td>{{ $period->disbursements }}</td>
                        <td>{{ $period->paymentDue }}</td>
                        <td>{{ $period->rentPayable->invoice_id ?? '' }}</td>
                        <td>{{ $period->landlordXeroInvRef }}</td>
                        <td>{{ $period->rentPayableStatus }}</td>
                        <td></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/" v-on:click.prevent="viewLandlordStatement({{ $period->id }})">View statement</a>
                                </div>
                            </div>
                        </td>
                    </tr>


                    @elseif ($period->rentPayableStatus == 'RECONCILED')
                    <tr class="table table-success">
                        <!-- <td>{{ $period->id }}</td> -->
                        <td>{{ $period->period }}</td>
                        <td>{{ $period->newdate }}</td>
                      <!--   <td>{{ $period->rentPayableAmount }}</td>
                        <td>{{ $period->commission }}</td>
                        <td>{{ $period->adminFees }}</td> -->
                        <td>{{ $period->disbursements }}</td>
                        <td>{{ $period->paymentDue }}</td>
                        <td>{{ $period->rentPayable->invoice_id }}</td>

                        <td>{{ $period->landlordXeroInvRef }}</td>
                        <td>{{ $period->rentPayableStatus }}</td>
                        <td></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/" v-on:click.prevent="viewLandlordStatement({{ $period->id }})">View statement</a>
                                </div>
                            </div>
                        </td>
                    </tr>


                    @elseif ($period->rentPayableStatus == 'AUTHORISED')
                    <tr class="table table-danger">
                        <!-- <td>{{ $period->id }}</td> -->
                        <td>{{ $period->period }}</td>
                        <td>{{ $period->newdate }}</td>
                      <!--   <td>{{ $period->rentPayableAmount }}</td>
                        <td>{{ $period->commission }}</td>
                        <td>{{ $period->adminFees }}</td> -->
                        <td>{{ $period->disbursements }}</td>
                        <td>{{ $period->paymentDue }}</td>
                        <td>{{ $period->rentPayable->invoice_id ?? '' }}</td>

                        <td>{{ $period->landlordXeroInvRef }}</td>
                        <td>{{ $period->rentPayableStatus }}</td>
                        <td></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/" v-on:click.prevent="openLandlordBill({{ $period->id }})">Mark as paid</a>
                                    <a class="dropdown-item" href="/" v-on:click.prevent="viewLandlordStatement({{ $period->id }})">View statement</a>
                                </div>
                            </div>
                        </td>
                    </tr>



                    @elseif ($period->rentalPayableStatus == 'DRAFT')
                    <tr class="table table-danger">
                     <td>{{ $period->period }}</td>
                     <td>{{ $period->newdate }}</td>
                    <!--    <td>{{ $period->rentPayableAmount }}</td>
                       <td>{{ $period->commission }}</td>
                       <td>{{ $period->adminFees }}</td> -->
                       <td>{{ $period->disbursements }}</td>
                       <td>{{ $period->paymentDue }}</td>
                       <td>{{ $period->rentPayable->invoice_id ?? '' }}</td>
                       <td>{{ $period->landlordXeroInvRef }}</td>
                       <td>{{ $period->rentPayableStatus }}</td>
                       <td></td>
                       <td>
                        <div class="dropdown">
                            <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="/" v-on:click.prevent="viewLandlordStatement({{ $period->id }})">View statement</a>
                            </div>
                        </div>
                    </td>
                </tr>

                @else

                <tr>
                    <!-- <td>{{ $period->id }}</td> -->
                    <td>{{ $period->period }}</td>
                    <td>{{ $period->newdate }}</td>
                 <!--    <td>{{ $period->rentPayableAmount }}</td>
                    <td>{{ $period->commission }}</td>
                    <td>{{ $period->adminFees }}</td> -->
                    <td>{{ $period->disbursements }}</td>
                    <td>{{ $period->paymentDue }}</td>
                    <td>{{ $period->rentPayable->invoice_id ?? '' }}</td>
                    
                    <td>{{ $period->landlordXeroInvRef }}</td>
                    <td>{{ $period->rentPayableStatus }}</td>
                    <td></td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="/" v-on:click.prevent="editLandlordSchedule({{ $period->id }})">Edit</a>
                                <a class="dropdown-item" href="/" v-on:click.prevent="viewLandlordStatement({{ $period->id }})">View statement</a>
                            </div>
                        </div>
                    </td>
                </tr>
                @endif
                
                @endforeach

            </tbody>
        </table>
    </div>
</div>
</div>