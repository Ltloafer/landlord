
<div class="modal" id="edit-landlord-schedule" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Edit landlord schedule</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form class="form-horizontal" method="POST" :action=" '/rentPayables/' + rentPayable.id">
            <input type="hidden" name="_method" value="PUT">

            {{ csrf_field() }}

            <div class="modal-body">
              <div class="container-fluid">
                <div class="form-group">
                  <label for="type" class="col-md-6 control-label">Scheduled invoice date</label>
                  <div class="col-md-8">
                    <input id="date" type="text" class="form-control" name="date" :value="rentPayableScheduledDate" :placeholder="rentPayableScheduledDate" required
                     autofocus>
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="startdate" class="col-md-8 control-label">Month starting</label>
                    <div class="col-md-10">
                      <input id="startdate" type="text" class="form-control" name="startdate" :value="startDate" :placeholder="endDate" required disabled>
                    </div>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="enddate" class="col-md-8 control-label">to</label>
                    <div class="col-md-10">
                      <input id="enddate" type="text" class="form-control" name="enddate" :value="endDate" :placeholder="endDate" required disabled>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                    <label for="enddate" class="col-md-8 control-label">Amount £</label>
                  <div class="col-md-8">
                    <input id="amount" type="text" class="form-control" name="amount" :value="amount" :placeholder="amount" required autofocus>
                  </div>
                </div>

                  <div class="form-group">
                    <label for="enddate" class="col-md-12 control-label">Additional info.</label>
                  <div class="col-md-12">
                      <input id="add_info" type="text" class="form-control" name="add_info" :value="addInfo" :placeholder="addInfo" autofocus>
                    <!-- <textarea class="form-control" input id="add_info" rows="2"  name="add_info" :value="addInfo" :placeholder="addInfo" autofocus></textarea> -->
                  </div>
                </div>

                <h6>Commission</h6>

                   <div class="form-group">
                    <label for="enddate" class="col-md-8 control-label">Amount £</label>
                  <div class="col-md-8">
                    <input id="amount" type="text" class="form-control" name="amount" :value="amount" :placeholder="amount" required autofocus>
                  </div>
                </div>

              </div>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Save</button>
              <button type="button" class="btn btn-secondary" v-on:click.prevent="closeEditLandlordSchedule()" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>