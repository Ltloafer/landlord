<!-- Modal -->
<div class="modal fade" id="add-splitComm-tenancy" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add New Split Comm Tenancy</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/tenancies">
                <input type="hidden" name="tenancy_type" value="split-comm">

                {{ csrf_field() }}

                <div class="modal-body">
                 <div class="container-fluid">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Property</div>
                            </div>
                            <select class="form-control" id="property" name="property_id" required>
                                <option disabled selected>Select</option>
                                @foreach($agentProperties as $property)
                                <option value="{{$property->id}}">
                                    {{$property->address}} 
                                    <span class="ml-2">({{ $property->agent->name }})</span>
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Tenant</div>
                            </div>
                            <select class="form-control" id="tenant" name="tenant" required>
                             <option disabled selected>Select</option>
                             @foreach($availableTenants as $tenant)
                             <option value="{{$tenant->id}}">{{$tenant->name}}</option>
                             @endforeach
                         </select>
                     </div>
                 </div>
             </div>

             <div class="form-row">
                <div class="form-group col-md-5">
                 <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Start date</div>
                    </div>
                    <input id="startdate" type="DATE" class="form-control" name="startdate" value="{{ old('startdate') }}" placeholder="yyyy-mm-dd" required>
                </div>
            </div>

            <div class="form-group col-md-5 offset-md-1">
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">End date</div>
                </div>
                <!-- <label for="enddate" class="col-md-4 control-label">End date </label> -->
                <!-- <div class="col-md-10"> -->
                    <input id="enddate" type="DATE" class="form-control" name="enddate" value="{{ old('enddate') }}" placeholder="yyyy-mm-dd" required>
                </div>
            </div>
        </div>

        <div class="form-row mt-3">
           <div class="form-group col-md-5">
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Monthly rate £</div>
                </div>
                <!-- <label for="enddate" class="col-md-8 control-label">Monthly rate</label> -->
                <!-- <div class="col-md-8"> -->
                    <input id="name" type="text" class="form-control" name="monthly_rate" value="{{ old('monthly_rate') }}" autocomplete="off">
                </div>
            </div>


            <div class="form-group col-md-5 offset-md-1">
               <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Commission rate %</div>
                </div>
                <input id="name" type="text" class="form-control" name="comm_rate" value="{{ old('comm_rate') }}" autocomplete="off">
            </div>
        </div>
    </div>
<!-- 
                <div class="form-row mt-4">
                    <div class="form-group col-md-5">
                     <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Security deposit £</div>
                        </div>
                        <input id="name" type="text" class="form-control" name="security_deposit" value="{{ old('security_deposit') }}">
                    </div>
                </div>

                <div class="form-group col-md-5 offset-md-1">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Holding deposit £</div>
                    </div>
                    <label for="enddate" class="col-md-8 control-label">Holding deposit</label>
                        <input id="name" type="text" class="form-control" name="holding_deposit" value="{{ old('holding_deposit') }}" placeholder="£" autofocus>
                    </div>
                </div>
            </div> -->

            <div class="form-row mt-3">
                <div class="form-group col-md-5">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Minimum notice period</div>
                        </div>
                        <!-- <label for="notice_period" class="col-md-8 control-label"></label> -->
                        <!-- <div class="col-md-8"> -->
                            <input id="name" type="text" class="form-control" name="notice_period" value="{{ old('notice_period') }}" placeholder="Days" required autocomplete="off">
                        </div>
                    </div>

                    <div class="form-group col-md-5 offset-md-1">
                      <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Break clause period</div>
                        </div>
                        <!-- <label for="break_clause" class="col-md-8 control-label">Break clause period</label> -->
                        <!-- <div class="col-md-8"> -->
                            <input id="name" type="text" class="form-control" name="break_clause" value="{{ old('break_clause') }}" placeholder="Months" required autofocus autocomplete="off">
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
</div>
</div>
</div>