
<!-- Add Fee modal ---------------------------------------------------------------->
<div class="modal" id="newScheduledInvoice" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Schedule invoices</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="POST" action="/scheduledInvoices">
        <input type="hidden" name="tenancyId" value="{{ $tenancy->id }}">

        {{ csrf_field() }}

        <div class="modal-body">
          <div class="container-fluid">

           <div class="form-group">
            <label for="type" class="col-md-4 control-label">Invoice date</label>
            <div class="col-md-8">
              <input id="date" type="DATE" class="form-control" name="date" value="{{ old('date') }}" placeholder="dd/mm/yyyy" required autofocus>
            </div>
          </div>


          <div class="form-group">
            <label for="type" class="col-md-4 control-label">Start date</label>
            <div class="col-md-8">
              <input id="date" type="DATE" class="form-control" name="start_date" value="{{ old('start_date') }}" placeholder="dd/mm/yyyy" required autofocus>
            </div>
          </div>


          <div class="form-group">
            <label for="type" class="col-md-4 control-label">End date</label>
            <div class="col-md-8">
              <input id="date" type="DATE" class="form-control" name="end_date" value="{{ old('end_date') }}" placeholder="dd/mm/yyyy" required autofocus>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-8">
              <input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}" placeholder="£ amount" required autofocus>
            </div>
          </div>

          <div class="form-group">
            <!-- <label for="enddate" class="col-md-12 control-label">Additional info.</label> -->
            <div class="col-md-12">
              <input id="add_info" type="text" class="form-control" name="add_info" value="{{ old('add_info') }}" placeholder="Any additional info." autofocus>
              <!-- <textarea class="form-control" input id="add_info" rows="2"  name="add_info" :value="addInfo" :placeholder="addInfo" autofocus></textarea> -->
            </div>
          </div>

        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<!-- End modal ---------------------------------------------------------------->
