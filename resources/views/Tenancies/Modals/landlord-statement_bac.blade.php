
<div class="modal" id="landlord-statement" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Landlord statement</h5>
     <!--    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>


      <div class="modal-body">
        <div class="container-fluid">
          <p><strong>Rental period @{{ statementData.period }}</strong></p>

          <p><strong>Rent £ @{{ rentAmount }}</strong></p>

          <div class="mt-4">
            <p><strong>Commission Fees</strong></p>
            <div v-if="commissions">
              <template v-for="commission in commissions">
                <div class="row">
                  <div class="col-md-6">
                    @{{ commission.date | moment }}
                  </div>
                  <div class="col-md-6">
                   £ @{{ commission.formattedAmount }} inc. Vat
                 </div>
               </div>
             </template>
           </div>
           <div v-else>
            <p>None.</p>
          </div>
        </div>

        <div class="mt-3">
          <p><strong>Expenses</strong></p>
          <div v-if="expenses.length > 0">
            <template v-for="expense in expenses">
             <div class="row">
              <div class="col-md-5">
                @{{ expense.date | moment }}
              </div>
                <div class="col-md-4">
                @{{ expense.type }}
              </div>
              <div class="col-md-3">
                £ @{{ expense.formattedAmount }}
              </div>
            </div>
          </template>
        </div>
        <div v-else>
          <p>None.</p>
        </div>
      </div>

      <div class="mt-3">
        <p><strong>Admin Fees</strong></p>
        <div v-if="fees">
          <template v-for="fee in fees">
           <div class="row">
            <div class="col-md-6">
              @{{ fee.date | moment }}
            </div>
            <div class="col-md-6">
              £ @{{ fee.formattedAmount}} inc. Vat
            </div>
          </div>
        </template>
      </div>
      <div v-else>
        <p>None.</p>
      </div>
    </div>

    <div class="row mt-5">
      <div class="col-md-12">
        <p><strong>Amount due to be paid £ @{{ statementData.amountDue }}</strong></p>
      </div>
    </div>

  </div>
</div>


<div class="modal-footer">
  <!-- <button type="submit" class="btn btn-primary">Save</button> -->
  <button type="button" class="btn btn-secondary" v-on:click.prevent="closeStatement()" data-dismiss="modal">Close</button>
</div>
</form>
</div>
</div>
</div>