
<!-- Add Fee modal ---------------------------------------------------------------->
<div class="modal" id="setReference" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Set reference</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="POST" action="/tenancies/{{ $tenancy->id }}/setReference">

        {{ csrf_field() }}

        <div class="modal-body">
          <div class="container-fluid">

            <div class="form-group">
              <div class="col-md-8">
                <input id="amount" type="text" class="form-control" name="reference" value="{{ $tenancy->reference }}" required autofocus>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
    <!-- End modal ---------------------------------------------------------------->