<div class="modal" id="fee-invoice" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirm payment of fee</h5>
                <button type="button" class="close" v-on:click.prevent="closeModel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" :action=" '/xero/payments' ">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <input type="hidden" name="invoiceID" :value="invoice.InvoiceID">
                        <input type="hidden" name="account" value="trading">
                        <input type="hidden" name="actionId" :value="actionid">

                      <!--   <p><strong>Reference </strong> @{{ invoice.Reference }}</p>
                        <p><strong>Xero Invoice Reference </strong> @{{ invoice.InvoiceNumber }}</p>
                        <p><strong>Invoice Status </strong> @{{ invoice.Status }}</p>

                        <strong>Items</strong>
                        <template v-for="item in lineItems">
                            <p><strong>Description </strong> @{{ item.Description }} </p>
                            <p><strong>Amount £</strong> @{{ item.UnitAmount }} </p>
                            <p><strong>Vat £</strong> @{{ item.TaxAmount }} </p>
                            <p><strong>Sub total £</strong> @{{ invoice.Total }} </p>
                        </template>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <p><strong>Total invoice amount £ </strong>@{{ invoice.AmountDue }}</p>
                            </div>
                        </div> -->

                        <div v-if="invoice.Status == 'DRAFT' ">
                            <p><strong>This invoice is still in Draft form on xero. Please authorise before continuing.</strong></p>
                      </div>

                      <div class="mt-3">
                        <div class="form-group">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Amount paid £</div>
                                </div>
                                <input id="amount" type="text" class="form-control" name="amount" :value="invoice.AmountDue" :placeholder="invoice.AmountDue" autocomplete="off" required autofocus>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <span v-if="invoice.Status != 'DRAFT' ">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </span>
                    <button type="button" class="btn btn-secondary" v-on:click.prevent="closeModel()" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>

