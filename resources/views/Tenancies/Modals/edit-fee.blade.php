
<div class="modal" id="edit-fee" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit fee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="POST" :action=" '/fees/' + fee.id ">
        <input type="hidden" name="_method" value="PUT">

        {{ csrf_field() }}

        <!-- @{{ fee }} -->

        <div class="modal-body">
          <div class="container-fluid">

           <div class="form-group">
             <div class="col-md-10">
               <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">Fee</div>
                </div>
                <!-- <label for="type" class="col-md-4 control-label">Select type</label> -->
                <!-- <div class="col-md-12"> -->
                  <select class="form-control" id="fee_type" name="fee_type" required>
                   <option selected value="fee.typeId">@{{ feeType }}</option>
                   @foreach(\App\FeeType::all() as $type)
                   <option value="{{ $type->id }}">{{ $type->type }}</option>
                   @endforeach
                 </select>
               </div>
             </div>
           </div>
           <!-- </div> -->

           <div class="form-group">
            <div class="col-md-8">
             <div class="input-group mb-2">
              <div class="input-group-prepend">
                <div class="input-group-text">Fee date</div>
              </div>
              <input id="fee_date" type="text" class="form-control" name="date" :value="feeDate" placeholder="dd/mm/yyyy" required autofocus>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-8">
           <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text">Amount</div>
            </div>
            <input id="fee_amount" type="text" class="form-control" name="amount" :value="feeAmount" placeholder="£ amount" required autofocus>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="col-md-8">
         <div class="input-group mb-2">
          <div class="input-group-prepend">
            <div class="input-group-text">Vat</div>
          </div>
          <input id="fee_vat" type="text" class="form-control" name="vatAmount" :value="feeAmountVat" placeholder="£ amount" required autofocus>
        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="col-md-8">
       <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text">Amount total</div>
        </div>
        <input id="fee_amount_total" type="text" class="form-control" name="totalAmount" :value="totalAmount" placeholder="£ amount" required autofocus>
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-8">
     <div class="input-group mb-2">
      <div class="input-group-prepend">
        <div class="input-group-text">Invoice</div>
      </div>
      <input id="fee_invoice" type="text" class="form-control" name="totalAmount" :value="fee.xero_ref" disabled>
    </div>
  </div>
</div>
</div>

<div class="modal-footer">
  <button type="submit" class="btn btn-primary">Save</button>
  <button type="button" class="btn btn-secondary" v-on:click.prevent="closeEditFee()" data-dismiss="modal">Close</button>
</div>
</div>
</form>
</div>
</div>
</div>
