
<!-- Add Fee modal ---------------------------------------------------------------->
<div class="modal" id="addTenancyExpense" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add a disbursement</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="POST" action="/expenses" enctype="multipart/form-data">
        <input type="hidden" name="tenancyId" value="{{ $tenancy->id }}">

        {{ csrf_field() }}

        <div class="modal-body">
          <div class="container-fluid">

           <div class="form-group">
            <label for="type" class="col-md-4 control-label">Select type</label>
            <div class="col-md-12">
              <select multiple class="form-control" id="expense_type" name="expense_type_id" required>
               @foreach($tenancy->getExpenseTypes() as $type)
               <option value="{{ $type->id }}">{{ $type->type }}</option>
               @endforeach
             </select>
           </div>
         </div>

         <div class="form-group">
          <label for="type" class="col-md-4 control-label">Select supplier</label>
          <div class="col-md-12">
            <select multiple class="form-control" id="supplier" name="supplier_id" required>
             @foreach($tenancy->getAllSuppliersWithXeroContacts() as $supplier)
             <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
             @endforeach
           </select>
         </div>
       </div>

       <div class="form-group">
        <label for="type" class="col-md-4 control-label">Supplier Invoice Reference</label>
        <div class="col-md-8">
          <input id="inv_ref" type="text" class="form-control" name="inv_ref" value="{{ old('inv_ref') }}" autofocus>
        </div>
      </div>

      <div class="form-group">
        <input type="file" class="form-control-file" name="file">
      </div>


      <div class="form-group">
        <label for="type" class="col-md-4 control-label">Date</label>
        <div class="col-md-8">
          <input id="date" type="DATE" class="form-control" name="date" value="{{ old('date') }}" placeholder="dd/mm/yyyy" required autofocus>
        </div>
      </div>

      <div class="form-group">
        <div class="col-md-8">
          <input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}" placeholder="£ amount" required autofocus>
        </div>
      </div>
    </div>

    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Save</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</form>
</div>
</div>
</div>
<!-- End modal ---------------------------------------------------------------->
