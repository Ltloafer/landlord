
<div class="modal" id="rental-invoice" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Rental Invoice @{{ invoice.InvoiceNumber }}</h5>
        <button type="button" class="close" v-on:click.prevent="closeModel()" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="POST" :action=" '/xero/payments' ">

        {{ csrf_field() }}

        <div class="modal-body">
          <div class="container-fluid">

            <input type="hidden" name="invoiceID" :value="invoice.InvoiceID">
            <input type="hidden" name="account" value="client">
            <input type="hidden" name="actionId" :value="actionid">

         <!--    <p><strong>Rental Period @{{ rentalPeriod }}</strong></p>
            <p><strong>Reference </strong> @{{ invoice.Reference }}</p>
            <p><strong>Xero Invoice Reference </strong> @{{ invoice.InvoiceNumber }}</p>
            <p><strong>Invoice Status </strong> @{{ invoice.Status }}</p> -->

           <!--  <template v-for="item in lineItems">
              <p><span><strong> @{{ item.Description }}</strong></span>
              <span class="ml-2 mr-2">£</strong> @{{ item.UnitAmount }}</span> <span>Vat £ @{{ item.TaxAmount }}</span>
              </p>
            </template> -->

            <div v-if="invoice.Status == 'DRAFT' ">
              <p><strong>This invoice is still in Draft form on xero. Please authorise before continuing.</strong></p>
            </div>

            <div class="row mt-3">
              <div class="col-md-12">
                <p><strong>Total invoice amount £ </strong>@{{ invoice.AmountDue }}</p>
              </div>
            </div>

            <div class="mt-3">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Amount paid £</div>
                  </div>
                  <input id="amount" type="text" class="form-control" name="amount" :value="invoice.AmountDue" :placeholder="invoice.AmountDue" required autofocus>
                </div>
              </div>
            </div>

          </div>
        </div>

        <div class="modal-footer">
          <span v-if="invoice.Status != 'DRAFT' ">
            <button type="submit" class="btn btn-primary">Save</button>
          </span>
          <button type="button" class="btn btn-secondary" v-on:click.prevent="closeModel()" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>