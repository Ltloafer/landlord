<!-- <strong>Fees</strong> -->
<div class="row">
    <div class="col-md-12">
        <div class="inset-items">
            <table class="table m-b-none">
                <thead>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Inv id</th>
                    <th>Xero Ref</th>
                    <th>Status</th>
                    <th>Amount</th>
                    <th>Vat</th>
                    <th>Total</th>
                </thead>

                <tbody>
                    @foreach ($fees as $fee)

                    @if ($fee->xeroStatus == 'DRAFT')
                    <tr class="table table-danger">
                        <td>{{ $fee->date->format('jS \\of F Y') }}</td>
                        <td>{{ $fee->feeType->type }}</td>
                        <td>{{ $fee->invoice_id }}</td>
                        <td>{{ $fee->xero_ref }}</td>
                        <td>{{ $fee->xeroStatus }}</td>
                        <td>{{ $fee->displayAmount() }}</td>
                        <td>{{ $fee->displayVat() }}</td>
                        <td>{{ $fee->displayTotal() }}</td>
                    </tr>

                    @elseif ($fee->xeroStatus == 'PAID')
                    <tr class="table table-warning"> 
                        <td>{{ $fee->date->format('jS \\of F Y') }}</td>
                        <td>{{ $fee->feeType->type }}</td>
                        <td>{{ $fee->invoice_id }}</td>

                        <td><a href="/invoices/viewXeroInvoice/{{ $fee->xero_inv_id }}">{{ $fee->xero_ref }}</a></td>
                        <td>{{ $fee->xeroStatus }}</td>
                        <td>{{ $fee->displayAmount() }}</td>
                        <td>{{ $fee->displayVat() }}</td>
                        <td>{{ $fee->displayTotal() }}</td>
                        <td> </td>
                    </tr>

                    @elseif ($fee->xeroStatus == 'RECONCILED')
                    <tr class="table table-success">
                        <td>{{ $fee->date->format('jS \\of F Y') }}</td>
                        <td>{{ $fee->feeType->type }}</td>
                        <td>{{ $fee->invoice_id }}</td>

                        <td><a href="/invoices/viewXeroInvoice/{{ $fee->xero_inv_id }}">{{ $fee->xero_ref }}</a></td>
                        <td>{{ $fee->xeroStatus }}</td>
                        <td>{{ $fee->displayAmount() }}</td>
                        <td>{{ $fee->displayVat() }}</td>
                        <td>{{ $fee->displayTotal() }}</td>
                        <td></td>
                    </tr>

                    @elseif ($fee->xeroStatus == 'AUTHORISED')
                    <tr class="table table-danger">
                      <td>{{ $fee->date->format('jS \\of F Y') }}</td>
                      <td>{{ $fee->feeType->type }}</td>
                      <td>{{ $fee->invoice_id }}</td>

                      <td><a href="/invoices/viewXeroInvoice/{{ $fee->xero_inv_id }}">{{ $fee->xero_ref }}</a></td>
                      <td>{{ $fee->xeroStatus }}</td>
                      <td>{{ $fee->displayAmount() }}</td>
                      <td>{{ $fee->displayVat() }}</td>
                      <td>{{ $fee->displayTotal() }}</td>
                      <td>
                       @if ($fee->fee_type_id !== 2)
                       <button class="btn btn-sm btn-outline float-right" type="button" v-on:click.prevent="openFeeInvoice( {{ $fee->id }})">
                        View
                    </button>
                    @endif
                </td>
            </tr>

            @else

            <tr>
                <td>{{ $fee->date->format('jS \\of F Y') }}</td>
                <td>{{ $fee->feeType->type }}</td>
                <td>{{ $fee->invoice_id }} <br> <small>{{ $fee->invoice->xero_ref ?? '' }}</small></td>
                <td></td>
                <td>{{ $fee->xeroStatus }}</td>
                <td>{{ $fee->displayAmount() }}</td>
                <td>{{ $fee->displayVat() }}</td>
                <td>{{ $fee->displayTotal() }}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="/" v-on:click.prevent="editFee({{ $fee }})">Edit</a>
                            @if (! $fee->invoice_id)
                            <a class="dropdown-item" href="/" v-on:click.prevent="invoiceFee({{ $fee }})">Invoice</a>
                            <hr>
                            <a class="dropdown-item" href="/" v-on:click.prevent="deleteFee({{ $fee }})">Delete</a>
                            @endif
                            
                        </div>
                    </div>
                </td>
            </tr>

            @endif

            @endforeach
        </tbody>

        <tfoot>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>Totals £</strong></td>
            <td></td>
            <td><strong>{{ $tenancy->displayTotalFees() }}</strong></td>
            <td><strong>{{ $tenancy->displayTotalFeesVat() }}</strong></td>
            <td><strong>{{ $tenancy->displayTotalFeesTotal() }}</strong></td>
        </tr>
    </tfoot>
</table>

</div>
</div>
</div>