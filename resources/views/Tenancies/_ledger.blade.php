         <strong>Account ledger</strong>

         <div class="row">
          <div class="col-md-12">
           <!-- <h6>Income</h6> -->

           <div class="inset-items">
            <table class="table table-sm table-hover m-b-none">
              <thead>
                <th></th>
                <th>Date</th>
                <th>Type</th>
                <th>Credit</th>
                <th>Debit</th>
                <th></th>
              </thead>

              <tbody>
               <tr class="table table-primary">
                <td><strong>INCOME</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>


              <tr>
                <td><strong>Admin Fees</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

              <tr>
                <td></td>
                <td></td>
                <td>Admin fee - tenant</td>
                <td>{{ $tenancy->getTenantAdminFee() }}</td>
                <td></td>
                <td></td>
              </tr>

              <tr>
                <td><strong>Deposit</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

              @foreach($tenancy->deposits as $deposit)
              <tr>
                <td></td>
                <td>{{ $deposit->created_at->format('jS \\of F y') }}</td>
                <td>Tenant deposit</td>
                <td>{{ $deposit->amount }}</td>
                <td></td>
                <td></td>
              </tr>
              @endforeach

              <tr>
                <td><strong>Rent</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

              @foreach ($tenancy->invoicedScheduledInvoices() as $s_inv)
              <tr>
                <td></td>
                <td>{{ $s_inv->scheduled_date->format('jS \\of F y') }}</td>
                <td>{{ $s_inv->start_date->format('jS \\of F y') }} - {{ $s_inv->end_date->format('jS \\of F y') }}</td>
                <td>{{ $s_inv->displayAmount() }}</td>
                <td></td>
                <td></td>
              </tr>
              @endforeach

              <tr>
                <td><strong>Total income</strong></td>
                <td></td>
                <td></td>
                <td><strong>15,764</strong></td>
                <td></td>
                <td></td>
              </tr>

              
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

              <tr class="table table-primary">
                <td><strong>EXPENSES</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

              <tr>
                <td><strong>Admin Fees</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

              <tr>
                <td></td>
                <td>1st September 2018</td>
                <td>Admin fee - tenant</td>
                <td></td>
                <td>{{ $tenancy->getTenantAdminFee() }}</td>
                <td></td>
              </tr>

              <tr>
                <td><strong>Commission fees</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

              @foreach($tenancy->landlordCommissions as $commission)
              <tr>
                <td></td>
                <td>{{ $commission->created_at->format('jS \\of F y') }}</td>
                <td>Commission</td>
                <td></td>
                <td>{{ $commission->amount }}</td>
                <td></td>
              </tr>
              @endforeach

              <tr>
                <td><strong>Disbursements</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

              <tr>
                <td></td>
                <td>10th January 2018</td>
                <td>Cleaning</td>
                <td></td>
                <td>420</td>
                <td></td>
              </tr>


              <tr>
                <td><strong>Rent to landlord</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

              @foreach($tenancy->rentPayables as $rentPayable)
              @if ($rentPayable->isPaid())
              <tr style="color:green">
                <td></td>
                <td>{{ $rentPayable->date->format('jS \\of F y') }}</td>
                <td>Rent to landlord</td>
                <td></td>
                <td>{{ $rentPayable->amount }}</td>
                <td></td>
              </tr>
              @else
              <tr style="color:red">
                <em>
                <td></td>
                <td>{{ $rentPayable->date->format('jS \\of F y') }}</td>
                <td>Rent to landlord</td>
                <td></td>
                <td>{{ $rentPayable->amount }}</td>
                <td></td>
              </em>
            </tr>
            @endif

            @endforeach

            <tr>
              <td><strong>Total expenses</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td><strong>15,784</strong></td>
              <td></td>
            </tr>
          </tbody>

          <tfoot>
            <tr>
              <td></td>
              <td></td>
              <td><strong>BALANCE £</strong></td>
              <td><strong></strong></td>
              <td><strong>-20</strong></td>
              <td></td>
            </tr>
          </tfoot>


          <!-- <tfoot>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td><strong>Totals £</strong></td>
              <td><strong>{{ $tenancy->displayLedgerCreditTotals() }}</strong></td>
              <td><strong>{{ $tenancy->displayLedgerDebitTotals() }}</strong></td>
            </tr>

            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td><strong>Balance £ </strong></td>
              <td><strong>{{ $tenancy->displayLedgerBalance() }}</strong></td>
            </tr>
          </tfoot> -->
        </table>

      </div>
    </div>
  </div>