       <!-- <strong>Deposits</strong> -->
       <div class="row">
        <div class="col-md-12">
          <div class="inset-items">
            <table class="table m-b-none">
              <thead>
                <th>Type</th>
                <th>Inv id</th>
                <th>Xero Ref</th>
                <th>Status</th>
                <th>Amount</th>
              </thead>

              <tbody>
                @foreach ($tenancy->deposits as $deposit)

                <tr class="table">
                  <td>{{ $deposit->type }}</td>
                  <td>{{ $deposit->invoice_id ?? '' }}</td>
                  <td>
                    @if ($deposit->isInvoiced())
                    <a href="/invoices/viewXeroInvoice/{{ $deposit->xero_inv_id }}">{{ $deposit->xero_ref }}</a>
                    @endif
                  </td>
                  <td>{{ $deposit->getStatus() }}</td>
                  <td>{{ $deposit->displayAmount() }}</td>
                </tr>

                @endforeach
              </tbody>

              <tfoot>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><strong>Totals £</strong></td>
                </tr>
              </tfoot>
            </table>

          </div>
        </div>
      </div>