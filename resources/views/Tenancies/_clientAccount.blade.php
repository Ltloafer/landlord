         <strong>Client account ledger</strong>
              <div class="row">
                <div class="col-md-12">
                  <div class="inset-items">
                    <table class="table m-b-none">
                      <thead>
                        <th></th>
                        <th>Date</th>
                        <th>Type</th>
                        <th></th>
                        <th>Credit</th>
                        <th>Debit</th>
                      </thead>

                      <tbody>
                        @foreach ($clientAccountEntries as $entry)
                        <tr>
                          <td></td>
                          <td>{{ $entry->date->format('jS \\of F Y') }}</td>

                          <td>{{ $entry->type }}</td>
                          <td></td>

                          @if ($entry->entry == 'credit')
                          <td>{{ $entry->displayAmount() }}</td>
                          <td></td>
                          @else 
                          <td></td>
                          <td>{{ $entry->displayAmount() }}</td>
                          @endif

                        </tr>
                        @endforeach
                      </tbody>

                      <tfoot>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td><strong>Totals £</strong></td>
                          <td><strong></strong></td>
                          <td><strong></strong></td>
                        </tr>

                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td><strong>Balance £ </strong></td>
                          <td><strong></strong></td>
                        </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>