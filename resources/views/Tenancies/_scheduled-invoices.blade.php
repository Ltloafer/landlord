<!-- <strong>Rental schedule</strong> -->
<div class="row">
    <div class="col-md-12">
        <div class="inset-items">
            <table class="table m-b-none">
                <thead>
                    <th>Period</th>
                    <th>Date of invoicing</th>
                    <th>Rental amount</th>
                    <!-- <th>Admin charges</th> -->
                    <th>Inv. id</th>
                    <th>Xero Ref.</th>
                    <th>Status</th>
                    <th></th>
                </thead> 

                <tbody>
                    @foreach ($scheduledPeriods as $period)

                    <tr class="table">
                        <td>{{ $period->period }}</td>
                        <td>{{ $period->scheduledDate }}</td>
                        <td>{{ $period->rentalAmount }}</td>
                        <td>{{ $period->invoice_id ?? '' }}</td>
                        <td>
                            @if ($period->invoice_id)
                            <a href="/invoices/viewXeroInvoice/{{ $period->xero_inv_id }}" target="_blank">{{ $period->xero_ref }}</a>
                            @endif
                        </td>
                        <td>{{ $period->rentalInvoiceStatus }}</td>
                        <td>
                            @if ($period->invoice_id)
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#" v-on:click.prevent="openRentalInvoice({{ $period->id }})">Mark as paid</a>
                                </div>
                            </div>

                            @else
                            
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                   <a class="dropdown-item" href="/" v-on:click.prevent="editScheduledInvoice({{ $period->id }})">Edit</a>
                                   <a class="dropdown-item" href="/" v-on:click.prevent="deleteScheduledInvoice({{ $period->id }})">Delete</a>
                               </div>
                           </div>
                           @endif
                       </td>
                   </tr>
                   @endforeach
               </tbody>
           </table>
       </div>
   </div>
</div>

