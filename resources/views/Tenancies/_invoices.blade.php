<!-- <strong>Fees</strong> -->
<div class="row">
    <div class="col-md-12">
        <div class="inset-items">
            <table class="table m-b-none">
                <thead>
                    <th>Id</th>
                    <th>Date</th>
                    <th>Reference</th>
                    <th>Customer</th>
                    <th>Type</th>
                    <!-- <th>Due date</th> -->
                    <th>Amount</th>
                    <th>Xero ref</th>
                    <th>Status</th>
                </thead>

                <tbody>
                    @foreach($tenancy->dateOrderedInvoices as $invoice)
                    <tr>
                        <td>{{ $invoice->id }}</td>
                        <td>{{ optional($invoice->date)->format('d/m/y') }}</td>
                        <td>{{ $invoice->reference }}</td>
                        <td>{{ $invoice->contact->name }}</td>
                        <td>{{ $invoice->type }}</td>
                        <!-- <td>{{ optional($invoice->due_date)->format('d/m/y') }}</td> -->
                        <td>{{ $invoice->displayAmount() }} <br>
                            <small>Vat {{ $invoice->displayVat() }} | Total {{ $invoice->displayTotalAmount() }}</small>
                        </td>
                        <td>
                            {{ $invoice->xero_ref }} <br>

                            <small>
                                @if ($invoice->status == 'Paid')
                                Paid {{ optional($invoice->paid_on)->format('d/m/y') }}
                                @else
                                Due {{ optional($invoice->due_date)->format('d/m/y') }}
                                @endif
                            </small> 
                        </td> 

                        <td>{{ $invoice->status }}</td>
                        
                        <td><a href="/invoices/{{ $invoice->id }}" class="btn btn-sm btn-outline-secondary">View</a></td>
                        <td>
                            @if ($invoice->isAuthorised())
                            <a href="/invoices/viewXeroInvoice/{{ $invoice->xero_inv_id }}" class="btn btn-sm btn-outline-secondary" target="_blank">Xero</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
