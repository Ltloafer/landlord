@extends('layouts.layout')
@section('content')

<contacts :contacts="{{ $contacts }}" inline-template>
  <div>

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Contacts
                        <button class="btn btn-sm btn-default heading-btn" data-toggle="modal" data-target="#addContact"> Add a contact +</button>
                        <input class="search float-right" type="text" v-model="searchQuery" placeholder="Filter contacts...">
                    </div>

                    <div class="card-body">
                        <display-contacts :data="{{ $contacts }}" :columns="['name', 'phone', 'address' ,'email']" :filter-key="searchQuery" inline-template>
                            <div>

                                <table class="table table-hover m-b-none customer-sales" style="min-width:100%; width:auto">
                                    <thead>
                                        <tr>
                                            <th v-for="key in columns"
                                            @click="sortBy(key)"
                                            :class="{ active: sortKey == key }">
                                            @{{ key | capitalize }}
                                            <span class="arrow" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'">
                                            </span>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <!-- <tr v-for="contact in filteredData" class='clickable-row' v-on:click="openContactModal(contact)"> -->
                                    <tr v-for="contact in filteredData" class='clickable-row' v-on:click="view(contact)">
                                        <td width="15%">@{{ contact.name }}</td>
                                        <td width="15%">@{{ contact.phone }}</td>
                                        <td width="25%">@{{ contact.address }}</td>
                                        <td width="25%">@{{ contact.email }}</td>
                                    </tr>
                                    <!-- <td><a href="/customers/" class="btn btn-primary btn-sm btn-alt">View</a> -->
                                    </tbody>
                                </table>

                                @include('Modals.add-contact')
                                @include('Modals.view-contact')

                                <!-- Modal -->
                                <div class="modal fade" id="addNote" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">@{{ selectedNote.created_at | moment }}</h5>
                                                <div class="modal-body">
                                                    <p>@{{ selectedNote.note}}</p>
                                                    <hr>
                                                    <p v-if="selectedNote.property">Property: @{{ selectedNote.property.address }}</p>
                                                    <p v-if="selectedNote.landlord">Landlord: @{{ selectedNote.landlord.name }}</p>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <!-- End of Modal -->

                          </div>
                      </display-contacts>

                  </div>
              </div>
          </div>
      </div>
  </div>

</div>

</contacts>

@endsection
