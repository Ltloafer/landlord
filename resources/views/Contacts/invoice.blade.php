@extends('layouts.layout')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Invoice
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th></th>
                            <th>Date</th>
                            <th>Due date</th>
                            <th>Invoice number</th>
                            <th>Customer</th>
                            <th>View</th>
                        </thead>

                        <tbody>
                            <tr>
                                <td></td>
                                <td>{{ $invoice['Date'] }}</td>
                                <td>{{ $invoice['DueDate'] }}</td>
                                <td>{{ $invoice['InvoiceNumber'] }}</td>
                                <td>{{ $invoice['Contact']['Name'] }}</td>
                                <td><a href="{{ $invUrl }}">View invoice</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
