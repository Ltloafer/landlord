<!-- Modal -->
<div v-show="editBookingModal" class="modal fade" id="edit-hotel-booking" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit booking</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" :action=" '/hotel-bookings/' + booking.id">

                {{ csrf_field() }}

                {{ method_field('PUT') }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Hotel</div>
                                    </div>
                                    <select class="form-control" id="hotel" name="hotel_id" required>
                                        <option selected :value="booking.hotel_id"> @{{ booking.hotelName }}</option>
                                        <template v-for="hotel in hotels">
                                            <option :value="hotel.id">
                                                @{{ hotel.name }} 
                                            </option>
                                        </template>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Confirmation reference</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="confirmation_reference" :value="booking.confirmation_reference" required autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Guest</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="room_type" :value="booking.searchName" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">

                            </div>
                        </div>

                        <hr>

                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Check-in date</div>
                                    </div>
                                    <input id="startdate" type="date" class="form-control" name="startdate" :value="booking.start_date | dateify" required>
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Check-out date</div>
                                    </div>
                                    <input id="enddate" type="date" class="form-control" name="enddate" :value="booking.finish_date | dateify" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Nightly rate £</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="nightly_rate" :value="booking.nightlyRate" autocomplete="off">
                                </div>
                            </div>


                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Commission rate %</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="comm_rate" :value="booking.commissionRate" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Room type</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="room_type" :value="booking.room_type" required autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">
                        
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
</div>