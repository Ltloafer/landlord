@extends('layouts.layout')

@section('content')

<hotel-bookings :hotels="{{ $hotels }}" inline-template>

    <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-md-12">
            <div class="card">
                <div class="card-header">Hotel bookings
                    <input class="search float-right" type="text" v-model="searchQuery" placeholder="Filter bookings">
                </div>

                <div class="card-body">

                    <displaybookings :data="bookings" :columns="['project', 'hotel' ,'guest',  'con. ref', 'dates', 'nights', 'room', 'comm', 'comm £',  'status']" :filter-key="searchQuery" :hotels="hotels" v-on:reloadbookings="getBookings" inline-template>
                        <div>

                            <table class="table m-b-none table-responsive-sm" style="table-layout: auto">
                                <thead>
                                    <tr>
                                        <th v-for="key in columns"
                                        @click="sortBy(key)"
                                        :class="{ active: sortKey == key }">
                                        @{{ key | capitalize }}
                                        <span class="arrow" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'">
                                        </span>
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                                <!-- <tr v-for="booking in filteredData" v-on:click.prevent="getBooking(booking.id) -->
                                    <tr v-for="booking in filteredData" v-bind:style="{ color: isHotelPaid(booking) }">
                                        <td width="10%">@{{ booking.production }}</td>
                                        <td width="10%">@{{ booking.hotelName }}</td>
                                        <td width="10%">@{{ booking.searchName }}</td>
                                        <td width="7%">@{{ booking.confirmation_reference }}</td>

                                        <td width="15%">@{{ booking.startDate }} - @{{ booking.finishDate }}</td>
                                        <td width="5%">@{{ booking.nights }}</td>
                                        <td width="8%">@{{ booking.room_type }}</td>
                                        <td width="5%">@{{ booking.commissionRate }}</td>
                                        <td width="8%">@{{ booking.totalCommission }}</td>
                                        <td width="10%">@{{ booking.status }}
                                            <!-- <span v-if="booking.f"><small>@{{ booking.xero_ref }}</small></span> -->
                                        </td>

                                        </td>
                                        <td width="8%">
                                            <button class="btn btn-sm btn-sm float-right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-color: transparent">
                                              <span class="fa-stack fa-lg">
                                                <i class="fas fa-circle fa-stack-2x" style="color:grey"></i>
                                                <i class="fas fa-ellipsis-v fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a v-if="! booking.hotel_paid" href="#" v-on:click.prevent="hotelPaymentReceived(booking)" class="dropdown-item">Payment received</a>
                                            <a  v-if="! booking.invoice_id" class="dropdown-item" v-on:click="editBooking(booking)">Edit booking</a>
                                            <a  v-if="! booking.invoice_id" class="dropdown-item" v-on:click="checkoutBooking(booking)">Check-out</a>
                                            <div class="dropdown-divider"></div>
                                            <a  v-if="! booking.invoice_id" class="dropdown-item" v-on:click="deleteBooking(booking)">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div v-if="openBooking">
                            @include('Hotels.booking-modal')
                        </div>
                        @include('Hotels.delete-booking-modal')
                        @include('Hotels.booking-checkout')
                        @include('Hotels.edit-hotel-booking')
                    </div>

                </displaybookings>
            </div>
        </div>
    </div>
</div>
</div>


</hotel-bookings>



@endsection
