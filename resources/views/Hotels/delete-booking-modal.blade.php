<!-- Modal -->
<div v-show="deleteBookingModal" class="modal fade" id="delete-booking" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete booking  - reference @{{ booking.confirmation_reference }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <form class="form-horizontal" method="POST" :action=" '/hotel-bookings/' + booking.id">
                 <input type="hidden" name="_method" value="DELETE">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <p>Are you sure you want to delete this booking?</p>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" v-on:click.prevent="deleteBookingConfirmed(booking)" class="btn btn-danger">Delete</button>
                    <button type="button" class="btn btn-secondary" v-on:click.prevent="closeModal()" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>