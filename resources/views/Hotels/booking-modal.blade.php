<!-- Modal -->
<div class="modal fade" id="view-booking" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Booking  - confirmation reference @{{ booking.confirmation_reference }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/Tenants">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

             <!--           <div class="row">
                        <div class="col-md-4">
                            <strong>Confirmation reference</strong>
                            <span class="ml-2">
                                @{{ booking.confirmation_reference }}
                            </span>
                        </div>
                    </div>
                -->
                <!-- <hr> -->

                <div class="row">
                    <div class="col-md-4">
                        <strong>Hotel</strong>
                        <span class="ml-2">
                            @{{ booking.hotel.name }}
                        </span>
                    </div>

                    <div class="col-md-4">
                        <strong>Guest</strong>
                        <span class="ml-2">
                            @{{ booking.search.name }}
                        </span>
                    </div>
                </div>


                 <!--    <table class="table m-b-none mt-5">
                        <thead>
                            <th></th>
                            <th>Check-in date</th>
                            <th>Check-out date</th>
                            <th>No. of nights</th>
                            <th>Room rate £</th>
                            <th>Commission %</th>
                            <th>Commission £</th>
                            <th>Checked out?</th>
                            <th>Invoice</th>
                        </thead>

                        <tbody>
                            <tr>
                                <td></td>
                                <td>@{{ booking.startDate }}</td>
                                <td>@{{ booking.finishDate }}</td>
                                <td>@{{ booking.nights }}</td>
                                <td>@{{ booking.room_type }}</td>
                                <td>@{{ booking.commissionRate }}</td>
                                <td>@{{ booking.totalCommission }}</td>
                                <td>@{{ booking.status }}</td>
                                <td>@{{ booking.xero_ref }}</td>

                                <td v-if="booking.completed"><button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#booking-checkout">Check-out</button></td>
                                <td v-else></td>

                            </tr>
                        </tbody>
                    </table>
                -->
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <strong>Check-in</strong> 28/10/19 <strong>Check-out</strong> 30/10/19
                        <button type="button" class="btn btn-success btn-sm" style="float: right"  v-on:click.prevent="closeModal()" data-dismiss="modal">Edit</button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <hr>
                        Confirmation recieved of hotel payment
                        <button type="button" class="btn btn-primary btn-sm" style="float: right"  v-on:click.prevent="closeModal()" data-dismiss="modal">Payment received</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" v-on:click.prevent="closeModal()" data-dismiss="modal">Close</button>
        </div>
    </form>
</div>
</div>
</div>