@extends('layouts.layout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
            <div class="card-header">Hotel booking
                @if (! $booking->completed)
                <button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#edit-hotel-booking"> Edit booking</button>
                @endif
             <!--    <div class="dropdown float-right display-inline">
                    <button class="btn btn-sm btn-outline-secondary add-btn float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Add a hotelBooking
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" data-toggle="modal" data-target="#add-direct-hotelBooking">Direct let (AST)</a>
                        <a class="dropdown-item" data-toggle="modal" data-target="#add-dir-install-hotelBooking">Short let (installments)</a>
                        <a class="dropdown-item" data-toggle="modal" data-target="#add-splitComm-hotelBooking">Split comm </a>
                    </div>
                </div> -->
            </div>


            <div class="card-body" style="padding: 25px; padding-left: 50px; padding-right: 50px">

                      <!--   <span class="right-button">
                            <button class="btn btn-sm btn-default" data-toggle="modal" data-target="#addAgent">Add agent +</button>
                        </span> -->

                        <div class="row">
                            <div class="col-md-4">
                                <strong>Confirmation reference</strong>
                                <span class="ml-2">
                                    {{ $booking->confirmation_reference }}
                                </span>
                            </div>
                        <!--     <div class="col-md-8">
                                <strong>Agent</strong>
                                <span style="margin-left: 50px">
                        
                              </span>
                          </div> -->
                      </div>

                      <hr>

                      <div class="row">
                        <div class="col-md-4">
                            <strong>Hotel</strong>
                            <span class="ml-2">
                                {{ $booking->hotel->name }}
                            </span>
                        </div>

                        <div class="col-md-4">
                            <strong>Guest</strong>
                            <span class="ml-2">
                                {{ $booking->search->name }}
                            </span>
                        </div>
                    </div>


                    <table class="table m-b-none mt-5">
                        <thead>
                            <th></th>
                            <th>Check-in date</th>
                            <th>Check-out date</th>
                            <th>No. of nights</th>
                            <th>Room rate £</th>
                            <th>Commission %</th>
                            <th>Commission £</th>
                            <th>Checked out?</th>
                            <th>Invoice</th>
                        </thead>

                        <tbody>
                            <tr>
                                <td></td>
                                <td>{{ $booking->start_date->format('jS F Y') }}</td>
                                <td>{{ $booking->finish_date->format('jS F Y') }}</td>
                                <td>{{ $booking->start_date->diffInDays($booking->finish_date) }}</td>
                                <td>{{ $booking->getNightlyRate() }}</td>
                                <td>{{ $booking->getCommissionRate() }}</td>
                                <td>{{ $booking->getCommissionTotal() }}</td>
                                <td>{{ $booking->isCompleted() }}</td>
                                <td>{{ $booking->xero_ref }}</td>
                                <td></td>

                                @if (! $booking->completed)
                                <td><button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#booking-checkout">Check-out</button></td>
                                @else
                                <td></td>
                                @endif

                            </tr>
                        </tbody>
                    </table>

                    <hr>
                </div>
            </div>
        </div>
        @include('Hotels.edit-hotel-booking')
    </div>
</div>




@endsection
