<!-- Modal -->
<div v-show="checkoutBookingModal" class="modal fade" id="booking-checkout" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Check-out booking</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/checkout-hotel-booking">

                {{ csrf_field() }}

                <input type="hidden" name="bookingId" :value= "booking.id">

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-1">
                                <strong>Hotel</strong>
                            </div>

                            <div class="col-md-3">
                                @{{ booking.hotelName }}<br>
                            </div>

                            <div class="col-md-3">
                                <strong>Confirmation no.</strong>
                            </div>
                            <div class="col-md-3">
                                @{{ booking.confirmation_reference }}
                            </div>
                        </div>


                        <table class="table m-b-none mt-4">
                            <thead>
                                <th>Check-in date</th>
                                <th>Check-out date</th>
                                <th>No. of nights</th>
                                <th>Room rate £</th>
                                <th>Commission %</th>
                                <th>Commission £</th>
                            </thead>

                            <tbody>
                                <tr>
                                    
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Check-out and invoice</button>
                </div>
            </form>
        </div>
    </div>
</div>