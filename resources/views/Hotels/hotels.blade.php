@extends('layouts.layout')
@section('content')

<hotel :hotels="{{ $hotels }}" inline-template>
    <div>

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">Hotels
                            <button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#add-hotel"> New hotel +</button>
                        </div>

                        <div class="card-body">
                            <table class="table m-b-none">
                                <thead>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Postcode</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Xero Contact</th>
                                    <th></th>
                                </thead>

                                <tbody>
                                    <tr v-for="hotel in hotels">
                                        <td>@{{ hotel.name }}</td>
                                        <td>@{{ hotel.address }}</td>
                                        <td>@{{ hotel.postcode }}</td>
                                        <td>@{{ hotel.email }}</td>
                                        <td>@{{ hotel.contact_name }}</td>
                                        <!-- <td width="25%">@{{ landlord.xero_contactID }}</td> -->
                                        <td v-if="hotel.xero_contactID">
                                            <i class="fas fa-user fa-2x" style="color:green"></i>
                                        </td>
                                        <td v-else>
                                            <a href="#" v-on:click="linkHotelToXeroContact(hotel)"><i class="fas fa-user-edit fa-2x" style="color:red"></i></a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-sm btn-outline-secondary" v-on:click="edit(hotel)">Edit</a>
                                        </td>
                                        <!-- <td><a v-bind:href=" ' /landlords/' + landlord.id" class="btn btn-alt btn-outline-primary btn-sm">View</a></td> -->
                                    </tr>
                                </tbody>
                            </table>
                            @include('Hotels.link-to-xero')
                            @include('Hotels.edit')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('Modals.add-hotel')

    </div>
</landlord>



@endsection
