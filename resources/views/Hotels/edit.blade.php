<!-- Modal -->
<div class="modal fade" id="edit-hotel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit hotel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>

          <!-- <form class="form-horizontal" method="POST" action="/hotels"> -->
            <form class="form-horizontal" method="POST" :action=" '/hotels/' + selectedHotel.id">
                <input type="hidden" name="_method" value="PUT">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name" :value="selectedHotel.name" placeholder="Name" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="phone" type="text" class="form-control" name="address" :value="selectedHotel.address" placeholder="Address">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="postcode" type="text" class="form-control" name="postcode" :value="selectedHotel.postcode" placeholder="Postcode">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="contact" type="text" class="form-control" name="contact_name" :value="selectedHotel.contact_name" placeholder="Contact name">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control" name="email" :value="selectedHotel.email" placeholder="Email">
                            </div>
                        </div>

                        <!-- <hr> -->

                   <!--      <div class="form-group">
                            <label for="name" class="col-md-2 control-label">Notes</label>
                            <div class="col-md-10">
                                <input id="note" type="text" class="form-control" name="note" value="{{ old('note') }}">
                            </div>
                        </div> -->
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </form>
        </div>
    </div>
</div>