<strong>Properties</strong>

<span class="right-button">
	<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#addLandlordProperty"> Add a property +</button>
</span>


<div class="inset-items">
	@foreach($landlord->properties as $property)
	<div class="row">
		<div class="col-md-8">
			{{ $property->address }}, 
			{{ $property->postcode }}
		</div>
	</div>
	@endforeach
</div>