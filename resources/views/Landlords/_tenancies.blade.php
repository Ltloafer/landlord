   <strong>Tenancies</strong>
   <div class="inset-items">
   	@foreach($landlord->properties as $property)
   	@foreach ($property->tenancy as $tenancy)

   	<div class="row">
   		<div class="col-md-5">
   			{{$tenancy->start_date->format('jS \\of F Y')}} to {{ $tenancy->finish_date->format('jS \\of F Y')}}
   		</div>
   		<div class="col-md-7">
   			{{ $tenancy->property->address, $tenancy->property->postcode }}
   		</div>
   	</div>
   	@endforeach
   	@endforeach
   </div>