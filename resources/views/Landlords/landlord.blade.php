@extends('layouts.layout')

@section('content')

<landlord inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        <strong>
                            {{ $landlord->name }}, 
                            {{ $landlord->address }},
                            {{ $landlord->phone }}
                        </strong>
                    </div>

                    <div class="card-body" style="padding: 25px">


                        <span class="right-button">
                            <button class="btn btn-sm btn-default" data-toggle="modal" data-target="#addLandlordAgent">Add agent +</button>
                        </span>

                        <div class="row">
                            <div class="col-md-4">
                                <strong>Agent</strong>
                                <span style="margin-left: 50px">
                                        @if ($landlord->agent) 
                                  {{ $landlord->agent->name }}
                                  @else
                                  Golden Ticket
                                  @endif
                              </span>
                          </div>
                      </div>

                      <hr>


                  <strong>Assistants</strong>

                  <span class="right-button">
                    <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#addLandlordAssistant">Add an assistant +</button>
                </span>

                @if ($landlord->assistants->count() > 0)

                <div class="inset-items">
                    <div class="row">
                        @foreach($landlord->assistants as $assistant)
                        <div class="col-md-4">
                            <strong>{{ $assistant->name }}</strong><br>
                            Phone <span class="ml-2">{{ $assistant->phone }}</span><br>
                            Email <span class="ml-2"><a href="mailto:{{$assistant->email}}">{{ $assistant->email}}</a></span><br>
                        </div>
                        @endforeach
                    </div>
                </div>

                @endif

                <hr>

                @include('documentStatus', ['model' => $landlord])

                <hr>

                @include('Landlords._properties')

                <hr>

                @include('Landlords._tenancies')

                <hr>

                <view-notes model="landlord" :modelid="{{ $landlord->id }}" :notes="{{ $notes }}"></view-notes>

            </div>

        </div>
    </div>
</div>
</div>

@include('Modals.add-landlord-assistant')

@include('Modals.add-landlord-agent')

@include('Modals.upload-document', ['modelType' => 'landlord', 'model' => $landlord])

@include('Modals.add-property')

@include('Modals.add-note', ['modelType' => 'landlord', 'model' => $landlord])

</div>
</landlord>


@endsection
