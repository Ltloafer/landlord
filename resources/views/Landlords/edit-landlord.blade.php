@extends('layouts.layout')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit landlord
                </div>

                <div class="card-body">

                    <form class="form-horizontal" method="POST" action="/landlords/{{ $landlord->id }}">

                        {{ method_field('PUT') }}
                        {{ csrf_field() }}

                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="form-group">
                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ $landlord->name }}" placeholder="Name" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8">
                                        <input id="phone" type="text" class="form-control" name="phone" value="{{ $landlord->phone }}" placeholder="Phone">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ $landlord->email }}" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="form-group">
                                <div class="col-md-8">
                                    <input id="address" type="text" class="form-control" name="address" value="{{ $landlord->address }}" placeholder="Home address">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8">
                                    <input id="postcode" type="text" class="form-control" name="postcode" value="{{ $landlord->postcode }}" placeholder="Postcode">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                <div class="modal-footer">
                                    <a href="/landlords" class="btn btn-secondary">Close</a>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </form>

              <!--       <div class="row">
                        <div class="col-md-8">
                            <form class="form-horizontal" method="POST" action="/landlords/{{ $landlord->id }}">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger ml-4">Delete</button>
                            </form>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
