@extends('layouts.layout')
@section('content')

<landlord :landlords="{{ $landlords }}" inline-template>
  <div>

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Landlords
                        <button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#add-landlord"> Add a landlord +</button>
                    </div>

                    <div class="card-body">
                        <table class="table m-b-none">
                            <thead>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Xero Contact</th>
                                <th></th>
                            </thead>

                            <tbody>
                                <tr v-for="landlord in landlords">
                                    <td>@{{ landlord.name }}
                                        <div class="small text-muted">
                                            <span>@{{ landlord.address }}.</span> @{{ landlord.postcode }}</div>
                                        </td>
                                        <td>@{{ landlord.phone }}</td>
                                        <td>@{{ landlord.email }}</td>
                                        <!-- <td width="25%">@{{ landlord.xero_contactID }}</td> -->
                                        <td v-if="landlord.xero_contactID">
                                            <i class="fas fa-user fa-2x" style="color:green"></i>
                                        </td>
                                        <td v-else>
                                            <a href="#" v-on:click="linkLandlordToXeroContact(landlord)"><i class="fas fa-user-edit fa-2x" style="color:red"></i></a>
                                        </td>
                                        <td>
                                            <a :href=" '/landlords/' + landlord.id + '/edit' " class="btn btn-sm btn-outline-secondary float-right">View/edit</a>
                                            <td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    @include('Landlords.link-to-xero')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @include('Modals.add-landlord')

            </div>
        </landlord>



        @endsection
