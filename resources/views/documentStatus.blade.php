<documentstatus inline-template>
    <div>

        <strong>Documentation status</strong> 

        <span class="document-status">
            @if ($model->hasDocumentationComplete($requiredDocuments))
            <i class="fas fa-check-circle fa-2x" style="color:green"></i>
            @else
            <i class="fas fa-times-circle fa-2x" style="color:red"></i>
            @endif
        </span>

        <span class="view-documents-button">
            <span v-if="! viewDocumentation">
                <button class="btn btn-sm btn-outline-primary" v-on:click="openDocumentationView()">View</button>
            </span>

            <span v-else>
                <button class="btn btn-sm btn-outline-primary" v-on:click="closeDocumentationView()">Close</button>
            </span>
        </span>

        <div v-if="viewDocumentation" class="inset-items">
            <div class="row">
                @foreach($requiredDocuments as $document)
                <div class="col-md-6">
                    <ul>
                        <li>{{ $document->name }}
                            <span class="list-details-checks">
                                @if ($model->hasDocument($document->id))

                                <form role="form" method="POST" action="downloadDocument/{{ $document->id }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="model_id" value="{{ $model->id }}">
                                    <button type="submit"><i class="fa fa-download" aria-hidden="true"></i></button>
                                </form>

                                <i class="fas fa-check"></i>
                                @else 
                                <i class="fas fa-times"></i>
                                @endif
                            </span>
                        </li>
                    </ul>
                </div>
                @endforeach
            </div>

            <p>
                <button class="btn btn-sm btn-primary" style="float:right" v-on:click="uploadDocument()"> Upload document</button>
            </p>
        </div>

    </div>
</documentstatus>