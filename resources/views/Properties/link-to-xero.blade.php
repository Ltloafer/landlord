
<!-- Add Fee modal ---------------------------------------------------------------->
<div class="modal" id="linkPropertyToXero" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Link @{{ property.address }} to a Xero nominal code</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" :action=" '/properties/' + selectedProperty.id">
                <input type="hidden" name="_method" value="PUT">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="form-group">
                            <label for="type" class="col-md-4 control-label">Select nomical code</label>
                            <div class="col-md-12">
                                <select class="form-control" id="xero_code" name="xero_code">
                                    <option disabled selected>Select nominal code</option>
                                    <template v-for="code in xeroCodes">
                                        <option :value="code.Code">@{{ code.Code }}, @{{ code.Name }}</option>
                                    </template>
                                </select>
                            </div>
                        </div>

                    <!--     <div class="row">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="createCodeOnXero" value="connect" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Create code on Xero
                                </label>
                            </div>
                        </div> -->
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- End modal ---------------------------------------------------------------->
