 <strong>Tenancies</strong>
<!-- 
 <span class="right-button">
    <button class="btn btn-sm btn-default" data-toggle="modal" data-target="#addNewTenancy">Add tenancy +</button>
</span> -->

<div class="inset-items">
    @foreach($property->tenancy as $tenancy)

    <div class="row">
        <div class="col-md-4">
            {{ $tenancy->start_date->format('jS \\of F Y') }} - {{ $tenancy->finish_date->format('jS \\of F Y') }}
        </div>

        <div class="col-md-4">
            {{ $tenancy->tenant->name }}
        </div>

        <div class="col-md-3">
            <a href="/tenancies/{{ $tenancy->id }}"> View</a>
        </div>
    </div>

    @endforeach
</div>