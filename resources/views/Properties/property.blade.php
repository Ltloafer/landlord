@extends('layouts.layout')

@section('content')

<property :propertyid="{{ $property->id }}" inline-template>

  <div>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        <a href="/properties" style="color: #6c757d">Properties / </a> <strong>{{$property->address}}. {{$property->postcode}}.</strong>
                    </div>

                    <div class="card-body" style="padding: 25px; padding-left: 50px; padding-right: 50px">

                      <!--   <span class="right-button">
                            <button class="btn btn-sm btn-default" data-toggle="modal" data-target="#addAgent">Add agent +</button>
                        </span> -->

                        <div class="row">
                            <div class="col-md-4">
                                <strong>Landlord</strong>
                                <span style="margin-left: 50px">
                                  @if ($property->landlord)
                                  {{ $property->landlord->name }}
                                @endif</span>
                            </div>
                            <div class="col-md-8">
                                <strong>Agent</strong>
                                <span style="margin-left: 50px">
                                  @if ($property->agent) 
                                  {{ $property->agent->name }}
                                  @else
                                  Golden Ticket
                                  @endif
                              </span>
                          </div>
                      </div>

                      <hr>

                      @include('documentStatus', ['model' => $property])

                      <hr>

                      @include('Properties._tenancies')

                      <hr>

                      <view-notes model="property" :modelid="{{ $property->id }}" :notes="{{ $notes }}"></view-notes>

                  </div>
              </div>
          </div>
      </div>
  </div>


  @include('Modals.upload-document', ['modelType' => 'property', 'model' => $property])

  @include('Modals.add-tenancy')

  @include('Modals.add-note', ['modelType' => 'property', 'model' => $property])

</div>
</property>

@endsection
