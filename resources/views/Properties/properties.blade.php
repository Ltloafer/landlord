@extends('layouts.layout')
@section('content')

<properties :properties="{{ $properties }}" inline-template>
    <div>

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Properties
                            <button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#addLandlordProperty"> Add a property +</button>
                            <!-- <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#addLandlordProperty"> Add a property +</button> -->
                        
                        </div>

                        <div class="card-body">
                            <table class="table m-b-none">
                                <thead>
                                    <th>Address</th>
                                    <th>Landlord</th>
                                    <th>Agent</th>
                                </thead>

                                <tbody>
                                <tr v-for="property in properties">
                                    <td>@{{ property.address }}, @{{ property.postcode }}</td>
                                    <td v-if="property.landlord">@{{ property.landlord.name }}</td>
                                    <td v-else></td>

                                    <td v-if="property.agent">@{{ property.agent.name }}</td>
                                    <td v-else>Golden Ticket</td>
                                  
                                    <td><a v-bind:href=" ' /properties/' + property.id" class="btn btn-outline-secondary btn-sm float-right">View</a></td>
                                </tr>
                            </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('Modals.add-property')

    </div>
</properties>

@endsection
