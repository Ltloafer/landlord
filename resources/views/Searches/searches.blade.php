@extends('layouts.layout')
@section('content')

<searches inline-template>

  <div>

   <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">

          <div class="card-header" style="line-height: 2.0" >Searches
            <button v-if="liveSearches" class="btn btn-sm btn-outline-secondary float-right ml-1" v-on:click="viewArchived()">include archived</button>
            <button v-else class="btn btn-sm btn-outline-secondary float-right ml-1" v-on:click="getLiveSearches()">include archived <i class="fas fa-check"></i></button>

            <button class="btn btn-sm btn-outline-secondary float-right ml-5" data-toggle="modal" data-target="#addSearch"> Add a search +</button>
            <input class="search float-right" type="text" v-model="searchQuery" placeholder="Filter searches...">
          </div>

          <div class="card-body">

            <display-searches :data="searches" :columns="['name', 'last note', '', '']" :filter-key="searchQuery" v-on:reloadsearches="getSearches" 
            inline-template>
            <div>

              <table class="table m-b-none table-hover customer-sales">
                <thead>
                 <tr>
                  <th v-for="key in columns"
                  @click="sortBy(key)"
                  :class="{ active: sortKey == key }">
                  @{{ key | capitalize }}
                  <span class="arrow" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'">
                  </span>
                </th>
              </tr>
            </thead>

            <tbody>
              <!-- <tr v-for="search in filteredData" class='clickable-row' v-on:click="openSearchesModal(search)"> -->
                <tr v-for="search in filteredData" v-bind:class="{ 'table-secondary': offerMadeOnSearch(search) }">
                  <td v-if="search.deleted_at" style="color: grey"><i>@{{ search.name }}</i></td>
                  <td v-else>@{{ search.name }}</td>

                  <td v-if="search.deleted_at" style="color: grey"><i>@{{ search.lastNote }}</i></td>
                  <td v-else>@{{ search.lastNote }}</td>

                  <td><a :href=" '/searches/' + search.id" class="btn btn-sm btn-outline-secondary float-right">View</a></td>

                  <td><span v-if="search.offer_sent" class="float-right">Offer sent @{{ search.offer_sent | dateFormat }}</span></td>

                  <td>
                    <span v-if="search.starred" style="color: red"  v-on:click.prevent="removeStarred(search)"><i class="fas fa-star fa-2x float-right"></i></span>
                    <span v-else><a href="#"  style="color: grey" v-on:click.prevent="searchIsStarred(search)"><i class="far fa-star fa-2x float-right"></i></a></span>
                  </td>
                </tr>
              </tbody>
            </table>

            @include('Modals.add-search')

            <div v-if="selectedSearch">
              <view-search :selectedsearch="selectedSearch" v-on:reloadsearches="reloadSearches"></view-search>
            </div>

          </div>
        </display-searches>

      </div>
    </div>
  </div>
</div>
</div>

</div>

</searches>


@endsection
