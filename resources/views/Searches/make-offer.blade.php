@extends('layouts.layout')

@section('content')

<searches :search="{{ $search }}" inline-template>
    <div>

        <div class="container-fluid">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <div class="card">

                    <div class="card-header" style="line-height: 2.0">

                        <a href="/searches" style="color: inherit; underline: none">Searches</a> /  

                        <strong>
                         <a href="/searches/{{ $search->id }}" style="color: inherit; underline: none">{{ $search->name }}</a>
                     </strong>
                     / make an offer
                 </div>

                 <div class="card-body" style="padding: 0px">
                   <div class="inset-items">
                   </div>

                   <div class="modal-body">
                    <div class="container-fluid">

                     <div class="row">
                        <div class="col-sm-12 offset-sm-0">

                            <div class="row">
                                <div class="col-sm-12">
                                    <address>
                                        <strong>Property</strong><span class="search-attribute">
                                            @if ($search->property)
                                            {{ $search->property->address ?? '' }}. 
                                            {{ $search->property->postcode ?? '' }}
                                            @else
                                            Required
                                            @endif
                                        </span> <br>
                                        <hr>
                                    </address>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <address>
                                        <strong>Tenants</strong>
                                        <span class="search-attribute">{{ $search->tenant->name ?? $search->name }}</span>
                                    </address>
                                    <hr>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-sm-12">
                                    <strong>Tenancy term</strong>
                                    <span class="search-attribute">
                                        {{ optional($search->start_date)->format('jS \\of F Y') }} - 
                                        {{ optional($search->end_date)->format('jS \\of F Y') }}
                                    </span>
                                    <hr>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-sm-12">
                                    <strong>Deposit £</strong>
                                    <span class="float-right">
                                        @if ($search->holding_deposit)
                                        {{  formatAmount($search->displayDeposit()) }}
                                        @else
                                        Required
                                        @endif
                                    </span>
                                    <hr>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-sm-12">
                                    <strong>Rent offered £</strong>
                                    <span class="float-right">
                                        @if ($search->offer_amount)
                                        {{ formatAmount($search->displayOfferAmount()) }}
                                        @else 
                                        Required
                                        @endif
                                    </span>
                                    <hr>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-sm-12">
                                    <strong>Conditions of offer</strong>
                                    <span class="float-right">Standard
                                    </span>
                                    <hr>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="/searches/{{ $search->id }}/send-offer" type="button" class="btn btn-outline-success btn-sm float-right mb-3 ml-2">Send offer</a>
                                    <a href="/searches/{{ $search->id }}" type="button" class="btn btn-outline-danger btn-sm float-right mb-3 ml-2">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    </div>
</searches>

@endsection
