@extends('layouts.layout')

@section('content')

<searches inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        <strong>
                            {{ $search->name }}
                            <!-- {{ $search->address }}, -->
                            <!-- {{ $search->phone }} -->
                        </strong>
                        <div class="dropdown float-right display-inline">
                            <button class="btn btn-sm btn-outline-secondary  float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Convert to tenancy
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-direct-tenancy">Direct let (AST)</a>
                                <a class="dropdown-item" href="/search-to-direct-let/{{ $search->id}}">Direct let (AST)</a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-dir-install-tenancy">Short let (installments)</a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-splitComm-tenancy">Split comm </a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-splitComm-installments">Split comm (installments)</a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-hotel-tenancy">Hotel booking</a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body" style="padding: 25px">
                     <div class="inset-items">
                        <p><strong>Details</strong></p>

                        <div class="row">
                            <div class="col-sm-4">
                                <p><span>Email</span> <span class="ml-3">{{ $search->email }}</span></p>
                                <p><span>Phone</span> <span class="ml-3">{{ $search->phone }}</span></p>
                                <p><span>Address</span> <span class="ml-3">{{ $search->address }}</span></p>
                                <p><span>Previous tenant</span> <span class="ml-3">{{ $search->forPreviousTenant() }}</span></p>
                            </div>
                            <div class="col-sm-4">
                                <p><span>Production</span> <span class="ml-3">{{ $search->production ?? 'n/a'}}</span></p>
                                <p><span>Production company</span> <span class="ml-3">{{ $search->productionCompany->name ?? '' }}</span></p>
                                <p><span>Production contact</span> <span class="ml-3">{{ $search->productionCompany->contact ?? '' }}</span></p>
                                <p><span>Production address</span> <span class="ml-3">{{ $search->productionCompany->address ?? ''  }}</span></p>
                                <p><span>Production contact phone</span> <span class="ml-3">{{ $search->productionCompany->phone ?? '' }}</span></p>
                            </div>
                            <div class="col-sm-4">
                                <p><span>Reference No. </span> <span class="ml-3">{{ $search->job_id }}</span></p>
                                <p><span>Start date</span> <span class="ml-3">{{ $search->start_date }}</span></p>
                                <p><span>End date</span> <span class="ml-3">{{ $search->end_date }}</span></p>
                            </div>

                        </div>
                        <hr>


                        <!-- <strong>Assistants</strong> -->

                       <!--  <span class="right-button">
                            <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#addLandlordAssistant">Add an assistant +</button>
                        </span> -->


                        <strong>Search areas</strong>
                        <span class="right-button">
                            <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#add-agent-search-area">Add / remove  areas</button>
                        </span>

                        <div class="inset-items">
                            <div class="row">
                                <!-- <template v-for="area in searchAreas">  -->
                                    @foreach ($searchAreas as $area)
                                    <div class="col-md-4">
                                        <p>{{ $area->area }}
                                            @if ($search->isRegisteredToArea($area))
                                            <span style="margin-left: 20px"><strong><i class="fas fa-check"></i></strong></span>
                                            @endif
                                        </p>
                                    </div>
                                    @endforeach
                                </div>

                                <span class="right-button">
                                    <!-- <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" >Search agents</button> -->
                                </span>

                            </div>

                            <hr>

                            <view-notes model="search" :modelid="{{ $search->id }}" :notes="{{ $search->notes }}"></view-notes>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        @include('Modals.add-search-search_area')
        @include('Searches.Modals.add-direct-installments')
        @include('Searches.Modals.add-direct-tenancy')
        @include('Searches.Modals.add-splitComm-tenancy')
        @include('Searches.Modals.add-splitComm-installments')
        @include('Searches.Modals.add-hotel-tenancy')

        @include('Modals.add-note', ['modelType' => 'search', 'model' => $search])

    </div>
</searches>



@endsection
