@extends('layouts.layout')

@section('content')

<searches :search="{{ $search }}" inline-template>
    <div>

        <div class="container-fluid">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <div class="card">

                    <div class="card-header" style="line-height: 2.0">

                        <a href="/searches" style="color: inherit; underline: none">Searches</a> /  

                        <strong>
                            {{ $search->name }}
                        </strong>

                        @if ($search->tenancies->isNotEmpty())
                        <div class="dropdown float-right display-inline">
                            <button class="btn btn-sm btn-outline-secondary float-right dropdown-toggle ml-2" type="button" id="dropdownTenancyButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                View
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownTenancyButton">
                                @foreach($search->tenancies as $searchTenancy)
                                <a href="/tenancies/{{ $searchTenancy->id }}" class="dropdown-item">Tenacy {{ $searchTenancy->id }}</a>
                                @endforeach
                            </div>
                        </div>
                        @endif

                        @if (! $search->hasBeenConverted())


                        <div class="dropdown float-right display-inline">
                            <button class="btn btn-sm btn-outline-secondary  float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Convert to tenancy
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @if ($search->property && $search->tenant)

                                @if ($search->property->landlord_id)
                                <a href="/searches/{{ $search->id }}/new-tenancy-direct" class="dropdown-item">Direct</a>
                                <!-- <a class="dropdown-item" data-toggle="modal" data-target="#add-direct-tenancy">Direct let (AST)</a> -->
                                <!-- <a class="dropdown-item" data-toggle="modal" data-target="#add-dir-install-tenancy">Short let (installments)</a> -->
                                @elseif ($search->property->agent_id)
                                <a href="/searches/{{ $search->id }}/new-tenancy" class="dropdown-item">Split comm </a>
                                <!-- <a class="dropdown-item" data-toggle="modal" data-target="#add-splitComm-tenancy">Split comm </a> -->
                                <!-- <a class="dropdown-item" data-toggle="modal" data-target="#add-splitComm-installments">Split comm (installments)</a> -->
                                @endif
                                @endif
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-hotel-tenancy">Hotel booking</a>
                            </div>
                        </div>

                        @if (! $search->offer_sent)
                        <a href="/searches/{{ $search->id }}/make-offer" type="button" class="btn btn-outline-success btn-sm float-right mr-2">Make offer</a>
                        @elseif ($search->offer_sent && ! $search->offer_accepted)
                        <a href="/searches/{{ $search->id }}/make-offer" type="button" class="btn btn-outline-success btn-sm float-right mr-2">Make new offer</a>
                        @endif

                        @endif

                    </div>



                    <div class="card-body" style="padding: 0px">
                     <div class="inset-items">
                        <!-- <p><strong>Tenant</strong></p> -->
                    </div>

                    <div class="modal-body">
                        <div class="container-fluid">

                            <div class="row">

                                <div class="col-sm-4">
                                    <address>
                                        <strong>Property</strong><span class="search-attribute">{{ $search->property->address ?? '' }}</span> <br>
                                        <hr>

                                        <div class="text-muted">

                                            Postcode<span class="search-attribute">{{ $search->property->postcode ?? '' }} </span><br><br>

                                            @if ($search->property)

                                            @if ($search->property->landlord)
                                            Type<span class="search-attribute">Direct</span><br>
                                            Landlord<span class="search-attribute">{{ $search->property->landlord->name ?? '' }} </span><br>

                                            @elseif ($search->property->agent)
                                            Type<span class="search-attribute">Split commission</span><br>
                                            Agent<span class="search-attribute">{{ $search->property->agent->name ?? '' }} </span><br>
                                            @endif

                                            @else
                                            <br><br>
                                            @endif
                                            <!-- Phone <span class="search-attribute">{{ $search->property->phone ?? $search->phone }} </span><br> -->
                                            <!-- Email <span class="search-attribute">{{ $search->tenant->email ?? $search->email}} </span><br><br> -->
                                            <!-- Agent<br> -->
                                            <br>
                                        </div>

                                    </address>
                                </div>

                                <div class="col-sm-4">
                                    <address>
                                        <strong>Tenant</strong> <span class="search-attribute">{{ $search->tenant->name ?? '' }}</span>
                                        <div class="text-muted">

                                            <hr>
                                            Address <span class="search-attribute">{{ optional($search->tenant)->displayAddress() }}</span> <br>
                                            Postcode <span class="search-attribute">{{ $search->tenant->contact->postcode ?? '' }} </span><br>
                                            Phone <span class="search-attribute">{{ $search->tenant->phone ?? $search->phone }} </span><br>
                                            Email <span class="search-attribute">{{ $search->tenant->email ?? $search->email}} </span><br><br>
                                        </div>

                                    </address>
                                </div>


                                <div class="col-sm-4">
                                    <address>
                                        <strong>Production company</strong><span class="search-attribute">{{ $search->productionCompany->name?? 'n/a' }}</span>
                                        <div class="text-muted">
                                            <hr>
                                            Address <span class="search-attribute">{{ optional($search->productionCompany)->displayAddress() }}</span> <br>
                                            Postcode <span class="search-attribute">{{ $search->productionCompany->postcode ?? '' }} </span><br>
                                            Phone <span class="search-attribute">{{ $search->productionCompany->phone ?? '' }} </span><br>
                                            Email <span class="search-attribute">{{ $search->productionCompany->email ?? '' }} </span><br><br>
                                            Invoice production company <span class="search-attribute"> {{ $search->productionCompany ? 'Yes' : 'No' }}</span><br>
                                        </div>
                                    </address>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="/" data-toggle="modal" data-target="#enter-property">Select property</a> / <a href="/" data-toggle="modal" data-target="#addLandlordProperty">new property</a>
                                    <hr>
                                </div>
                                <div class="col-sm-4">
                                    <a href="/" data-toggle="modal" data-target="#enter-tenant">Select tenant</a> /
                                    <a href="/" data-toggle="modal" data-target="#add-tenant">new tenant</a> 
                                    <hr>
                                </div>
                                <div class="col-sm-4">
                                    <a href="/" data-toggle="modal" data-target="#enter-production-company">Production company</a> / 
                                    <a href="/" data-toggle="modal" data-target="#add-production-company">new production company</a>
                                    <hr>
                                </div>
                            </div>


                            <div class="row mt-3">
                                <div class="col-sm-4">
                                    <strong>Search period</strong>
                                    <span class="search-attribute">
                                        @if (! $search->start_date && ! $search->end_date)
                                        <a href="#" data-toggle="modal" data-target="#add-search-period">Add search period</a>
                                        @else
                                        <a href="#" data-toggle="modal" data-target="#edit-search-period">
                                            {{ optional($search->start_date)->format('jS \\of F Y') }} - 
                                            {{ optional($search->end_date)->format('jS \\of F Y') }}
                                        </a>
                                        @endif
                                    </span>
                                    <hr>
                                </div>

                                <div class="col-sm-4">
                                    <strong>Break clause</strong>
                                    <span class="float-right">
                                        @if ($search->break_clause)
                                        <a href="#" data-toggle="modal" data-target="#add-break-clause">{{ $search->break_clause }} months</a>
                                        @else
                                        <a href="#" data-toggle="modal" data-target="#add-break-clause">Add</a>
                                        @endif
                                    </span>
                                    <hr>
                                </div>

                                <div class="col-sm-4">
                                    <strong>Commission</strong>
                                    <span class="float-right">
                                        @if ($search->comm_rate)
                                        <a href="#" data-toggle="modal" data-target="#edit-commission">{{ $search->displayCommissionRate() }} %</a>
                                        @else
                                        <a href="#" data-toggle="modal" data-target="#edit-commission">Add</a>
                                        @endif
                                    </span>
                                    <hr>
                                </div>
                            </div>


                            <div class="row mt-3">
                                <div class="col-sm-4">
                                    <strong>Deposit £</strong>
                                    <span class="float-right">
                                        @if ($search->holding_deposit)
                                        <a href="#" data-toggle="modal" data-target="#add-deposit">{{  formatAmount($search->displayDeposit()) }}</a>
                                        @else
                                        <a href="#" data-toggle="modal" data-target="#add-deposit">Add</a>
                                        @endif
                                    </span>
                                    <hr>
                                </div>

                                <div class="col-sm-4">
                                    <strong>Amount offered per week £</strong>
                                    <span class="float-right">
                                        @if ($search->offer_amount)
                                        <a href="#" data-toggle="modal" data-target="#add-offer-amount">{{ formatAmount($search->displayOfferAmount()) }}</a>
                                        @else
                                        <a href="#" data-toggle="modal" data-target="#add-offer-amount">Add</a>
                                        @endif
                                    </span>
                                    <hr>
                                </div>

                                <div class="col-sm-4">
                                    <strong>Offer sent</strong>
                                    <span class="float-right">
                                        {{ optional($search->offer_sent)->format('jS \\of F Y H:i') ?? 'No offer made' }}
                                    </span>
                                    <hr>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-sm-12">
                                    <strong>Search areas</strong>
                                    <span class="right-button">
                                        <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#add-agent-search-area">Add / remove  areas</button>
                                    </span>
                                </div>
                            </div>

                            <hr>

                            <view-notes model="search" :modelid="{{ $search->id }}" :notes="{{ $search->notes }}"></view-notes>

                            <hr>

                            @if (! $search->hasBeenConverted())

                            <button type="button" class="btn btn-outline-danger btn-sm float-right mb-3" data-toggle="modal" data-target="#archive-search">Archive</button>

                            @else 

                            <p>{{ $search->hotelBookings }}</p>
                            <p>{{ $search->tenancies }}</p>

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('Searches.Modals.edit-search-period')

    </div>
</searches>

@include('Modals.add-tenant')
@include('Searches.Modals.enter-tenant')
@include('Searches.Modals.enter-property')
@include('Searches.Modals.enter-production_company')

<!-- @include('Searches.Modals.add-direct-installments') -->
@include('Searches.Modals.add-direct-tenancy')

@include('Modals.add-search-search_area')
<!-- @include('Searches.Modals.add-direct-installments') -->
<!-- @include('Searches.Modals.add-direct-tenancy') -->
<!-- @include('Searches.Modals.add-splitComm-tenancy') -->
<!-- @include('Searches.Modals.add-splitComm-installments') -->
@include('Searches.Modals.add-hotel-tenancy')

@include('Searches.Modals.add-property')
@include('Searches.Modals.add-tenant')
@include('Searches.Modals.add-production-co')
@include('Searches.Modals.archive')

@include('Searches.Modals.add-search-period')
@include('Searches.Modals.add-break-clause')
@include('Searches.Modals.add-commission')
@include('Searches.Modals.add-deposit')
@include('Searches.Modals.add-offer-amount')


@include('Modals.add-note', ['modelType' => 'search', 'model' => $search])



@endsection
