@extends('layouts.layout')

@section('content')

<searches :search="{{ $search }}" inline-template>
    <div>

        <div class="container-fluid">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <div class="card">

                    <div class="card-header" style="line-height: 2.0">
                        <a href="/searches" style="color: inherit; underline: none">Searches</a> /  
                        <strong>
                           <a href="/searches/{{ $search->id }}" style="color: inherit; underline: none">{{ $search->name }}</a>
                       </strong>
                       / New tenancy  
                   </div>

                   <div class="card-body" style="padding: 0px">
                     <div class="inset-items">
                        <!-- <p><strong>Tenant</strong></p> -->
                    </div>

                    <div class="modal-body">
                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-sm-10 offset-sm-1">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <address>
                                                <strong>Property</strong><span class="search-attribute">{{ $search->property->address ?? '' }}. {{ $search->property->postcode ?? '' }} </span> <br>
                                                <hr>
                                            </address>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <address>
                                                <strong>Type</strong> <span class="search-attribute">Direct</span>
                                            </address>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <address>
                                                <strong>Landlord</strong> <span class="search-attribute">{{ $search->property->landlord->name ?? '' }} </span>
                                            </address>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <address>
                                                <strong>Tenant</strong> <span class="search-attribute">{{ $search->tenant->name ?? '' }}</span>
                                            </address>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <address>
                                                <strong>Production company</strong><span class="search-attribute">{{ $search->productionCompany->name?? 'n/a' }}</span>
                                            </address>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-12">
                                            <strong>Tenancy period</strong>
                                            <span class="search-attribute">
                                                @if (! $search->start_date && ! $search->end_date)
                                                Incomplete
                                                @else
                                                {{ optional($search->start_date)->format('jS \\of F Y') }} - 
                                                {{ optional($search->end_date)->format('jS \\of F Y') }}
                                                @endif
                                            </span>
                                            <!-- <hr> -->
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-sm-12">
                                            <strong>Break clause</strong>
                                            <span class="float-right">
                                                @if ($search->break_clause)
                                                {{ $search->break_clause }} months
                                                @else
                                                None
                                                @endif
                                            </span>
                                            <!-- <hr> -->
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-sm-12">
                                            <strong>Commission</strong>
                                            <span class="float-right">
                                              {{ $search->displayCommissionRate() ?? '0' }} %
                                          </span>
                                          <!-- <hr> -->
                                      </div>
                                  </div>

                                  <div class="row mt-3">
                                    <div class="col-sm-12">
                                        <strong>Deposit £</strong>
                                        <span class="float-right">
                                            @if ($search->holding_deposit)
                                            {{  formatAmount($search->displayDeposit()) }}
                                            @else
                                            None
                                            @endif
                                        </span>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-sm-12">
                                        <strong>Amount offered per week £</strong>
                                        <span class="float-right">
                                            @if ($search->offer_amount)
                                            {{ formatAmount($search->displayOfferAmount()) }}
                                            @else
                                            None
                                            @endif
                                        </span>
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                      <a href="#" type="button" class="btn btn-outline-success btn-sm float-right mb-3" data-toggle="modal" data-target="#add-direct-tenancy">Complete (AST)</a>
                                      <!-- <a class="dropdown-item" data-toggle="modal" data-target="#add-direct-tenancy">Direct let (AST)</a> -->

                                      <a href="#" type="button" class="btn btn-outline-success btn-sm float-right mb-3 mr-2" data-toggle="modal" data-target="#add-dir-install-tenancy">Complete with installments</a>

                                      <a href="/searches/{{ $search->id }}" type="button" class="btn btn-outline-danger btn-sm float-right mb-3 mr-2">Back</a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          @include('Searches.Modals.add-splitComm-tenancy')
          @include('Searches.Modals.add-direct-installments')
          @include('Searches.Modals.add-direct-tenancy')

      </div>
  </searches>

  @endsection
