<!-- Modal -->
<div class="modal fade" id="enter-property" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Choose property</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/setPropertyOnSearch">
                <input type="hidden" name="searchId" value="{{ $search->id }}">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Property</div>
                                    </div>
                                    <select class="form-control" id="property" name="property_id" required>
                                        <option disabled selected>Select</option>
                                        @foreach(\App\Property::all()->sortBy('address') as $property)
                                        <option value="{{$property->id}}">{{$property->address}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>