<!-- Modal -->
<div class="modal fade" id="add-production-company" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">

        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Add new production company</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <form class="form-horizontal" method="POST" action="/production-companies">

            <input type="hidden" name="searchId" value="{{ $search->id }}">

            {{ csrf_field() }}

            <div class="modal-body">
                <div class="container-fluid">

                    <div class="form-group">
                        <!-- <label for="house_no" class="col-md-2 control-label">Name</label> -->

                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="phone" type="text" class="form-control" name="contact" value="{{ old('contact') }}" placeholder="Contact">
                        </div>
                    </div>

                       <div class="form-group">
                            <div class="col-md-8">
                                <input id="line1" type="text" class="form-control" name="line_1" value="{{ old('line_1') }}" placeholder="Address Line 1">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="line2" type="text" class="form-control" name="line_2" value="{{ old('line_2') }}" placeholder="Line 2">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input id="line3" type="text" class="form-control" name="line_3" value="{{ old('line_3') }}" placeholder="Line 3">
                            </div>
                        </div>



                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone">
                        </div>
                    </div>

            <!--         <div class="form-group">
                        <div class="col-md-10">
                            <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="Address">
                        </div>
                    </div> -->

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="postcode" type="text" class="form-control" name="postcode" value="{{ old('postcode') }}" placeholder="Postcode">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

        </form>
    </div>
</div>
</div>