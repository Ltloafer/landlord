
<div class="modal" id="edit-commission" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add commission</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="POST" action="/searches/{{ $search->id}}">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="searchId" value="{{ $search->id }}">

        {{ csrf_field() }}


        <div class="modal-body">
          <div class="container-fluid">

            <div class="form-group">
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">Commission %</div>
                </div>
                  <input id="comm_rate" type="text" class="form-control" name="comm_rate" value="{{ $search->displayCommissionRate() }}" placeholder=" {{ $search->displayCommissionRate() }}" required autocomplete="off">
                </div>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-secondary" v-on:click.prevent="closeEditSchedule()" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>