<!-- Modal -->
<div class="modal fade" id="add-hotel-tenancy" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add new hotel booking</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/hotel-bookings">
                <input type="hidden" name="searchId" value="{{ $search->id }}">


                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Hotel</div>
                                    </div>
                                    <select class="form-control" id="hotel" name="hotel_id" required>
                                        <option disabled selected>Select</option>
                                        @foreach(\App\Hotel::all()->sortBy('name') as $hotel)
                                        <option value="{{ $hotel->id }}">
                                            {{ $hotel->name }} 
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Confirmation reference</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="confirmation_reference" value="{{ old('confirmation_reference') }}" required autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <p>Add new hotel?</p>

                        <div class="form-row mt-3">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Hotel name</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="hotel_name" value="{{ old('hotel_name') }}" autocomplete="off">
                                </div>
                            </div>


                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Email</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="hotel_email" value="{{ old('hotel_email') }}" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Address</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="hotel_address" value="{{ old('hotel_address') }}" autocomplete="off">
                                </div>
                            </div>


                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Postcode</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="hotel_postcode" value="{{ old('hotel_postcode') }}" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="form-row">
                            <div class="form-group col-md-10">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Working title / Production company</div>
                                    </div>
                                    @if ($search->production)
                                    <input id="name" type="text" class="form-control" name="production" value="{{ $search->production }}" autocomplete="off">
                                    @else
                                    <input id="name" type="text" class="form-control" name="production" value="{{ old('production') }}" autocomplete="off" required>
                                    @endif
                                </div>
                            </div>
                        </div>

                    <!--     <div class="form-row">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Tenant</div>
                                    </div>
                                    @if ($search->tenant)
                                    <input id="name" type="text" class="form-control" value="{{ $search->tenant->name }}" autocomplete="off" disabled>
                                    @else
                                    <input id="name" type="text" class="form-control" name="tenant_name" value="{{ $search->name }}" autocomplete="off" required>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Address</div>
                                    </div>
                                    @if ($search->tenant)
                                    <input id="name" type="text" class="form-control" value="{{ $search->tenant->address }}" autocomplete="off" disabled>
                                    @else
                                    <input id="name" type="text" class="form-control" name="tenant_address" value="{{ $search->address }}" autocomplete="off" required>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Phone</div>
                                    </div>
                                    @if ($search->tenant)
                                    <input id="name" type="text" class="form-control" value="{{ $search->tenant->phone }}" autocomplete="off" disabled>
                                    @else
                                    <input id="name" type="text" class="form-control" name="tenant_phone" value="{{ $search->phone }}" autocomplete="off" required>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Email</div>
                                    </div>
                                    @if ($search->tenant)
                                    <input id="name" type="text" class="form-control" value="{{ $search->tenant->email}}" autocomplete="off" disabled>
                                    @else
                                    <input id="name" type="text" class="form-control" name="tenant_email" value="{{ $search->email }}" autocomplete="off" required>
                                    @endif
                                </div>
                            </div>
                        </div> -->


                        @if ($search->forProductionCompany())

                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Production</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="tenant_name" value="{{ $search->production ?? '' }}" disabled>
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Company</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="tenant_name" value="{{ $search->productionCompany->name }}" disabled>
                                </div>
                            </div>
                        </div>
                        @endif

                        <!-- <hr> -->

                        <div class="form-row mt-3">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Check-in date</div>
                                    </div>
                                    <input id="startdate" type="DATE" class="form-control" name="startdate" value="{{ old('startdate') }}" placeholder="yyyy-mm-dd" required>
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Check-out date</div>
                                    </div>
                                    <input id="enddate" type="DATE" class="form-control" name="enddate" value="{{ old('enddate') }}" placeholder="yyyy-mm-dd" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-1">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Nightly rate £</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="nightly_rate" value="{{ old('nightly_rate') }}" autocomplete="off">
                                </div>
                            </div>


                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Commission rate %</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="comm_rate" value="{{ old('comm_rate') }}" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-1">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Room type</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="room_type" value="{{ old('room_type') }}" required autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">
                          <!--     <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Confirmation number</div>
                                </div>
                                <input id="name" type="text" class="form-control" name="confirmation_number" value="{{ old('confirmation_number') }}" required autocomplete="off">
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
</div>