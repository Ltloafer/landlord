<div class="modal fade" id="addLandlordProperty" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">


      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Register a property</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="POST" action="/properties">

        <input type="hidden" name="searchId" value="{{ $search->id }}">

        {{ csrf_field() }}

        <div class="modal-body">
         <div class="container-fluid">

<!-- 
          <div class="row">
            <div class="form-group col-md-12">
              <label for="landlord" class="col-md-8 control-label">Select landlord or agent</label>
              <div class="col-md-12">
                <select class="form-control" id="landlordId" name="landlordId">
                  @foreach(\App\Landlord::all() as $landlord)
                  <option value="{{$landlord->id}}">{{$landlord->name}}</option>
                  @endforeach
                  @foreach(\App\Agent::all() as $agent)
                  <option value="{{$agent->id}}">{{$agent->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div> -->

          <div class="row">
           <div class="form-group col-md-12">
             <div class="input-group mb-2">
              <div class="input-group-prepend">
                <div class="input-group-text">Landlord or agent</div>
              </div>
              <!-- <label for="landlord" class="col-md-8 control-label">Select agent</label> -->
              <!-- <div class="col-md-12"> -->
                <select class="form-control" id="agentId" name="agentOrLandlordId">
                  @foreach(\App\Landlord::all() as $landlord)
                  <option value="landlord_{{$landlord->id}}">{{$landlord->name}}</option>
                  @endforeach
                  @foreach(\App\Agent::all() as $agent)
                  <option value="agent_{{$agent->id}}">{{$agent->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="form-group col-md-12">
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">Address</div>
                </div>
                <input id="line1" type="text" class="form-control" name="address" value="{{ old('address') }}" required>
              </div>
            </div>
          </div>
<!-- 
          <div class="row">
            <div class="form-group col-md-12">
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">Line 1</div>
                </div>
                <input id="line1" type="text" class="form-control" name="line1" value="{{ old('line1') }}" required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-md-12">
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">Line 2</div>
                </div>
                <input id="line1" type="text" class="form-control" name="line1" value="{{ old('line1') }}" required>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-md-12">
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">Line 3</div>
                </div>
                <input id="line1" type="text" class="form-control" name="line1" value="{{ old('line1') }}" required>
              </div>
            </div>
          </div> -->

          <div class="row">
            <div class="form-group col-md-12">
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">Postcode</div>
                </div>
                <input id="postcode" type="text" class="form-control" name="postcode" value="{{ old('postcode') }}" required>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>

  </div>
</div>
</div>
