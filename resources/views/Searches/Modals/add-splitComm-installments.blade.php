<!-- Modal -->
<add-tenancy-form inline-template>
    <div class="modal fade" id="add-splitComm-installments" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Complete with installments</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal" method="POST" action="/tenancies">
                    <input type="hidden" name="tenancy_type" value="split-comm">
                    <input type="hidden" name="let_type" value="installments">
                    <input type="hidden" name="searchId" value="{{ $search->id }}">

                    {{ csrf_field() }}

                    <div class="modal-body">
                        <div class="container-fluid">

                         <!--    <div class="form-row">
                                <div class="form-group col-md-5">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Start date</div>
                                        </div>
                                        <input id="startdate" type="DATE" class="form-control" name="startdate" value="{{ old('startdate') }}" placeholder="yyyy-mm-dd" required>
                                    </div>
                                </div>

                                <div class="form-group col-md-5 offset-md-1">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">End date</div>
                                        </div>
                                        <input id="enddate" type="DATE" class="form-control" name="enddate" value="{{ old('enddate') }}" placeholder="yyyy-mm-dd" required>
                                    </div>
                                </div>
                            </div>
 -->
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Total rent £</div>
                                        </div>
                                        <input id="name" type="text" class="form-control" name="total_rent" value="{{ old('total_rent') }}"autocomplete="off" required>
                                    </div>
                                </div>

                                <div class="form-group col-md-5 offset-md-1">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Commission rate %</div>
                                        </div>
                                        <input id="name" type="text" class="form-control" name="comm_rate" value="{{ $search->displayCommissionRate() }}" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row mt-3">
                                <div class="form-group col-md-5">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Commission invoiced</div>
                                        </div>
                                        <select class="form-control" id="property" name="commission_charging" required>
                                            <option selected value="to_break">To break</option>
                                            <option value="all_upfront">All up front</option>
                                            <option value="by_period">By period</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-5">

                                </div>
                            </div>

                            <div class="form-row mt-4">
                                <div class="form-group col-md-5">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Number of installments</div>
                                        </div>
                                        <select v-model="numberOfInstallments" class="form-control" id="exampleFormControlSelect1">
                                          <option :value=1>1</option>
                                          <option :value=2>2</option>
                                          <option :value=3>3</option>
                                          <option :value=4>4</option>
                                          <option :value=5>5</option>
                                          <option :value=6>6</option>
                                          <option :value=7>7</option>
                                          <option :value=8>8</option>
                                          <option :value=9>9</option>
                                          <option :value=10>10</option>
                                      </select>
                                  </div>
                              </div>
                          </div>

                          <template v-for="no in numberOfInstallments">
                             <div class="form-row">
                                <div class="form-group col-md-4">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Invoice date</div>
                                        </div>
                                        <input id="name" type="DATE" class="form-control" :name="installmentDate(no)">
                                    </div>
                                </div>

                                <div class="form-group col-md-4 offset-md-2">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Payment £</div>
                                        </div>
                                        <input id="name" type="text" class="form-control" :name="installmentPayment(no)" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </template>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
</add-tenancy-form>