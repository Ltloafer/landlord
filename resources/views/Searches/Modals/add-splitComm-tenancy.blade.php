<!-- Modal -->
<div class="modal fade" id="add-splitComm-tenancy" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Complete split comm tenancy</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/tenancies">
                <input type="hidden" name="tenancy_type" value="split-comm">
                <input type="hidden" name="searchId" value="{{ $search->id }}">
                
                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="form-row mt-3">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Monthly rate £</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="monthly_rate" value="{{ old('monthly_rate') }}" autocomplete="off">
                                </div>
                            </div>


                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Commission rate %</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="comm_rate" value="{{ $search->displayCommissionRate() }}" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Commission invoiced</div>
                                    </div>
                                    <select class="form-control" id="property" name="commission_charging" required>
                                        <option selected value="to_break">To break</option>
                                        <option value="all_upfront">All up front</option>
                                        <option value="by_period">By period</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-5">

                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="form-group col-md-5">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Minimum notice period</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="notice_period" value="{{ old('notice_period') }}" placeholder="Days" required autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group col-md-5 offset-md-1">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Break clause period</div>
                                    </div>
                                    <input id="name" type="text" class="form-control" name="break_clause" value="{{ $search->break_clause }}" placeholder="Months" required autofocus autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>