<!-- Modal -->
<div class="modal fade" id="add-direct-tenancy" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add direct let tenancy</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/tenancies">
                <input type="hidden" name="tenancy_type" value="direct">
                <input type="hidden" name="searchId" value="{{ $search->id }}">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                  <!--   <div class="form-row mt-3">
                        <div class="form-group col-md-5">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Start date</div>
                                </div>
                                <input id="startdate" type="DATE" class="form-control" name="startdate" value="{{ old('startdate') }}" placeholder="yyyy-mm-dd" required>
                            </div>
                        </div>

                        <div class="form-group col-md-5 offset-md-1">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">End date</div>
                                </div>
                                <input id="enddate" type="DATE" class="form-control" name="enddate" value="{{ old('enddate') }}" placeholder="yyyy-mm-dd" required>
                            </div>
                        </div>
                    </div> -->

                    <div class="form-row mt-3">
                        <div class="form-group col-md-12">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Monthly rate £</div>
                                </div>
                                <input id="name" type="text" class="form-control" name="monthly_rate" value="{{ old('monthly_rate') }}" autocomplete="off" required>
                            </div>
                        </div>

<!-- 
                        <div class="form-group col-md-5 offset-md-1">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Commission rate %</div>
                                </div>
                                <input id="name" type="text" class="form-control" name="comm_rate" value="{{ old('comm_rate') }}" autocomplete="off" required>
                            </div>
                        </div> -->
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Commission invoiced</div>
                                </div>
                                <select class="form-control" id="property" name="commission_charging" required>
                                    <option selected value="to_break">To break</option>
                                    <!-- <option value="all_upfront">All up front</option> -->
                                    <option value="by_period">By period</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-0">

                        </div>
                    </div>

        <!--             <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="landlord_admin_fee" id="defaultCheck1" name="landlord_admin_fee" checked>
                      <label class="form-check-label" for="defaultCheck1">
                        Landlord admin fee
                    </label>
                </div>

                <div class="form-check mt-1">
                  <input class="form-check-input" type="checkbox" value="tenant_admin_fee" id="defaultCheck1" name ="tenant_admin_fee" checked>
                  <label class="form-check-label" for="defaultCheck1">
                    Tenant admin fee
                </label>
            </div> -->

            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Security deposit £</div>
                        </div>
                        <input id="name" type="text" class="form-control" name="security_deposit" value="{{ old('security_deposit') }}" autocomplete="off">
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12 offset-md-0">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Holding deposit £</div>
                        </div>
                        <input id="name" type="text" class="form-control" name="holding_deposit" value="{{ old('holding_deposit') }}" autocomplete="off">
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Minimum notice period</div>
                        </div>
                        <input id="name" type="text" class="form-control" name="notice_period" value="{{ old('notice_period') }}" placeholder="Days" autocomplete="off" required>
                    </div>
                </div>

            <!--     <div class="form-group col-md-5 offset-md-1">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Break clause period</div>
                        </div>
                        <input id="name" type="text" class="form-control" name="break_clause" value="{{ old('break_clause') }}" placeholder="Months" autocomplete="off" required autofocus>
                    </div>
                </div> -->
            </div>

            <!-- <div class="form-row mt-3"> -->
              <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="landlord_admin_fee" id="defaultCheck1" name="landlord_admin_fee" checked>
                  <label class="form-check-label" for="defaultCheck1">
                    Landlord admin fee
                </label>
            </div>

            <div class="form-check mt-1">
              <input class="form-check-input" type="checkbox" value="tenant_admin_fee" id="defaultCheck1" name ="tenant_admin_fee" checked>
              <label class="form-check-label" for="defaultCheck1">
                Tenant admin fee
            </label>
            <!-- </div> -->
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Save</button>
</div>
</form>
</div>
</div>
</div>