<!-- Modal -->
<div class="modal fade" id="enter-production-company" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Production company</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/setProductionCompanyOnSearch">
                <input type="hidden" name="searchId" value="{{ $search->id }}">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row"> 
                            <div class="col-sm-12">
                                <div class="form-group col-md-10">
                                    <select class="form-control" id="production_company_id" name="production_company_id" required>
                                        <option disabled selected>Select company</option>
                                        <option value="none">None</option>
                                        @foreach(\App\ProductionCompany::all()->sortBy('name') as $company)
                                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>