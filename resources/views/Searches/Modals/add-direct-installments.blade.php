<!-- Modal -->
<add-tenancy-form inline-template>
    <div class="modal fade" id="add-dir-install-tenancy" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Short let tenancy (installments)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal" method="POST" action="/tenancies">
                    <input type="hidden" name="tenancy_type" value="direct">
                    <input type="hidden" name="let_type" value="installments">
                    <input type="hidden" name="searchId" value="{{ $search->id }}">

                    {{ csrf_field() }}

                    <div class="modal-body">
                        <div class="container-fluid">

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Total rent £</div>
                                        </div>
                                        <input id="name" type="text" class="form-control" name="total_rent" value="{{ old('total_rent') }}"autocomplete="off" required>
                                    </div>
                                </div>
                            </div>

                    <!--         <div class="form-row">
                                <div class="form-group col-md-10">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Commission rate %</div>
                                        </div>
                                        <input id="name" type="text" class="form-control" name="comm_rate" value="{{ old('comm_rate') }}" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                        -->
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Number of installments</div>
                                    </div>
                                    <select v-model="numberOfInstallments" class="form-control" id="exampleFormControlSelect1">
                                      <option :value=1>1</option>
                                      <option :value=2>2</option>
                                      <option :value=3>3</option>
                                      <option :value=4>4</option>
                                      <option :value=5>5</option>
                                      <option :value=6>6</option>
                                      <option :value=7>7</option>
                                      <option :value=8>8</option>
                                      <option :value=9>9</option>
                                      <option :value=10>10</option>
                                  </select>
                              </div>
                          </div>
                      </div>

                      <template v-for="no in numberOfInstallments">
                       <div class="form-row">
                        <div class="form-group col-md-7">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Installment date @{{ no }}</div>
                                </div>
                                <input id="name" type="DATE" class="form-control" :name="installmentDate(no)">
                            </div>
                        </div>

                        <div class="form-group col-md-5 offset-md-0">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Payment £</div>
                                </div>
                                <input id="name" type="text" class="form-control" :name="installmentPayment(no)" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </template>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Commission invoiced</div>
                            </div>
                            <select class="form-control" id="property" name="commission_charging" required>
                                <option disabled selected>Select</option>
                                <option value="all_upfront">All up front</option>
                                <!-- <option value="to_break">To the break</option> -->
                                <option value="by_period">By period</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-md-0">

                    </div>
                </div>


                <div class="form-row">
                    <div class="form-group col-md-12">
                       <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Security deposit £</div>
                        </div>
                        <input id="name" type="text" class="form-control" name="security_deposit" value="{{ old('security_deposit') }}" autocomplete="off">
                    </div>
                </div>
            </div>

             <!--    <div class="form-row">
                <div class="form-group col-md-12 offset-md-0">
                 <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Holding deposit £</div>
                    </div>
                        <input id="name" type="text" class="form-control" name="holding_deposit" value="{{ old('holding_deposit') }}" autocomplete="off">
                    </div>
                </div>
            </div> -->

            <div class="form-row">
                <div class="form-group col-md-12">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Minimum notice period</div>
                    </div>
                    <input id="name" type="text" class="form-control" name="notice_period" value="{{ old('notice_period') }}">
                </div>
            </div>
        </div>
<!-- 
        <div class="form-row">
            <div class="form-group col-md-12 offset-md-0">
             <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Break clause period</div>
                </div>
                <input id="name" type="text" class="form-control" name="break_clause" value="{{ old('break_clause') }}">
            </div>
        </div>
    </div> -->
</div>

<div class="form-check">
  <input class="form-check-input" type="checkbox" value="landlord_admin_fee" id="defaultCheck1" name="landlord_admin_fee" checked>
  <label class="form-check-label" for="defaultCheck1">
    Landlord admin fee
</label>
</div>

<div class="form-check mt-1">
  <input class="form-check-input" type="checkbox" value="tenant_admin_fee" id="defaultCheck1" name ="tenant_admin_fee" checked>
  <label class="form-check-label" for="defaultCheck1">
    Tenant admin fee
</label>
</div>

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Save</button>
</div>
</form>
</div>
</div>
</div>
</add-tenancy-form>