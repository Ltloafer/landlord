  <span class="float-right">
Landlord to pay for a professional deep clean to the property prior to the tenant moving in<br>
Landlord to pay for ALL utility bills (including wifi, council tax, gas, electric, TV service,  wifi, water and any ground maintenance fees etc. in all the bills)<br>
Landlord to provide 3 sets of keys to the property<br>
Property to come fully furnished as seen to include the following in the kitchen:<br>
6 x tumblers/mugs/wine glasses each<br>
full set of metal cooking utensils<br>
full set of baking trays<br>
6 x saucepans inc. frying pan/wok<br>
toaster/kettle/french press<br>
coffee machine<br>
ironing board/hoover/iron<br>
clothing drying rack<br>
trash cans in each bath, bedroom + kitchen<br>
4 x tea towels<br>
can/bottle opener, wine opener<br>
4 x chopping boards<br>
8 x knives/forks/spoons/desert spoons/serving spoons/kitchen knives<br>
8 x bowls/plates/side plates<br>
2 x serving bowls<br>
2 x serving platters<br>
broom/ mop & bucket & toilet brush<br>
2 sets of linen per bed (minimum 300 thread count)<br>
1 set of linen to be laundered and put on each bed<br>
2 sets of towels per bed<br>
</span>