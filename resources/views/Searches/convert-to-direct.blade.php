@extends('layouts.layout')

@section('content')

<searches inline-template>
    <div>

        <div class="container-fluid">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        <strong>
                            {{ $search->name }}
                            <!-- {{ $search->address }}, -->
                            <!-- {{ $search->phone }} -->
                        </strong>
                        <div class="dropdown float-right display-inline">
                            <button class="btn btn-sm btn-outline-secondary  float-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Convert to tenancy
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-direct-tenancy">Direct let (AST)</a>
                                <a class="dropdown-item" href="/search-to-direct-let/{{ $search->id}}">Direct let (AST)</a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-dir-install-tenancy">Short let (installments)</a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-splitComm-tenancy">Split comm </a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-splitComm-installments">Split comm (installments)</a>
                                <a class="dropdown-item" data-toggle="modal" data-target="#add-hotel-tenancy">Hotel booking</a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body" style="padding: 0px">
                       <div class="inset-items">
                        <!-- <p><strong>Tenant</strong></p> -->
                        

                    </div>

                    <form class="form-horizontal" method="POST" action="/tenancies">
                        <input type="hidden" name="tenancy_type" value="direct">
                        <input type="hidden" name="searchId" value="{{ $search->id }}">

                        {{ csrf_field() }}

                        <div class="modal-body">
                            <div class="container-fluid">

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-row" v-show="enterPropertyForm">
                                            <div class="form-group col-md-12">
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">Property</div>
                                                    </div>
                                                    <select class="form-control" id="property" name="property_id" required>
                                                        <option disabled selected>Select</option>
                                                        @foreach($directProperties as $property)
                                                        <option value="{{$property->id}}">{{$property->address}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div> 
                                        </div>

                                        <address>
                                            <strong>Property</strong><span style="float:right">{{ $search->property->address }}</span> <br>
                                            <hr>

                                            Postcode <span style="float:right">{{ $search->property->postcode }} </span><br>
                                            Type <span style="float:right">Direct</span><br><br>

                                            @if ($search->property->landlord)
                                            Landlord <span style="float:right">{{ $search->property->landlord->name }} </span><br> 
                                            @endif

                                            <!-- Phone <span style="float:right">{{ $search->property->phone ?? $search->phone }} </span><br> -->
                                            <!-- Email <span style="float:right">{{ $search->tenant->email ?? $search->email}} </span><br><br> -->
                                            <!-- Agent<br> -->
                                            <br>

                                            Invoice admin fee <span style="float: right">Yes</span><br>

                                        </address>
                                        
                                        <!-- <a href="/" data-toggle="modal" data-target="#enter-property">Select property</a> -->
                                        <!-- <hr> -->
                                    </div>

                                    <div class="col-sm-4">
                                        <address>
                                            <strong>Tenant</strong> <span style="float:right">{{ $search->tenant->name ?? '' }}</span>
                                            <hr>
                                            Address <span style="float:right">{{ $search->tenant->address ?? '' }}</span> <br>
                                            Postcode <span style="float:right">{{ $search->tenant->postcode ?? '' }} </span><br>
                                            Phone <span style="float:right">{{ $search->tenant->phone ?? $search->phone }} </span><br>
                                            Email <span style="float:right">{{ $search->tenant->email ?? $search->email}} </span><br><br>
                                            Invoice admin fee <span style="float: right">Yes</span><br>

                                        </address>

                                        <!-- <a href="/" data-toggle="modal" data-target="#enter-tenant">Select tenant</a> -->
                                    </div>


                                    <div class="col-sm-4">
                                        <address>
                                            <strong>Production company</strong><span style="float:right">{{ $search->productionCompany->name ?? 'n/a' }}</span>
                                            <hr>

                                            Address <span style="float:right">{{ $search->productionCompany->address ?? '' }}</span> <br>
                                            Postcode <span style="float:right">{{ $search->productionCompany->postcode ?? '' }} </span><br>
                                            Phone <span style="float:right">{{ $search->productionCompany->phone ?? '' }} </span><br>
                                            Email <span style="float:right">{{ $search->productionCompany->email ?? '' }} </span><br><br>
                                            Invoice production company <span style="float: right">Yes</span><br>

                                        </address>
                                        
                                        <!-- <a href="/" data-toggle="modal" data-target="#enter-production-company">Enter Production company</a> -->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <a href="/" data-toggle="modal" data-target="#enter-property">Select property</a>
                                        <hr>
                                    </div>
                                    <div class="col-sm-4">
                                        <a href="/" data-toggle="modal" data-target="#enter-tenant">Select tenant</a>
                                    </div>
                                    <div class="col-sm-4">
                                        <a href="/" data-toggle="modal" data-target="#enter-production-company">Enter Production company</a>
                                    </div>
                                </div>






                    <strong>Search areas</strong>
                    <span class="right-button">
                        <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#add-agent-search-area">Add / remove  areas</button>
                    </span>

                    <hr>

                    <view-notes model="search" :modelid="{{ $search->id }}" :notes="{{ $search->notes }}"></view-notes>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</searches>

@include('Modals.add-tenant')
@include('Searches.Modals.enter-tenant')
@include('Searches.Modals.enter-property')
@include('Searches.Modals.enter-production_company')

@include('Searches.Modals.add-direct-installments')
@include('Searches.Modals.add-direct-tenancy')


@endsection
