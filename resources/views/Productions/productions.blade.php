@extends('layouts.layout')

@section('content')

<contacts :contacts="{{ $productions }}" inline-template v-cloak>
    <div>

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Productions
                           <button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#add-production"> Add +</button>
                           <input class="search float-right mr-4" type="text" v-model="searchQuery" placeholder="Filter....">
                           
                       </div>

                       <div class="card-body">
                        
                        <display-contacts :data="{{ $productions }}" :columns="['name', 'phone', 'email']" :filter-key="searchQuery" inline-template v-cloak>
                            <div>

                                <table class="table table-hover m-b-none customer-sales" style="min-width:100%; width:auto">
                                    <thead>
                                        <tr>
                                            <th v-for="key in columns"
                                            @click="sortBy(key)"
                                            :class="{ active: sortKey == key }">
                                            @{{ key | capitalize }}
                                            <span class="arrow" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'">
                                            </span>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="contact in filteredData" class='clickable-row' v-on:click="viewProduction(contact)">
                                        <td>@{{ contact.fullName }}</td>
                                        <td>@{{ contact.phone }}</td>
                                        <td>@{{ contact.email }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    </display-contacts>
                    
                    @include('Productions.add-production-modal')

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
