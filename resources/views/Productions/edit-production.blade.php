@extends('layouts.layout')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit production
                </div>

                <div class="card-body">

                    <form class="form-horizontal" method="POST" action="/productions/{{ $production->id }}">

                        {{ method_field('PUT') }}
                        {{ csrf_field() }}

                        <div class="modal-body">
                            <div class="container-fluid">
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $production->first_name }}" placeholder="First name" required autofocus>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $production->last_name }}" placeholder="Last name" required>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <input id="phone" type="text" class="form-control" name="phone" value="{{ $production->phone }}" placeholder="Phone">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ $production->email }}" placeholder="Email" required>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <input id="line_1" type="text" class="form-control" name="line1" value="{{ $production->line_1 }}" placeholder="Address line 1">
                                            </div>
                                        </div>
                                        
                                         <div class="form-group">
                                            <div class="col-md-10">
                                                <input id="line_2" type="text" class="form-control" name="line2" value="{{ $production->line_2 }}" placeholder="Address line 2">
                                            </div>
                                        </div>
                                        
                                         <div class="form-group">
                                            <div class="col-md-10">
                                                <input id="line_3" type="text" class="form-control" name="line3" value="{{ $production->line_3 }}" placeholder="Address line 3">
                                            </div>
                                        </div>
                                        
                                         <div class="form-group">
                                            <div class="col-md-10">
                                                <input id="line_4" type="text" class="form-control" name="line4" value="{{ $production->line_4 }}" placeholder="Address line 4">
                                            </div>
                                        </div>
                                        
                                         <div class="form-group">
                                            <div class="col-md-10">
                                                <input id="postcode" type="text" class="form-control" name="postcode" value="{{ $production->postcode }}" placeholder="Postcode">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-11">
                                    <div class="modal-footer">
                                        <a href="/productions" class="btn btn-secondary">Close</a>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row">
                            <div class="col-md-8">
                                <form class="form-horizontal" method="POST" action="/productions/{{ $production->id }}">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger ml-4">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
