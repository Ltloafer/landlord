<!-- Modal -->
<div class="modal fade" id="add-production" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">

        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Add production</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <form class="form-horizontal" method="POST" action="/productions">

            {{ csrf_field() }}

            <div class="modal-body">
                <div class="container-fluid">

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="First name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last name" required>
                        </div>
                    </div>

                 <!--    <div class="form-group">
                        <div class="col-md-8">
                            <input id="phone" type="text" class="form-control" name="contact" value="{{ old('contact') }}" placeholder="Contact">
                        </div>
                    </div> -->


                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                        </div>
                    </div>
                    
                    <hr>
                    
                      <div class="form-group">
                        <div class="col-md-8">
                            <input id="line1" type="text" class="form-control" name="line1" value="{{ old('line1') }}" placeholder="Address line 1">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="line2" type="text" class="form-control" name="line2" value="{{ old('line2') }}" placeholder="Address line 2">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="line3" type="text" class="form-control" name="line3" value="{{ old('line3') }}" placeholder="Address line 3">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="line4" type="text" class="form-control" name="line4" value="{{ old('line4') }}" placeholder="Address line 4">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="postcode" type="text" class="form-control" name="postcode" value="{{ old('postcode') }}" placeholder="Postcode">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

        </form>
    </div>
</div>
</div>