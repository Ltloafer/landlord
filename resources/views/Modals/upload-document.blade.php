<!-- Modal -->
<div class="modal fade" id="uploadDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modla-lg modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Upload document</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="POST" action="/{{ $modelType }}/{{ $model->id }}/documents" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="modal-body">
         <div class="container-fluid">

          <div class="form-group">
            <label for="landlord" class="col-md-8 control-label">Select document type</label>
            <div class="col-md-10">
              <select multiple class="form-control" id="tenant" name="documentTypeId">
                @foreach($requiredDocuments as $document)
                <option value="{{ $document->id }}">{{$document->name}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group mt-4">
            <!-- <div class="custom-file"> -->
              <!-- <input type="file" class="custom-file-input" id="customFile" name="uploadedDocument"> -->
              <input type="file" class="form-control-file" name="uploadedDocument" >
              <!-- </div> -->
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>