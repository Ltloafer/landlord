
<div class="modal" id="edit-scheduled-invoice" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Edit scheduled invoice</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <form class="form-horizontal" method="POST" :action=" '/scheduledInvoices/' + scheduledinvoice.id ">
            <input type="hidden" name="_method" value="PUT">

            {{ csrf_field() }}


            <div class="modal-body">
              <div class="container-fluid">
                <div class="form-group">
                  <label for="type" class="col-md-6 control-label">Scheduled invoice date</label>
                  <div class="col-md-8">
                    <input id="date" type="text" class="form-control" name="date" :value="scheduledDate" :placeholder="scheduledDate" required autofocus>
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="startdate" class="col-md-8 control-label">Month starting</label>
                    <div class="col-md-10">
                      <input id="startdate" type="text" class="form-control" name="startdate" :value="startDate" :placeholder="endDate" required>
                    </div>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="enddate" class="col-md-8 control-label">to</label>
                    <div class="col-md-10">
                      <input id="enddate" type="text" class="form-control" name="enddate" :value="endDate" :placeholder="endDate" required>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                    <label for="enddate" class="col-md-8 control-label">Amount £</label>
                  <div class="col-md-8">
                    <input id="amount" type="text" class="form-control" name="amount" :value="amount" :placeholder="amount" required autofocus>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Save</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>