<!-- Modal -->
<div class="modal fade" id="addLandlordProperty" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">


      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Register a property</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="POST" action="/properties">

        {{ csrf_field() }}

        <div class="modal-body">
         <div class="container-fluid">


          <div class="row">
            <div class="form-group col-md-12">
              <label for="landlord" class="col-md-8 control-label">Select landlord</label>
              <div class="col-md-12">
                <select multiple class="form-control" id="landlordId" name="landlordId">
                  @foreach($landlords as $landlord)
                  <option value="{{$landlord->id}}">{{$landlord->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="row">
           <div class="form-group col-md-12">
            <label for="landlord" class="col-md-8 control-label">Select agent</label>
            <div class="col-md-12">
              <select multiple class="form-control" id="agentId" name="agentId">
                @foreach($agents as $agent)
                <option value="{{$agent->id}}">{{$agent->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="phone" class="col-md-6 control-label">Address</label>

              <div class="col-md-12">
                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus>
              </div>
            </div>

            <div class="form-group">
              <label for="postcode" class="col-md-4 control-label">Postcode</label>

              <div class="col-md-8">
                <input id="postcode" type="text" class="form-control" name="postcode" value="{{ old('postcode') }}" required autofocus>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary">Save</button>
    </div>

  </form>

</div>
</div>
</div>
<!-- Modal End -->