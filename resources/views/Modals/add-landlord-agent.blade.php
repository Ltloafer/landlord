<!-- Modal -->
<div class="modal fade" id="addLandlordAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add agent</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/landlords/{{ $landlord->id }}">

                {{ csrf_field() }}

                {{ method_field('PUT') }}

                <div class="modal-body">
                   <div class="container-fluid">

                     <div class="form-group">
                        <label for="landlord" class="col-md-4 control-label"></label>
                        <div class="col-md-6">
                            <select multiple class="form-control" id="agentId" name="agentId" required>
                                @foreach($agents as $agent)
                                <option value="{{$agent->id}}">{{$agent->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
</div>