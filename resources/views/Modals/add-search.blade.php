<!-- Modal -->
<div class="modal fade" id="addSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add new search</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>

          <form class="form-horizontal" method="POST" action="/searches">

            {{ csrf_field() }}

            <div class="modal-body">
                <div class="container-fluid">

               <!--      <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group col-md-10">
                                <select class="form-control" id="tenant" name="tenant_id" required>
                                    <option disabled selected>Previous tenant</option>
                                    @foreach(\App\Tenant::all() as $tenant)
                                    <option value="{{ $tenant->id }}">{{ $tenant->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <hr>
                -->
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="col-md-10">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Guest name" autofocus>
                            </div>
                        </div>
                    </div>
                </div>

                      <!--       <div class="form-group">
                                <div class="col-md-10">
                                    <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}"  placeholder="Address">
                                </div>
                            </div> -->

                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"  placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}"  placeholder="Phone">
                                        </div>
                                    </div>

                       <!--      <div class="form-group">
                                <div class="col-md-8">
                                    <input id="postcode" type="text" class="form-control" name="postcode" value="{{ old('postcode') }}"  placeholder="Postcode">
                                </div>
                            </div> -->
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <div class="col-md-10">
                                    <input id="name" type="text" class="form-control" name="production" value="{{ old('production') }}" placeholder="Working title / Production">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group col-md-10">
                                <select class="form-control" id="production-company" name="production_company_id">
                                    <option disabled selected>Production company</option>
                                    @foreach(\App\ProductionCompany::all() as $company)
                                    <option value="{{ $company->id }}">{{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="col-md-10">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Start date</div>
                                        </div>
                                        <input id="startdate" type="DATE" class="form-control" name="startdate" value="{{ old('startdate') }}" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="col-md-10">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">End date</div>
                                        </div>
                                        <input id="startdate" type="DATE" class="form-control" name="enddate" value="{{ old('enddate') }}" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-3">
                        <div class="col-md-12">
                            <input id="note" type="text" class="form-control" name="note" value="{{ old('note') }}" placeholder="Notes" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
</div>