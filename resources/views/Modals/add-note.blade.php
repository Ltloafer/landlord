<!-- Modal -->
<div class="modal fade" id="addNote" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add a note</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/notes">
                <input type="hidden" name="modelId" value="{{ $model->id }}">
                <input type="hidden" name="model-type" value ="{{ $modelType }}">

                {{ csrf_field() }}

                <div class="modal-body">
                   <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <input id="note" type="text" class="form-control" name="note" value="{{ old('note') }}" autocomplete="off" required autofocus>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
</div>