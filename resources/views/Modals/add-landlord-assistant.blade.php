  <!-- Modal -->
  <div class="modal fade" id="addLandlordAssistant" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add an assistant</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/landlord/{{ $landlord->id }}/assistants">
                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control" name="assistant_name" value="{{ old('assistant_name') }}" placeholder="Name" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8">
                                        <input id="phone" type="text" class="form-control" name="assistant_phone" value="{{ old('assistant_phone') }}" placeholder="Phone" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-9">
                                        <input id="email" type="email" class="form-control" name="assistant_email" value="{{ old('assistant_email') }}" placeholder="Email"  required autofocus>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>

        </div>
    </div>
</div>



<!-- Modal End -->