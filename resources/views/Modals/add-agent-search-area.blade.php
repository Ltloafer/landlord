<!-- Modal -->
<div class="modal fade" id="add-agent-search-area" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add search area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/agent/{{ $agent->id }}/search-areas">

                {{ csrf_field() }}

                <div class="modal-body">
                 <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <div class="col-md-12">
                                    @foreach($searchAreas as $area)
                                        <div class="form-check mt20">
                                            @if ($agent->isRegisteredToArea($area))
                                                <input class="form-check-input" type="checkbox" name="{{ $area->id}}" checked="checked" value="_" id="defaultCheck1">
                                            @else
                                                <input class="form-check-input" type="checkbox" name="{{ $area->id }}" value="_" id="defaultCheck1">
                                            @endif
                                          <label class="form-check-label" for="defaultCheck1">
                                            {{ $area->area }}
                                        </label>
                                    </div>
                                @endforeach
                                <!-- <input id="note" type="text" class="form-control" name="area" value="{{ old('area') }}" required autofocus> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
</div>
</div>
</div>