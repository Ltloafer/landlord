<!-- Modal -->

<add-tenancy inline-template>

    <div class="modal fade" id="addNewTenancy" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add new tenancy</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal" method="POST" action="/tenancies">
                    <input type="hidden" name="property_id" value="{{$property->id}}">

                    {{ csrf_field() }}

                    <div class="modal-body">
                     <div class="container-fluid">

                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label for="landlord" class="col-md-8 control-label">Select tenant</label>
                                <div class="col-md-12">
                                    <select multiple class="form-control" id="tenant" name="tenant" required>
                                        @foreach($tenants as $tenant)
                                        <option value="{{$tenant->id}}">{{$tenant->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-4 col-md-offset-1 pull-right">
                                <label for="landlord" class="col-md-8 control-label">Select tenancy type</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="standardLongLet" id="exampleRadios1" v-model="picked" value="1" checked>
                                    <label class="form-check-label" for="exampleRadios1">
                                        Standard long let
                                    </label>
                                </div>
                                <div class="form-check mt10">
                                    <input class="form-check-input" type="radio" name="shortTermLet" id="exampleRadios2" v-model="picked" value="2">
                                    <label class="form-check-label" for="exampleRadios2">
                                        Short term let
                                    </label>
                                </div>
                                <div class="form-check mt10">
                                    <input class="form-check-input" type="radio" name="scottySpecial" id="exampleRadios3" v-model="picked" value="3">
                                    <label class="form-check-label" for="exampleRadios3">
                                        Scotty special
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-4">
                            <div class="form-group col-md-6">
                                <label for="startdate" class="col-md-4 control-label">Start date </label>
                                <div class="col-md-10">
                                    <input id="startdate" type="DATE" class="form-control" name="startdate" value="{{ old('startdate') }}" placeholder="yyyy-mm-dd" required>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="enddate" class="col-md-4 control-label">End date </label>
                                <div class="col-md-10">
                                    <input id="enddate" type="DATE" class="form-control" name="enddate" value="{{ old('enddate') }}" placeholder="yyyy-mm-dd" required>
                                </div>
                            </div>
                        </div>

                        <div v-if="picked == 1">
                            <div class="form-group-row mt-3">
                                <label for="enddate" class="col-md-4 col-form-label">Commission rate</label>
                                <div class="col-md-4">
                                    <input id="name" type="text" class="form-control" name="comm_rate" value="{{ old('comm_rate') }}" placeholder="%" autofocus>
                                </div>
                            </div>

                            <div class="form-row mt-4">
                                <div class="form-group col-md-6">
                                    <label for="enddate" class="col-md-4 control-label">Monthly rate</label>
                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control" name="monthly_rate" value="{{ old('monthly_rate') }}" placeholder="£" autofocus>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="enddate" class="col-md-8 control-label">Holding deposit</label>
                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control" name="holding_deposit" value="{{ old('holding_deposit') }}" placeholder="£" autofocus>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row mt-3">
                                <div class="form-group col-md-6">
                                    <label for="notice_period" class="col-md-8 control-label">Minimum notice period</label>
                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control" name="notice_period" value="{{ old('notice_period') }}" placeholder="Days" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="break_clause" class="col-md-8 control-label">Break clause period</label>
                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control" name="break_clause" value="{{ old('break_clause') }}" placeholder="Months" required autofocus>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div v-if="picked != 1">
                            <div class="form-row mt-2">
                                <div class="form-group col-md-4">
                                    <label for="enddate" class="col-md-8 col-form-label">Commission rate</label>
                                    <div class="col-md-10">
                                        <input id="name" type="text" class="form-control" name="comm_rate" value="{{ old('comm_rate') }}" placeholder="%" autofocus>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                 <label for="enddate" class="col-md-8 control-label">Holding deposit</label>
                                 <div class="col-md-8">
                                    <input id="name" type="text" class="form-control" name="holding_deposit" value="{{ old('holding_deposit') }}" placeholder="£" autofocus>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="enddate" class="col-md-8 control-label">Total rent</label>
                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control" name="total_rent" value="{{ old('total_rent') }}" placeholder="£" autofocus>
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-4">
                            <div class="form-group col-md-6">
                                <label for="enddate" class="col-md-4 control-label">Installments</label>
                                <div class="col-md-8">
                                    <input id="name" type="DATE" class="form-control" name="installment_1_date" value="{{ old('installment_1_date') }}" placeholder="£" autofocus>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="enddate" class="col-md-8 control-label">Payment</label>
                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control" name="installment_1_payment" value="{{ old('installment_2_payment') }}" placeholder="£ payment" autofocus>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-if="picked == 3">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div class="col-md-8">
                                    <input id="name" type="DATE" class="form-control" name="installment_2_date" value="{{ old('installment_2_date') }}" placeholder="£" autofocus>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control" name="installment_2_payment" value="{{ old('installment_2_payment') }}" placeholder="£ payment" autofocus>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div class="col-md-8">
                                    <input id="name" type="DATE" class="form-control" name="installment_3_date" value="{{ old('installment_3_date') }}" placeholder="£" autofocus>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control" name="installment_3_payment" value="{{ old('installment_3_payment') }}" placeholder="£ payment" autofocus>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
</div>

</add-tenancy>