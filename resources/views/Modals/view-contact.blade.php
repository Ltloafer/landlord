<!-- Modal -->
<div class="modal fade" id="viewContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">@{{ selectedContact.name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>

  <form class="form-horizontal" method="POST" action="/contacts">

    {{ csrf_field() }}

    <div class="modal-body">
       <div class="container-fluid">
            <div class="col-md-8">
                <!-- <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone" required autofocus> -->
                <!-- <p><strong>@{{ selectedContact.name }}</strong></p> -->
                <p>@{{ selectedContact.address }}</p>
                <p>@{{ selectedContact.postcode }}</p>
                <p>@{{ selectedContact.phone }}</p>
                <p>Email: @{{ selectedContact.email }}</p>
            </div>
            
        <hr>

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Notes</label>
            <div class="col-md-12">
                <input id="note" type="text" class="form-control" name="note" value="{{ old('note') }}" required autofocus>
            </div>
        </div>

    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <!-- <button type="submit" class="btn btn-primary">Save</button> -->
</div>

</form>
</div>
</div>
</div>