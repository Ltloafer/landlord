<!-- Modal -->
<div class="modal fade" id="viewTenant" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@{{ selectedTenant.name }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/Tenants">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-md-8">
                            <p>@{{ selectedTenant.address }}</p>
                            <p>@{{ selectedTenant.phone }}</p>
                            <p>Email: @{{ selectedTenant.email }}</p>

                            <p v-if="selectedTenant.xero_contactID"> Connected to xero</p>
                            <p v-else><a href="#" v-on:click.prevent="linkTenantToXeroContact()">Link to xero contact</a></p>
                        </div>

                        <div v-if="listXeroContacts" class="col-md-12">
                            <template v-for="contact in xeroContacts">
                                <div class="row mt-1">
                                    <div class="col-md-4">
                                        @{{ contact.Name }}
                                    </div>
                                    <div class="col-md-4">
                                        @{{ contact.EmailAddress }}
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" v-on:click.prevent="closeModal()" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>