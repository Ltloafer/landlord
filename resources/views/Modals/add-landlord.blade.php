  <!-- Modal -->
  <add-landlord inline-template>
    <div>
      <div class="modal fade" id="add-landlord" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Register new landlord</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal" method="POST" action="/landlords">
                    {{ csrf_field() }}

                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <div class="col-md-8">
                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8">
                                            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                                        </div>
                                    </div>

                                    <hr>
                                    <p>Optional</p>

                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="Home address">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8">
                                            <input id="postcode" type="text" class="form-control" name="postcode" value="{{ old('postcode') }}" placeholder="Postcode">
                                        </div>
                                    </div>

                                    <button v-if="! addAssistantForm" type="button" class="btn btn-secondary btn-sm" v-on:click="addAssistant()">Add an assistant +</button>

                                    <button v-if="! addAgentForm" type="button" class="btn btn-secondary btn-sm" v-on:click="addAgent()">Add an agent +</button>

                                    <div v-if="addAgentForm">
                                        <hr>
                                        <h5 class="modal-title">Add an agent</h5> 

                                        <!-- <div v-if="! addNewAgentForm"> -->
                                            <div class="form-group">
                                                <label for="landlord" class="col-md-4 control-label"></label>
                                                <div class="col-md-6">
                                                    <select multiple class="form-control" id="agentId" name="agentId">
                                                        @foreach($agents as $agent)
                                                        <option value="{{$agent->id}}">{{$agent->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- </div> -->

                                            <div v-if="! addNewAgentForm">
                                                <p><a href="/" v-on:click.prevent="addNewAgent()">Not on this list? Add an agent</a></p>
                                            </div>
                                            <div v-else>
                                                <p>Add agents details below.</p>
                                            </div>

                                            <div v-if="addNewAgentForm">
                                                <p></p>
                                                <div class="form-group">
                                                    <div class="col-md-8">
                                                        <input id="name" type="text" class="form-control" name="agent_name" value="{{ old('agent_name') }}" placeholder="Name" required autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-8">
                                                        <input id="phone" type="text" class="form-control" name="agent_phone" value="{{ old('agent_phone') }}" placeholder="Phone" required autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-9">
                                                        <input id="email" type="email" class="form-control" name="agent_email" value="{{ old('agent_email') }}" placeholder="Email"  required autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <div v-if="addAssistantForm">
                                            <hr>
                                            <h5 class="modal-title">Add an assistant</h5> 
                                            <p></p>
                                            <div class="form-group">
                                                <div class="col-md-8">
                                                    <input id="name" type="text" class="form-control" name="assistant_name" value="{{ old('assistant_name') }}" placeholder="Name" required autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-8">
                                                    <input id="phone" type="text" class="form-control" name="assistant_phone" value="{{ old('assistant_phone') }}" placeholder="Phone" required autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <input id="email" type="email" class="form-control" name="assistant_email" value="{{ old('assistant_email') }}" placeholder="Email"  required autofocus>
                                                </div>
                                            </div>
                                        </div>

                                        <hr>

                                        <h5 class="modal-title">Register new rental property</h5> 
                                        <p></p>
                                        <hr>

                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <input id="address" type="text" class="form-control" name="property_address" value="{{ old('property_address') }}" placeholder="Address">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-8">
                                                <input id="postcode" type="text" class="form-control" name="property_postcode" value="{{ old('property_postcode') }}" placeholder="Postcode">
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" v-on:click.prevent="closeForm()">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</add-landlord>



<!-- Modal End -->