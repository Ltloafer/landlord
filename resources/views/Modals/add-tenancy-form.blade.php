<!-- Modal -->
<div class="modal fade" id="add-tenancy" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add new tenancy</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/tenancies">

                {{ csrf_field() }}

                <div class="modal-body">
                 <div class="container-fluid">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="landlord" class="col-md-8 control-label">Select property</label>
                            <select multiple class="form-control" id="property" name="property_id" required>
                                @foreach(\App\Property::all() as $property)
                                <option value="{{$property->id}}">{{$property->address}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-5">
                            <label for="landlord" class="col-md-8 control-label">Select tenant</label>
                            <select multiple class="form-control" id="tenant" name="tenant" required>
                                @foreach($availableTenants as $tenant)
                                <option value="{{$tenant->id}}">{{$tenant->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="startdate" class="col-md-4 control-label">Start date </label>
                          <div class="col-md-10">
                            <input id="startdate" type="DATE" class="form-control" name="startdate" value="{{ old('startdate') }}" placeholder="yyyy-mm-dd" required>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                     <label for="enddate" class="col-md-4 control-label">End date </label>
                     <div class="col-md-10">
                        <input id="enddate" type="DATE" class="form-control" name="enddate" value="{{ old('enddate') }}" placeholder="yyyy-mm-dd" required>
                    </div>
                </div>
            </div>

        <!-- <div class="form-group-row mt-3">
            <div class="input-group col-md-4 mt-3">
              <div class="input-group-prepend">
                <span class="input-group-text">%</span>
            </div>
                <input id="name" type="text" class="form-control" name="comm_rate" value="{{ old('comm_rate') }}" autofocus>
        </div>
    </div> -->


    <div class="form-group-row mt-3">
        <label for="enddate" class="col-md-4 col-form-label">Commission rate</label>
        <div class="col-md-4">
            <input id="name" type="text" class="form-control" name="comm_rate" value="{{ old('comm_rate') }}" placeholder="%" autofocus>
        </div>
    </div>

    <div class="form-row mt-4">
        <div class="form-group col-md-6">
            <label for="enddate" class="col-md-4 control-label">Monthly rate</label>
            <div class="col-md-8">
                <input id="name" type="text" class="form-control" name="monthly_rate" value="{{ old('monthly_rate') }}" placeholder="£" autofocus>
            </div>
        </div>

        <div class="form-group col-md-6">
            <label for="enddate" class="col-md-8 control-label">Holding deposit</label>
            <div class="col-md-8">
                <input id="name" type="text" class="form-control" name="holding_deposit" value="{{ old('holding_deposit') }}" placeholder="£" autofocus>
            </div>
        </div>
    </div>

    <div class="form-row mt-3">
        <div class="form-group col-md-6">
         <label for="notice_period" class="col-md-8 control-label">Minimum notice period</label>
         <div class="col-md-8">
            <input id="name" type="text" class="form-control" name="notice_period" value="{{ old('notice_period') }}" placeholder="Days" required autofocus>
        </div>
    </div>

    <div class="form-group col-md-6">
        <label for="break_clause" class="col-md-8 control-label">Break clause period</label>
        <div class="col-md-8">
            <input id="name" type="text" class="form-control" name="break_clause" value="{{ old('break_clause') }}" placeholder="Months" required autofocus>
        </div>
    </div>
</div>

</div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Save</button>
</div>
</form>
</div>
</div>
</div>