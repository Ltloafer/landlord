<!-- Modal -->
<div class="modal fade" id="add-hotel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add hotel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>

          <form class="form-horizontal" method="POST" action="/hotels">

            {{ csrf_field() }}

            <div class="modal-body">
                <div class="container-fluid">

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="phone" type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="Address" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="postcode" type="text" class="form-control" name="postcode" value="{{ old('postcode') }}" placeholder="Postcode">
                        </div>
                    </div>

                      <div class="form-group">
                        <div class="col-md-8">
                            <input id="contact" type="text" class="form-control" name="contact_name" value="{{ old('contact_name') }}" placeholder="Contact name">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email"  required>
                        </div>
                    </div>

                    <!-- <hr> -->

                <!--     <div class="form-group">
                        <label for="name" class="col-md-2 control-label">Notes</label>
                        <div class="col-md-10">
                            <input id="note" type="text" class="form-control" name="note" value="{{ old('note') }}">
                        </div>
                    </div> -->
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

        </form>
    </div>
</div>
</div>