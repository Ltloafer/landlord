@extends('layouts.layout')

@section('content')

<!-- <poo-sticks></poo-sticks> -->

<agent inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        <strong>
                            {{ $agent->name }}, 
                            {{ $agent->address }},
                            {{ $agent->phone }}
                        </strong>
                    </div>

                    <div class="card-body" style="padding: 25px">

                   <!--      <strong>Assistants</strong>

                        <span class="right-button">
                            <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#addLandlordAssistant">Add an assistant +</button>
                        </span>

                        <hr> -->

                        <strong>Search areas</strong>
                        <span class="right-button">
                            <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#add-agent-search-area">Add / remove  areas</button>
                        </span>

                        <div class="inset-items">
                            <div class="row">
                                <!-- <template v-for="area in searchAreas">  -->
                                    @foreach ($searchAreas as $area)
                                    <div class="col-md-4">
                                        <p>{{ $area->area }}
                                            @if ($agent->isRegisteredToArea($area))
                                            <span style="margin-left: 20px"><strong><i class="fas fa-check"></i></strong></span>
                                            @endif
                                        </p>
                                    </div>
                                    @endforeach
                            </div>
                        </div>

                        <hr>

                        <view-notes model="agent" :modelid="{{ $agent->id }}" :notes="{{ $notes }}"></view-notes>

                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('Modals.add-agent-search-area')

    @include('Modals.add-note', ['modelType' => 'agent', 'model' => $agent])

</div>
</landlord>


@endsection
