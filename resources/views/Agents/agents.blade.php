@extends('layouts.layout')
@section('content')

<agents :agents= "{{ $agents }}" inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">Agents
                            <button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#add-agent"> Add an agent +</button>
                        </div>

                        <div class="card-body">
                            <table class="table m-b-none">
                                <thead>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Postcode</th>
                                    <th>Email</th>
                                    <th>Xero Contact</th>
                                </thead>

                                <tbody>
                                    <tr v-for="agent in agents">
                                        <td width="15%">@{{ agent.name }}</td>
                                        <td width="15%">@{{ agent.phone }}</td>
                                        <td width="25%">@{{ agent.address }}</td>
                                        <td width="25%">@{{ agent.postcode }}</td>
                                        <td width="25%">@{{ agent.email }}</td>
                                        <!-- <td width="25%">@{{ agent.xero_contactID }}</td> -->
                                        <td v-if="agent.xero_contactID">
                                            <i class="fas fa-user fa-2x" style="color:green"></i>
                                        </td>

                                        <td v-else>
                                            <a href="#" v-on:click="linkAgentToXeroContact(agent)"><i class="fas fa-user-edit fa-2x" style="color:red"></i></a>
                                        </td>

                                        <td></td>

                                        <!-- <td><a v-bind:href=" ' /agents/' +  agent.id" class="btn btn-alt btn-outline-primary btn-sm">View</a></td> -->
                                    </tr>
                                </tbody>
                            </table>

                            @include('Agents.link-to-xero')

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('Modals.add-agent')

    </div>
</agents>


@endsection
