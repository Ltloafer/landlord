@extends('layouts.layout')
@section('content')

<suppliers :suppliers= "{{ $suppliers }}" inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">suppliers
                            <button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#addNewSupplier"> Add an supplier +</button>
                            <!-- <button class="btn btn-sm btn-default add-btn float-right" data-toggle="modal" data-target="#addNewSupplier">Add new supplier + </button> -->

                        </div>

                        <div class="card-body">
                            <table class="table m-b-none">
                                <thead>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Postcode</th>
                                    <th>Email</th>
                                    <th></th>
                                </thead>

                                <tbody>
                                    <tr v-for="supplier in suppliers">
                                        <td width="15%">@{{ supplier.name }}</td>
                                        <td width="15%">@{{ supplier.phone }}</td>
                                        <td width="25%">@{{ supplier.address }}</td>
                                        <td width="25%">@{{ supplier.postcode }}</td>
                                        <td width="25%">@{{ supplier.email }}</td>
                                        <!-- <td width="25%">@{{ supplier.xero_contactID }}</td> -->
                                        <td v-if="supplier.xero_contactID">
                                            <i class="fas fa-user fa-2x" style="color:green"></i>
                                        </td>

                                        <td v-else>
                                            <a href="#" v-on:click="linksupplierToXeroContact(supplier)"><i class="fas fa-user-edit fa-2x" style="color:red"></i></a>
                                        </td>

                                        <td></td>

                                        <!-- <td><a v-bind:href=" ' /suppliers/' +  supplier.id" class="btn btn-alt btn-outline-primary btn-sm">View</a></td> -->
                                    </tr>
                                </tbody>
                            </table>

                            @include('suppliers.link-to-xero')

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('Modals.add-supplier')

    </div>
</suppliers>


@endsection
