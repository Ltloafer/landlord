@extends('layouts.layout')
@section('content')

<dashboard inline-template>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mt-5">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif

                        You are logged in!
                    </div>

                </div>
            </div>
        </div>
<!-- 
        <tenancy inline-template>
            <div>
                <div class="row justify-content-center mt-5">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Required actions</div>

                            <div class="card-body">
                                @foreach ($tenancies as $tenancy)
                                @if ($tenancy->pendingActions->count() > 0)

                                <span class="ml-2 mt-2">
                                    <span class="mr-2" style="color:cornflowerblue">
                                        <i class="fas fa-exclamation-circle fa-lg"></i>
                                    </span>

                                    <a href="/tenancies/{{ $tenancy->id }}">
                                        There are {{ $tenancy->pendingActions->count() }} actions required on {{ $tenancy->property->address }}
                                    </a>
                                    <br>
                                </span>
                                @endif
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
        </div>
    </tenancy> -->

    <div class="row justify-content-center mt-5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Searches</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    <span v-for="search in searches">
                        <div class="row mt5 ">
                              <!--   <div class="col-md-4">
                                    @{{ search.created_at | moment }}
                                </div>
                            -->
                            <div class="col-md-8">
                                @{{ search.name }}
                            </div>

                            <div class="col-md-4">
                                <button class="btn btn-sm btn-outline-secondary"  v-on:click="openSearchesModal(search)">View</button>
                            </div>

                        </div>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div v-if="selectedSearch">
        <view-search :selectedsearch="selectedSearch" v-on:reloadsearches="getSearches"></view-search>
    </div>


    <div class="row justify-content-center mt-5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Properties becoming available within 60 days</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    <p>None at present</p>

                </div>
            </div>
        </div>
    </div>
</div>

</dashboard>
@endsection
