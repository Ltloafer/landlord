@extends('layouts.layout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
            <div class="card-header">Scheduled invoices</div>

            <div class="card-body">
                <table class="table m-b-none">
                    <thead>
                        <th></th>
                        <th>Property</th>
                        <th>Scheduled invoice date</th>
                        <th>Month start date</th>
                        <th>Month end date</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Xero reference</th>
                    </thead>

                    <tbody>
                        @foreach ($scheduledInvoices as $s_inv)
                        <tr>
                            <td></td>
                            <td>{{ $s_inv->tenancy->property->address }}</td>
                            <td>{{ $s_inv->scheduled_date->format('d/m/y') }}</td>
                            <td>{{ $s_inv->start_date->format('d/m/y') }}</td>
                            <td>{{ $s_inv->end_date->format('d/m/y') }}</td>
                            <td>{{ $s_inv->displayAmount() }}</td>
                            <td>{{ $s_inv->status }}</td>
                            <td>{{ $s_inv->xero_ref }}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
