@extends('layouts.layout')
@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <!-- <a href="/invoices">Invoices</a> /  -->
                    <strong>{{ $invoice->xero_ref }}</strong>
                    @if ($invoice->isAuthorised())
                    <a href="/invoices/viewXeroInvoice/{{ $invoice->xero_inv_id }}" class=" btn btn-outline-secondary btn-sm float-right" target="_blank">View on xero</a>
                    @endif
                </div>

                <div class="card-body">

                   <div class="row" style="padding:20px">

                    <div class="col-sm-2">
                        <p><strong>To</strong></p>
                        <p><strong>Reference</strong></p>
                        <p><strong>Issued</strong></p>
                        <p><strong>Due</strong></p>
                        <p><strong>Status</strong></p>
                    </div>
                    <div class="col-sm-3">
                        <p>{{ $invoice->contact->name }}</p>
                        <p>{{ $invoice->reference ?? '' }}</p>
                        <p>{{ optional($invoice->date)->format('jS \\of F Y') }}</p>
                        <p>{{ optional($invoice->due_date)->format('jS \\of F Y') }}</p>
                        <p>{{ $invoice->status ?? '' }}</p>
                    </div>
                    <div class="col-sm-2">
                    </div>
                    <div class="col-sm-1">
                        <p><strong>From</strong></p>
                    </div>
                    <div class="col-sm-4">
                        Daleside Consulting Limited T/A Golden Ticket<br>
                        5th Floor<br>
                        104 Oxford Street<br>
                        Fitzrovia, London<br>
                        W1D 1LP<br>
                    </div>
                </div>
<!-- 
                    <table class="table m-b-none mt-4">
                        <thead>
                            <th>Id</th>
                            <th>Date</th>
                            <th>Reference</th>
                            <th>Customer</th>
                            <th>Type</th>
                            <th>Due date</th>
                            <th>Status</th>
                            <th>Amount</th>
                            <th>Vat</th>
                            <th>Total</th>
                        </thead>

                        <tbody>
                            <tr>
                                <td>{{ $invoice->id }}</td>
                                <td>{{ $invoice->type }}</td>
                                <td>{{ $invoice->status }}</td>
                                <td>{{ $invoice->displayVat() }}</td>
                                <td>{{ $invoice->displayTotalAmount() }}</td>
                                <td><a href="/invoices/viewXeroInvoice/{{ $invoice->xero_inv_id }}" target="_blank">Xero</a></td>
                            </tr>
                        </tbody>
                    </table> -->
                    <div class="row" style="padding: 20px">
                        <table class="table m-b-none mt-4">
                            <thead>
                                <th>Description</th>
                                <th>Date</th>
                                <th>Reference</th>
                                <!-- <th>Type</th> -->
                                <th>Amount</th>
                                <th>Vat</th>
                                <th>Total</th>
                            </thead>

                            <tbody>

                              @foreach($invoice->hotelBookings as $booking)
                              <tr>
                                <td>Hotel booking</td>
                                <td>{{ optional($booking->date)->format('jS \\of F Y') }}</td>
                                <td>Confirmation {{ $booking->confirmation_reference }}</td>
                                <td>{{ $booking->getCommissionTotal() }}</td>
                                <td>{{ $booking->getVat() }}</td>
                                <td>{{ $booking->getTotal() }}</td>
                            </tr>
                            @endforeach

                            @foreach($invoice->fees as $fee)
                            <tr>
                                <td>{{ $fee->feeType->type }}</td>
                                <td>{{ $fee->date->format('jS \\of F Y') }}</td>
                                <td>{{ $fee->id }}</td>
                                <td>{{ $fee->displayAmount() }}</td>
                                <td>{{ $fee->displayVat() }}</td>
                                <td>{{ $fee->displayTotal() }}</td>
                            </tr>
                            @endforeach

                            @foreach($invoice->deposits as $deposit)
                            <tr>
                                <td>{{ $deposit->type }}</td>
                                <td>{{ $deposit->date->format('jS \\of F Y') }}</td>
                                <td>{{ $deposit->id }}</td>
                                <td>{{ $deposit->displayAmount() }}</td>
                                <td>{{ $deposit->displayVat() }}</td>
                                <td>{{ $deposit->displayTotal() }}</td>
                            </tr>
                            @endforeach

                            @foreach($invoice->scheduledInvoices as $period)
                            <tr>
                                <td>Rental payment</td>
                                <td>{{ $period->scheduled_date->format('jS \\of F Y')}}</td>
                                <!-- <td>{{ $period->id }}</td> -->
                                <td>{{ $period->getPeriodDates() }}</td>
                                <td>{{ $period->displayAmount() }}</td>
                                <td>{{ $period->displayVat() }}</td>
                                <td>{{ $period->displayTotal() }}</td>
                            </tr>
                            @endforeach

                            @foreach($invoice->rentPayables as $period)
                            <tr>
                                <td>Landlord payment</td>
                                <td>{{ $period->date->format('d/m/y')}}</td>
                                <td>{{ $period->scheduledInvoice->getPeriodDates() }}</td>
                                <td>{{ $period->displayAmount() }}</td>
                                <td>{{ $period->displayVat() }}</td>
                                <td>{{ $period->displayTotal() }}</td>
                            </tr>
                            @endforeach

                        </tbody>
                        <tfooter>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>{{ $invoice->displayAmount() }}</th>
                                <th>{{ $invoice->displayVat() }}</th>
                                <th>{{ $invoice->displayTotalAmount() }}</th>
                            </tr>
                        </tfooter>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>


@endsection
