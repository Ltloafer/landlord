@extends('layouts.layout')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Invoices payable
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th></th>
                            <th>Invoice number</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Type</th>
                            <th>Due date</th>
                            <th>Status</th>
                            <th>Total</th>
                            <th></th>
                        </thead>

                        <tbody>
                            @foreach($invoices as $invoice)
                            <tr>
                                <!-- <td>{{ $invoice }}</td> -->
                                <td></td>
                                <td>{{ $invoice["InvoiceNumber"] }}</td>
                                <td>{{ $invoice["Date"] }}</td>
                                <td>{{ $invoice["Contact"]["Name"] }}</td>
                                <td>{{ $invoice["Type"] }}</td>
                                <td>{{ $invoice["DueDate"] }}</td>
                                <td>{{ $invoice["Status"] }}</td>
                                <td>{{ $invoice["Total"] }}</td>
                                <td><a href="/invoices/{{ $invoice["InvoiceNumber"] }}">View</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
