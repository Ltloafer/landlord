@extends('layouts.layout')
@section('content')

<notes :notes="{{ $notes }}" inline-template>

  <div>

   <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">

          <div class="card-header">Notes
            <input class="search float-right" type="text" v-model="searchQuery" placeholder="Search notes...">
          </div>

          <div class="card-body">

            <display-notes :data="{{ $notes }}" :columns="['date', 'note']" :filter-key="searchQuery" inline-template>
              <div>

                <table class="table table-hover m-b-none customer-sales" style="min-width:100%; width:auto">
                  <thead>
                   <tr>
                    <th v-for="key in columns"
                    @click="sortBy(key)"
                    :class="{ active: sortKey == key }">
                    @{{ key | capitalize }}
                    <span class="arrow" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'">
                    </span>
                  </th>
                </tr>
              </thead>

              <tbody>
                <!-- <tr v-for="order in filteredData" class='clickable-row' data-toggle="modal" data-target="#addNote"> -->
                  <tr v-for="note in filteredData" class='clickable-row' v-on:click="openNoteModal(note)">
                    <td width="25%">@{{ note.created_at | moment }}</td>
                    <td style="display: none">@{{ note.landlordName }}</td>
                    <td width="auto">@{{ note.note }}</td>
                  </tr>
                  <!-- <td><a href="/customers/" class="btn btn-primary btn-sm btn-alt">View</a> -->
                  </tbody>
                </table>

                <!-- Modal -->
                <div class="modal fade" id="addNote" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">

                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">@{{ selectedNote.created_at | moment }}</h5>
                      </div>

                      <div class="modal-body">
                        <p>@{{ selectedNote.note}}</p>
                        <p v-if="selectedNote.property"><strong>Property </strong><span class="ml-2">@{{ selectedNote.property.address }}</span></p>
                        <p v-if="selectedNote.landlord"><strong>Landlord </strong><span class="ml-2">@{{ selectedNote.landlord.name }}</span></p>
                        <p v-if="selectedNote.contact"><strong>Contact </strong><span class="ml-2">@{{ selectedNote.contact.name }}</span></p>
                      </div>

                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <!-- End of Modal -->

            </div>
          </display-notes>

        </div>
      </div>
    </div>
  </div>
</div>

</div>

</notes>


@endsection
