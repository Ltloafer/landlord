@extends('layouts.layout')
@section('content')

<production-company :companies= "{{ $companies }}" inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">Production companies
                            <button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#addNewProductionCompany"> Add a company +</button>
                        </div>

                        <div class="card-body">
                            <table class="table m-b-none">
                                <thead>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Postcode</th>
                                    <th>Email</th>
                                    <th>Xero Contact</th>
                                </thead>

                                <tbody>
                                    <tr v-for="company in companies">
                                        <td width="15%">@{{ company.name }}</td>
                                        <td width="15%">@{{ company.phone }}</td>
                                        <td width="25%">@{{ company.address }}</td>
                                        <td width="25%">@{{ company.postcode }}</td>
                                        <td width="25%">@{{ company.email }}</td>
                                        <!-- <td width="25%">@{{ company.xero_contactID }}</td> -->
                                        <td v-if="company.xero_contactID">
                                            <i class="fas fa-user fa-2x" style="color:green"></i>
                                        </td>

                                        <td v-else>
                                            <a href="#" v-on:click="linkcompanyToXeroContact(company)"><i class="fas fa-user-edit fa-2x" style="color:red"></i></a>
                                        </td>

                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</production-company>

@include('Modals.add-production-company')

@endsection
