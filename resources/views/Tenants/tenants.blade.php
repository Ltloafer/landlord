@extends('layouts.layout')
@section('content')

<tenants :tenants="{{ $tenants }}" inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Tenants
                            <button class="btn btn-sm btn-outline-secondary float-right" data-toggle="modal" data-target="#addNewTenant">Add new tenant + </button>
                        </div>

                        <div class="card-body" v-cloak>
                            <table class="table m-b-none">
                                <thead>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Xero Contact</th>
                                </thead>

                                <tbody>

                                    <tr v-for="tenant in tenants">
                                        <td width="15%">@{{ tenant.name }}</td>
                                        <td width="25%">@{{ tenant.address }}</td>
                                        <td width="15%">@{{ tenant.phone }}</td>
                                        <td width="25%">@{{ tenant.email }}</td>
                                        <!-- <td width="25%">@{{ tenant.xero_contactID }}</td> -->
                                        <td v-if="tenant.xero_contactID">
                                            <i class="fas fa-user fa-2x" style="color:green"></i>
                                        </td>
                                        
                                        <td v-else>
                                            <a href="#" v-on:click="linkTenantToXeroContact(tenant)"><i class="fas fa-user-edit fa-2x" style="color:red"></i></a>
                                        </td>
                                        <!-- <td><a v-bind:href=" 'xero/contacts/' + tenant.xero_contactID" class="btn btn-primary active btn-sm btn-alt" role="button">View </a></td> -->
                                    </tr>

                                </tbody>
                                
                                @include('Modals.view-tenant')

                            </table>
                            @include('Tenants.link-to-xero')

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</tenants>

@include('Modals.add-tenant')


@endsection
