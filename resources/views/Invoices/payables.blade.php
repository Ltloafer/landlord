@extends('layouts.layout')
@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Invoices - Payable
                    <a href="/invoices/sync-with-xero" class="btn btn-sm btn-outline-secondary float-right">Refresh</a>
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th>Id</th>
                            <th>Date</th>
                            <th>Reference</th>
                            <th>Customer</th>
                            <th>Due date</th>
                            <th>Status</th>
                            <th>Amount</th>
                            <th>Vat</th>
                            <th>Total</th>
                            <th>Xero ref</th>
                        </thead>

                        <tbody>
                            @foreach($invoices as $invoice)
                            <tr>
                                <td>{{ $invoice->id }}</td>
                                <td>{{ optional($invoice->date)->format('d/m/y') }}</td>
                                <td>{{ $invoice->reference }}</td>
                                <td>{{ $invoice->contact->name }}</td>
                                <td>{{ optional($invoice->due_date)->format('d/m/y') }}</td>
                                <td>{{ $invoice->status }}</td>
                                <td>{{ $invoice->displayAmount() }}</td>
                                <td>{{ $invoice->displayVat() }}</td>
                                <td>{{ $invoice->displayTotalAmount() }}</td>
                                <td>{{ $invoice->xero_ref }}</td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-sm btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Actions
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="/invoices/{{ $invoice->id }}">View</a>
                                        </div>
                                    </div>
                                </td>
                                <!-- <td><a href="/invoices/{{ $invoice->id }}" class="btn btn-sm btn-outline-secondary">View</a></td> -->
                                <td>
                                    @if ($invoice->isAuthorised())
                                    <a href="/invoices/viewXeroInvoice/{{ $invoice->xero_inv_id }}" class="btn btn-sm btn-outline-secondary" target="_blank">Xero</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
