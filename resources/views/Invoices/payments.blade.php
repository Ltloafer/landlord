@extends('layouts.layout')
@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Payments
                    <!-- <a href="/invoices/sync-with-xero" class="btn btn-sm btn-outline-secondary float-right">Refresh</a> -->
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th>Date</th>
                            <th>Inv no.</th>
                            <th>Customer</th>
                            <th>Reference</th>
                            <th>Amount</th>
                            <th>Bank Acc.</th>
                            <th>Reconciled</th>
                            <th>Status</th>
                        </thead>

                        <tbody>
                            @foreach($payments as $payment)
                            <tr>
                                <td>{{ $payment->date }}</td>
                                <td>{{ $payment->invNo }}</td>
                                <td>{{ $payment->customer}}</td>
                                <td>{{ $payment->reference}}</td>
                                <td>{{ $payment->amount }}</td>
                                <td>{{ $payment->bank }}</td>
                                <td>
                                    @if ($payment->isReconciled())
                                    <i class="far fa-check-circle fa-lg"></i>
                                    @endif
                                </td>

                                <td>{{ $payment->status }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
