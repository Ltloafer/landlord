<p>Please find details below on our offer.</p>

<div class="row">

	<div class="col-sm-10">
		<strong>Property</strong>
		{{ $search->property->address ?? '' }}. 
		{{ $search->property->postcode ?? '' }}
		<br>
	</div>
</div>

<br>

<div class="row">
	<div class="col-sm-10">
		<strong>Tenants</strong>
		<span class="search-attribute">{{ $search->tenant->name ?? $search->name }}</span>
	</div>
</div>

<br>

<div class="row mt-3">
	<div class="col-sm-10">
		<strong>Tenancy term</strong>
		<span class="search-attribute">
			{{ optional($search->start_date)->format('jS \\of F Y') }} - 
			{{ optional($search->end_date)->format('jS \\of F Y') }}
		</span>
	</div>
</div>

<br>

<div class="row mt-3">
	<div class="col-sm-10">
		<strong>Deposit </strong>@if ($search->holding_deposit) £{{formatAmount($search->displayDeposit())}} @endif
	</div>
</div>

<br>

<div class="row mt-3">
	<div class="col-sm-10">
		<strong>Rent offered </strong>@if ($search->offer_amount) £{{ formatAmount($search->displayOfferAmount()) }} @endif
	</div>
</div>

<br>

<div class="row mt-3">
	<div class="col-sm-10">
		<strong>Conditions of offer</strong><br>
		@include('Searches.offer-conditions')
	</div>
</div>



<p>Thanks</p>
<p>Golden Ticket London</p>