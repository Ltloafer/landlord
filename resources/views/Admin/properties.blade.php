@extends('layouts.layout')
@section('content')

<properties :properties="{{ $properties }}" inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Properties
                        </div>

                        <div class="card-body">
                          <table class="table m-b-none"> 
                            <thead>
                                <th>Address</th>
                                <th>Landlord</th>
                                <th>Agent</th>
                                <th>Xero Code</th>
                            </thead>

                            <tbody>
                                <tr v-for="property in properties">
                                    <td>@{{ property.address }}, @{{ property.postcode }}</td>
                                    <td v-if="property.landlord">@{{ property.landlord.name }}</td>
                                    <td v-else></td>

                                    <td v-if="property.agent">@{{ property.agent.name }}</td>
                                    <td v-else>Golden Ticket</td>
                                    <td>@{{ property.xero_code }}</td>

                                    <td v-if="property.xero_code">
                                        <i class="fas fa-user fa-2x" style="color:green"></i>
                                    </td>
                                    <td v-else>
                                        <a href="#" v-on:click="linkPropertyToXeroCode(property)"><i class="fas fa-user-edit fa-2x" style="color:red"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        @include('Properties.link-to-xero')
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</landlord>


@endsection
