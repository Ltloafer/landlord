@extends('layouts.layout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="card-header">Search areas
                        <button class="btn btn-sm btn-outline-secondary" style="float: right" data-toggle="modal" data-target="#add-area"> Add area +</button>
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th></th>
                            <th>Id</th>
                            <th>Area</th>
                        </thead>

                        <tbody>
                            @foreach($areas as $area)
                            <tr>
                                <td></td>
                                <td>{{ $area->id }}</td>
                                <td>{{ $area->area }}</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

                @include('Modals.add-area')


            </div>
        </div>
    </div>
</div>

@endsection
