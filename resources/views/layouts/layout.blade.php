<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns= "http://www.w3.org/1999/xhtml">

<head>
  <meta name="google" content="notranslate">
  <meta http-equiv="Content-Language" content="en">
  <meta name="robots" content="noindex" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="token" id="token" value="{{ csrf_token() }}">


  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

  <style>
    .form-control {
     font-size: 0.7rem;
     height: 2rem;
     color: salmon;
     font-weight: bolder;
     background-color: aliceblue
   }

   .container-fluid {
    padding-right: 0px;
    padding-left: 0px;
  }

  .pt-4, .py-4 {
    padding-top: 0rem !important;
  }

  [v-cloak] {display: none}
</style>

</head>
<body>
  <div id="app">
    <nav class="navbar navbar-expand-md fixed-top navbar-light navbar-laravel">
      <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/') }}">
          {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">

          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
            <!-- <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li> -->
            @else

            <li><a class="nav-link" href="/home">Dashboard</a></li>
            <li><a class="nav-link" href="/searches">Searches</a></li>

            <!-- <li><a class="nav-link" href="/tenancies">Tenancies</a></li> -->
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Tenancies<span class="caret"></span>
              </a>

              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
               <!-- <h6 class="dropdown-header">GENERAL</h6> -->
               <a class="dropdown-item" href="/tenancies?t=running">Running</a>
               <a class="dropdown-item" href="/tenancies?t=completed">Completed</a>
               <a class="dropdown-item" href="/tenancies">All</a>
             </div>
           </li>
           
           <li><a class="nav-link" href="/hotel-bookings">Hotel bookings</a></li>
           <!-- <li><a class="nav-link" href="/contacts">Contacts</a></li> -->
           <li><a class="nav-link" href="/notes">Notes</a></li>

           <li><a class="nav-link" href="/productions">Productions</a></li>

                    <!--     <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Xero<span class="caret"></span>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/accounts">Accounts</a>
                                <a class="dropdown-item" href="/xero/contacts">Contacts</a>
                                <a class="dropdown-item" href="/invoices">Authorised invoices</a>
                                <a class="dropdown-item" href="/invoices/invoicesPayable">Invoices payable</a>
                                <a class="dropdown-item" href="/viewDraftInvoices">Draft invoices</a>
                                <a class="dropdown-item" href="/xero/tracking">Tracking categories</a>
                                <a class="dropdown-item" href="/xero/branding-themes">Branding themes</a>
                            </div>
                        </li>
                      -->
                      <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Admin<span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                         <h6 class="dropdown-header">GENERAL</h6>
                         <a class="dropdown-item" href="/landlords">Landlords</a>
                         <a class="dropdown-item" href="/properties">Properties</a>
                         <a class="dropdown-item" href="/tenants">Tenants</a>
                         <a class="dropdown-item" href="/suppliers">Suppliers</a>
                         <a class="dropdown-item" href="/agents">Agents</a>
                         <a class="dropdown-item" href="/production-companies">Production companies</a>
                         <a class="dropdown-item" href="/hotels">Hotels</a>
                         <a class="dropdown-item" href="/search-areas">Search areas</a>
                         <hr>
                         <h6 class="dropdown-header">XERO</h6>
                         <a class="dropdown-item" href="/xero/contacts">Contacts</a>
                         <a class="dropdown-item" href="/xero/tracking">Tracking categories</a>
                         <a class="dropdown-item" href="/xero/branding-themes">Branding themes</a>
                         <!-- <a class="dropdown-item" href="/xero/tenants">Tenants</a> -->
                       </div>
                     </li>

                     <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Accounts<span class="caret"></span>
                      </a>

                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                       <h6 class="dropdown-header">GENERAL</h6>
                       <a class="dropdown-item" href="/invoices">Invoices</a>
                       <a class="dropdown-item" href="/invoices/payables">Payables</a>
                       <a class="dropdown-item" href="/payments">Payments</a>
                       <hr>
                       <h6 class="dropdown-header">XERO</h6>
                       <a class="dropdown-item" href="/accounts">Accounts</a>
                       <a class="dropdown-item" href="/xero/contacts">Contacts</a>
                       <a class="dropdown-item" href="/xero-invoices">Authorised invoices</a>
                       <!-- <a class="dropdown-item" href="/invoices/invoicesPayable">Invoices payable</a> -->
                       <a class="dropdown-item" href="/viewDraftInvoices">Draft invoices</a>
                       <a class="dropdown-item" href="/xero/tracking">Tracking categories</a>
                       <a class="dropdown-item" href="/xero/branding-themes">Branding themes</a>
                           <hr>
                       <h6 class="dropdown-header">XERO (DIRECTS)</h6>
                       <!-- <a class="dropdown-item" href="/accounts">Accounts</a> -->
                       <a class="dropdown-item" href="/xero/directs/contacts">Contacts</a>
                       <!-- <a class="dropdown-item" href="/xero-invoices">Authorised invoices</a> -->
                     </div>
                   </li>




                   <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </div>
                </li>

                @endguest
              </ul>
            </div>
          </div>
        </nav>

        <main class="py-4">

          @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          @if (session('alert_message'))
          <div class="alert alert-danger alert-dismissible fade show ml-5 mr-5" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            {{ session('alert_message') }}
          </div>
          @endif

          @if (session('success_message'))
          <div class="alert alert-success alert-dismissible fade show ml-5 mr-5 mt-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            {{ session('success_message') }}
          </div>
          @endif

          @yield('content')
        </main>
      </div>

      <!-- Scripts -->
      <script src="{{ mix('/js/app.js') }}"></script>
    </body>
    </html>
