@extends('layouts.layout')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Xero Branding Themes
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th>Name</th>
                            <th>Theme ID</th>
                        </thead>

                        <tbody>
                            @foreach($themes as $theme)
                            <tr>
                                <td>{{ $theme["Name"] }}</td>
                                <td>{{ $theme["BrandingThemeID"] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
