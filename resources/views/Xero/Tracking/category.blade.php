@extends('layouts.layout')
@section('content')
@inject('categoryService', 'App\Services\invoiceService')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $category["Name"] }}</div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th></th>
                            <th>Option</th>
                            <th>View</th>
                        </thead>

                        <tbody>
                            @foreach($options as $option)
                            <tr>
                                <td></td>
                                <td>{{ $option["Name"] }}</td>
                                <td>{{ $option["Status"] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
