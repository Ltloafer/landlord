@extends('layouts.layout')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Tracking categories
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th>Name</th>
                            <th>Status</th>
                            <!-- <th>Id</th> -->
                            <th>View</th>
                        </thead>

                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td>{{ $category["Name"] }}</td>
                                <td>{{ $category["Status"] }}</td>
                                <td><a href="/xero/tracking/{{ $category["TrackingCategoryID"] }}">View</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
