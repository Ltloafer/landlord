@extends('layouts.layout')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Xero Accounts
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>Account ID</th>
                        </thead>

                        <tbody>
                            @foreach($accounts as $account)
                            <tr>
                                <td>{{ $account["Code"] }}</td>
                                <td>{{ $account["Name"] }}</td>
                                <td>{{ $account["Type"] }}</td>
                                <td>{{ $account["Description"] }}</td>
                                <td>{{ $account["accountID"] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
