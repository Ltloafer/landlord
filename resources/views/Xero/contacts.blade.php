@extends('layouts.layout')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Contacts
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th></th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Guid</th>
                        </thead>

                        <tbody>
                            @foreach($contacts as $contact)
                            <tr>
                                <td></td>
                                <td>{{ $contact["Name"] }}</td>
                                <td>{{ $contact["EmailAddress"] }}</td>
                                <td>{{ $contact["ContactStatus"] }}</td>
                                <td>{{ $contact["ContactID"] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
