
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 // require('date-input-polyfill');

 window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


 Vue.component('example-component', require('./components/ExampleComponent.vue'));
 Vue.component('add-property', require('./components/AddProperty.vue'));
 Vue.component('landlord', require('./components/Landlord.vue'));
 Vue.component('property', require('./components/Property.vue'));
 Vue.component('view-notes', require('./components/View-notes.vue'));
 Vue.component('dashboard', require('./components/Dashboard.vue'));
 Vue.component('notes', require('./components/Notes.vue'));
 Vue.component('searches', require('./components/Searches.vue'));
 Vue.component('tenancy', require('./components/Tenancy.vue'));
 Vue.component('contacts', require('./components/Contacts.vue'));
 Vue.component('tenants', require('./components/Tenants.vue'));
 Vue.component('properties', require('./components/Properties.vue'));
 Vue.component('hotel', require('./components/Hotel.vue'));
 Vue.component('display-notes', require('./components/display-data.vue'));
 Vue.component('display-productions', require('./components/display-data.vue'));
 Vue.component('display-bookings', require('./components/display-hotel-bookings.vue'));
 Vue.component('display-searches', require('./components/display-data.vue'));
 Vue.component('hotel-booking', require('./components/hotel-booking.vue'));
 Vue.component('view-search', require('./components/view-search.vue'));
 Vue.component('display-contacts', require('./components/display-data.vue'));
 Vue.component('add-landlord', require('./components/Add-landlord.vue'));
 Vue.component('add-tenancy', require('./components/add-tenancy.vue'));
 Vue.component('agents', require('./components/Agent.vue'));
 Vue.component('suppliers', require('./components/supplier.vue'));
 Vue.component('production-company', require('./components/ProductionCompanies.vue'));
 Vue.component('scheduled-invoice', require('./components/scheduledInvoice'));
 Vue.component('landlord-schedule', require('./components/landlordSchedule'));
 Vue.component('landlord-statement', require('./components/landlordStatement'));
 Vue.component('landlord-bill', require('./components/LandlordBill'));
 Vue.component('rental-invoice', require('./components/RentalInvoice'));
 Vue.component('fee-invoice', require('./components/FeeInvoice'));
 Vue.component('fee', require('./components/Fee'));
 Vue.component('expense', require('./components/Expense'));
 Vue.component('add-tenancy-form', require('./components/AddTenancyForm.vue'));
 Vue.component('hotel-bookings', require('./components/Hotel-bookings.vue'));


 // Vue.component('poo', poo);
 // import poo from './plugins/Poo.js'
 // import poo from 'poo-sticks'
 // Vue.use(poo);
 // 
 import {Tabs, Tab} from 'vue-tabs-component';

Vue.component('tabs', Tabs);
Vue.component('tab', Tab);

 const app = new Vue({
 	el: '#app',

 	data: {
 		hasFullMembership: null,
 	},

 	mounted() {
 		//
 	},


 });
