import Poosticks from './Poo.vue';

export default {
    install (Vue) {
        Vue.component('poo-sticks', Poosticks);
    }
};

